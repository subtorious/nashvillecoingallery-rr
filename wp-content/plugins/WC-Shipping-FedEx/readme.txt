=== WooCommerce FedEx Shipping Module ===
 
A shipping extension to integrate realtime shipping rates for FedEx.  

== Important Note ==

V1.0 only support USD. Please make sure your currency is set to USD before start using WooFedEx.

FedEx is US only. Make sure set your origin country/region to US. 

== Support ==

If you have any problems, questions or suggestions please post on the Extension Works support forums: http://help.extensionworks.com
