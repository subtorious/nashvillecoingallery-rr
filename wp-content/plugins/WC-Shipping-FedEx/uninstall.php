<?php

if( !defined( 'ABSPATH' ) && !defined( 'WP_UNINSTALL_PLUGIN' ) )
    exit();


$key = 'woocommerce_EW_WC_Shipping_Fedex_WSDL_settings';

delete_option( $key );
