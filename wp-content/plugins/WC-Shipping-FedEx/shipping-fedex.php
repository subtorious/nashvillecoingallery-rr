<?php
/*
  Plugin Name: WooCommerce FedEx
  Plugin URI: http://www.extensionworks.com
  Description: Fedex Shipping for WooCommerce
  Version: 2.8.6
  Author: Extension Works
  Author URI: http://www.extensionworks.com

  Copyright: ©Extension Works.

 */

add_action('plugins_loaded', 'woocommerce_fedex_wsdl_init');

function woocommerce_fedex_wsdl_init() {

	/**
	 * Define plugin global path
	 */
	if( !defined( 'EW_WC_FEDEX_PLUGIN_NAME' ) )
		define('EW_WC_FEDEX_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)),'/'));

	if( !defined( 'EW_WC_FEDEX_PLUGIN_UPDATE' ) )
		define('EW_WC_FEDEX_PLUGIN_UPDATE', plugin_basename(__FILE__) );

	if( !defined( 'EW_WC_FEDEX_PLUGIN_DIR' ) )
		define('EW_WC_FEDEX_PLUGIN_DIR', WP_PLUGIN_DIR.'/'.EW_WC_FEDEX_PLUGIN_NAME);

	/**
	 * Define pluign version number
	 */
	if( !defined('EW_WC_FEDEX_VERSION_KEY'))
		define('EW_WC_FEDEX_VERSION_KEY','ew_wc_fedex_verion');

	if(!defined('EW_WC_FEDEX_VERSION_NUM'))
		define('EW_WC_FEDEX_VERSION_NUM','2.8.6');

	add_option( EW_WC_FEDEX_VERSION_KEY,EW_WC_FEDEX_VERSION_NUM );

	/**
	 * Load essential files
	 */
	require_once  EW_WC_FEDEX_PLUGIN_DIR . '/includes/extensionworks-functions.php';


    if( !check_plugin_dependency() )
        return;

	// if ( ! class_exists( 'nusoap_base' ) )
	// 	include_once(plugin_dir_path(__FILE__) . 'nusoap.php');

	/**
	 * Plugin updates
	 */
	extensionworks_queue_update( plugin_basename( __FILE__ ), '3874f4411b9efd5233ad37615b9e5b16', 'woo-fedex' );

	/**
	 * Load Shipping class
	 */
	require_once EW_WC_FEDEX_PLUGIN_DIR . '/includes/class-fedex-shipping.php';



	function wc_shipping_fedex_plugin_links( $links ) {

        $setting_url = admin_url( 'admin.php?page=woocommerce_settings&tab=shipping&section=EW_WC_Shipping_Fedex_WSDL' );

        if ( check_woo_version('2.1') ){
            $setting_url = admin_url( 'admin.php?page=wc-settings&tab=shipping&section=ew_wc_shipping_fedex_wsdl' );
        }

        $plugin_links = array(
            '<a href="' . $setting_url . '">' . __( 'Settings', 'extensionworks' ) . '</a>',
            '<a href="http://help.extensionworks.com/hc/en-us/requests/new">' . __( 'Support', 'extensionworks' ) . '</a>',
            '<a href="http://help.extensionworks.com/hc/en-us/categories/200037688-WooCommerce-FedEx">' . __( 'Docs', 'extensionworks' ) . '</a>',
        );

        return array_merge( $plugin_links, $links );
    }

    add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_shipping_fedex_plugin_links' );


	function add_fedex_method($methods) {
		$methods[] = 'EW_WC_Shipping_Fedex_WSDL';
		return $methods;
	}

	add_filter('woocommerce_shipping_methods', 'add_fedex_method');
	add_filter( 'extensionworks_shipping_methods', 'add_fedex_method' );

}
