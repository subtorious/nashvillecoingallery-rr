<?php
class EW_WC_Shipping_Fedex_WSDL extends EW_Shipper {

	var $allowed_origin_countries = array('US', 'CA');
	var $allowed_currencies       = array('USD', 'CAD');

	var $services = array(
		'FEDEX_GROUND'                       => array( 'name' => 'FedEx Ground - Commercial Address'),
		'GROUND_HOME_DELIVERY'               => array( 'name' => 'FedEx Ground - Home Address'),
		'FEDEX_EXPRESS_SAVER'                => array( 'name' => 'FedEx Express Saver'),
		'FEDEX_2_DAY'                        => array( 'name' => 'FedEx 2Day'),
		'FEDEX_2_DAY_AM'                     => array( 'name' => 'FedEx 2Day AM'),
		'STANDARD_OVERNIGHT'                 => array( 'name' => 'FedEx Standard Overnight'),
		'PRIORITY_OVERNIGHT'                 => array( 'name' => 'FedEx Priority Overnight'),
		'FIRST_OVERNIGHT'                    => array( 'name' => 'FedEx First Overnight'),
		'INTERNATIONAL_ECONOMY'              => array( 'name' => 'FedEx International Economy'),
		'INTERNATIONAL_FIRST'                => array( 'name' => 'FedEx International First'),
		'INTERNATIONAL_PRIORITY'             => array( 'name' => 'FedEx International Priority'),
		'EUROPE_FIRST_INTERNTIONAL_PRIORITY' => array( 'name' => 'FedEx Europe First International Priority'),
		'FEDEX_1_DAY_FREIGHT'                => array( 'name' => 'FedEx 1Day Freight'),
		'FEDEX_2_DAY_FREIGHT'                => array( 'name' => 'FedEx 2Day Freight'),
		'FEDEX_3_DAY_FREIGHT'                => array( 'name' => 'FedEx 3Day Freight'),
		'INTERNATIONAL_ECONOMY_FREIGHT'      => array( 'name' => 'FedEx Economy Freight'),
		'INTERNATIONAL_PRIORITY_FREIGHT'     => array( 'name' => 'FedEx Priority Freight'),
		'FEDEX_FREIGHT'                      => array( 'name' => 'Fedex Freight'),
		'FEDEX_NATIONAL_FREIGHT'             => array( 'name' => 'FedEx National Freight'),
		'INTERNATIONAL_GROUND'               => array( 'name' => 'FedEx International Ground'),
		'SMART_POST'                         => array( 'name' => 'FedEx Smart Post'),
		'FEDEX_FIRST_FREIGHT'                => array( 'name' => 'FedEx First Freight'),
		'FEDEX_FREIGHT_ECONOMY'              => array( 'name' => 'FedEx Freight Economy'),
		'FEDEX_FREIGHT_PRIORITY'             => array( 'name' => 'FedEx Freight Priority'),
	);

	var $id                  = 'EW_WC_Shipping_Fedex_WSDL';
	var $method_title        = 'FedEx';
	var $carrier             = "FedEx";
	var $rateservice_version = '16';
	var $plugin              = '';

	function __construct() {

		parent::__construct();

		$this->plugin           = EW_WC_FEDEX_PLUGIN_UPDATE;

		$this->production       = ( $this->production == 'yes' ) ? true : false;

		if ( $this->quick_quote == 'yes' ) $this->production = false;

		// HZ dev test details
		$this->account  = ( $this->quick_quote == "yes" ) ? "510087020" : $this->account;
		$this->meter    = ( $this->quick_quote == "yes" ) ? "118547677" : $this->meter;
		$this->key      = ( $this->quick_quote == "yes" ) ? "xGtvBDJgQ6teXWXn" : $this->key;
		$this->password = ( $this->quick_quote == "yes" ) ? "anURSjaBgSkU632HOvLF2JSJP" : $this->password;

		$this->residential            = ( $this->settings['residential'] == 'yes') ? true : false;

		$this->selected_shipping_type = (is_array(get_option("woocommerce_fedex_shipping_type", array()))) ? get_option("woocommerce_fedex_shipping_type", array()) : array();

	}



	public function add_form_fields(){

		$this->form_fields['quick_quote'] = array(
			'title'       => 'Quick Quote',
			'type'        => 'checkbox',
			'label'       => 'Enable Quick Quote',
			'description' => 'If you don\'t have a FedEx account and developer details, use quick quote. NOTE: quick quote uses the FedEx Developer API and can occasionally go down for maintenance - we recommend that you obtain your own PRODUCTION key from FedEx if using this in a live environment.',
			'default'     => 'yes'
		);

		$this->form_fields['residential'] = array(
			'title'       => 'Residential delivery',
			'type'        => 'checkbox',
			'description' => 'Deliver to residential address.',
			'default'     => 'no'
		);

		$this->form_fields['production'] = array(
			'title'       => 'Production mode',
			'type'        => 'checkbox',
			'label'       => 'Enable production mode',
			'description' => 'Enable this if you are using a production key.',
			'default'     => 'no'
		);

		$this->form_fields['account'] = array(
			'title'       => 'Account Number',
			'type'        => 'text',
			'class'       => 'cridential',
			'description' => 'Your FedEx account number.',
			'default'     => ''
		);

		$this->form_fields['meter'] = array(
			'title'       => 'Meter Number',
			'type'        => 'text',
			'class'       => 'cridential',
			'description' => 'Your FedEx meter number.',
			'default'     => ''
		);

		$this->form_fields['key'] = array(
			'title'       => 'Key',
			'type'        => 'text',
			'class'       => 'cridential',
			'description' => 'FedEx web services key.',
			'default'     => ''
		);

		$this->form_fields['password'] = array(
			'title'       => 'Password',
			'type'        => 'text',
			'class'       => 'cridential',
			'description' => 'FedEx web services password.',
			'default'     => ''
		);

		$this->form_fields['smartposthubid'] = array(
			'title'       => 'SmartPost HubID',
			'type'        => 'text',
			'description' => 'Required for SmartPost.',
			'default'     => ''
		);

		$this->form_fields['insure'] = array(
			'title'       => 'Insurance',
			'type'        => 'checkbox',
			'description' => 'Rates include insurance.',
			'default'     => ''
		);

		$this->form_fields['account_type'] = array(
			'title'       => 'Rate Type',
			'type'        => 'select',
			'description' => '',
			'default'     => 'PAYOR_ACCOUNT_PACKAGE',
			'options'     => array(
				'PAYOR_LIST'            =>'LIST',
                'PAYOR_ACCOUNT'           =>'ACCOUNT',
			),
		);

		$this->form_fields['drop_off'] = array(
			'title'       => 'Drop-Off Type',
			'type'        => 'select',
			'description' => '',
			'default'     => 'REGULAR_PICKUP',
			'options'     => array(
				'REGULAR_PICKUP'            =>'Regular Pickup',
                'REQUEST_COURIER'           =>'By Courier Request',
                'DROP_BOX'                  =>'FedEx Drop Box',
                'BUSINESS_SERVICE_CENTER'   =>'FedEx Service Center',
                'STATION'                   =>'FedEx Station'
			),
		);

		$this->form_fields['include_tax'] = array(
			'title'       => 'Include Surcharges',
			'type'        => 'checkbox',
			'label'       => 'Enable',
			'default'     => 'yes',
			'description' => 'show the price include surcharges'
		);

		$this->form_fields['price_format'] = array(
			'title'       => 'Price format',
			'type'        => 'text',
			'default'     => '%label%',
			'description' => 'default to show finel price. support tag: %label%, %base%, %due%, %surcharge%'
		);

		$this->form_fields['enable_feight'] = array(
			'title'       => 'Enable Feight rates',
			'type'        => 'checkbox',
			'label'       => 'Enable',
			'description' => '',
			'default'     => 'no'
		);

		$this->form_fields['freight_account'] = array(
			'title'       => 'FedEx Feight Account Number',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_address_1'] = array(
			'title'       => 'Billing Address 1',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_address_2'] = array(
			'title'       => 'Billing Address 2',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_address_2'] = array(
			'title'       => 'Billing Address 2',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_city'] = array(
			'title'       => 'Billing City',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_state'] = array(
			'title'       => 'Billing State Code',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_postcode'] = array(
			'title'       => 'Billing Postcode',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_billing_country'] = array(
			'title'       => 'Billing Country Code',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_address_1'] = array(
			'title'       => 'Shipper Address 1',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_address_2'] = array(
			'title'       => 'Shipper Address 2',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_city'] = array(
			'title'       => 'Shipper City',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_state'] = array(
			'title'       => 'Shipper State Code',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_postcode'] = array(
			'title'       => 'Shipper Postcode',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_shipper_country'] = array(
			'title'       => 'Shipper Country Code',
			'type'        => 'text',
			'description' => '',
			'default'     => ''
		);

		$this->form_fields['freight_class'] = array(
			'title'       => 'freight class',
			'type'        => 'select',
			'description' => '',
			'default'     => '',
			'options'     => array(

				'CLASS_050'   => 'CLASS_050',
				'CLASS_055'   => 'CLASS_055',
				'CLASS_060'   => 'CLASS_060',
				'CLASS_065'   => 'CLASS_065',
				'CLASS_070'   => 'CLASS_070',
				'CLASS_077_5' => 'CLASS_077_5',
				'CLASS_085'   => 'CLASS_085',
				'CLASS_092_5' => 'CLASS_092_5',
				'CLASS_100'   => 'CLASS_100',
				'CLASS_110'   => 'CLASS_110',
				'CLASS_125'   => 'CLASS_125',
				'CLASS_150'   => 'CLASS_150',
				'CLASS_200'   => 'CLASS_200',
				'CLASS_250'   => 'CLASS_250',
				'CLASS_300'   => 'CLASS_300',
			),
		);

	}



	function get_package_rates( $packages, $methods ) {

		$this->available_methods = $methods;

		global $woocommerce;

		$data = $this->calculate_packages( $packages );


		if ( $data['weight'] ) {

			$rates = $this->get_shipping_response($packages);

			$this->add_debug_message( $rates, "FedEx Rates");


			if( $this->enable_feight == 'yes' && $woocommerce->customer->get_shipping_city() != ''){
				$freight_rates = $this->get_freight_response( $packages );

				$this->add_debug_message( $freight_rates, "FedEx Freight Rates");
				if( is_array( $freight_rates) ){
					$rates = array_merge( $rates, $freight_rates );
				}
			}

			$this->add_debug_message( $rates, "FedEx All Rates");

			// Add rates based on rates
			if ( $rates && is_array($rates) ) {

				return $rates;
			}
		}
	}

	function calculate_packages( $packages ){

		$data['weight'] = 0;
		$data['value']  = 0;

		foreach ($packages as $package ) {
			$data['weight'] += $package['container']->get_weight('gross');
			$data['value']  += $package['container']->get_price();
		}

		return $data;
	}

	function get_freight_response( $packages ){

		global $woocommerce;

		$rates          = array();
		$customer       = $woocommerce->customer;
		$debug_response = array();

		$data           = $this->calculate_packages( $packages );

		$shipping_data = array(
			'Pickup_Postcode'      => $packages[0]['container']->get_location(),
			'Pickup_Country'       => $woocommerce->countries->get_base_country(),
			'Pickup_State'         => $packages[0]['container']->get_state(),
			'Pickup_City'          => $packages[0]['container']->get_city(),
			'Shipping_Postcode'  => $customer->get_shipping_postcode(),
			'Shipping_State'     => $customer->get_shipping_state(),
			'Shipping_Country'   => $customer->get_shipping_country(),
			'Shipping_City'      => $customer->get_shipping_city(),
			'Shipping_Address_1' => $customer->get_shipping_address(),
			'Shipping_Address_2' => $customer->get_shipping_address_2(),
			'Shipping_City'      => $customer->get_shipping_city(),
			'Shipping_Address_1' => $customer->get_shipping_address(),
			'Shipping_Address_2' => $customer->get_shipping_address_2(),
			'Weight'             => $data['weight'],
			'Value'              => $data['value'],
			'Packages'           => $packages,
		);


		$data   = $this->get_freight_encode( $shipping_data );

		$result = $this->fedex_shipping($data);

		if ( $result ) {
			if ( in_array($result->HighestSeverity, array('FAILURE', 'ERROR', 'WARNING')) ) {
				if ( $this->debug == 'yes' ) {
					$debug_response[] = $result->HighestSeverity . " : " . $result->Notifications->Message;
					$this->add_debug_message( $debug_response , 'FEDEX Freight Response');
				}

				return array();
			}
		}

		$RatedReply = &$result->RateReplyDetails;

		// Workaround for when an object is returned instead of array
		if ( is_object( $RatedReply ) && isset( $RatedReply->ServiceType ) )
			$RatedReply = array( $RatedReply );

		if ( ! is_array( $RatedReply ) )
			return false;

		$this->add_debug_message( $RatedReply , 'FEDEX Freight Rate Raw Response');

		foreach ( $RatedReply as $quote ) {

			if ( is_array( $this->available_methods ) && ! in_array( $quote->ServiceType, $this->available_methods))
				continue;

			if ( is_array($quote->RatedShipmentDetails) ) {
				foreach ($quote->RatedShipmentDetails as $i => $d) {
					if ($d->ShipmentRateDetail->RateType == $this->account_type) {
						$details = &$quote->RatedShipmentDetails[$i];
						break;
					}
				}
			}else
				$details = &$quote->RatedShipmentDetails;

			if (!isset($details))
				continue;

			$amount = apply_filters('woocommerce_fedex_total', $details->ShipmentRateDetail->TotalNetCharge->Amount, $details);

			$rate = array(
				'id'        => $quote->ServiceType,
				'due'       => $amount,
				'base'      => $details->ShipmentRateDetail->TotalNetFreight->Amount,
				'surcharge' => $details->ShipmentRateDetail->TotalSurcharges->Amount,
			);
			$rates[] = $rate;

		}
		return $rates;

	}



	/**
	 * Set shipping rates from cache or from FedEx API
	 * @global type $woocommerce
	 * @param type $data
	 */
	function get_shipping_response( $packages ) {

		global $woocommerce;

		$rates          = array();
		$customer       = $woocommerce->customer;
		$debug_response = array();

		$data           = $this->calculate_packages( $packages );
		$shipping_data = array(
			'Pickup_Postcode'      => $packages[0]['container']->get_location(),
			'Pickup_Country'       => $woocommerce->countries->get_base_country(),
			'Pickup_State'         => $packages[0]['container']->get_state(),
			'Pickup_City'          => $packages[0]['container']->get_city(),
			'Destination_Postcode' => $customer->get_shipping_postcode(),
			'State'                => $customer->get_shipping_state(),
			'Country'              => $customer->get_shipping_country(),
			'Weight'               => $data['weight'],
			'Value'                => $data['value'],
			'Packages'             => $packages,
		);



		$data   = $this->fedex_encode( $shipping_data );


		$result = $this->fedex_shipping($data);

		if ( $result ) {
			if ( in_array($result->HighestSeverity, array('FAILURE', 'ERROR', 'WARNING')) ) {
				if ( $this->debug == 'yes' ) {
					$debug_response[] = $result->HighestSeverity . " : " . $result->Notifications->Message;
					$this->add_debug_message( $debug_response , 'FEDEX Response');
				}

				return array();
			}
		}

		$RatedReply = &$result->RateReplyDetails;

		// Workaround for when an object is returned instead of array
		if ( is_object( $RatedReply ) && isset( $RatedReply->ServiceType ) )
			$RatedReply = array( $RatedReply );

		if ( ! is_array( $RatedReply ) )
			return false;

		$this->add_debug_message( $RatedReply , 'FEDEX Rate Raw Response');

		foreach ( $RatedReply as $quote ) {

			if ( is_array( $this->available_methods ) && ! in_array( $quote->ServiceType, $this->available_methods)){
				continue;
			}

			$details = array();

			if ( is_array($quote->RatedShipmentDetails) ) {
				foreach ($quote->RatedShipmentDetails as $i => $d) {
					if ( stripos($d->ShipmentRateDetail->RateType, $this->account_type) !== false ) {
						$details = &$quote->RatedShipmentDetails[$i];
						break;
					}
				}
			}else
				$details = &$quote->RatedShipmentDetails;

			if (!isset($details))
				continue;

			$amount = apply_filters('woocommerce_fedex_total', $details->ShipmentRateDetail->TotalNetCharge->Amount, $details);

			$rate = array(
				'id'        => $quote->ServiceType,
				'due'       => $amount,
				'base'      => $details->ShipmentRateDetail->TotalNetFreight->Amount,
				'surcharge' => $details->ShipmentRateDetail->TotalSurcharges->Amount,
			);
			$rates[] = $rate;
		}

		return $rates;
	}


	function get_freight_encode( $data ){

		global $woocommerce;

		$request = array();

		$request['WebAuthenticationDetail'] = array(
			'UserCredential' => array(
				'Key'      => $this->key,
				'Password' => $this->password
			)
		);
		$request['ClientDetail'] = array(
			'AccountNumber' => $this->account,
			'MeterNumber'   => $this->meter
		);
		$request['TransactionDetail'] = array( 'CustomerTransactionId' => '*** Freight Rate Available Services Request from WooCommerce ***' );
		$request['Version'] = array(
			'ServiceId'    => 'crs',
			'Major'        => $this->rateservice_version,
			'Intermediate' => '0',
			'Minor'        => '0'
		);

		$request['ReturnTransitAndCommit'] = true;
		$request['RequestedShipment']['DropoffType'] = $this->drop_off; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
		$request['RequestedShipment']['ShipTimestamp'] = date('c');


		$request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...


		$request['RequestedShipment']['Shipper'] = $this->get_shipper_address();



		$request['RequestedShipment']['Recipient'] = array(
		    'Address' => array(
				'Residential'         => $this->residential,
				'PostalCode'          => $data['Shipping_Postcode'],
				'CountryCode'         => $data['Shipping_Country'],
				'StreetLines'         => array( $data['Shipping_Address_1'], $data['Shipping_Address_2']),
				'City'                => $data['Shipping_City'],
				'StateOrProvinceCode' => $data['Shipping_State'],
		    )
		);



		$request['RequestedShipment']['ShippingChargesPayment'] = array(
			'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
			'Payor'       => array(
				'ResponsibleParty' => array(
				'AccountNumber'    => $this->freight_account,
				'CountryCode'      => $data['Pickup_Country'])
		    )
		);
		$request['RequestedShipment']['FreightShipmentDetail'] = array(
			'FedExFreightAccountNumber'            => $this->freight_account,
			'FedExFreightBillingContactAndAddress' => $this->get_shipper_billing_address(),

			'Role'                                 => 'SHIPPER',
			'PaymentType'                          => 'PREPAID',
			'LineItems'                            => array(
				'FreightClass' => $this->freight_class,
				'Packaging'    => 'PALLET',
				'Weight'       => array(
					'Value' => $data['Weight'],
					'Units' => 'LB'
				),
			)
		);


		$request['RequestedShipment']['PackageCount']     = count( $data['Packages'] );

		return $request;

	}

	function get_shipper_address(){

		$address = array(

			'Address' => array(
				'StreetLines'         => array( strtoupper( $this->freight_shipper_address_1 ), strtoupper( $this->freight_shipper_address_2 ) ),
				'City'                => strtoupper( $this->freight_shipper_city ),
				'StateOrProvinceCode' => strtoupper( $this->freight_shipper_state ),
				'PostalCode'          => strtoupper( $this->freight_shipper_postcode ),
				'CountryCode'         => strtoupper( $this->freight_shipper_country ),
				'Residential'         => $this->freight_shipper_residential
			),
		);

		return $address;
	}

	function get_shipper_billing_address(){
		$address = array(

			'Address' => array(
				'StreetLines'         => array( strtoupper( $this->freight_billing_address_1 ), strtoupper( $this->freight_billing_address_2 ) ),
				'City'                => strtoupper( $this->freight_billing_city ),
				'StateOrProvinceCode' => strtoupper( $this->freight_billing_state ),
				'PostalCode'          => strtoupper( $this->freight_billing_postcode ),
				'CountryCode'         => strtoupper( $this->freight_billing_country ),
			),
		);

		return $address;
	}

	function fedex_encode($data = false) {

		$request = array();

		$request['WebAuthenticationDetail'] = array(
			'UserCredential' => array(
				'Key'      => $this->key,
				'Password' => $this->password
			)
		);
		$request['ClientDetail'] = array(
			'AccountNumber' => $this->account,
			'MeterNumber'   => $this->meter
		);
		$request['TransactionDetail'] = array( 'CustomerTransactionId' => '*** Rate Available Services Request from WooCommerce ***' );
		$request['Version'] = array(
			'ServiceId'    => 'crs',
			'Major'        => $this->rateservice_version,
			'Intermediate' => '0',
			'Minor'        => '0'
		);


		$request['ReturnTransitAndCommit'] = true;
		$request['RequestedShipment']['DropoffType'] = $this->drop_off; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
		$request['RequestedShipment']['ShipTimestamp'] = date('c');

		//$request['RequestedShipment']['ServiceType'] = $data['Shipping_Type']; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...

		if (is_array( $this->available_methods ) && in_array('SMART_POST', $this->available_methods ) && !empty( $this->smartposthubid )) {
			$request['RequestedShipment']['SmartPostDetail'] = array(
				'Indicia' => 'PARCEL_SELECT',
				'HubId'   => $this->smartposthubid
			);
		}

		$request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...

		if ( $this->insure == 'yes' )
			$request['RequestedShipment']['TotalInsuredValue'] = array( 'Amount' => $data['Value'] );

		$request['RequestedShipment']['Shipper'] = array(
		    'Address' => array(
				'PostalCode'  => $data['Pickup_Postcode'],
				'CountryCode' => $data['Pickup_Country']
		    )
		);
		$request['RequestedShipment']['Recipient'] = array(
		    'Address' => array(
				'Residential' => $this->residential,
				'PostalCode'  => $data['Destination_Postcode'],
				'CountryCode' => $data['Country']
		    )
		);

		// if (in_array($data['Country'], array('US', 'CA'))) {
		// 	$request['RequestedShipment']['Recipient']['Address']['StateOrProvinceCode'] = $data['State'];
		// }

		$request['RequestedShipment']['ShippingChargesPayment'] = array(
			'PaymentType' => 'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
			'Payor'       => array(
				'AccountNumber' => $this->account,
				'CountryCode'   => $data['Pickup_Country']
		    )
		);
		$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; // LIST or ACCOUNT
		$request['RequestedShipment']['PackageCount']     = count( $data['Packages'] );
		$request['RequestedShipment']['PackageDetail']    = 'INDIVIDUAL_PACKAGES';

		foreach ($data['Packages'] as $key => $package) {
			$request['RequestedShipment']['RequestedPackageLineItems'][] = array(
				'SequenceNumber'    => $key +1 ,
				'GroupPackageCount' => 1,
				'Weight'            => array(
					'Value' => round($package['container']->get_weight('gross'),2),
					'Units' => 'LB'
			    ),
			    'Dimensions' => array(
					'Length' => round($package['container']->get_length('outer'),2),
					'Width'  => round($package['container']->get_width('outer'),2),
					'Height' => round($package['container']->get_height('outer'),2),
					'Units'  => 'IN'
				)

			);
		}

		return $request;
	}

	public function add_rates( $rates ){
		$adapt_rates = array();

		if ( !empty( $rates )){
			foreach ($rates as $key => $rate ) {
				$adapt_rates[$key] = array(
					'id' => $rate['id'],
					'cost' => $rate['due']
				);

				if( $this->include_tax == 'no' )
					$adapt_rates[ $key ]['cost'] = $rate['base'];
			}
		}

		if ( is_array( $adapt_rates ) && count( $adapt_rates ) >0 ) {

            //apply fee to rate
            $adapt_rates = $this->fee_to_cart( $adapt_rates );

            $adapt_rates = $this->apply_adjustment_fee( $adapt_rates );

            //set rates order
            $adapt_rates = $this->arrange_rates( $adapt_rates, $this->rate_type, $this->rate_order );

            //set rate label and remove zero rate
            foreach ( $adapt_rates as $key => $rate ) {

                if ( $rate['cost'] > 0  && $this->custom_services[ $rate['id'] ]['enabled']  ) {
                    $label = $this->custom_services[ $rate['id'] ]['name'] == '' ?  $this->services[ $rate['id'] ]['name'] : $this->custom_services[ $rate['id'] ]['name'];

                    //format label
                    $format = $this->price_format;
                    $format = str_replace( '%base%' , $rates[ $key ]['base'], $format );

            		$format = str_replace( '%surcharge%' , $rates[ $key ]['surcharge'], $format );

            		$format = str_replace( '%due%' , $rates[ $key ]['due'], $format );

            		$format = str_replace( '%label%' , $label, $format );

            		$rate['label'] = $format;
                    $this->add_to_rate( $rate );
                }
            }
        }else if ( $this->fallback ) {
            $rate = array(
                'id'    => 'fallback',
                'cost'  => $this->fallback,
                'label' => $this->fallback_name,
            );
            $this->add_to_rate( $rate );
        }
	}

	/**
	 * Shipping result from the end point
	 * @param type $data
	 * @param type $cache
	 * @return response
	 */
	function fedex_shipping($data = false, $cache = false) {


		if ( ! class_exists( 'nusoap_base' ) )
			include_once( EW_WC_FEDEX_PLUGIN_DIR . '/includes/fedex-api/nusoap.php');

		try {
			if ( class_exists( 'SoapClient' ) ) {

				ini_set("soap.wsdl_cache_enabled", "0");

				if ( ! $this->production ) {
					$client = new SoapClient( EW_WC_FEDEX_PLUGIN_DIR . '/includes/fedex-api/test/RateService_v' . $this->rateservice_version. '.wsdl', array( 'trace' => 1 ) );
				} else {
					$client = new SoapClient( EW_WC_FEDEX_PLUGIN_DIR . '/includes/fedex-api/production/RateService_v' . $this->rateservice_version. '.wsdl', array( 'trace' => 1 ) );
				}

				$response = $client->getRates( $data );

			} elseif (class_exists('nusoap_client')) {

				if ( ! $this->production ) {
					$client = new nusoap_client( EW_WC_FEDEX_PLUGIN_DIR . '/includes/fedex-api/test/RateService_v' . $this->rateservice_version. '.wsdl' , 'wsdl');
				} else {
					$client = new nusoap_client( EW_WC_FEDEX_PLUGIN_DIR . '/includes/fedex-api/production/RateService_v' . $this->rateservice_version. '.wsdl' , 'wsdl');
				}

				$response = $client->call('getRates', array('RateRequest' => $data));

				mkobject($response);


			} else {
				$this->add_debug_message( __("FedEx Rates cannot be used because this server does not have SOAP support.", "extensionworks"), 'Fedex Error');
			}
		} catch (Exception $e) {

			$this->add_debug_message( __("FedEx Rates cannot be used because this server cannot connect to Fedex Host.", "extensionworks"), 'Fedex Error');
			return false;
		}

		return $response;
	}
}

?>
