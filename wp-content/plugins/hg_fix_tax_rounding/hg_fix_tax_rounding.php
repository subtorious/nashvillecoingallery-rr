<?php
/*
Plugin Name: HG Fix Tax Rounding
Description: Adds the woocommerce_calculate_totals action and number_formats each tax item before they are added together by woocommerce to avoid a rounding error with QBPOS.
Version: 1.0
Author: Horton Group, Dev: Mark Freeman
Author URI: http://hortongroup.com
*/

function action_woocommerce_calculate_totals( $pCart ) 
{
	$pCart->tax_total= number_format($pCart->tax_total, 2);
	$pCart->shipping_tax_total= number_format($pCart->shipping_tax_total, 2); 

	foreach($pCart->cart_contents as &$line_item)
	{
		$line_item->line_tax= number_format($line_item['line_tax'], 2);
		$line_item->line_subtotal_tax= number_format($line_item['line_subtotal_tax'], 2);
		$line_item->line_tax_data->total= number_format($line_item['line_tax_data']['total'], 2);
		$line_item->line_tax_data->subtotal= number_format($line_item['line_tax_data']['subtotal'], 2);
	}
	unset($line_item);

	foreach($pCart->taxes as &$tax_item)
	{
		$tax_item= number_format($tax_item, 2);
	}
	unset($tax_item);


	foreach($pCart->shipping_taxes as &$tax_item)
	{
		$tax_item= number_format($tax_item, 2);
	}
	unset($tax_item);

	// $pCart->cart_contents_total= number_format($pCart->cart_contents_total, 2);
	// $pCart->shipping_total= number_format($pCart->shipping_total, 2);
	// $pCart->fee_total= number_format($pCart->fee_total, 2);
	// $pCart->dp= number_format($pCart->dp, 2);
};
add_action( 'woocommerce_calculate_totals', 'action_woocommerce_calculate_totals', 10, 1 );
?>