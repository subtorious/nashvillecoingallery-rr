<?php


// If uninstall not called from WordPress exit
if( !defined( 'ABSPATH' ) && !defined( 'WP_UNINSTALL_PLUGIN' ) )
    exit();

// Delete option from options table delete_option( woocommerce_us_ps_settings );
$key = 'woocommerce_us_ps_settings';

delete_option( $key );

//delete version number
delete_option( 'ew_woo_usps_verion' );

?>
