<?php
/*
  Plugin Name: WooCommerce USPS
  Plugin URI: http://www.extensionworks.com
  Description: Flat Rate and Realtime shipping rates from USPS.
  Version: 2.9.1
  Author: Extension Works
  Author URI: http://www.extensionworks.com
  Copyright: © Extension Works.

 */


add_action( 'plugins_loaded', 'woocommerce_usps_init', 10 );


function woocommerce_usps_init() {


	/**
	 * Define plugin global path
	 */
	if( !defined( 'EW_WOO_USPS_PLUGIN_NAME' ) )
		define('EW_WOO_USPS_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)),'/'));

	if( !defined( 'EW_WOO_USPS_PLUGIN_UPDATE' ) )
		define('EW_WOO_USPS_PLUGIN_UPDATE', plugin_basename(__FILE__) );


	if( !defined( 'EW_WOO_USPS_PLUGIN_DIR' ) )
		define('EW_WOO_USPS_PLUGIN_DIR', WP_PLUGIN_DIR.'/'.EW_WOO_USPS_PLUGIN_NAME);


	/**
	 * Define pluign version number
	 */
	if( !defined('EW_WOO_USPS_VERSION_KEY'))
		define('EW_WOO_USPS_VERSION_KEY','ew_woo_usps_verion');

	if(!defined('EW_WOO_USPS_VERSION_NUM'))
		define('EW_WOO_USPS_VERSION_NUM','2.9.1');

	add_option( EW_WOO_USPS_VERSION_KEY,EW_WOO_USPS_VERSION_NUM );



	/**
	 * Load essential files
	 */
	require_once  EW_WOO_USPS_PLUGIN_DIR . '/includes/extensionworks-functions.php';


	/**
	 * Check plugin dependency
	 */
	if( !check_plugin_dependency() )
		return;


	/**
	 * Plugin updates
	 */
	extensionworks_queue_update( plugin_basename( __FILE__ ), 'b81ce36710a3de098a3c9363d996f6aa', 'woo-usps' );


	/**
	 * Load Shipping class
	 */
	require_once EW_WOO_USPS_PLUGIN_DIR . '/includes/class-usps-shipping.php';


	/**
	 * Add to WooCommerce Hook
	 */
	function add_ew_woo_usps_method($methods) {
        $methods[] = 'EW_WOO_Shipping_USPS';
        return $methods;
    }

	add_filter( 'woocommerce_shipping_methods', 'add_ew_woo_usps_method' );
	add_filter( 'extensionworks_shipping_methods', 'add_ew_woo_usps_method' );



	/**
	 * Plugin Information Link
	 */
	function ew_woo_usps_plugin_links( $links ) {

		$setting_url = admin_url( 'admin.php?page=woocommerce_settings&tab=shipping&section=EW_WOO_Shipping_USPS' );

        if ( check_woo_version('2.1') ){
            $setting_url = admin_url( 'admin.php?page=wc-settings&tab=shipping&section=ew_woo_shipping_usps' );
        }

        $plugin_links = array(
            '<a href="' . $setting_url . '">' . __( 'Settings', 'extensionworks' ) . '</a>',
            '<a href="http://help.extensionworks.com/hc/en-us/requests/new">' . __( 'Support', 'extensionworks' ) . '</a>',
            '<a href="http://help.extensionworks.com/hc/en-us/categories/200037668-WooCommerce-USPS">' . __( 'Docs', 'extensionworks' ) . '</a>',
        );

        return array_merge( $plugin_links, $links );
    }

    add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ew_woo_usps_plugin_links' );

}

