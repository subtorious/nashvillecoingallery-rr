<?php


/**
 * USPS Rate Package class
 */
class USPS_Rate_Package{

	/**
	* Size constants
	*/
	const SIZE_LARGE   = 'LARGE';
	const SIZE_REGULAR = 'REGULAR';

	public function set_field( $name, $value ){
		$this->fields[ ucwords( $name ) ] = $value;
	}

	public function set_Pounds( $value ){

		$this->fields['Pounds'] = $value;
	}

	public function set_ounces( $value ){

		$this->fields['Ounces'] = $value;
	}

	public function set_container( $value ){
		$this->fields['Container'] = $value;
	}

	public function set_size( $value ){
		$this->fields['Size'] = $value;
	}

	public function set_width( $value ){
		$this->fields['Width'] = $value;
	}

	public function set_length( $value ){
		$this->fields['Length'] = $value;
	}

	public function set_height( $value ){
		$this->fields['Height'] = $value;
	}

	public function set_girth( $value ){
		$this->fields['Girth'] = $value;
	}

	public function set_machinable( $value ){
		$this->fields['Machinable'] = $value;
	}

	public function get_fields(){
		return $this->fields;
	}

}


class USPS_International_Package extends USPS_Rate_Package{


	/**
	 * Mail Type value
	 */
	const MAILTYPE_ALL            = 'ALL';
	const MAILTYPE_PACKAGE        = 'Package';
	const MAILTYPE_POSTCARDS      = 'Postcards';
	const MAILTYPE_AEROGRAMMES    = 'aerogrammes';
	const MAILTYPE_ENVELOPE       = 'Envelope';
	const MAILTYPE_LARGE_ENVELOPE = 'LargeEnvelope';
	const MAILTYPE_FLATRATE       = 'FlatRate';


	/**
	 * Container type value
	 */
	const CONTAINER_RECTANGULAR    = 'RECTANGULAR';
	const CONTAINER_NONRECTANGULAR = 'NONRECTANGULAR';


	protected $fields = array(
		//Value must be numeric. Package weight generally cannot exceed 70 pounds.
		//Refer to the International Mail Manual (IMM) for weight requirements
		//per country and mail service. The IMM can be found at the Postal Explorer web site.
		//require
		//minInclusive=0.0
		'Pounds'          => '',
		'Ounces'          => '',

		//Indicates whether or not the item is machinable.
		//A surcharge is applied to a First-Class Mail International item
		//if it has one or more non-machinable characteristics.
		//See International Mail Manual (IMM) Section 241.217 for more information.
		//default         =true
		//optional
		'Machinable'      => 'true',

		//Package type being shipped
		//required
		'MailType'        => '',

		//used to compute Insurance fee (if insurance is available for service and destination)
		//and indemnity coverage.
		//required
		//minExclusive    =0.0
		'ValueOfContents' => '',

		//Entries must be from the USPS list of valid countries from
		//the International Country Listings. To access the
		//International Country Listings, go to the Index of Countries string and Localities.
		//http://pe.usps.gov/text/imm/immctry.htm
		//required
		'Country'         => '',


		//Use to specify special containers or container attributes
		//that may affect postage
		'Container'       => '',

		'Size'            => '',
		'Width'           => '',
		'Length'          => '',
		'Height'          => '',
		'Girth'           => '',
 	);

	public function set_country( $value ){
		$this->fields['Country'] = strtoupper( $value );
	}

	public function set_container( $value ){
		$this->fields['Container'] =  $value;
	}

	public function set_valueOfContents( $value ){
		$this->fields['ValueOfContents'] =  $value;
	}

	public function set_mailType( $value ){
		$this->fields['MailType'] =  $value;
	}

	public function set_originZipcode( $value ){
		$this->fields['OriginZip'] = $value;
	}

	public function set_destinationZipcode( $value ){
		$this->fields['DestinationPostalCode'] = $value;
	}

	public function set_acceptDatetime( $value ){
		$this->fields['AcceptanceDateTime'] = $value;
	}


}

class USPS_Domestic_Package extends USPS_Rate_Package {

	/**
	* Services constants
	*/
	const SERVICE_FIRST_CLASS                = 'FIRST CLASS';
	const SERVICE_FIRST_CLASS_COMMERCIAL     = 'FIRST CLASS COMMERCIAL';
	const SERVICE_FIRST_CLASS_HFP_COMMERCIAL = 'FIRST CLASS HFP COMMERCIAL';
	const SERVICE_PRIORITY                   = 'PRIORITY';
	const SERVICE_PRIORITY_COMMERCIAL        = 'PRIORITY COMMERCIAL';
	const SERVICE_PRIORITY_HFP_COMMERCIAL    = 'PRIORITY HFP COMMERCIAL';
	const SERVICE_EXPRESS                    = 'EXPRESS';
	const SERVICE_EXPRESS_COMMERCIAL         = 'EXPRESS COMMERCIAL';
	const SERVICE_EXPRESS_SH                 = 'EXPRESS SH';
	const SERVICE_EXPRESS_SH_COMMERCIAL      = 'EXPRESS SH COMMERCIAL';
	const SERVICE_EXPRESS_HFP                = 'EXPRESS HFP';
	const SERVICE_EXPRESS_HFP_COMMERCIAL     = 'EXPRESS HFP COMMERCIAL';
	const SERVICE_PARCEL                     = 'PARCEL';
	const SERVICE_MEDIA                      = 'MEDIA';
	const SERVICE_LIBRARY                    = 'LIBRARY';
	const SERVICE_ALL                        = 'ALL';
	const SERVICE_ONLINE                     = 'ONLINE';
	/**
	* First class mail type
	* required when you use one of the first class services
	*/
	const MAIL_TYPE_LETTER          = 'LETTER';
	const MAIL_TYPE_FLAT            = 'FLAT';
	const MAIL_TYPE_PARCEL          = 'PARCEL';
	const MAIL_TYPE_POSTCARD        = 'POSTCARD';
	const MAIL_TYPE_PACKAGE_SERVICE = 'PACKAGE SERVICE';
	/**
	* Container constants
	*/
	const CONTAINER_VARIABLE                     = 'VARIABLE';
	const CONTAINER_FLAT_RATE_ENVELOPE           = 'FLAT RATE ENVELOPE';
	const CONTAINER_PADDED_FLAT_RATE_ENVELOPE    = 'PADDED FLAT RATE ENVELOPE';
	const CONTAINER_LEGAL_FLAT_RATE_ENVELOPE     = 'LEGAL FLAT RATE ENVELOPE';
	const CONTAINER_SM_FLAT_RATE_ENVELOPE        = 'SM FLAT RATE ENVELOPE';
	const CONTAINER_WINDOW_FLAT_RATE_ENVELOPE    = 'WINDOW FLAT RATE ENVELOPE';
	const CONTAINER_GIFT_CARD_FLAT_RATE_ENVELOPE = 'GIFT CARD FLAT RATE ENVELOPE';
	const CONTAINER_FLAT_RATE_BOX                = 'FLAT RATE BOX';
	const CONTAINER_SM_FLAT_RATE_BOX             = 'SM FLAT RATE BOX';
	const CONTAINER_MD_FLAT_RATE_BOX             = 'MD FLAT RATE BOX';
	const CONTAINER_LG_FLAT_RATE_BOX             = 'LG FLAT RATE BOX';
	const CONTAINER_REGIONALRATEBOXA             = 'REGIONALRATEBOXA';
	const CONTAINER_REGIONALRATEBOXB             = 'REGIONALRATEBOXB';
	const CONTAINER_REGIONALRATEBOXC             = 'REGIONALRATEBOXC';
	const CONTAINER_RECTANGULAR                  = 'RECTANGULAR';
	const CONTAINER_NONRECTANGULAR               = 'NONRECTANGULAR';


	protected $fields = array(
		'Service'        => '',
		'ZipOrigination' =>'',
		'ZipDestination' =>'',
		'Pounds'         => '',
		'Ounces'         => '',
		'Container'      => '',
		'Size'           => '',
		'Width'          => '',
		'Length'         => '',
		'Height'         => '',
		'Girth'          => '',
		'Machinable'     => '',
		'ShipDate'       => ''
 	);


	public function set_first_class_mail_type( $value ){

		$this->fields['FirstClassMailType'] =  $value;
	}

	public function set_field( $name, $value ){
		$this->fields[ ucwords( $name ) ] = $value;
	}

	public function set_shipdate( $value ){
		$this->fields['ShipDate'] = $value;
	}

	public function set_origination_zip( $value ){
		$this->fields['ZipOrigination'] = $value;
	}

	public function set_destination_zip( $value ){
		$this->fields['ZipDestination'] = $value;
	}

	public function set_service( $value ){

		$this->fields['Service'] = $value;
	}


}
