<?php

require_once 'class-USPS-API.php';

/**
 * USPS Rate API Class
 */
class USPS_Rate_API extends USPS_API{


	protected $live_url = 'http://production.shippingapis.com/shippingapi.dll';

	/**
	 * API version number
	 * @var string
	 */
	protected $api_version = 'RateV4';


	/**
	 * holds usps packages
	 * @var array
	 */
	protected $packages = array();

	protected $fields = array();


	public function get_rates(){

		$rates = $this->post();

		return $rates;
	}


	public function get_fields(){

		return $this->packages;
	}

	/**
	 * Change to international methods
	 */
	public function set_international(){
		$this->api_version = 'IntlRateV2';
	}


	/**
	 * Add usps package
	 * @param USPS_Rate_Package $package [description]
	 * @param [type]            $id      [description]
	 */
	public function add_package( USPS_Rate_Package $package, $id = null ){
		$id = $id !== null ? $id : (count($this->packages)+1);

    	$this->packages['Package'][] = array_merge(array('@attributes' => array('ID' => $id)), $package->get_fields());
	}
}



