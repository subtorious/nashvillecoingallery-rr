<?php

class USPS_API{

	/**
	 * USPS usesrname
	 * @var string
	 */
	private $username = '';


	/**
	 * USPS api list
	 * @var array
	 */
	protected $apiCodes = array(
		'RateV2'                          => 'RateV2Request',
		'RateV4'                          => 'RateV4Request',
		'IntlRateV2'                      => 'IntlRateV2Request',
		'Verify'                          => 'AddressValidateRequest',
		'ZipCodeLookup'                   => 'ZipCodeLookupRequest',
		'CityStateLookup'                 => 'CityStateLookupRequest',
		'TrackV2'                         => 'TrackFieldRequest',
		'FirstClassMail'                  => 'FirstClassMailRequest',
		'SDCGetLocations'                 => 'SDCGetLocationsRequest',
		'ExpressMailLabel'                => 'ExpressMailLabelRequest',
		'OpenDistributePriorityV2'        => 'OpenDistributePriorityV2.0Request',
		'OpenDistributePriorityV2Certify' => 'OpenDistributePriorityV2.0CertifyRequest',
		'ExpressMailIntl'                 => 'ExpressMailIntlRequest',
		'PriorityMailIntl'                => 'PriorityMailIntlRequest',
		'FirstClassMailIntl'              => 'FirstClassMailIntlRequest',
	);




	private $params = array();


	public function __construct( $username ){
		$this->username = $username;
	}


	/**
	 * Get XML string
	 * @return string
	 */
	public function get_xml_string(){


		require_once 'XMLParser.php';

		// add usps account to default
	    $account_info = array(
	      '@attributes' => array('USERID' => $this->username),
	      'Revision' => 2,
	    );

	    // Add in the sub class data
	    $postFields = array_merge( $account_info, $this->get_fields() );


	    $xml = XMLParser::createXML( $this->apiCodes[ $this->api_version ], $postFields);

	    return $xml->saveXML();
	}


	/**
	 * Return post value
	 * @return Array
	 */
	public function get_post_field(){
		$fields = array('API' => $this->api_version, 'XML' => $this->get_xml_string());

	    return $fields;
	}


	/**
	 * Check errors
	 * @return boolean
	 */
	public function has_error(){

		return empty( $this->errors ) ? false : true;
	}


	/**
	 * Return errors
	 * @return  array
	 */
	public function get_errors(){
		return $this->errors;
	}


	public function decode_response(){

		if( $this->response ) {
      		$this->decode_response =  XML2Array::createArray( $this->response );
    	}

    	return $this->decode_response;
	}

	public function getValueByKey($array,$key) {
		foreach($array as $k=>$each) {

			// 0 == 'Error'
			// so using ===
			if($k===$key) {
				return $each;
			}

			if(is_array($each)) {
				if($return = $this->getValueByKey($each,$key)) {
					return $return;
				}
			}
		}

		// Nothing matched
		return null;
	}


	/**
	 * Post request
	 * @return type
	 */
	public function post(){

		$ch = curl_init();


		curl_setopt($ch, CURLOPT_URL, $this->live_url );
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
		// curl_setopt($ch, CURLOPT_PORT, 443);
		curl_setopt($ch, CURLOPT_USERAGENT, 'usps-php');
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->get_post_field(), null, '&'));


		$content = curl_exec( $ch );

		if ( $content !== false ){

			$this->response = $content;

			$decode_response = $this->decode_response( $this->response );

			$error = $this->getValueByKey($decode_response, 'Error');
			if( $error ){
				$this->errors[ $error['Number'] ] = $error['Description'];
			}else{
				return $decode_response;
			}

		}else{
			$this->errors[ curl_errno($ch) ] = curl_error($ch);
		}

	    $response = curl_getinfo( $ch );
	    curl_close ( $ch );

	}
}
