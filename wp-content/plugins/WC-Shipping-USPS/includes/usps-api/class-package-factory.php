<?php

require_once 'class-USPS-rate-package.php';

/**
 * Factory to create usps packages
 */
class Package_Factory{

	private $data = null;


	/**
	 * Create package for international method
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function create_international_package( $data ){
		$container = $data['container'];

		$usps_package = new USPS_International_Package();

		$weight   = $container->get_weight('gross');
		$height   = $container->get_height('outer');
		$length   = $container->get_length('outer');
		$width    = $container->get_width('outer');
		$girth    = $container->get_girth();

		$price  = $container->get_price();

		$usps_package->set_Pounds( floor( $weight ) );

		$usps_package->set_ounces( number_format(( $weight - floor( $weight ) ) * 16, 2) );

		$size    = ( max( $height, $length, $width ) > 12 ) ? USPS_Rate_Package::SIZE_LARGE : USPS_Rate_Package::SIZE_REGULAR;

		$usps_package->set_size( $size );

		$usps_package->set_width( $width );

		$usps_package->set_length( $length );

		$usps_package->set_height( $height );


		$usps_package->set_girth(  round( $girth ) );

		$usps_package->set_mailType( USPS_International_Package::MAILTYPE_ALL );

		$usps_package->set_machinable( 'true' );

		$usps_package->set_valueOfContents( $price );

		$usps_package->set_container( USPS_International_Package::CONTAINER_RECTANGULAR );

		return $usps_package;
	}


	/**
	 * Create package to domestic methods
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	public function create_domestic_package( $data ){
		$container = $data['container'];

		$usps_package = new USPS_Domestic_Package();

		$weight   = $container->get_weight('gross');
		$height   = $container->get_height('outer');
		$length   = $container->get_length('outer');
		$width    = $container->get_width('outer');
		$girth    = $container->get_girth();

		$usps_package->set_Pounds( floor( $weight ) );

		$usps_package->set_ounces( number_format(( $weight - floor( $weight ) ) * 16, 2) );

		$size    = ( max( $height, $length, $width ) > 12 ) ? USPS_Rate_Package::SIZE_LARGE : USPS_Rate_Package::SIZE_REGULAR;

		$usps_package->set_service( USPS_Domestic_Package::SERVICE_ALL );

		$usps_package->set_size( $size );
		//
		$usps_package->set_width( $width );

		$usps_package->set_length( $length );

		$usps_package->set_height( $height );


		$usps_package->set_girth( round( $girth ) );

		$usps_package->set_machinable( 'true' );

		$usps_package->set_shipdate( date( "d-M-Y", ( current_time('timestamp') + (60 * 60 * 24) ) ) );

		return $usps_package;
	}

}
