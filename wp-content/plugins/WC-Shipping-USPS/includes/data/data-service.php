<?php
return array(

		//Domestic

		//First class
		'd0p'  => array( 'name' => "First-Class Mail&#0174; Parcel"),
		'd0l'  => array( 'name' => "First-Class Mail&#0174; Large Envelope"),
		'd0s'  => array( 'name' => "First-Class Mail&#0174; Stamped Letter"),
		'd0pc' => array( 'name' => "First-Class Mail&#0174; Postcards"),
		'd12'  => array( 'name' => "First-Class&#8482; Postcard Stamped"),
		'd15'  => array( 'name' => "First-Class&#8482; Large Postcards"),
		'd19'  => array( 'name' => "First-Class&#8482; Keys and IDs"),
		'd61'  => array( 'name' => "First-Class&#8482; Package Service"),
		'd53'  => array( 'name' => "First-Class&#8482; Package Service Hold For Pickup"),

		//Express mail
		'd2'  => array( 'name' => "Priority Mail Express&#8482; Hold for Pickup"),
		'd3'  => array( 'name' => "Priority Mail Express&#8482; PO to Address"),
		'd23' => array( 'name' => "Priority Mail Express&#8482; Sunday/Holiday"),

		//priority mail
		"d1"  => array( 'name' => "Priority Mail&#0174;"),
		"d18" => array( 'name' => "Priority Mail&#0174; Keys and IDs"),
		// "d47" => array( 'name' => "Priority Mail&#0174; Regional Rate Box A"),
		// "d49" => array( 'name' => "Priority Mail&#0174; Regional Rate Box B"),
		// "d58" => array( 'name' => "Priority Mail&#0174; Regional Rate Box C"),

		'd4'  => array( 'name' => "Standard Post&#8482;"),
		'd5'  => array( 'name' => "Bound Printed Matter"),
		'd6'  => array( 'name' => "Media Mail&#0174;"),
		'd7'  => array( 'name' => "Library Mail"),


		//international
		"i1"  => array( 'name' => "Priority Mail Express International&#8482;"),

		"i2"  => array( 'name' => "Priority Mail International&#0174;"),

		"i4"  => array( 'name' => "Global Express Guaranteed&#0174;"),
		"i5"  => array( 'name' => "Global Express Guaranteed&#0174; Document used"),
		"i6"  => array( 'name' => "Global Express Guaranteed&#0174; Non-Document Rectangular"),
		"i7"  => array( 'name' => "Global Express Guaranteed&#0174; Non-Document Non-Rectangular"),
		"i12" => array( 'name' => "Global Express Guaranteed&#0174; Envelope"),
		"i13" => array( 'name' => "First Class Package Service&#8482; International Letters"),
		"i14" => array( 'name' => "First Class Package Service&#8482; International Large Envelope"),
		"i15" => array( 'name' => "First Class Package Service&#8482; International Parcel"),
		"i21" => array( 'name' => "International Postcards"),
	);