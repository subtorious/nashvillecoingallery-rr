<?php

/**
 * Shipping method class
 * */
class EW_WOO_Shipping_USPS extends EW_Shipper {

	/**
	 * Shipping method ID
	 * @var string
	 */
	var $id                       = 'us_ps';

	/**
	 * Shipping method name
	 * @var string
	 */
	var $method_title             = 'U.S. Postal Service';

	/**
	 * Shipping method carrier
	 * @var string
	 */
	var $carrier                  = "U.S. Postal Service";

	/**
	 * Shipping method description
	 * @var string
	 */
	var $description              = "U.S. Postal Service calculates shipping price base on United States Postal Service standard.";

	/**
	 * Allowed shipping origin countries
	 * @var array
	 */
	var $allowed_origin_countries = array('US', 'PR', 'VI');

	/**
	 * Allowed currencies
	 * @var array
	 */
	var $allowed_currencies       = array('USD');

	/**
	 * Default dimension unit
	 * @var string
	 */
	var $dimension_unit           = 'IN';

	/**
	 * Default weight unit
	 * @var string
	 */
	var $weight_unit              = 'LBS';

	/**
	 * Extensionworks default USPS user ID
	 * @var string
	 */
	var $userid                   = "474HYPNO7276";


	/**
	 * some country name is different with
	 * woo country name
	 * @var array
	 */
	var $differ_country_name = array(
		'GB' => 'UNITED KINGDOM',
		'IE' => 'Ireland', //Republic of Ireland
	);


	var $flat_boxes = array();
	public function __construct(){

		$this->flat_boxes = include('data/data-flat-box.php');

		$this->services = include( 'data/data-service.php' );
		parent::__construct();

		$this->plugin = EW_WOO_USPS_PLUGIN_UPDATE;

		if( $this->user_id == '' )
			$this->user_id = $this->userid;

	}

	/**
	 * Check if it's international shipping
	 */
	function is_international() {
		global $woocommerce;

		$customer = $woocommerce->customer;

		if ( $customer->get_shipping_country() == "US" || $customer->get_shipping_country() == "PR" || $customer->get_shipping_country() == "VI" ) {
			return false;
		}
		return true;
	}


	/**
	 * Add plugin setting form fields
	 */
	public function add_form_fields(){

		$this->form_fields['usps_rate_type'] = array(
			'title'       => __('USPS shipping rate type', 'extensionworks'),
			'type'        => 'select',
			'description' => __('Choose which rates to show your customers, ONLINE rates are normally cheaper than OFFLINE', 'extensionworks'),
			'default'     => 'online',
			'options'     => array(
            	'online'      => __( 'Use Commercial Rates', 'extensionworks' ),
				'all'         => __( 'Use Retail Rates', 'extensionworks' ),
            ),

		);

		$this->form_fields['user_id'] = array(
			'title'       => __('USPS User ID', 'extensionworks'),
			'type'        => 'text',
			'description' => __(' You can use your own user USPS Web Tools ID by signing up to <a href="https://www.usps.com/business/web-tools-apis/developers-center.htm">USPS</a>, if the USPS ID is blank it will default to an account ID that was registered by Extension Works. Customers using the USPS label printing and tracking code extension, your own Web Tools User ID is required for use.', 'extensionworks'),
			'default'     => ''

		);

		$this->form_fields['flat_rate_option'] = array(
			'title' => __('USPS Flat Rates Option', 'extensionworks'),
			'type'  => 'title',
		);

		$this->form_fields['enable_flat_rate_box'] = array(
			'title'   => __('USPS Flat Rate Boxes', 'extensionworks'),
			'type'    => 'select',
			'default' => 'yes',
			'options' => array(
				'yes'         => __( 'Yes - Enable flat rate boxes', 'extensionworks' ),
				'no'          => __( 'No - Disable flat rate boxes', 'extensionworks' ),
				'priority'    => __( 'Enable Priority flat rate boxes only', 'extensionworks' ),
				'express'     => __( 'Enable Express flat rate boxes only', 'extensionworks' ),
			),
			'description'     => __( 'Enable this option to offer shipping using USPS Flat Rate boxes and Services. Items will be packed into the boxes and the customer will be offered a single rate from these.<br />
									For more information, please click <a href="https://www.usps.com/ship/priority-mail-flat.htm?" >priority mail flat rate</a> or <a href="https://www.usps.com/ship/priority-mail-express-flat-rate.htm?" >priority mail express flat rate</a>', 'extensionworks' )
		);

		$this->form_fields['custom_flat_rate_box'] = array(
			'title' => __('USPS Flat Rate Boxes', 'extensionworks'),
			'type'  => 'flat_box',

		);

		$this->form_fields['flat_rate_express_title'] = array(
			'title'           => __( 'Express Flat Rate Box Name', 'extensionworks' ),
			'type'            => 'text',
			'description'     => '',
			'default'         => '',
			'placeholder'     => 'Priority Mail Express Flat Rate&#0174;'
		);

		$this->form_fields['flat_rate_priority_title'] = array(
			'title'           => __( 'Priority Flat Rate Box Name', 'extensionworks' ),
			'type'            => 'text',
			'description'     => '',
			'default'         => '',
			'placeholder'     => 'Priority Mail Flat Rate&#0174;'
		);


		$this->form_fields = apply_filters( 'shipping_usps_field', $this->form_fields );


	}


	/**
	 * Load plugin setting
	 */
    protected function load_setting() {
    	parent::load_setting();

        $this->flat_boxes = $this->load_flat_box();
    }


    /**
     * Load flat box setting
     */
    function load_flat_box(){

    	$boxes= array();

    	foreach ($this->flat_boxes as $key => $value) {
    		if( !isset( $this->custom_flat_rate_box[$key] ) ||
    			$this->custom_flat_rate_box[$key]['outer_length'] <= 0 ||
    			$this->custom_flat_rate_box[$key]['outer_width'] <= 0 ||
    			$this->custom_flat_rate_box[$key]['outer_height'] <= 0
    			)
    			$boxes[ $key ] =$value;
    		else
    			$boxes[ $key ] = array_merge( $value, $this->custom_flat_rate_box[$key]);
    	}

    	return $boxes;
    }


    /**
     * Save package data
     * @param  string $order_id
     * @param  [type] $posted   [description]
     * @return [type]           [description]
     */
    public function save_package( $order_id, $posted ){


        $packages = get_transient( '_package_' );

        if( $packages ){
            foreach ($packages as $key => $package ) {
                foreach ($package as $k => $v) {
                    $packages[$key][$k]['container'] = $v['container']->to_array();
                }

            }

            update_post_meta( $order_id, '_order_package', $packages );
        }


        $packages = get_transient( '_package_priority' );

        if( $packages ){
            foreach ($packages as $key => $package ) {
                $packages[$key]['container'] = $package['container']->to_array();
            }

            update_post_meta( $order_id, '_order_package_priority', $packages );
        }



        $packages = get_transient( '_package_express' );

        if( $packages ){
            foreach ($packages as $key => $package ) {
                $packages[$key]['container'] = $package['container']->to_array();
            }

            update_post_meta( $order_id, '_order_package_express', $packages );
        }


    }


    public function calculate_shipping( $packages = array() ) {

    	//if haven't get postcode and country is empty, return
        if( !$this->is_international() && !$this->get_shipping_postcode() )
        	return;

    	$shippable_products =  $this->prepare_products( $packages );


    	$total = 0;
        foreach ($shippable_products as $key => $product ) {
            $total += $product->get_price();
        }


        if( $this->order_limit > 0  && $total >= $this->order_limit ){

            $rate = array(
                    'id'    => $this->id,
                    'cost'  => 0,
                    'label' => $this->free_text,
                );
            $this->add_to_rate( $rate );

            return;
        }

    	$rates = array();

    	//get api rates
    	if( $this->shipping_method =='yes' ){

			$api_rates = $this->get_api_rates( $shippable_products );

			if( !is_array( $api_rates ) )
				$api_rates = array();

			//set rate label and remove zero rate
            foreach ( $api_rates as $key => $rate ) {
                if ( $rate['cost'] > 0  && isset($this->custom_services[ $rate['id'] ]) && $this->custom_services[ $rate['id'] ]['enabled']  ) {
                    $rate['label'] = $this->custom_services[ $rate['id'] ]['name'] == '' ?  $this->services[ $rate['id'] ]['name'] : $this->custom_services[ $rate['id'] ]['name'];

                    $api_rates[ $key ] = $rate;
                }else{
                	unset( $api_rates[ $key ] );
                }
            }

			$api_rates = $this->apply_adjustment_fee( $api_rates );

    		$rates = array_merge( $rates, $api_rates );

    	}

    	//get flat rates
    	if( $this->enable_flat_rate_box =='yes' ){

    		$flat_rates = $this->get_flat_rate( $shippable_products );

    		$rates = array_merge( $rates, $flat_rates );
    	}

    	//remove zero cost
    	foreach ($rates as $id => $rate) {
    		if( $rate['cost'] <= 0 )
    			unset( $rates[ $id ] );
    	}

    	if ( count( $rates ) > 0 ) {

            $rates = $this->arrange_rates( $rates, $this->rate_type, $this->rate_order );

            $rates = $this->fee_to_cart( $rates );

            foreach ( $rates as $key => $rate ) {

                if ( $rate['cost'] > 0 )
                    $this->add_to_rate( $rate );
            }
        }else if ( $this->fallback ) {
            $rate = array(
				'id'    => 'fallback',
				'cost'  => $this->fallback,
				'label' => $this->fallback_name,
            );

            $this->add_to_rate( $rate );
        }
    }




    /**
     * Calculate rate from usps api server
     * @param
     * @return array       rates
     */
    public function get_api_rates( $products ){

    	$available_boxes = $this->prepare_boxes( $this->boxes, $this->packing_method );

		$robot = $this->set_packing_robot( $this->dimension_unit, $this->weight_unit );

		$robot->set_containers( $available_boxes );


		$locations = $this->pick_items_by_location($products);


		$packages = array();
        foreach ($locations as $key => $product ) {

        	$packages[] = $this->package_items($robot, $product );
        }

		//separate different package
		$location = array();
        foreach ($packages as $package ) {
            $items = $package[0]['container']->get_items();

            if($items[0]->get_location() != '' ){
                $location[$items[0]->get_location()] = $package;
            }else{
                $location[ $this->origin ] = $package;
            }
        }

        set_transient( '_package_', $location );

        //calculate rates
        $rates = array();
        foreach ($location as $key => $packages) {

            $rate = $this->get_package_rates( $packages, $this->prepare_methods() );

            if( empty( $rate ) ){
                if ( $this->fallback ) {
                    $rate = array(
                        'id'    => 'fallback',
                        'cost'  => $this->fallback,
                        'label' => $this->fallback_name,
                    );
                    $this->add_to_rate( $rate );
                }

                return;
            }

            $rates[$key] = $rate;
        }

        $rates = $this->merge_rates( $rates);

		//remove unexpected flat rates
		//because when the package size is regular,
		//api server return flat rate doesn't according to
		//the package size that send to it. For example, the package is suit to medium flat
		//box, but the api still return flat small rate.
		// $api_rates = $this->remove_flat_rate( $api_rates );

		return $rates;
    }



    /**
     * Calculate usps flat rate
     * USPS flat rate is fixed, not calculate through
     * USPS API server
     * @param  EW_Product $packages
     * @return array               rates
     */
	public function get_flat_rate( $packages ){

		$rates = array();
		$locations = $this->pick_items_by_location($packages);

		if ( $this->enable_flat_rate_box == 'yes' || $this->enable_flat_rate_box == 'priority' ) {

			$total = 0;
			$priority_rates = array();
			$flag = true;
			foreach ($locations as $key => $products ) {

				// Priority
				$priority_rates = $this->calculate_flat_rates( $products, 'priority' );

                if( empty( $priority_rates ) ){
                	$flag = false;
                	break;
                }

                $total += $priority_rates['cost'];
            }

            $priority_rates['cost'] = $total;

			if ( $priority_rates && $flag )
				$rates[ $priority_rates['id'] ] = $priority_rates;
		}

		if ( $this->enable_flat_rate_box == 'yes' || $this->enable_flat_rate_box == 'express' ) {

			$total = 0;
			$express_rates = array();
			$flag = true;
			foreach ($locations as $key => $products ) {

				// Priority
				$express_rates = $this->calculate_flat_rates( $products, 'express' );


                if( empty( $express_rates ) ){
                	$flag = false;
                	break;
                }
                $total += $express_rates['cost'];
            }

            $express_rates['cost'] = $total;

			if ( $express_rates && $flag )
				$rates[ $express_rates['id'] ] = $express_rates;
		}

		return $rates;
	}

	function calculate_flat_rates( $packages, $type ){


		$containers = $this->prepare_flat_box( $type );

		$this->add_debug_message( $containers, "USPS Flat rate $type boxes");

		$robot = $this->set_packing_robot(  $this->dimension_unit, $this->weight_unit );

		$robot->set_containers( $containers );

		$robot->set_items( $packages );

		$packages = $robot->packing();

		$packages = $this->handle_unpacked_items( $packages );

		set_transient( '_package_' . $type, $packages );

		$cost = 0.0;
		$unpackable = false;
		if( $packages ){
			foreach ($packages as $key => $package ) {
				$id = $package['container']->get_id();
				$this_cost = 0;
				if(  $id != '' && isset( $this->flat_boxes[ $id ] ) ){
					$box = $this->flat_boxes[ $id ];

					$shipping_country = $this->get_shipping_country();

					if( $this->usps_rate_type =='online' && $shipping_country =="CA" && isset( $box['online_cost_ca']) )
						$this_cost= $box['online_cost_ca'];
					else if( $this->usps_rate_type =='online' && isset( $box['online_cost'] ) )
						$this_cost= $box['online_cost'];
					else if( $shipping_country =="CA" && isset( $box['cost_CA']) )
						$this_cost = $box['cost_CA'];
					else
						$this_cost = $box['cost'];
				}else{
					$this->add_debug_message( $package, "USPS $type Flat rate unpackable package");
					$unpackable = true;
					break;
				}
				$cost += $this_cost;
			}
		}

		if ( $type == 'express' ) {
			$label = ! empty( $this->flat_rate_express_title ) ? $this->flat_rate_express_title : 'Priority Mail Express Flat Rate&#0174;';
		} else {
			$label = ! empty( $this->flat_rate_priority_title ) ? $this->flat_rate_priority_title : 'Priority Mail Flat Rate&#0174;';
		}

		$rate = array(
			'id' 	=> 'flat_rate_box_' . $type,
			'label' => $label,
			'cost' 	=> $cost,
			'sort'  => ( $type == 'express' ? -1 : -2 )
		);

		if( $unpackable ){
			$rate['cost'] = 0;
		}

		$this->add_debug_message( $rate, "USPS $type Flat rate");
		return $rate;

	}

	function prepare_flat_box( $type ){

		// Define boxes
		$containers = array();

		foreach ( $this->flat_boxes as $service_code => $box ) {

			if ( $box['type'] != $type || !$box['is_enable'] )
				continue;

			$domestic_service = substr( $service_code, 0, 1 ) == 'd' ? true : false;

			if ( !$this->is_intel_shipping() && $domestic_service || $this->is_intel_shipping() && ! $domestic_service ) {

				$container = new EW_Container( $box );
				$container->set_dimension_unit( $this->dimension_unit );
            	$container->set_weight_unit( $this->weight_unit );
            	$container->set_id( $service_code );
            	$containers []= $container;

			}
		}
		return $containers;

	}


	/**
    * Get rates for each package with certain methods
    */
    public function get_package_rates( $packages = array(), $methods = array() ) {

    	include_once EW_WOO_USPS_PLUGIN_DIR . '/includes/usps-api/class-USPS-Rate-API.php';

    	include_once EW_WOO_USPS_PLUGIN_DIR . '/includes/usps-api/class-package-factory.php';

    	$package_factory = new Package_Factory();

    	$rate_api = new USPS_Rate_API( $this->user_id );

    	$num_package = count( $packages );

    	if( $this->is_international() ){

    		$rate_api->set_international();

    		global $woocommerce;

			$country      = $woocommerce->countries->get_allowed_countries();

			$country_code = $this->get_shipping_country();

    		if( !isset( $country[ $country_code ] ) ){

    			ew_add_response_message( 'not allow shipping to country ' . $country_code, 'Setting Error', 'error' );
    			return;
    		}

    		//international packages
    		foreach ($packages as $package ) {

	    		//create usps package
	    		$usps_package = $package_factory->create_international_package( $package );

	    		//set ship to county
	    		$usps_package->set_country( isset( $this->differ_country_name[ $country_code] ) ? $this->differ_country_name[ $country_code] : $country[ $country_code ] );

	    		if( $country_code == 'CA' ){
	    			$usps_package->set_originZipcode( $package['container']->get_location() );
	    			$date = new DateTime();
	    			$usps_package->set_acceptDatetime( $date->format(DateTime::W3C) );
	    			$usps_package->set_destinationZipcode( $this->get_shipping_postcode() );
	    		}

	    		//add package to api
	    		$rate_api->add_package( $usps_package );
	    	}

    	}else{
    		//domestic package

    		$from_postcode = $this->origin;

    		$to_postcode = $this->get_shipping_postcode();


			if( $this->get_shipping_country() == 'US' && strlen( $to_postcode) > 5 )
				$to_postcode = substr( $to_postcode, 0, 5);


    		foreach ($packages as $package ) {

	    		//create usps package
	    		$usps_package = $package_factory->create_domestic_package( $package );

	    		//set package service

	    		//set package sender postcode
	    		$usps_package->set_origination_zip( $from_postcode );

	    		//set package recipient postcode
	    		$usps_package->set_destination_zip( $to_postcode );

	    		//add package to api
	    		$rate_api->add_package( $usps_package );
	    	}
    	}


    	//get api rates
    	$rates = $rate_api->get_rates();


    	if( $rate_api->has_error() ){

    		ew_add_response_message( $rate_api->get_errors(), 'API Error', 'error' );
    		return;
    	}

		//get packages postage
		$package_postages = $rate_api->getValueByKey( $rates, 'Package');

		//add multiple package postages togather
		if( $num_package > 1){
    		foreach ($package_postages as $each_rate ) {

    			//transfer rates information
    			$package_rates[] = $this->filter_rates( $each_rate);
    		}

    		//add packages rate togather
    		$package_rates = $this->merge_same_rates( $package_rates );

    	}else{

    		//transfer rates information
    		$package_rates = $this->filter_rates( $package_postages);
    	}

    	return $package_rates;
    }

    /**
     * Find common service and add them togather
     * @param  [type] $rates_array [description]
     * @return [type]              [description]
     */
    function merge_same_rates( $rates_array ){

    	$loop = count( $rates_array );
    	$common = $rates_array[0];
    	for ($i=1; $i < $loop ; $i++) {
    		$common = array_intersect_key( $common, $rates_array[ $i ] );
    	}

    	foreach ($common as $sid => $value) {
    		$value['cost'] = 0;
    		foreach ($rates_array as $pid => $rate) {
    			$value['cost'] += $rate[ $sid ]['cost'];
    		}

    		$common[ $sid ] = $value;
    	}

    	return $common;
    }


    function filter_rates( $package_rates ){

    	$prefix = $this->is_international() ? 'i' : 'd';



    	$postages = $this->is_international() ? $package_rates['Service'] : $package_rates['Postage'];

    	$rates = array();

    	//just one result
    	if( isset( $postages['@attributes'] ) ){
    		$tmp[] = $postages;

    		$postages = $tmp;
    	}

    	foreach ( $postages as $postage ) {

    		$service_id = $this->is_international() ? $postage['@attributes']['ID'] : $postage['@attributes']['CLASSID'];

    		$cost = $this->is_international()? $postage['Postage'] : $postage['Rate'];

    		$label = $this->is_international()? $postage['SvcDescription'] : $postage['MailService'];



    		if( strpos( $label, 'First-Class Mail') !== false ){

    			if( strpos( $label, 'Parcel') !== false )
    				$service_id .= 'p';
    			elseif ( strpos( $label, 'Large Envelope') !== false )
    				$service_id .= 'l';
    			elseif( strpos( $label, 'Stamped Letter') !== false )
    				$service_id .= 's';
    			elseif( strpos( $label, 'Postcards') !== false && strpos( $label, 'Large Postcards') == false )
    				$service_id .= 'pc';
    		}


    		$rates[ $prefix . $service_id ] = array(
    			'cost' => $cost,
    			'id' => $prefix . $service_id,
    			'label' =>  strip_tags(htmlspecialchars_decode($label)),
    		);
    	}

    	return $rates;
    }



	/**
	 * Output flat box section
	 */
	public function generate_flat_box_html() {
        ob_start();
		?>
        <tr valign="top" id="flat_box_setting">
            <th scope="row" class="titledesc"><?php _e( 'Flat Box Sizes', 'extensionworks' ); ?></th>
            <td class="forminp">
                <style type="text/css">
                    .flat_box_form td, .shipping_services td {
                        vertical-align: middle;
                        padding: 4px 7px;
                    }
                    .flat_box_form td input {
                        margin-right: 4px;
                    }
                    .flat_box_form .check-column {
                        vertical-align: middle;
                        text-align: left;
                        padding: 0 7px;
                    }
                </style>
                <table class="flat_box_form widefat">
                    <thead>
                        <tr>
                            <th class="check-column"><input type="checkbox" /><span>Select Box</span></th>
							<th><?php _e( 'Outer Length', 'extensionworks' ); ?></th>
							<th><?php _e( 'Outer Width', 'extensionworks' ); ?></th>
							<th><?php _e( 'Outer Height', 'extensionworks' ); ?></th>
							<th><?php _e( 'Inner Length', 'extensionworks' ); ?></th>
							<th><?php _e( 'Inner Width', 'extensionworks' ); ?></th>
							<th><?php _e( 'Inner Height', 'extensionworks' ); ?></th>
							<th><?php _e( 'Box Weight', 'extensionworks' ); ?></th>
							<th><?php _e( 'Max Weight', 'extensionworks' ); ?></th>
							<th><?php _e( 'Label', 'extensionworks' ); ?></th>
							<th><?php _e( 'Weight Based', 'extensionworks' ); ?></th>
							<th><?php _e( 'Letter', 'extensionworks' ); ?></th>
                        </tr>
                    </thead>
                    <tfoot>

                    </tfoot>
                    <tbody id="flat_boxes">
                        <?php
					        if ( $this->flat_boxes ) {
					            foreach ( $this->flat_boxes as $key => $box ) {
						?>
                                    <tr>
                                        <td class="check-column"><input type="checkbox" name="flat_boxes_is_enable[<?php echo $key; ?>]" <?php checked( isset( $box['is_enable'] ) && $box['is_enable'] == true, true ); ?> /></td>
                                        <td><input type="text" size="5" name="flat_boxes_outer_length[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_length'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_outer_width[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_width'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_outer_height[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_height'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_inner_length[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_length'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_inner_width[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_width'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_inner_height[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_height'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_box_weight[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['box_weight'] ); ?>" /><?php echo $this->weight_unit; ?></td>
                                        <td><input type="text" size="5" name="flat_boxes_max_weight[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['max_weight'] ); ?>" /><?php echo $this->weight_unit; ?></td>
                                        <td><input type="text" size="25" name="flat_boxes_label[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['label'] ); ?>" /></td>
                                        <td><input type="checkbox" name="flat_boxes_is_weight_based[<?php echo $key; ?>]" <?php checked( isset( $box['is_weight_based'] ) && $box['is_weight_based'] == true, true ); ?> /></td>
                                        <td><input type="checkbox" name="flat_boxes_is_letter[<?php echo $key; ?>]" <?php checked( isset( $box['is_letter'] ) && $box['is_letter'] == true, true ); ?> /></td>
                                    </tr>
                                    <?php
					            }
					        }
						?>
                    </tbody>
                </table>
                <script type="text/javascript">

                    jQuery(window).load(function(){

                    	 jQuery('#woocommerce_us_ps_enable_flat_rate_box').change(function(){

                            if ( jQuery(this).val() !== 'no' )
                                jQuery('#flat_box_setting').show();
                            else
                                jQuery('#flat_box_setting').hide();

                        }).change();
                    });

                </script>
            </td>
        </tr>
        <?php
        return ob_get_clean();
    }


    /**
     * Validate flat box post data
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
	public function validate_flat_box_field( $key ) {

        $boxes = array();

        if ( isset( $_POST['flat_boxes_outer_length'] ) ) {
			$boxes_is_enable       = isset( $_POST['flat_boxes_is_enable'] ) ? $_POST['flat_boxes_is_enable'] : array();
			$boxes_outer_length    = $_POST['flat_boxes_outer_length'];
			$boxes_outer_width     = $_POST['flat_boxes_outer_width'];
			$boxes_outer_height    = $_POST['flat_boxes_outer_height'];
			$boxes_inner_length    = $_POST['flat_boxes_inner_length'];
			$boxes_inner_width     = $_POST['flat_boxes_inner_width'];
			$boxes_inner_height    = $_POST['flat_boxes_inner_height'];
			$boxes_box_weight      = $_POST['flat_boxes_box_weight'];
			$boxes_max_weight      = $_POST['flat_boxes_max_weight'];
			$boxes_label           = $_POST['flat_boxes_label'];
			$boxes_is_weight_based = isset( $_POST['flat_boxes_is_weight_based'] ) ? $_POST['flat_boxes_is_weight_based'] : array();
			$boxes_is_letter       = isset( $_POST['flat_boxes_is_letter'] ) ? $_POST['flat_boxes_is_letter'] : array();

            foreach ($boxes_outer_length as $key => $value) {
            	$boxes[$key] = array(
						'label'           => $boxes_label[$key] == '' ? $boxes_outer_length[ $key ] .'*' . $boxes_outer_width[ $key ] .'*' .$boxes_outer_height[ $key ] : $boxes_label[$key],
						'outer_length'    => floatval( $boxes_outer_length[ $key ] ),
						'outer_width'     => floatval( $boxes_outer_width[ $key ] ),
						'outer_height'    => floatval( $boxes_outer_height[ $key ] ),
						'inner_length'    => floatval( $boxes_inner_length[ $key ] ),
						'inner_width'     => floatval( $boxes_inner_width[ $key ] ),
						'inner_height'    => floatval( $boxes_inner_height[ $key ] ),
						'box_weight'      => floatval( $boxes_box_weight[ $key ] ),
						'max_weight'      => floatval( $boxes_max_weight[ $key ] ),
						'is_weight_based' => isset( $boxes_is_weight_based[ $key ] ) ? true : false,
						'is_letter'       => isset( $boxes_is_letter[ $key ] ) ? true : false,
						'is_enable'       => isset( $boxes_is_enable[$key] ) ? true: false
                    );
            }

        }

        return $boxes;
    }

}
