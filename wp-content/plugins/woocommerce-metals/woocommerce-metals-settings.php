<?php

/**

COPYRIGHT (c) 2013, 2014, 2015, 2016 - IgniteWoo.com - ALL RIGHTS RESERVED

*/
	
class IgniteWoo_Metals_Settings extends WC_Integration {

	function __construct() {
		global $user_ID;
		
		$this->id = 'ignitewoo_metals';

		$this->method_title = __( 'Precious Metals', 'ignitewoo_metals' );
		
		$this->method_description = sprintf( __( 'Be sure to read the <a href="%s" target="_blank">documentation</a> for details about feeds and settings.', 'ignitewoo_metals' ), 'http://ignitewoo.com/ignitewoo-software-documentation/' );
		
		$this->method_description2 = sprintf( __( 'WooCommerce Precious Metals includes open access to the precious metals feed from IgniteWoo.com. Your access is valid as long as your product license key is valid. To enable this feed choose IgniteWoo in the Feed Source settings below.', 'ignitewoo_metals' ), 'http://ignitewoo.com/ignitewoo-software-documentation/' );

		$this->init_form_fields();

		$this->init_settings();

		//add_action( 'woocommerce_update_options_integration_' . $this->id , array( &$this, 'process_admin_options') );
		add_action( 'woocommerce_update_options_integration_ignitewoo_metals', array( $this, 'process_admin_options') );
		
		add_action( 'wp_ajax_ign_hide_metal_feed_notice', array( &$this, 'ign_hide_metal_feed_notice' ) );
		
		$test = get_user_meta( $user_ID, 'precious_metal_new_feed' );
		
		if ( empty( $test ) ) { 
			add_action( 'admin_notices', array( &$this, 'admin_notices' ) );
		}

	}

	function ign_hide_metal_feed_notice() { 
		global $user_ID;
		update_user_meta( $user_ID, 'precious_metal_new_feed', 1 );
	}
	
	function admin_notices() { 
		?>
		<style>
		.padding_it { padding-left: 10px; }
		</style>
		<script>
		jQuery( document ).ready( function($) { 
			$( '.hide_metal_feed_notice' ).click( function() { 
				$.post( ajaxurl, { action: 'ign_hide_metal_feed_notice' }, function( data ) {
					$( '.hide_metal_feed_notice' ).closest( 'div' ).fadeOut( 'slow' );
				})
			})
			
		})
		</script>
		<div class="updated fade" id="message"><p><strong><?php echo sprintf( __( 'WooCommerce Precious Metals has a new free metal price feed! <a href="">Review the settings</a> to see details.</strong><span class="padding_it"></span> [ <a href="#" class="hide_metal_feed_notice">Hide this</a> ]', 'ignitewoo_metals' ), admin_url( 'admin.php?page=wc-settings&tab=integration&section=ignitewoo_metals' ) )?></p></div>
		<?php
	}
	
	function init_form_fields() {

		$tz = get_option( 'timezone_string', false );
		
		if ( !$tz ) 
			$tz = ' GMT ' . get_option( 'gmt_offset', false );

		$this->form_fields = array(
				
				'feed_source' => array(
					'title' 	=> __( 'Feed Source','ignitewoo_metals' ),
					'label'		=> 'secons',
					'description' 	=> __( "Choose the source of your XML metals feed. The IgniteWoo metals feed is free for users with a valid product license key",'ignitewoo_metals' ),
					'type' 		=> 'select',
					'desc_tip' 	=> false,
					'default' 	=> 'xmlcharts',
					'options'	=> array(
							'ign' => __( 'IgniteWoo', 'ignitewoo_metals' ),
							'goldfeed' => __( 'Gold-feed.com', 'ignitewoo_metals' ),
							'xmlcharts' => __( 'XMLCharts.com', 'ignitewoo_metals' ),
							),
				),
				'api_key' => array(
					'title' 	=> __( 'IgniteWoo License Key','ignitewoo_metals' ),
					'label'		=> 'secons',
					'description' 	=> __( "<strong style='color:#cf0000'>REQUIRED</strong>: Enter the license key you received from the purchase of this plugin from IgniteWoo.com",'ignitewoo_metals' ),
					'type' 		=> 'text',
					'desc_tip' 	=> false,
					'default' 	=> '',
					
				),
				'price_type' => array(
					'title' 	=> __( 'Price Type','ignitewoo_metals' ),
					'label'		=> '',
					'description' 	=> __( 'Choose the price type. "Ask" is typically used as the spot price.','ignitewoo_metals' ),
					'type' 		=> 'select',
					'desc_tip' 	=> false,
					'default' 	=> 'ask',
					'options'	=> array(
							'bid' => __( 'Bid', 'ignitewoo_metals' ),
							'ask' => __( 'Ask', 'ignitewoo_metals' ),
							),
				),
				'xml_url' => array(  
					'title' 	=> __( 'XML Feed URL','ignitewoo_metals' ),
					'label'		=> 'URL',
					'description' 	=> __( 'The URL to your precious metals XML feed from <a href="http://www.xmlcharts.com/precious-metals.html" target="_blank">XMLCharts.com</a> or Gold-feed.com.<br/><br/> Note that the free XMLCharts.com feed ONLY UPDATES PRICES ONCE PER DAY, and use of the feed requires that you insert links back to XMLCharts.com in your site. <a href="http://www.xmlcharts.com/terms-of-service.html" target="_blank">Read their Terms of Service</a> for more information. Use the free feed, or buy a subscription to their feed to avoid having to place links on your site<br><br>If you leave the URL empty the plugin will use the free feed: http://www.xmlcharts.com/cache/precious-metals.php','ignitewoo_metals' ),
					'type' 		=> 'text',
					'desc_tip' 	=> false,
					'css'		=> 'width:80%',
					'default' 	=> 'http://www.xmlcharts.com/cache/precious-metals.php'
				),
				'update_interval' => array(  
					'title' 	=> __( 'Update Interval','ignitewoo_metals' ),
					'label'		=> 'secons',
					'description' 	=> __( "Number of seconds to wait between downloading metal prices and updating all of your metal-related products. Minimum value is 30. If you set the value lower then the plugin will assume 30 seconds.",'ignitewoo_metals' ),
					'type' 		=> 'number',
					'custom_attributes'	=> array( 'min' => 30 ),
					'css'		=> 'width: 100px',
					'desc_tip' 	=> true,
					'default' 	=> '7200'
				),
				/*
				'single_product_page_price_update_interval' => array(  
					'title' 	=> __( 'Auto-refresh price display','ignitewoo_metals' ),
					'label'		=> 'secons',
					'description' 	=> __( "EXPERIMENTAL FEATURE - THIS MAY NOT WORK ON YOUR SITE THEME: When a shopper is viewing an individual product page automatically update the product price seen on the page ever X number of seconds. WARNING: Do not set this lower than 30 as it could cause your server to overload! Leave this empty to disable this feature.",'ignitewoo_metals' ),
					'type' 		=> 'number',
					'custom_attributes'	=> array( 'min' => 30 ),
					'css'		=> 'width: 100px',
					'desc_tip' 	=> true,
					'default' 	=> ''
				),
				*/
				'product_page_price_display' => array(  
					'title' 	=> __( 'Spot Price Display Type','ignitewoo_metals' ),
					'label'		=> 'Troy Ounce',
					'description' 	=> __( 'When editing a product the plugin displays current spot prices in grams by default. If you prefer to see the price in Troy ounces check this box.','ignitewoo_metals' ),
					'type' 		=> 'checkbox',
					'desc_tip' 	=> true,
					'default' 	=> 'no'
				),
				'cancel_orders' => array(  
					'title' 	=> __( 'Cancel Unpaid Orders','ignitewoo_metals' ),
					'label'		=> 'minutes',
					'description' 	=> __( 'Cancel all unpaid orders after this many minutes. Leave this blank to never expire unpaid orders. This plugin considers unpaid orders to be all orders that do not have an order status of Refunded, Cancelled, On-Hold, Processing, or Completed', 'ignitewoo_metals' ),
					'type' 		=> 'number',
					'css'		=> 'width:100px',
					'desc_tip' 	=> true,
					'default' 	=> '30'
				),
				'close_store' => array(  
					'title' 	=> __( 'Close Store After Hours','ignitewoo_metals' ),
					'label'		=> 'Enable',
					'description' 	=> sprintf( __( "Enabling this option prevents everyone from accessing pages related to WooCommerce after market hours. London markets are open Monday through Friday, 10.30am to 3.00pm GMT time. Current site time is %s %s, current GMT time is %s",'ignitewoo_metals' ), current_time( 'mysql' ), $tz, current_time( 'mysql', true ) ),
					'type' 		=> 'checkbox',
					'desc_tip' 	=> false,
					'default' 	=> 'no'
				),
				
		);

	}


	function admin_options() {
		global $woocommerce, $wpdb;

		$sql = 'select ID, post_title from ' . $wpdb->posts . ' where post_type = "page" and post_status = "publish" ';
		
		$pages = $wpdb->get_results( $sql, ARRAY_A );
		
		if ( !$pages )
			$pages = array();

		$this->settings = get_option( $this->plugin_id . $this->id . '_settings' );

		$gold_cat = isset( $this->settings['gold_category'] ) ? $this->settings['gold_category'] : '';
		
		$silver_cat = isset( $this->settings['silver_category'] ) ? $this->settings['silver_category'] : '';
		
		$platinum_cat = isset( $this->settings['platinum_category'] ) ? $this->settings['platinum_category'] : '';
		
		$palladium_cat = isset( $this->settings['palladium_category'] ) ? $this->settings['palladium_category'] : '';

		?>
		<script>
		jQuery( document ).ready( function( $ ) { 

			$( '#woocommerce_ignitewoo_metals_feed_source' ).change( function() { 

				if ( 'ign' == $( '#woocommerce_ignitewoo_metals_feed_source' ).val() ) {
				
					$( '#woocommerce_ignitewoo_metals_xml_url' ).closest( 'tr' ).hide( 'fast' );
					$( '#woocommerce_ignitewoo_metals_api_key' ).closest( 'tr' ).show( 'fast' );
					$( '#woocommerce_ignitewoo_metals_price_type' ).closest( 'tr' ).show( 'fast' );
					$( '#woocommerce_ignitewoo_metals_price_type' ).show( 'fast' );
					
				} else if ( 'goldfeed' == $( '#woocommerce_ignitewoo_metals_feed_source' ).val() ) { 
				
					$( '#woocommerce_ignitewoo_metals_price_type' ).closest( 'tr' ).show( 'fast' );
					$( '#woocommerce_ignitewoo_metals_price_type' ).show( 'fast' );
					$( '#woocommerce_ignitewoo_metals_api_key' ).closest( 'tr' ).hide( 'fast' );
					$( '#woocommerce_ignitewoo_metals_xml_url' ).closest( 'tr' ).show( 'fast' );
					
				} else {
				
					$( '#woocommerce_ignitewoo_metals_api_key' ).closest( 'tr' ).hide( 'fast' );
					$( '#woocommerce_ignitewoo_metals_price_type' ).closest( 'tr' ).hide( 'fast' );
					$( '#woocommerce_ignitewoo_metals_price_type' ).hide( 'fast' );
					$( '#woocommerce_ignitewoo_metals_xml_url' ).closest( 'tr' ).show( 'fast' );
				}
				
			})
			
			$( '#woocommerce_ignitewoo_metals_feed_source' ).trigger( 'change' );
		
		})
		</script>
		<table  class="form-table">

			<p><?php echo $this->method_description; ?></p>
			
			<div class="updated fade" id="message"><p><?php echo $this->method_description2; ?></p></div>
			
			<?php 
			$gold = get_option( 'ign_gold_price' );
			if ( empty( $gold ) )
				$gold = __( 'Not updated yet', 'ignitewoo_metals' );
				
			$silver = get_option( 'ign_silver_price' );
			if ( empty( $silver ) )
				$silver = __( 'Not updated yet', 'ignitewoo_metals' );
				
			$plat = get_option( 'ign_platinum_price' );
			if ( empty( $plat ) )
				$plat = __( 'Not updated yet', 'ignitewoo_metals' );
				
			$pall = get_option( 'ign_palladium_price' );
			if ( empty( $pall ) )
				$pall = __( 'Not updated yet', 'ignitewoo_metals' );
			
			$spots = array();
			$spots['gold'] = array( 'label' => __( 'Gold', 'ignitewoo_metals' ), 'amount' => $gold );
			$spots['silver'] = array( 'label' => __( 'Silver', 'ignitewoo_metals' ), 'amount' => $silver );
			$spots['platinum'] = array( 'label' => __( 'Platinum', 'ignitewoo_metals' ), 'amount' => $plat );
			$spots['palladium'] = array( 'label' => __( 'Palladium', 'ignitewoo_metals' ), 'amount' => $pall );
			
			if ( ( isset( $this->settings['product_page_price_display'] ) && !empty( $this->settings['product_page_price_display'] ) ) && 'yes' == $this->settings['product_page_price_display'] ) {
				foreach( $spots as $metal => $vals ) {
					if ( floatval( $spots[ $metal ]['amount'] ) > 0 )
						$spots[ $metal ]['amount'] = $spots[ $metal ]['amount'] * 31.1034768;
				}
			}
			
			$out = array();
			foreach( $spots as $metal => $vals ) {
				$out[] = $spots[ $metal ]['label'] . ': ' . $spots[ $metal ]['amount'];
			}
			$spots = implode( ' | ', $out );
			
			
			?>
			<div style="border: 1px solid #ddd;padding: 5px 10px;border-radius:4px; background:#fff">
				
				<?php 
				if ( ( isset( $this->settings['product_page_price_display'] ) && !empty( $this->settings['product_page_price_display'] ) ) && 'yes' == $this->settings['product_page_price_display'] )
					_e('Current spot prices per troy ounce from your selected feed:', 'ignitewoo_metals');
				else
					_e('Current spot prices per gram from your selected feed:', 'ignitewoo_metals');
				?>
				&nbsp;
				<?php echo $spots ?>
			
			</div>
			
			<?php 
				//parent::admin_options(); 
				$this->generate_settings_html(); 
			?>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Store Closed Redirect', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( "When the store is closed empty the shopper's cart and redirect them to this page if they try to access any pages related to WooCommerce", 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<?php
							$pid = isset( $this->settings['page_id'] ) ? $this->settings['page_id'] : null;
							$args = array( 'selected' => $pid ); 
						?>
						<?php wp_dropdown_pages( $args ); ?>
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Checkout Timeout', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( "When set, a shopper has this many minutes to complete the checkout process before their cart times out and is automatically emptied. Leave this empty to disable the timeout.", 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<input type="text" name="checkout_timeout" value="<?php echo isset( $this->settings['checkout_timeout'] ) ? $this->settings['checkout_timeout'] : ''; ?>" style="width:50px">
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Checkout Timeout Redirect', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( "If the shopper's checkout timeout occurs redirect them to this page", 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<?php 
							$pid = isset( $this->settings['checkout_redirect_to'] ) ? $this->settings['checkout_redirect_to'] : null;
							$args = array( 'selected' => $pid  ); 
						?>
						<?php 
							ob_start();
							
							wp_dropdown_pages( $args );
							
							$pages = ob_get_clean();
							
							
							
							$pages = str_replace( 'page_id', 'checkout_redirect_to', $pages );
							
							echo $pages;
						?>
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Gold Category', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Selected the category that contains gold products', 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<select name="<?php echo $this->id ?>_gold_category">
						<?php $this->product_dropdown_cats(0 , $gold_cat ) ?>
						</select>
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Silver Category', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Selected the category that contains gold products', 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<select name="<?php echo $this->id ?>_silver_category">
						<?php $this->product_dropdown_cats( 0 , $silver_cat ) ?>
						</select>
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Platinum Category', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Selected the category that contains gold products', 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<select name="<?php echo $this->id ?>_platinum_category">
						<?php $this->product_dropdown_cats( 0, $platinum_cat ) ?>
						</select>
					</fieldset>
				</td>
			</tr>
			
			<tr valign="top">
				<th class="titledesc" scope="row">
					<label for="woocommerce_ignitewoo_metals_update_interval"><?php _e( 'Palladium Category', 'ignitewoo_metals' ) ?></label>
					<img width="16" height="16" src="http://localhost/wp5/wp-content/plugins/woocommerce/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Selected the category that contains gold products', 'ignitewoo_metals' ) ?>">
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"></legend>
						<select name="<?php echo $this->id ?>_palladium_category">
						<?php $this->product_dropdown_cats( 0, $palladium_cat ) ?>
						</select>
					</fieldset>
				</td>
			</tr>
		</table>		
		<!-- Section -->
		<div><input type="hidden" name="section" value="<?php echo $this->id; ?>" /></div>
		
		<?php
	}


	function product_dropdown_cats( $currentcat = 0, $currentparent = 0, $parent = 0, $level = 0, $categories = 0 ) {
		
		if ( !$categories )
			$categories = get_categories( array('hide_empty' => false, 'taxonomy' => 'product_cat' ) );

		if ( $categories ) {
			foreach ( $categories as $category ) {
				if ( $currentcat != $category->term_id && $parent == $category->parent) {
					$pad = str_repeat( '&#8211; ', $level );
					$category->name = esc_html( $category->name );
					echo "\n\t<option value='$category->term_id'";
					if ( $currentparent == $category->term_id )
						echo " selected='selected'";
					echo ">$pad$category->name</option>";
					$this->product_dropdown_cats( $currentcat, $currentparent, $category->term_id, $level +1, $categories );
				}
			}
			
		} else {
			return false;
		}
		
	}
	
	public function process_admin_options() {

		if ( version_compare( WC_VERSION, '2.6.', '<' ) ) {
		
			$this->validate_settings_fields();

			if ( count( $this->errors ) > 0 ) {
		
				$this->display_errors();
			
				return false;
			}
			
		
			delete_transient( 'ign_update_valid' );
				
			parent::process_admin_options();

			$this->sanitized_fields[ 'gold_category' ] = $_POST[ $this->id . '_gold_category' ];
			
			$this->sanitized_fields[ 'silver_category' ] = $_POST[ $this->id . '_silver_category' ];
			
			$this->sanitized_fields[ 'platinum_category' ] = $_POST[ $this->id . '_platinum_category' ];
			
			$this->sanitized_fields[ 'palladium_category' ] = $_POST[ $this->id . '_palladium_category' ];
			
			$this->sanitized_fields[ 'page_id' ] = $_POST[ 'page_id' ];
			
			$this->sanitized_fields[ 'checkout_timeout' ] = $_POST[ 'checkout_timeout' ];
			
			$this->sanitized_fields[ 'checkout_redirect_to' ] = $_POST[ 'checkout_redirect_to' ];

			update_option( $this->plugin_id . $this->id . '_settings', apply_filters( 'woocommerce_settings_api_sanitized_fields_' . $this->id, $this->sanitized_fields ) );

			return true;
			
		} else { 
		
			delete_transient( 'ign_update_valid' );
				
			parent::process_admin_options();
			
			$this->sanitized_fields = get_option( 'woocommerce_ignitewoo_metals_settings' );

			$this->sanitized_fields[ 'gold_category' ] = $_POST[ $this->id . '_gold_category' ];
			
			$this->sanitized_fields[ 'silver_category' ] = $_POST[ $this->id . '_silver_category' ];
			
			$this->sanitized_fields[ 'platinum_category' ] = $_POST[ $this->id . '_platinum_category' ];
			
			$this->sanitized_fields[ 'palladium_category' ] = $_POST[ $this->id . '_palladium_category' ];
			
			$this->sanitized_fields[ 'page_id' ] = $_POST[ 'page_id' ];
			
			$this->sanitized_fields[ 'checkout_timeout' ] = $_POST[ 'checkout_timeout' ];
			
			$this->sanitized_fields[ 'checkout_redirect_to' ] = $_POST[ 'checkout_redirect_to' ];

			update_option( $this->plugin_id . $this->id . '_settings', $this->sanitized_fields );
		
		}
		
		
	}
	
}
