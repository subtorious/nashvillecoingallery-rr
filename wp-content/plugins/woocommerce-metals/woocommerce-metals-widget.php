<?php 
/** 

COPYRIGHT (c) 2013, 2014, 2015, 2016 - IgniteWoo.com - ALL RIGHTS RESERVED

*/

class IGN_Metals_Widget extends WP_Widget {

	function __construct() {
	
		parent::__construct(
			'wpb_widget',
			__('WooCommerce Precious Metal Prices', 'ignitewoo_metals'),
			array( 'description' => __( 'Display a list of metal spot prices', 'ignitewoo_metals' ) )
		);
	}

	public function widget( $args, $instance ) {
		global $wpdb;

		$title = apply_filters( 'widget_title', $instance['title'] );
		
		if ( empty( $instance['grams_or_ounces'] ) )
			$instance['grams_or_ounces'] = 'ounces';
		
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		echo '<ul class="wc_precious_metals_prices"><table class="ign_metal_price_table" style="width:100%">';
		
		foreach( array( 'gold', 'silver', 'platinum', 'palladium' ) as $metal ) { 
		
			if ( empty( $instance[ $metal ] ) )
				continue; 
				
			$price = get_option( 'ign_' . $metal . '_price', false );
			
			if ( !empty( $price ) ) { 
			
				if ( 'ounces' == $instance['grams_or_ounces'] )
					$price = round ( ( $price * 31.10 ), 2 );
			
				echo '<tr><td>' . ucwords( $metal ). '</td><td>' . wc_price( $price ) . '</td></tr>';
				
			}
			
		}
			
		echo '</table></ul>';

		echo $args['after_widget'];
	}

	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) )
			$title = $instance[ 'title' ];
		else
			$title = __( 'Spot Prices', 'ignitewoo_metals' );

		if ( !isset( $instance[ 'gold' ] ) )
			$instance[ 'gold' ] = '';
			
		if ( !isset( $instance[ 'silver' ] ) )
			$instance[ 'silver' ] = '';
		
		if ( !isset( $instance[ 'platinum' ] ) )
			$instance[ 'platinum' ] = '';
			
		if ( !isset( $instance[ 'palladium' ] ) )
			$instance[ 'palladium' ] = '';
			
		if ( !isset( $instance[ 'grams_or_ounces' ] ) )
			$instance[ 'grams_or_ounces' ] = '';
		?>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input  id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<?php _e( 'Display prices per troy ounce or per gram', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
			<select name="<?php echo $this->get_field_name( 'grams_or_ounces' ); ?>" id="<?php echo $this->get_field_id( 'grams_or_ounces' ); ?>" >
				<option value="ounces" <?php selected( $instance['grams_or_ounces'], 'ounces', true ) ?>><?php _e( 'Troy ounces', 'ignitewoo_metals' ) ?></option>
				<option value="grams" <?php selected( $instance['grams_or_ounces'], 'grams', true ) ?>><?php _e( 'Grams', 'ignitewoo_metals' ) ?></option>
			</select>
			
			<br/><br/>
		
			<?php _e( 'Select which metal prices to display', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
			<input type="checkbox" name="<?php echo $this->get_field_name( 'gold' ); ?>" value="yes"  id="<?php echo $this->get_field_id( 'gold' ); ?>" <?php checked( $instance['gold'], 'yes', true ) ?>/> <?php _e( 'Gold', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
			<input type="checkbox" name="<?php echo $this->get_field_name( 'silver' ); ?>" value="yes"  id="<?php echo $this->get_field_id( 'silver' ); ?>" <?php checked( $instance['silver'], 'yes', true ) ?>/> <?php _e( 'Silver', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
			<input type="checkbox" name="<?php echo $this->get_field_name( 'platinum' ); ?>" value="yes"  id="<?php echo $this->get_field_id( 'platinum' ); ?>" <?php checked( $instance['platinum'], 'yes', true ) ?>/> <?php _e( 'Platinum', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
			<input type="checkbox" name="<?php echo $this->get_field_name( 'palladium' ); ?>" value="yes"  id="<?php echo $this->get_field_id( 'palladium' ); ?>" <?php checked( $instance['palladium'], 'yes', true ) ?>/> <?php _e( 'Palladium', 'ignitewoo_metals' ) ?>
			<br/><br/>
			
		</p>
		<?php
	}

	public function update( $new_instance, $old_instance ) {
	
		$instance = array();
		
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		
		$instance['grams_or_ounces'] = !empty( $new_instance['grams_or_ounces'] ) ? $new_instance['grams_or_ounces'] : 'ounces';
		
		$instance['gold'] = !empty( $new_instance['gold'] ) ? $new_instance['gold'] : '';
		$instance['silver'] = !empty( $new_instance['silver'] ) ? $new_instance['silver'] : '';
		$instance['platinum'] = !empty( $new_instance['platinum'] ) ? $new_instance['platinum'] : '';
		$instance['palladium'] = !empty( $new_instance['palladium'] ) ? $new_instance['palladium'] : '';
		
		return $instance;
	}
}

add_action( 'widgets_init', create_function( '', 'return register_widget("IGN_Metals_Widget");' ), 1 );
