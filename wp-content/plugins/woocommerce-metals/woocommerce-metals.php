<?php 
/*
Plugin Name: WooCommerce Precious Metals
Plugin URI: http://ignitewoo.com
Description: Retrieves spot prices for metals via XML feed and updates product prices on the fly to reflect spot prices + markup. See the documentation for available template tags and shortcodes.
Author: IgniteWoo.com
Version: 2.6.26
Author URI: http://ignitewoo.com
*/

/** ********************************************************

COPYRIGHT (c) 2016 - IgniteWoo.com - ALL RIGHTS RESERVED

********************************************************** */

/* 
	TODO: Add support for sale prices? 
	
	Sell metal feed access direct without the plugin?
	
*/

class IGN_Metals {
	
	var $metals_message;
	var $supported_currencies_goldfeed;
	var $interval;
	
	function __construct() { 

		$this->supported_currencies_xmlcharts = array( 
			'AUD',
			'BRL',
			'CAD',
			'CHF',
			'CNY',
			'EUR',
			'GBP',
			'INR',
			'JPY',
			'MXN',
			'RUB',
			'USD',
			'ZAR',
		);
		
		$this->supported_currencies_goldfeed = array( 
			'USD',
		);
		
		$this->settings = get_option( 'woocommerce_ignitewoo_metals_settings' );
		
		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );

		add_action( 'init', array( &$this, 'init' ), 15 );
		
		if ( is_admin() ) { 
			require_once( dirname( __FILE__ ) . '/woocommerce-metals-admin.php' );
			$admin = new IGN_Metals_Admin();
		}
		
		require_once( dirname( __FILE__ ) . '/woocommerce-metals-widget.php' );
		
		add_action( 'wp_head', array( &$this, 'checkout_timer' ), 99999 );
		//add_action( 'wp_head', array( &$this, 'maybe_update_price_display' ), 99999 );

		add_action( 'wp', array( &$this, 'maybe_do_shop_closed_redirect' ), 9999 );
		
		add_action( 'wp_ajax_checkout_timeout', array( &$this, 'empty_cart' ) );
		add_action( 'wp_ajax_nopriv_checkout_timeout', array( &$this, 'empty_cart' ) );
		
		//add_action( 'wp_ajax_update_metal_product_page', array( &$this, 'update_metal_product_page' ) );
		//add_action( 'wp_ajax_nopriv_update_metal_product_page', array( &$this, 'update_metal_product_page' ) );
		
		add_shortcode( 'ign_gold_price', 'ign_gold_price' );
		
		add_shortcode( 'ign_silver_price', 'ign_silver_price' );
		
		add_shortcode( 'ign_platinum_price', 'ign_platinum_price' );
		
		add_shortcode( 'ign_palladium_price', 'ign_palladium_price' );
		
		add_filter( 'woocommerce_integrations', array( &$this, 'ignitewoo_metals_settings' ), 10 );
		
	}
	
	function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'ignitewoo_metals' );

		load_textdomain( 'ignitewoo_metals', WP_LANG_DIR.'/woocommerce/ignitewoo_metals-'.$locale.'.mo' );

		$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		load_plugin_textdomain( 'ignitewoo_metals', false, $plugin_rel_path );
	}


	function ignitewoo_metals_settings( $integrations = array() ) {

		require_once( dirname( __FILE__ ) . '/woocommerce-metals-settings.php' );
			
		$integrations[] = 'IgniteWoo_Metals_Settings';
		
		return $integrations;
	}

	function init() { 

		if ( empty( $this->settings ) )
			return;
			
		if ( !empty( $this->settings['cancel_orders'] ) )
			$this->maybe_expire_orders();

		// For debugging purposes, must be logged in to admin area
		if ( isset( $_GET['update_metals'] ) && is_admin() ) {
			@ini_set( 'memory_limit', '1024M' );
			@set_time_limit( 0 );
			$this->update_products();
			die;	
		}

		if ( get_transient( 'ign_update_valid' ) ) 
			return;

		@ini_set( 'memory_limit', '1024M' );
		
		@set_time_limit( 0 );
		
		$this->update_metal_prices();

		$this->update_products();

		$this->interval = isset( $this->settings['update_interval'] ) ? absint( $this->settings['update_interval'] ) : '';

		if ( $this->interval <= 0 ) 
			$this->interval = 30;
			
		// Failsafe, do not allow an interval lower than 30 seconds
		if ( $this->interval <= 30 )
			$this->interval = 30;

		set_transient( 'ign_update_valid', 1, $this->interval ); // timeout for refresh
	}

 	function maybe_expire_orders() { 
		global $wpdb;

		$expire_after = $this->settings['cancel_orders'];
		
		if ( empty( $expire_after ) || '' == $expire_after )
			return; 
		
		$expire_after = $expire_after * 60; // convert minutes to seconds

		if ( version_compare( WOOCOMMERCE_VERSION, '2.2', '>=' ) ) { 
		
			$sql = "
				SELECT ID, post_date_gmt from {$wpdb->posts} as posts WHERE 
				posts.post_type = 'shop_order'
				AND
				posts.post_status NOT IN ('" . implode( "','", array( 'wc-completed', 'wc-processing', 'wc-on-hold', 'wc-refunded', 'wc-cancelled' ) ) . "')
			";
				
			$orders = $wpdb->get_results( $sql );
			
			if ( $orders ) 
			foreach( $orders as $o ) { 
			
				$order_last_modified = strtotime( $o->post_date_gmt );

				$now = current_time( 'timestamp', true ); // GMT

				if ( ( $now - $order_last_modified ) > $expire_after )
					$wpdb->query( 'update ' . $wpdb->posts . ' set post_status = "wc-cancelled" where ID = ' . $o->ID );
					
			}
			
		} else { 
			
			$sql = "
				SELECT ID, post_date_gmt from {$wpdb->posts} as posts 
				LEFT JOIN {$wpdb->term_relationships} AS rel ON posts.ID = rel.object_ID
				LEFT JOIN {$wpdb->term_taxonomy} AS tax USING( term_taxonomy_id )
				LEFT JOIN {$wpdb->terms} AS term USING( term_id )
				WHERE 
				posts.post_type = 'shop_order'
				AND
				tax.taxonomy = 'shop_order_status'
				AND term.slug NOT IN ('" . implode( "','", array( 'completed', 'processing', 'on-hold', 'refunded', 'cancelled' ) ) . "')
			";
			
			$orders = $wpdb->get_results( $sql );
			
			if ( $orders ) 
			foreach( $orders as $o ) { 
			
				$order_last_modified = strtotime( $o->post_date_gmt );

				$now = current_time( 'timestamp', true ); // GMT

				if ( ( $now - $order_last_modified ) > $expire_after )
					wp_set_post_terms( $o->ID, 'cancelled', 'shop_order_status' );
					
			}
		}	
	} 
	
	function update_metal_prices() { 
	
		$url = isset( $this->settings['xml_url'] ) ? $this->settings['xml_url'] : '';
		
		if ( empty( $url ) )
			return;

		if ( 'ign' == $this->settings['feed_source'] ) {
		
			$this->update_from_ign();
			
			return;
		
		} else if ( 'goldfeed' == $this->settings['feed_source'] ) {
		
			$this->update_from_goldfeed( $url );
		
			return;
			
		} else if ( 'xmlcharts' == $this->settings['feed_source'] ) { 
		
			$this->update_from_xml_charts();
			
			return;
		
		}
	}
	
	function update_from_ign() { 

		$url = 'http://metal-feed.ignitewoo.com/api.php';
		
		$key = $this->settings['api_key']; 
		
		if ( empty( $key ) )
			return;
			
		$url .= '?key=' . $key;

		$args = array(
			'timeout'     => 10,
			'redirection' => 5,
			'httpversion' => '1.0',
			'sslverify'   => false,
		); 

		$res = wp_remote_get( $url, $args );

		if ( !$res || is_wp_error( $res ) )
			return;
			
		if ( !is_array( $res ) || empty( $res['body'] ) )
			return;
		
		$xml = $this->produce_xml_object_tree( $res['body'] );
	
		if ( !$xml ) 
			return;

		$s = '';
		
		if ( !empty( $xml->keyfiletext ) )
			$s = (string)$xml->keyfiletext; 
		
		if ( !empty( $s ) ) {
		
			update_option( 'ign_metals_feed_key_file_empty', (string)$xml->keyfiletext );
			return;
		
		} else { 
			delete_option( 'ign_metals_feed_key_file_empty' );
		}
		
		$s = '';
		
		if ( !empty( $xml->invalidtext ) )
			$s = (string)$xml->invalidtext; 
		
		if ( !empty( $s ) ) {
		
			update_option( '', (string)$xml->invalidtext );
			return;
		
		} else { 
			delete_option( '' );
		}
		
		$s = '';
		
		if ( !empty( $xml->expiredtext ) )
			$s = (string)$xml->expiredtext;
		
		// Expired? Add the message and return
		if ( !empty( $s ) ) {
		
			update_option( 'ign_metals_feed_expired', (string)$xml->expiredtext );
			delete_option( 'ign_metals_feed_expiration_notice' );
			return;
		
		} else { 
			delete_option( 'ign_metals_feed_expired' );
		}

		$s = '';
		
		if ( !empty( $xml->message->messagetext ) )
			$s = (string)$xml->message->messagetext;
		
		// Expiring soon? Add a message and unset the message, then processing the feed
		if ( !empty( $s ) ) {

			update_option( 'ign_metals_feed_expiration_notice', (string)$xml->message->messagetext );
			delete_option( 'ign_metals_feed_expired' );
			unset( $xml->message );
			
		} else { 
			delete_option( 'ign_metals_feed_expiration_notice' );
		}

		$store_currency = get_option( 'woocommerce_currency', false );

		if ( !$store_currency ) 
			return;

		$price_type = $this->settings['price_type'];

		if ( empty( $price_type ) )
			$price_type = 'ask';
		
		foreach ( $xml as $currency => $data ) {

			if ( $currency != $store_currency )
				continue;

			foreach( array( 'gold', 'silver', 'platinum', 'palladium' ) as $metal ) { 

				$value = floatval( $data->$metal->$price_type );

				update_option( 'ign_' . $metal . '_price', $value );
				
			}

		}
	}
	
	
	function update_from_xml_charts() { 
		
		$url = $this->settings['xml_url']; 

		if ( empty( $url ) ) 
			$url = 'http://www.xmlcharts.com/cache/precious-metals.php';
		
		$args = array(
			'timeout'     => 10,
			'redirection' => 5,
			'httpversion' => '1.0',
			'sslverify'   => false,
		); 

		$res = wp_remote_get( $url, $args );

		if ( !$res || is_wp_error( $res ) )
			return;
			
		if ( !is_array( $res ) || empty( $res['body'] ) )
			return;
		
		$xml = $this->produce_xml_object_tree( $res['body'] );
	
		if ( !$xml ) 
			return;

		$store_currency = get_option( 'woocommerce_currency', false );
		
		if ( !$store_currency ) 
			return;
		else
			$store_currency = strtolower( $store_currency );
		
		foreach ( $xml->currency as $node ) {

			$attributes = $node->attributes();
			
			$xml_currency = $attributes['access'];
			
			if ( $xml_currency != $store_currency )
				continue;

			foreach ( $node->price as $value ) {

				$attributes = $value->attributes();
				
				$commodity = $attributes['access'];

				$commodity = (string)$commodity[0];

				$digits = floatval( strval( $value ) );

				update_option( 'ign_' . $commodity . '_price', $digits );
				
			}

		}
	} 
	
	function update_from_goldfeed( $url = '' ) { 

		/*    .|..    */
		
		if ( empty( $url ) )
			return;

		$store_currency = get_option( 'woocommerce_currency', false );
			
		$type = !empty( $this->settings['price_type'] ) ? $this->settings['price_type'] : 'bid';
		 
		if ( !$store_currency ) 
			return;
		else
			$store_currency = strtolower( $store_currency );
			
		if ( 'usd' != $store_currency )
			return;

		$args = array(
			'timeout'     => 10,
			'redirection' => 5,
			'httpversion' => '1.0',
			'sslverify'   => false,
		); 

		$res = wp_remote_get( $url, $args );

		if ( !$res || is_wp_error( $res ) )
			return;
			
		if ( !is_array( $res ) || empty( $res['body'] ) )
			return;
		
		$xml = $this->produce_xml_object_tree( $res['body'] );

		if ( !$xml ) 
			return;

		if ( false === strpos( $url, 'xmlgold_usd.php' ) ) {
			$type = '';
			$alt = false;
		} else { 
			$alt = true;
		}

		if ( 'bid' == $type ) 
			$type = '';

		if ( true === $alt ) { 
				
			foreach( array( 'gold', 'silver', 'platinum', 'palladium' ) as $metal ) { 
				
				$types[ $metal ] = $metal . $type;
			}
			
			foreach( $types as $metal => $typ ) { 

				$price = floatval( $xml->$typ->price );

				update_option( 'ign_' . $metal . '_price', $price );
				
			}

			return;
		}	

		foreach ( $xml as $commodity => $node ) {

			$price = (string)$node->price;
			
			update_option( 'ign_' . $commodity . '_price', $price );

		}

	}

	function produce_xml_object_tree( $raw_xml ) {
		
		libxml_use_internal_errors( true );
		
		try {
		
			$xmltree = new SimpleXMLElement( $raw_xml );
			
		} catch (Exception $e) {
		
			return false; 
			
		}
		
		/*
			// Something went wrong.
			$error_message = 'SimpleXMLElement threw an exception.';
		
			foreach(libxml_get_errors() as $error_line) {
				$error_message .= "\t" . $error_line->message;
			}
			
			trigger_error($error_message);
			
			return false;
		}
		*/
		
		return $xmltree;
	}

	// Runs when after a editing a product the product settings are saved.
	// If the product is variable then this function will be called once per variation. 
	// Test for terms being set so that it returns fast if no terms are found, variations do not have terms, only the parent product does.
	function update_single_product( $pid = NULL, $type = 'simple' ) { 
		global $wpdb;

		if ( !$this->settings )
			$this->settings = get_option( 'woocommerce_ignitewoo_metals_settings' );
		
		if ( empty( $this->settings ) )
			return;

		if ( empty( $pid ) )
			return;
			
		$metal_weight = get_post_meta( $pid, '_metal_weight', true );
		
		$metal_weight_unit = get_post_meta( $pid, '_metal_weight_unit', true );
		
		$metal_purity = get_post_meta( $pid, '_metal_purity', true );

		if ( !empty( $metal_purity ) ) 
			$metal_purity = str_replace( array( 'g', 's', 'pl', 'pa' ), '', $metal_purity );
			
		if ( !empty( $metal_weight_unit ) && !empty( $metal_weight ) )
			$this->weight_unit = $metal_weight_unit; 
		if ( empty( $this->weight_unit ) )
			$this->weight_unit = get_option( 'woocommerce_weight_unit', false ); 
		if ( !$this->weight_unit ) 
			return;

		// Prices are per gram
		$gold_price = get_option( 'ign_gold_price', false );

		$silver_price = get_option( 'ign_silver_price', false );
		
		$platinum_price = get_option( 'ign_platinum_price', false );
		
		$palladium_price = get_option( 'ign_palladium_price', false );

		$terms = get_the_terms( $pid, 'product_cat' );
		
		if ( empty( $terms ) || is_wp_error( $terms ) )
			return;
			
		$metal = '';

		// Find the first metal term for the product, if any. Then process that and continue to the next product
		foreach( $terms as $term ) {
	
			if ( $this->post_is_in_descendant_category( $this->settings['gold_category'], $term->term_id ) ) { 

				$metal = $gold_price; 

			} else if ( $this->post_is_in_descendant_category( $this->settings['silver_category'], $term->term_id ) ) { 

				$metal = $silver_price;

			} else if ( $this->post_is_in_descendant_category( $this->settings['platinum_category'], $term->term_id ) ) { 
				
				$metal = $platinum_price; 
				
			} else if ( $this->post_is_in_descendant_category( $this->settings['palladium_category'], $term->term_id ) ) { 

				$metal = $palladium_price;

			}
	
			//if ( !empty( $metal ) && !empty( $metal_purity ) )
			//	$metal = $metal * $metal_purity;

			if ( !empty( $metal ) && !empty( $metal_weight_unit ) && !empty( $metal_weight ) )
				$this->add_markup( $pid, $metal, $metal_purity, $metal_weight );
			else if ( !empty( $metal ) )
				$this->add_markup( $pid, $metal, $metal_purity, '' );

			if ( !empty( $metal ) )
				break;

		}
	}
	
	function update_products() { 
		global $wpdb;
		
		if ( !$this->settings )
			$this->settings = get_option( 'woocommerce_ignitewoo_metals_settings' );
		
		$term_ids = array( 
				$this->settings['gold_category'],
				$this->settings['silver_category'],
				$this->settings['platinum_category'],
				$this->settings['palladium_category']
			);
			
		foreach( $term_ids as $tid ) { 
		
			$descendants = get_term_children( $tid, 'product_cat' );
			
			if ( !empty( $descendants ) )
				$term_ids = array_merge( $term_ids, $descendants );
		}

		/*
		$args = array( 
				'post_type' => 'product',
				'posts_per_page' => 9999999,
				'order_by' => 'id',
				'order' => 'desc',
				'tax_query' => array( 
							array( 
								'taxonomy' => 'product_cat',
								'field' => 'id',
								'terms' => $term_ids,
							)
				)
		);

		if ( $pid ) 
			$args['p'] = $pid;

		$items = new WP_Query( $args );
		*/

		$sql = "
			SELECT ID FROM {$wpdb->posts}
			LEFT JOIN {$wpdb->term_relationships} ON
			({$wpdb->posts}.ID = {$wpdb->term_relationships}.object_id)
			LEFT JOIN $wpdb->term_taxonomy ON
			({$wpdb->term_relationships}.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
			WHERE
			{$wpdb->term_taxonomy}.taxonomy = 'product_cat'
			AND {$wpdb->term_taxonomy}.term_id IN (" . implode( ',', $term_ids ) . ")
			AND {$wpdb->posts}.post_type = 'product'
			AND {$wpdb->posts}.post_status IN ( 'publish', 'draft' )
			ORDER BY {$wpdb->posts}.ID DESC LIMIT 0, 9999999
		";

		$items = $wpdb->get_results( $sql, ARRAY_A );

		$items = array_filter( $items );

		if ( empty( $items ) ) 
			return;
			
		/* 
		Check for a partial update that did not complete - PHP timed out probably. 
		If the file exists and has a product ID number in it then the update did not complete.
		So in that case pick up where it left off by removing all previous IDs from the list.
		Note that the ID in this file will only contain the parent product ID in the case of variable products.
		*/
		$last_id = null;
		
		if ( file_exists( dirname( __FILE__ ) . '/metal-update-progress.dat' ) ) { 
			
			$f = @fopen( dirname( __FILE__ ) . '/metal-update-progress.dat', 'r' );
			
			if ( $f ) { 
				$last_id = @fread( $f, 128 );
				fclose( $f );
				if ( $last_id )
					$last_id = absint( trim( $last_id ) );
			}
			
			if ( $last_id <= 0 )
				$last_id = null;
		}
		
		if ( null !== $last_id ) {
			foreach( $items as $key => $item ) {
				if ( (int)$item['ID'] < (int)$last_id ) 
					unset( $items[ $key ] );
			}
		
		}

		//if ( !$items || count( $items->posts ) <= 0 ) 
		//	return;
		
		/*
		if ( $pid ) { 
			$metal_weight = get_post_meta( $pid, '_metal_weight', true );
			$metal_weight_unit = get_post_meta( $pid, '_metal_weight_unit', true );
		}
		*/
		
		//if ( !empty( $metal_weight_unit ) && !empty( $metal_weight ) )
		//	$this->weight_unit = $metal_weight_unit; 
		if ( empty( $this->weight_unit ) )
			$this->weight_unit = get_option( 'woocommerce_weight_unit', false ); 
		if ( !$this->weight_unit ) 
			return;

		// Prices are per gram
		$gold_price = get_option( 'ign_gold_price', false );

		$silver_price = get_option( 'ign_silver_price', false );
		
		$platinum_price = get_option( 'ign_platinum_price', false );
		
		$palladium_price = get_option( 'ign_palladium_price', false );

		$now = current_time( 'timestamp', false ); 
		
		//foreach( $items->posts as $key => $item ) {
		foreach( $items as $key => $item ) { 

			$f = @fopen( dirname( __FILE__ ) . '/metal-update-progress.dat', 'w' );
			
			if ( $f ) { 
				fwrite( $f, $item['ID'] );
				fclose( $f );
			}
			
			//$terms = get_the_terms( $item->ID, 'product_cat' );
			$terms = get_the_terms( $item['ID'], 'product_cat' );
			
			if ( empty( $terms ) || is_wp_error( $terms ) )
				return;
			
			//if ( empty( $pid ) ) { 
			
				//$metal_weight = get_post_meta( $item->ID, '_metal_weight', true );
				//$metal_weight_unit = get_post_meta( $item->ID, '_metal_weight_unit', true );
				$metal_weight = get_post_meta( $item['ID'], '_metal_weight', true );
				$metal_weight_unit = get_post_meta( $item['ID'], '_metal_weight_unit', true );
				$metal_purity = get_post_meta( $item['ID'], '_metal_purity', true );
			
				// if the weight is set for the metal then the unit must also be set
				if ( !empty( $metal_weight_unit ) && !empty( $metal_weight ) )
					$this->weight_unit = $metal_weight_unit; 
				//else 
				//	$weight_unit = null;
			//}

			$metal = '';
			$metal_price = '';
			
			// Find the first metal term for the product, if any. Then process that and continue to the next product
			foreach( $terms as $term ) {
		
				if ( $this->post_is_in_descendant_category( $this->settings['gold_category'], $term->term_id ) ) { 

					$metal_price = $gold_price; 

				} else if ( $this->post_is_in_descendant_category( $this->settings['silver_category'], $term->term_id ) ) { 

					$metal_price = $silver_price;

				} else if ( $this->post_is_in_descendant_category( $this->settings['platinum_category'], $term->term_id ) ) { 
					
					$metal_price = $platinum_price; 
					
				} else if ( $this->post_is_in_descendant_category( $this->settings['palladium_category'], $term->term_id ) ) { 

					$metal_price = $palladium_price;

				}

				//if ( !empty( $metal ) && !empty( $metal_purity ) )
				//	$metal = $metal * $metal_purity;
				
				
				//if ( !empty( $metal ) && !empty( $metal_weight_unit ) && !empty( $metal_weight ) )
				//	$this->add_markup( $item->ID, $metal, $metal_weight );
				//else if ( !empty( $metal ) )
				//	$this->add_markup( $item->ID, $metal, '' );

				if ( !empty( $metal_price ) && !empty( $metal_weight_unit ) && !empty( $metal_weight ) ) {
					$this->add_markup( $item['ID'], $metal_price, $metal_purity, $metal_weight );
					break;
				} else if ( !empty( $metal_price ) ) { 
					$this->add_markup( $item['ID'], $metal_price, $metal_purity, '' );
					break;
				}
					
				if ( !empty( $metal_price ) )
					break;

			}
			
			unset( $items[ $key ] );

		}
		
		// Delete the progress file if it exists
		if ( file_exists( dirname( __FILE__ ) . '/metal-update-progress.dat' ) )
			@unlink( dirname( __FILE__ ) . '/metal-update-progress.dat' ); 

	}

	function wc_clean( $var ) {
		return is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
	}
		
	function wc_format_decimal( $number, $dp = false, $dps = false, $trim_zeros = false ) {
		$locale   = localeconv();
		$decimals = array( $dps, $locale['decimal_point'], $locale['mon_decimal_point'] );

		// Remove locale from string
		if ( ! is_float( $number ) ) {
			$number = $this->wc_clean( str_replace( $decimals, '.', $number ) );
		}

		if ( $dp !== false ) {
			$dp     = intval( $dp == "" ? wc_get_price_decimals() : $dp );
			$number = number_format( floatval( $number ), $dp, '.', '' );

		// DP is false - don't use number format, just return a string in our format
		} elseif ( is_float( $number ) ) {
			$number = $this->wc_clean( str_replace( $decimals, '.', strval( $number ) ) );
		}

		if ( $trim_zeros && strstr( $number, '.' ) ) {
			$number = rtrim( rtrim( $number, '0' ), '.' );
		}

		return $number;
	}
	
	function format_price( $price ) { 
		if ( empty( $this->decimals ) ) 
			$this->decimals = get_option( 'woocommerce_price_num_decimals' );
			
		if ( empty( $this->decimal_separator ) ) 
			$this->decimal_separator = wp_specialchars_decode( stripslashes( get_option( 'woocommerce_price_decimal_sep' ) ), ENT_QUOTES );
		/*
		if ( empty( $this->thousand_separator ) ) 
			$this->thousand_separator = wp_specialchars_decode( stripslashes( get_option( 'woocommerce_price_thousand_sep' ) ), ENT_QUOTES );
		*/	
		if ( empty( $this->decimals ) )
			$this->decimals = 2;
		
		return $this->wc_format_decimal( $price, $this->decimals, $this->decimal_separator );
		//return number_format( $price, $this->decimals, $this->decimal_separator, $this->thousand_separator );
	}
	
	// If $weight is set then it's the parent weight - for variable products
	function add_markup( $product_id, $metal, $metal_purity = '', $weight = '' ) { 

//if ( 1000 != $product_id ) 
//	return;
	
		$_product = get_product( $product_id );

		// Prices are stored in grams, 
		// so how many GRAMS per unit of measure based on the setting detected? 
		if ( 'kg' == $this->weight_unit )
			$multiplier = 1000;
		else if ( 'g' == $this->weight_unit )
			$multiplier = 1;
		else if ( 'lbs' == $this->weight_unit )
			$multiplier = 453.488691744;
		else if ( 'oz' == $this->weight_unit ) // troy ounce = 31.1034768 grams
			$multiplier = 31.1034768;
		else if ( 'pw' == $this->weight_unit ) // penny weight = 1.55517 per gram
			$multiplier = 1.55517;

		if ( $_product->is_type( 'simple' ) || $_product->is_type( 'variation' ) ) {

			if ( empty( $weight ) )
				$weight = get_post_meta( $_product->id, '_weight', true );

			if ( empty( $weight ) ) {
				unset( $_product );
				return;
			}
			
			if ( empty( $metal_purity ) ) 
				$metal_purity = get_post_meta( $_product->id, '_metal_purity', true );
			
			if ( !empty( $metal_purity ) ) 
				$metal_purity = str_replace( array( 'g', 's', 'pl', 'pa' ), '', $metal_purity );
			
			if ( !empty( $metal ) && !empty( $metal_purity ) )
				$metal = $metal * $metal_purity;
				
			// Convert weight to grams
			$weight = $weight * $multiplier;

			$markup = get_post_meta( $_product->id, '_markup_rate', true );
			$markup2 = get_post_meta( $_product->id, '_markup_rate_2', true );
			
			$markup = apply_filters( 'ign_metal_product_markup', $markup );
			$markup2 = apply_filters( 'ign_metal_product_markup_2', $markup2 );

			// Percentage
			if ( false !== strpos( $markup, '%' ) ) {
			
				$markup = str_replace( '%', '', $markup );

				// Adjusted price for the weight of the item - this is the metal price based on the weight
				$adjusted = $weight * $metal;
				
				$cost = ( $adjusted * $markup ) + $adjusted;
				
			// Flat amount
			} else { 

				$cost = floatval( $markup ) + ( $weight * $metal );
				
			}

			if ( !empty( $markup2 ) ) { 
				if ( false !== strpos( $markup2, '%' ) ) {
					$markup2 = str_replace( '%', '', $markup2 );
					$cost = ( $cost * $markup2 ) + $cost;
				} else { 
					$cost = $cost + floatval( $markup2 );
				}
			}

			$cost = $this->format_price( $cost );
			//$cost = money_format( '%!i', $cost );
			
			$cost = apply_filters( 'ign_metal_product_cost', $cost, $_product, $markup, $metal, $weight, $markup, $markup2 );
			
			$min_cost = get_post_meta( $product_id, '_metal_min_price', true );
		
			if ( isset( $min_cost ) && !empty( $min_cost ) && floatval( $min_cost ) > $cost )
				$cost = $this->format_price( $min_cost );
				//$cost = money_format( '%!i', $min_cost );

			update_post_meta( $product_id, '_price', $cost );
			
			update_post_meta( $product_id, '_regular_price', $cost );

			if ( $_product->is_type( 'variation' ) )
				$_product->variable_product_sync( $_product->post->ID );
			
			unset( $_product );
						
		} else if ( $_product->is_type( 'variable' ) ) { 

			$children = $_product->get_children();

			if ( empty( $children ) ) {
				unset( $_product );
				return;
			}

			// parent product weight set? if not try to get it
			if ( empty( $weight ) )
				$parent_weight = get_post_meta( $_product->id, '_weight', true ); 
			else
				$parent_weight = $weight;
				
			foreach( $children as $child_id ) { 

				$child_weight = get_post_meta( $child_id, '_metal_weight', true );

				$child_weight_unit = get_post_meta( $child_id, '_metal_weight_unit', true );
				
				if ( !empty( $child_weight ) && !empty( $child_weight_unit ) ) { 
				
					if ( 'kgs' == $child_weight_unit )
						$multiplier = 1000;
					else if ( 'g' == $child_weight_unit )
						$multiplier = 1;
					else if ( 'lbs' == $child_weight_unit )
						$multiplier = 453.488691744;
					else if ( 'oz' == $child_weight_unit )
						$multiplier = 31.1034768;
						
					$weight = $child_weight;
				
				} else { 

					if ( 'kgs' == $this->weight_unit )
						$multiplier = 1000;
					else if ( 'g' == $this->weight_unit )
						$multiplier = 1;
					else if ( 'lbs' == $this->weight_unit )
						$multiplier = 453.488691744;
					else if ( 'oz' == $this->weight_unit )
						$multiplier = 31.1034768;
						
					$weight = get_post_meta( $child_id, '_weight', true );
					
					if ( empty( $weight ) )
						$weight = $parent_weight;
				
				}

				if ( empty( $child_weight ) )
					$child_weight = $parent_weight;

				if ( !empty( $child_weight ) ) 
					$weight = $child_weight;

				//if ( empty( $child_weight ) )
				//	continue;

				$metal_purity = get_post_meta( $child_id, '_metal_purity', true );
				
				if ( !empty( $metal_purity ) ) 
					$metal_purity = str_replace( array( 'g', 's', 'pl', 'pa' ), '', $metal_purity );

				if ( !empty( $metal ) && !empty( $metal_purity ) )
					$metal_calced = $metal * $metal_purity;
				else 
					$metal_calced = $metal;

				$weight = $weight * $multiplier;

				$markup = get_post_meta( $child_id, '_markup_rate', true );
				$markup2 = get_post_meta( $_product->id, '_markup_rate_2', true );
				
				$markup = apply_filters( 'ign_metal_product_markup', $markup );
				$markup2 = apply_filters( 'ign_metal_product_markup', $markup2 );

				if ( false !== strpos( $markup, '%' ) ) {
				
					$markup = floatval( str_replace( '%', '', $markup ) );

					// Adjusted price for the weight of the item - this is the metal price based on the weight
					$adjusted = $weight * $metal_calced;

					$cost = ( $adjusted * $markup ) + $adjusted;

				} else { 
					
					$cost = floatval( $markup ) + ( $weight * $metal_calced );
				}

				if ( !empty( $markup2 ) ) { 
					if ( false !== strpos( $markup2, '%' ) ) {
						$markup2 = str_replace( '%', '', $markup2 );
						$cost = ( $cost * $markup2 ) + $cost;
					} else { 
						$cost = $cost + floatval( $markup2 );
					}
				}
				
				$cost = $this->format_price( $cost );
				//$cost = money_format( '%!i', $cost );
					
				$cost = apply_filters( 'ign_metal_product_cost', $cost, $_product, $markup, $metal_calced, $weight, $markup, $markup2 );

				$min_cost = get_post_meta( $child_id, '_metal_min_price', true );
				
				if ( isset( $min_cost ) && !empty( $min_cost ) && floatval( $min_cost ) > $cost )
					$cost = $this->format_price( $min_cost );
					//$cost = money_format( '%!i', $min_cost );
					
				update_post_meta( $child_id, '_price', $cost );
			
				update_post_meta( $child_id, '_regular_price', $cost );
	
			}
		
			//$_product->sync( $_product->id );
		
			unset( $_product );
			
		}
		
		//if ( method_exists( $_product, 'variable_product_sync' ) && isset( $_product->post->ID ) )
		//	$_product->variable_product_sync( $_product->post->ID );
				
		//unset( $_product );
	}
	
	function post_is_in_descendant_category( $cats, $term_id ) {
	
		if ( !is_array( $cats ) && $cats == $term_id )
			return true;
			
		foreach ( (array) $cats as $cat ) {
		
			$descendants = get_term_children( (int) $cat, 'product_cat' );

			if ( $descendants && in_array( $term_id, $descendants ) )
				return true;
		}
		
		return false;
	}

	// Check if time is outside of market hours
	function is_market_open() {

		$day = date( 'l',  current_time( 'timestamp', true ) );
		
		$hour = date( 'G', current_time( 'timestamp', true ) );
		
		$minute = date( 'i', current_time( 'timestamp', true ) );

		// Closed Sat & Sun 
		if (
			$day == 'Saturday' ||
			$day == 'Sunday' 
		)
			return false;

		// Closed every weekday from 3pm to 10am GMT
		if (
			$hour < 10 ||
			( $hour >= 15 && $minute >= 0 ) 
		)
			return false;
		else
			return true;
	}

	function maybe_do_shop_closed_redirect( $request = '' ) {
		global $post, $wpdb, $woocommerce;
		
		if ( is_admin() ) 
			return;
			
		if ( empty( $this->settings ) || empty( $this->settings['close_store'] ) || 'no' == $this->settings['close_store'] ) 
			return;

		if ( $this->is_market_open() )
			return;
			
		if ( $post->ID == $this->settings['page_id'] )
			return;
		
		$sql = 'select option_value from ' . $wpdb->options . ' where option_name like "woocommerce_%_page_id"';
		
		$pages = $wpdb->get_results( $sql, ARRAY_A );

		if ( $pages )
		foreach( $pages as $p )
			$wc_pages[] = $p['option_value'];

		// Redirect if on a shop page
		if ( 
			( 'product' == $post->post_type ) || ( in_array( $post->ID, $wc_pages ) )
			
		) {

			if ( isset( $woocommerce->cart ) && !empty( $woocommerce->cart ) )
				$woocommerce->cart->empty_cart();
				
			wp_redirect( get_permalink( $this->settings['page_id'] ) );

			die;

		}
	}
	
	function empty_cart() { 
		global $woocommerce;
		
		if ( empty( $woocommerce->cart ) ) 
			return;
			
		$woocommerce->cart->empty_cart();
		
		die;
	}
	
	function checkout_timer() { 
		global $woocommerce;
		
		if ( empty( $this->settings['checkout_timeout'] ) )
			return;
			
		if ( empty( $this->settings['checkout_redirect_to'] ) )
			return;

		$items = $woocommerce->cart->cart_contents;

		if ( empty( $items ) )
			return;
			
		$has_metal = false;
		 
		foreach( $items as $item_key => $item ) { 
		
			$terms = get_the_terms( $item['product_id'], 'product_cat' );

			if ( !empty( $terms ) && !is_wp_error( $terms ) )
			foreach( $terms as $term ) {
					
				if ( $this->post_is_in_descendant_category( $this->settings['gold_category'], $term->term_id ) ) { 

					$has_metal = true;

				} else if ( $this->post_is_in_descendant_category( $this->settings['silver_category'], $term->term_id ) ) { 

					$has_metal = true;

				} else if ( $this->post_is_in_descendant_category( $this->settings['platinum_category'], $term->term_id ) ) { 
					
					$has_metal = true;
					
				} else if ( $this->post_is_in_descendant_category( $this->settings['palladium_category'], $term->term_id ) ) { 

					$has_metal = true;

				}

			}

		}

		if ( !$has_metal ) 
			return;
			
		$timeout_redirect_to = get_permalink( $this->settings['checkout_redirect_to'] );
		
		// Convert minutes to milliseconds
		$timeout = absint( $this->settings['checkout_timeout'] ) * 60 * 1000; 
		
		?>
		<script> 

		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		
		jQuery( document ).ready( function() { 
		
			setTimeout( function() {
			
				jQuery.post( ajaxurl, { action: 'checkout_timeout' }, function() { 
					window.location = '<?php echo $timeout_redirect_to ?>';
				})
			
			}, <?php echo $timeout ?> )
		
		})

		</script>
		
		<?php
	}
	
	/*
	function maybe_update_price_display() { 
		global $post;

		if ( 'product' !== $post->post_type && 'variable_product' !== $post->post_type )
			return;
			
		$n = wp_create_nonce( 'update_metal_product_page' );
		
		$interval = isset( $this->settings['single_product_page_price_update_interval'] ) ? absint( $this->settings['single_product_page_price_update_interval'] ) : '';
		
		$interval = trim( $interval ); 
		
		if ( empty( $interval ) )
			return;

		if ( $interval < 30 ) 
			$interval = 30; 
//$interval = 5;
		$interval = $interval * 1000;
		
		?>
		<script>
		function ign_metal_price_poll() {
			jQuery.post( '<?php echo admin_url( 'admin-ajax.php' ) ?>', { action: 'update_metal_product_page', pid: <?php echo $post->ID ?>, n: 
			'<?php echo $n ?>' } ).done( function(data) {
				if ( 'error' == data )
					return;
				
				try { 
					var res = jQuery.parseJSON( data )
					jQuery( '.product .summary p.price' ).html( res.price );
					
					console.log( res.variations );
					if ( null !== res.variations || res.variations.length > 0 ) {

						jQuery( '.product .variations_form' ).removeAttr( 'data-product_variations' );
						jQuery( '.product .variations_form' ).attr( 'data-product_variations', res.variations );
						jQuery.wc_variations_form.trigger( 'reload_product_variations' );
						jQuery( '.single_variation_wrap' ).remove();
					}
				} catch( e ) { 
				
				}
				
					
			}).always( function() { 
				setTimeout(ign_metal_price_poll, <?php echo $interval ?> ); 
			});
		}
		setTimeout( function() {
			ign_metal_price_poll();
		}, <?php echo $interval ?> );
		</script>
		<?php
	}
	
	function update_metal_product_page() { 
		global $product, $post;
		
		if ( empty( $_POST['n'] ) || !wp_verify_nonce( $_POST['n'], 'update_metal_product_page' ) )
			die( 'error' );
		
		if ( empty( $_POST['pid'] ) )
			die( 'error' );
			
		if ( function_exists( 'wc_get_product' ) )
			$product = wc_get_product( absint( $_POST['pid'] ) );
		else 
			$product = get_product( absint( $_POST['pid'] ) );
			
		if ( empty( $product ) || is_wp_error( $product ) )
			die( 'error' );
		
		$post = $product->post;
		
		$price = $product->get_price_html();
		
		$variations = null;
		
		if ( $product->is_type( 'variable' ) ) {
			$variations = $product->get_available_variations();
			$variations = json_encode( $variations );
	
		}
		
		die( json_encode( array( 'price' => $price, 'variations' => $variations ) ) );
	}
	*/
}

// global $ign_metals; 
// $ign_metals = new IGN_Metals();

// ============== Template tags ===============
function ign_gold_price( $attrs = '' ) { 
	
	$attrs = shortcode_atts( array(
		'weight' => 'grams',
	), $attrs );

	$price = get_option( 'ign_gold_price' );
	
	if ( 'ounce' == $attrs['weight'] )
		$price = round ( ( $price * 31.10 ), 2 );
		
	return number_format( $price, 2, '.', '' );
	
}
function ign_silver_price( $attrs = '' ) { 
	$attrs = shortcode_atts( array(
		'weight' => 'grams',
	), $attrs );

	$price = get_option( 'ign_silver_price' );
	
	if ( 'ounce' == $attrs['weight'] || 'ounces' == $attrs['weight'] )
		$price = round ( ( $price * 31.10 ), 2 );
		
	return number_format( $price, 2, '.', '' );
}
function ign_platinum_price( $attrs = '' ) { 
	$attrs = shortcode_atts( array(
		'weight' => 'grams',
	), $attrs );

	$price = get_option( 'ign_platinum_price' );
	
	if ( 'ounce' == $attrs['weight'] || 'ounces' == $attrs['weight'] )
		$price = round ( ( $price * 31.10 ), 2 );
		
	return number_format( $price, 2, '.', '' );

}
function ign_palladium_price( $attrs = '' ) { 
	$attrs = shortcode_atts( array(
		'weight' => 'grams',
	), $attrs );

	$price = get_option( 'ign_palladium_price' );
	
	if ( 'ounce' == $attrs['weight'] || 'ounces' == $attrs['weight'] )
		$price = round ( ( $price * 31.10 ), 2 );
		
	return number_format( $price, 2, '.', '' );
}



// END




























if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );


ignitewoo_queue_update( plugin_basename( __FILE__ ), 'b8a2b3280c0984345b4e0017daa7416d', '3899' );
