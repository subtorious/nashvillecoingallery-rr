<?php 
/** 

COPYRIGHT (c) 2013, 2014, 2015, 2016 - IgniteWoo.com - ALL RIGHTS RESERVED

*/

class IGN_Metals_Admin { 

	var $settings; 
	
	function __construct() { 
	
		add_action( 'init', array( &$this, 'init' ), 20 );
		
		add_action( 'admin_notices', array( &$this, 'add_metals_message' ) );
		
		add_action( 'woocommerce_product_options_pricing', array( &$this, 'markup_field' ) );
		
		add_action( 'woocommerce_product_after_variable_attributes', array( &$this, 'after_variable_attributes' ), 20, 3 );
			
		// after WC does it own save settings work
		add_action( 'woocommerce_process_product_meta', array( &$this, 'meta_boxes_save' ), 9999 );
		add_action( 'woocommerce_ajax_save_product_variations', array( &$this, 'ajax_process_product_meta_variable' ), 5 );
	
		add_action( 'plugin_row_meta', array( &$this, 'add_meta_links' ), 10, 2 );

		add_action ( 'admin_footer', array( &$this, 'admin_head' ), 999 ); 
		
	}

	function init() {
		global $ign_metals; 
		
		$this->settings = $ign_metals->settings; 
		
		$currency = get_option( 'woocommerce_currency', false );
		
		$source = isset( $ign_metals->settings['feed_source'] ) ? $ign_metals->settings['feed_source'] : '';
		
		if ( empty( $source ) ) 
			return;
			
		if ( 'goldfeed' == $source ) {

			if ( !in_array( $currency, $ign_metals->supported_currencies_goldfeed ) ) 
				add_action('admin_notices', array( &$this, 'admin_notice' ), 999 );
				
		} else if ( 'ign' == $source ) {
		
			// Do nothing
			
		} else { 
		
			if ( !in_array( $currency, $ign_metals->supported_currencies_xmlcharts ) ) 
				add_action('admin_notices', array( &$this, 'admin_notice' ), 999 );
		}
	}
	
	function admin_notice() { 
		global $ign_metals; 
		
		if ( 'goldfeed' == $ign_metals->settings['feed_source'] )
			$curr = $ign_metals->supported_currencies_goldfeed;
		else
			$curr = $ign_metals->supported_currencies_xmlcharts;
	
		?>
		
		<div class="error" style="font-weight: bold">
			<p>
				<?php _e( 'Your store currency is not compatible with the WooCommerce Metal Price Updater', 'ignitewoo_metals' ) ?>
			</p>
			<p>
				<?php 
					echo sprintf( __( 'Supported currencies for %s include', 'ignitewoo_metals' ), strtoupper( $ign_metals->settings['feed_source'] ) );
					echo ' ' . implode( ', ' , $curr );

				?>
			</p>
			
		</div>
		
		<?php
	}
	
	function admin_head() { 
		global $typenow, $post, $ign_metals;

		if ( empty( $_GET['post'] ) || empty( $post ) || 'product' !== $post->post_type )
			return;

		$terms = get_the_terms( absint( $_GET['post'] ), 'product_cat' );

		if ( !$terms || is_wp_error( $terms ) ) 
			return;

		$is_metal = false;
		
		$metal_cats[] = $this->settings['gold_category'];
		
		$metal_cats[] = $this->settings['silver_category'];
		
		$metal_cats[] = $this->settings['platinum_category'];
		
		$metal_cats[] = $this->settings['palladium_category'];

		foreach( $terms as $t ) 
			if ( $ign_metals->post_is_in_descendant_category( $metal_cats, $t->term_id ) )
				$is_metal = true;
				
		if ( !$is_metal ) 
			return;

		?>
		
		<script>
		jQuery( document ).ready( function() { 
			jQuery( '#_sale_price' ).val( '' );
			jQuery( '._sale_price_field' ).css( 'display', 'none' );
		});
		</script>
		
		<?php 
	}

	function add_metals_message() { 
	
		$msg = get_option( 'ign_metals_feed_key_file_empty' );
			
		if ( !empty( $msg ) ) { 
			?>
			<div class="error fade" id="warning"><p><strong><?php echo $msg ?></strong></p></div>
			<?php

			return;
		
		}
		
		$msg = get_option( 'ign_metals_feed_key_invalid' );
			
		if ( !empty( $msg ) ) { 
			?>
			<div class="error fade" id="warning"><p><strong><?php echo $msg ?></strong></p></div>
			<?php

			return;
		
		}
	
		$msg = get_option( 'ign_metals_feed_expiration_notice' );

		if ( !empty( $msg ) ) { 
			?>
			<div class="updated fade" id="message" style="border-left:4px solid #ffba00"><p><strong><?php echo $msg ?></strong></p></div>
			<?php
			
			return;
		}
		
		$msg = get_option( 'ign_metals_feed_expired' );
			
		if ( !empty( $msg ) ) { 
			?>
			<div class="error fade" id="warning"><p><strong><?php echo $msg ?></p></strong></div>
			<?php

			return;
		
		}
		
	}

	function add_meta_links( $links, $file ) {

		$plugin_path = trailingslashit( dirname(__FILE__) );
		
		$plugin_dir = trailingslashit( basename( $plugin_path ) );

		if ( $file == $plugin_dir . 'woocommerce-metals.php' ) {

			$links[]= '<a href="http://ignitewoo.com/ignitewoo-software-documentation/" target="_blank"><strong>' . __( 'Documentation', 'ignitewoo_metals' ) . '</strong></a>';

			$links[]= '<a href="http://ignitewoo.com/contact-us" target="_blank"><strong>' . __( 'Support', 'ignitewoo_metals' ) . '</strong></a>';
			
			$links[]= '<a href="http://ignitewoo.com target="_blank"">' . __( 'View Add-ons / Upgrades' ) . '</a>';
			
			$links[]= '<img style="height:24px;vertical-align:bottom;margin-left:12px" src="http://ignitewoo.com/wp-content/uploads/2012/02/ignitewoo-bar-black-bg-rounded2-300x86.png">';

		}
		
		return $links;
	}

	function markup_field( $post_id = 0, $loop = 0, $variation_data = null, $variation = null ) { 
		global $post, $ign_metals; 
		
		$spots = array();
		$spots['gold'] = array( 'label' => __( 'Gold', 'ignitewoo_metals' ), 'amount' => get_option( 'ign_gold_price' ) );
		$spots['silver'] = array( 'label' => __( 'Silver', 'ignitewoo_metals' ), 'amount' => get_option( 'ign_silver_price' ) );
		$spots['platinum'] = array( 'label' => __( 'Platinum', 'ignitewoo_metals' ), 'amount' => get_option( 'ign_platinum_price' ) );
		$spots['palladium'] = array( 'label' => __( 'Palladium', 'ignitewoo_metals' ), 'amount' => get_option( 'ign_palladium_price' ) );
		
		if ( ( isset( $ign_metals->settings['product_page_price_display'] ) && !empty( $ign_metals->settings['product_page_price_display'] ) ) && 'yes' == $ign_metals->settings['product_page_price_display'] ) {
			foreach( $spots as $metal => $vals ) {
				if ( floatval( $spots[ $metal ]['amount'] ) > 0 )
					$spots[ $metal ]['amount'] = $spots[ $metal ]['amount'] * 31.1034768;
			}
		}
		
		if ( !empty( $post_id ) )
			$pid = $post_id;
		else
			$pid = $post->ID;

		do_action( 'ign_metals_add_fields_before', $pid );
		
		$amount = null;
		
		$amount = get_post_meta( $pid, '_markup_rate', true );

		$args = array( 
			'id' => '_markup_rate[' . $loop . ']', 
			'class' => 'wc_input_markup short', 
			'label' => __('Markup Amount', 'ignitewoo_metals'),
			'description' => ' ' . __( 'The metal markup amount is added to the spot price. Enter a decimal number (without a currency symbol) for fixed amount of markup, or enter a number and percent symbol for a percentage markup.<br>Examples: 45 - for $45 markup; 1.45% for 145% markup.', 'ignitewoo_metals' ),
			'desc_tip' => true,
			'style'	=> 'width:100px; margin: 0px 5px!important',
		);
		
		if ( $amount >= 0 )
			$args['value'] = $amount;
		
		woocommerce_wp_text_input( $args );
		
		$amount2 = get_post_meta( $pid, '_markup_rate_2', true );

		$args = array( 
			'id' => '_markup_rate_2[' . $loop . ']', 
			'class' => 'wc_input_markup short', 
			'label' => __('Markup Amount 2', 'ignitewoo_metals'),
			'description' => ' ' . __( 'OPTIONAL. This second metal markup amount is added to the total price AFTER the first markup amount is added to the spot price. Enter a decimal number (without a currency symbol) for fixed amount of markup, or enter a number and percent symbol for a percentage markup.<br>Examples: 45 - for $45 markup; 1.45% for 145% markup.', 'ignitewoo_metals' ),
			'desc_tip' => true,
			'style'	=> 'width:100px; margin: 0px 5px!important',
		);
		
		if ( $amount >= 0 )
			$args['value'] = $amount2;
		
		woocommerce_wp_text_input( $args );
		
		do_action( 'ign_metals_add_fields_after_markup', $pid );

		$weight = get_post_meta( $pid, '_metal_weight', true );
		$weight_unit = get_post_meta( $pid, '_metal_weight_unit', true );
		$purity = get_post_meta( $pid, '_metal_purity', true );
		$min = get_post_meta( $pid, '_metal_min_price', true );

		?>
		<p class="form-field dimensions_field" style="margin: 20px 5px!important">
			<label for="product_length"><?php _e('Metal Weight', 'ignitewoo_metals') ?></label>
			<span class="wrap">
				<input type="text" placeholder="<?php _e( 'Optional', 'ignitewoo_metals' ) ?>" value="<?php echo $weight ?>" id="_metal_weight[<?php echo $loop ?>]" name="_metal_weight[<?php echo $loop ?>]" class="wc_input_markup short" style="height:26px !important; width: 100px !important">
				<select name="_metal_weight_unit[<?php echo $loop ?>]" style="height:26px; width:120px !important">
					<option value="oz" <?php selected( $weight_unit, 'oz', true ) ?>><?php _e('oz', 'ignitewoo_metals') ?> </option>
					<option value="g" <?php selected( $weight_unit, 'g', true ) ?> ><?php _e('g', 'ignitewoo_metals') ?> </option>
					<option value="kg" <?php selected( $weight_unit, 'kg', true ) ?> ><?php _e('kg', 'ignitewoo_metals') ?> </option>
					<option value="lbs" <?php selected( $weight_unit, 'lbs', true ) ?>><?php _e('lbs', 'ignitewoo_metals') ?> </option>
					<option value="pw" <?php selected( $weight_unit, 'pw', true ) ?>><?php _e('pennyweight', 'ignitewoo_metals') ?> </option>
					
				</select>
			</span>
			<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'The metal weight. If you leave this empty the plugin will calculate prices based on the shipping weight!', 'ignitewoo_metals' ) ?>">
		</p>
		
		<p class="form-field dimensions_field" style="margin: 20px 5px!important">
			<label for="product_length"><?php _e('Metal Purity', 'ignitewoo_metals') ?></label>
			<span class="wrap">
				<select name="_metal_purity[<?php echo $loop ?>]" style="height:26px; width:170px !important">
					<option value="" <?php selected( $purity, '', true ) ?>></option>
					<optgroup label="<?php _e( 'Gold', 'ignitewoo_metals' ) ?>">
						<option value="g.99999" <?php selected( $purity, 'g.99999', true ) ?>><?php _e('.99999 ( purest )', 'ignitewoo_metals') ?> </option>
						<option value="g.9999" <?php selected( $purity, 'g.9999', true ) ?>><?php _e('.9999', 'ignitewoo_metals') ?> </option>
						<option value="g.999" <?php selected( $purity, 'g.999', true ) ?>><?php _e('.999 (24k)', 'ignitewoo_metals') ?> </option>
						<option value="g.995" <?php selected( $purity, 'g.995', true ) ?>><?php _e('.995', 'ignitewoo_metals') ?> </option>
						<option value="g.990" <?php selected( $purity, 'g.990', true ) ?>><?php _e('.990', 'ignitewoo_metals') ?> </option>
						<option value="g.986" <?php selected( $purity, 'g.996', true ) ?>><?php _e('.986', 'ignitewoo_metals') ?> </option>
						<option value="g.9583" <?php selected( $purity, 'g.9583', true ) ?>><?php _e('.9583 (23k)', 'ignitewoo_metals') ?> </option>
						<option value="g.916" <?php selected( $purity, 'g.916', true ) ?>><?php _e('.916 (22k)', 'ignitewoo_metals') ?> </option>
						<option value="g.900" <?php selected( $purity, 'g.900', true ) ?>><?php _e('.900', 'ignitewoo_metals') ?> </option>
						<option value="g.834" <?php selected( $purity, 'g.834', true ) ?>><?php _e('.934 (20k)', 'ignitewoo_metals') ?> </option>
						<option value="g.750" <?php selected( $purity, 'g.750', true ) ?>><?php _e('.750 (18k)', 'ignitewoo_metals') ?> </option>
						<option value="g.625" <?php selected( $purity, 'g.625', true ) ?>><?php _e('.626 (15k)', 'ignitewoo_metals') ?> </option>
						<option value="g.585" <?php selected( $purity, 'g.585', true ) ?>><?php _e('.585 (14k)', 'ignitewoo_metals') ?> </option>
						<option value="g.4995" <?php selected( $purity, 'g.4995', true ) ?>><?php _e('.4995 (12k)', 'ignitewoo_metals') ?> </option>
						<option value="g.417" <?php selected( $purity, 'g.417', true ) ?>><?php _e('.417 (10k)', 'ignitewoo_metals') ?> </option>
						<option value="g.375" <?php selected( $purity, 'g.375', true ) ?>><?php _e('.375 (9k)', 'ignitewoo_metals') ?> </option>
						<option value="g.333" <?php selected( $purity, 'g.333', true ) ?>><?php _e('.333 (8k)', 'ignitewoo_metals') ?></option>
					</optgroup>
					<optgroup label="<?php _e( 'Silver', 'ignitewoo_metals' ) ?>">
						<option value="s.9999" <?php selected( $purity, 's.9999', true ) ?>><?php _e('.9999 (ultra fine)', 'ignitewoo_metals') ?></option>
						<option value="s.999" <?php selected( $purity, 's.999', true ) ?>><?php _e('.999 (fine)', 'ignitewoo_metals') ?></option>
						<option value="s.980" <?php selected( $purity, 's.980', true ) ?>><?php _e('.980', 'ignitewoo_metals') ?></option>
						<option value="s.958" <?php selected( $purity, 's.958', true ) ?>><?php _e('.958', 'ignitewoo_metals') ?></option>
						<option value="s.950" <?php selected( $purity, 's.950', true ) ?>><?php _e('.950', 'ignitewoo_metals') ?></option>
						<option value="s.925" <?php selected( $purity, 's.925', true ) ?>><?php _e('.925 (sterling)', 'ignitewoo_metals') ?></option>
						<option value="s.917" <?php selected( $purity, 's.917', true ) ?>><?php _e('.917', 'ignitewoo_metals') ?></option>
						<option value="s.900" <?php selected( $purity, 's.900', true ) ?>><?php _e('.900 (90%)', 'ignitewoo_metals') ?></option>
						<option value="s.835" <?php selected( $purity, 's.835', true ) ?>><?php _e('.835', 'ignitewoo_metals') ?></option>
						<option value="s.833" <?php selected( $purity, 's.833', true ) ?>><?php _e('.833', 'ignitewoo_metals') ?></option>
						<option value="s.830" <?php selected( $purity, 's.830', true ) ?>><?php _e('.830', 'ignitewoo_metals') ?></option>
						<option value="s.800" <?php selected( $purity, 's.800', true ) ?>><?php _e('.800', 'ignitewoo_metals') ?></option>
						<option value="s.750" <?php selected( $purity, 's.750', true ) ?>><?php _e('.750', 'ignitewoo_metals') ?></option>
						<option value="s.720" <?php selected( $purity, 's.720', true ) ?>><?php _e('.720', 'ignitewoo_metals') ?></option>
					</optgroup>
					<optgroup label="<?php _e( 'Platinum', 'ignitewoo_metals' ) ?>">
						<option value="pl.9995" <?php selected( $purity, 'pl.9995', true ) ?>><?php _e('.9995', 'ignitewoo_metals') ?></option>
						<option value="pl.999" <?php selected( $purity, 'pl.999', true ) ?>><?php _e('.990', 'ignitewoo_metals') ?></option>
						<option value="pl.950" <?php selected( $purity, 'pl.950', true ) ?>><?php _e('.950', 'ignitewoo_metals') ?></option>
						<option value="pl.925" <?php selected( $purity, 'pl.925', true ) ?>><?php _e('.925', 'ignitewoo_metals') ?></option>
						<option value="pl.900" <?php selected( $purity, 'pl.900', true ) ?>><?php _e('.900 (90%)', 'ignitewoo_metals') ?></option>
						<option value="pl.850" <?php selected( $purity, 'pl.850', true ) ?>><?php _e('.850', 'ignitewoo_metals') ?></option>
						<option value="pl.750" <?php selected( $purity, 'pl.750', true ) ?>><?php _e('.750', 'ignitewoo_metals') ?></option>
					</optgroup>
					<optgroup label="<?php _e( 'Palladium', 'ignitewoo_metals' ) ?>">
						<option value="pa.9995" <?php selected( $purity, 'pa.9995', true ) ?>><?php _e('.9995', 'ignitewoo_metals') ?></option>
						<option value="pa.999" <?php selected( $purity, 'pa.999', true ) ?>><?php _e('.990', 'ignitewoo_metals') ?></option>
						<option value="pa.950" <?php selected( $purity, 'pa.950', true ) ?>><?php _e('.950', 'ignitewoo_metals') ?></option>
						<option value="pa.925" <?php selected( $purity, 'pa.925', true ) ?>><?php _e('.925', 'ignitewoo_metals') ?></option>
						<option value="pa.900" <?php selected( $purity, 'pa.900', true ) ?>><?php _e('.900 (90%)', 'ignitewoo_metals') ?></option>
						<option value="pa.850" <?php selected( $purity, 'pa.850', true ) ?>><?php _e('.850', 'ignitewoo_metals') ?></option>
						<option value="pa.750" <?php selected( $purity, 'pa.750', true ) ?>><?php _e('.750', 'ignitewoo_metals') ?></option>
					</optgroup>
				</select>
			</span>
			<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Optional metal purity. If you set this option then it is used to reduce the spot metal price used for price calculations. See the documentation for help.', 'ignitewoo_metals' ) ?>">
		</p>
		
		<?php do_action( 'ign_metals_add_fields_after' ); ?>

		<p class="form-field dimensions_field" style="margin: 20px 5px!important">
			<label for="product_length"><?php _e('Minimum Price', 'ignitewoo_metals') ?></label>
			<span class="wrap">
				<input type="text" placeholder="<?php _e( 'Optional', 'ignitewoo_metals' ) ?>" value="<?php echo $min ?>" id="_metal_weight[<?php echo $loop ?>]" name="_metal_min_price[<?php echo $loop ?>]" class="wc_input_markup short" style="height:26px !important; width: 100px !important">
			</span>
			<img height="16" width="16" src="<?php echo WC()->plugin_url() ?>/assets/images/help.png" class="help_tip" data-tip="<?php _e( 'Optionally set a minimum price for this product. If the spot metal prices fall to the point that the calculated item cost would be below this minimum amount then the price will be set to this minimum amount. DO NOT include a currency symbol.', 'ignitewoo_metals' ) ?>">
		</p>
		
		<p class="form-field dimensions_field">
			<label for="product_length"><?php _e('Current Spot Prices', 'ignitewoo_metals') ?></label>
			<?php 
			$out = array();
			foreach( $spots as $metal => $vals ) {
				$out[] = $spots[ $metal ]['label'] . ': ' . $spots[ $metal ]['amount'];
			}
			echo implode( ' | ', $out );
			?>
			<br/><em><?php 
			if ( ( isset( $ign_metals->settings['product_page_price_display'] ) && !empty( $ign_metals->settings['product_page_price_display'] ) ) && 'yes' == $ign_metals->settings['product_page_price_display'] )
				_e( '( Spot prices are indicated per troy ounce )', 'ignitewoo_metals' );
			else 
				_e( '( Spot prices are indicated per gram )', 'ignitewoo_metals' );
			?></em>
		</p>

		<?php

	}

	
	function after_variable_attributes( $loop, $variation_data, $variation ) {

		echo '<tr class="ign_metals ign_metals_variation"><td colspan="2">';

		$this->markup_field( $variation->ID, $loop, $variation_data, $variation );

		echo '</td></tr>';
		
	}

	function ajax_process_product_meta_variable( ) {
		global $ign_metals;
		
		if ( empty( $_POST['variable_post_id'] ) ) 
			return;
			
		$max_loop = max( array_keys( $_POST['variable_post_id'] ) );

		for ( $i = 0; $i <= $max_loop; $i++ ) {
		
			$variation_id = isset( $_POST['variable_post_id'][ $i ] ) ? absint( $_POST['variable_post_id'][ $i ] ) : null;

			if ( is_null( $variation_id ) ) 
				continue;	
			
			$rate = !empty( $_POST['_markup_rate'][ $i ] ) ? trim( $_POST['_markup_rate'][ $i ] ) : '';
			
			update_post_meta( $variation_id, '_markup_rate', $rate );
			
			$rate = !empty( $_POST['_markup_rate_2'][ $i ] ) ? trim( $_POST['_markup_rate_2'][ $i ] ) : '';
			
			update_post_meta( $variation_id, '_markup_rate_2', $rate );
			
			$weight = !empty( $_POST['_metal_weight'][ $i ] ) ? floatval( $_POST['_metal_weight'][ $i ] ) : '';

			update_post_meta( $variation_id, '_metal_weight', $weight );

			$weight_unit = !empty( $_POST['_metal_weight_unit'][ $i ] ) ? $_POST['_metal_weight_unit'][ $i ] : '';

			update_post_meta( $variation_id, '_metal_weight_unit', $weight_unit );
			
			$weight_unit = !empty( $_POST['_metal_purity'][ $i ] ) ? $_POST['_metal_purity'][ $i ] : '';

			update_post_meta( $variation_id, '_metal_purity', $weight_unit );
			
			$min = !empty( $_POST['_metal_min_price'][ $i ] ) ? $_POST['_metal_min_price'][ $i ] : '';

			update_post_meta( $variation_id, '_metal_min_price', $min );
		}

		if ( !empty( $_POST['ID'] ) )
			$ign_metals->update_single_product( absint( $_POST['ID'] ) );

	}

	function meta_boxes_save( $post_id, $variation = false ) {
		global $post, $product, $ign_metals;

		if ( $post->post_type != 'product' && $post->post_type != 'shop_order' && $post->post_type != 'shop_coupon' ) 
			return;

		if ( empty( $_POST['_markup_rate'] ) || !is_array( $_POST['_markup_rate'] ) )
			return;
			
		if ( function_exists( 'wc_get_product' ) )
			$product = wc_get_product( $post_id );
		else 
			$product = get_product( $post_id );

		if ( empty( $product ) || is_wp_error( $product ) )
			return;
			
		if ( $product->is_type( 'variable' ) ) {
			$this->ajax_process_product_meta_variable();
			return;
		}
		
		// Only process simple products here. 
		
		if ( !$product->is_type( 'simple' ) )
			return;

		$rate = !empty( $_POST['_markup_rate'][0] ) ? trim( $_POST['_markup_rate'][0] ) : 0;
			
		update_post_meta( $post_id, '_markup_rate', $rate );
		
		$rate = !empty( $_POST['_markup_rate_2'][0] ) ? trim( $_POST['_markup_rate_2'][0] ) : 0;

		update_post_meta( $post_id, '_markup_rate_2', $rate );
		
		$weight = !empty( $_POST['_metal_weight'][ 0 ] ) ? floatval( $_POST['_metal_weight'][ 0 ] ) : '';

		update_post_meta( $post_id, '_metal_weight', $weight );

		$weight_unit = !empty( $_POST['_metal_weight_unit'][ 0 ] ) ? $_POST['_metal_weight_unit'][ 0 ] : '';

		update_post_meta( $post_id, '_metal_weight_unit', $weight_unit );
		
		$weight_unit = !empty( $_POST['_metal_purity'][ 0 ] ) ? $_POST['_metal_purity'][ 0 ] : '';

		update_post_meta( $post_id, '_metal_purity', $weight_unit );
		
		$min = !empty( $_POST['_metal_min_price'][ 0 ] ) ? $_POST['_metal_min_price'][ 0 ] : '';

		update_post_meta( $post_id, '_metal_min_price', $min );

		$ign_metals->update_single_product( $post_id );

		
	}
}
