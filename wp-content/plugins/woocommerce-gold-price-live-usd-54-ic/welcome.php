<?php 
/***************************************************************************
Plugin Name: WooCommerce Gold Price Live
Plugin URI: https://gold-feed.com
Description: Real time precious metals shop and cart pricing for WooCommerce. (&#174;2009-2015 Gold Feed Inc.)
Author: Gold Feed Inc.
Version: 4.0
Author URI: https://gold-feed.com
***************************************************************************/

/***************************************************************************

All Rights Reserved 2015 Gold Feed Inc.

***************************************************************************/

require 'woocommerce-gold-price-live.php';