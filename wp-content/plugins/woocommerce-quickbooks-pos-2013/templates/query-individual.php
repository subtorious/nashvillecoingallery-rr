<?php 
$xml = '<?xml version="1.0"?>
						<?qbposxml version="2.0"?>
							<QBPOSXML>
								<QBPOSXMLMsgsRq onError="continueOnError"> 
									<CustomerQueryRq>
										<EMailFilter>
											<MatchStringCriterion>Equal</MatchStringCriterion>
											<EMail>'.$wc_order->billing_email.'</EMail>
										</EMailFilter>
										<FirstNameFilter>
											<MatchStringCriterion>Equal</MatchStringCriterion>
											<FirstName>'.$wc_order->billing_first_name.'</FirstName>
										</FirstNameFilter>
										<LastNameFilter>
											<MatchStringCriterion>Equal</MatchStringCriterion>
											<LastName>'.$wc_order->billing_last_name.'</LastName>
										</LastNameFilter>
									</CustomerQueryRq>
								</QBPOSXMLMsgsRq>
							</QBPOSXML>';
return $xml;