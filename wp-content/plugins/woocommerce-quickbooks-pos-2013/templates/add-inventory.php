<?php 
//Check for parent
$post = get_post( $ID );
if($post->post_parent && $post->post_parent > 0){
	
	$parent = get_post( $post->post_parent );
};
if($item->get_sku()){
	$xml = '<?xml version="1.0" ?>
			<?qbposxml version="2.0"?>
				<QBPOSXML>
				  <QBPOSXMLMsgsRq onError="continueOnError">
				    <ItemInventoryAddRq requestID="'.$requestID.'">
				      <ItemInventoryAdd>
							<DepartmentListID>'.$dept.'</DepartmentListID>';
						  	if(isset($tax_code)):
								$xml.='<TaxCode>'.$tax_code.'</TaxCode>';
							endif;
							$xml.='
							<Desc1>'.$name.'</Desc1>';
							if($alu !==''):
								$xml.=$alu;
							endif;
							if($upc):
								$xml.=$upc;
							endif;
							$xml.='<ItemType>'.$item_type.'</ItemType>'
				    		.$price1.$price2.'
				    		<OnHandStore01>'.$stock.'</OnHandStore01>
				    	</ItemInventoryAdd>
				    </ItemInventoryAddRq>
				  </QBPOSXMLMsgsRq>
		</QBPOSXML>';
}else{
	$xml = qbpos_get_template_part('query','company', $params );
}
return $xml;