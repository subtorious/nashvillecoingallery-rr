<?php 
$iterator='';
$quickbooks         = new SOD_QuickbooksPOS_Data;
if(!empty($extra)){
	if(array_key_exists('iteratorID',$extra)){
		$iterator='iterator="Continue" iteratorID="'.$extra['iteratorID'].'"';
	}
}else{
	$iterator='iterator="Start"';
}
$xml ='<?xml version="1.0"?>
		<?qbposxml version="2.0"?>
			<QBPOSXML>
				<QBPOSXMLMsgsRq onError="continueOnError">
					<ItemInventoryQueryRq requestID="'.$requestID.'" '.$iterator.'>
						<MaxReturned>' . $quickbooks->settings->max_returned .'</MaxReturned>
						<IncludeRetElement>ListID</IncludeRetElement>
						<IncludeRetElement>ALU</IncludeRetElement>
						<IncludeRetElement>UPC</IncludeRetElement>
						<IncludeRetElement>ItemNumber</IncludeRetElement> 
						<IncludeRetElement>QuantityOnHand</IncludeRetElement>
						<IncludeRetElement>Price1</IncludeRetElement>
						<IncludeRetElement>Price2</IncludeRetElement>
						<IncludeRetElement>Price3</IncludeRetElement>
						<IncludeRetElement>Price4</IncludeRetElement>
						<IncludeRetElement>Price5</IncludeRetElement>
					</ItemInventoryQueryRq>
				</QBPOSXMLMsgsRq>
			</QBPOSXML>';
return $xml;