<?php 
$xml = '<?xml version="1.0"?>
				<?qbposxml version="2.0"?>
				<QBPOSXML>
					<QBPOSXMLMsgsRq onError="continueOnError">
						<CustomerAddRq requestID="' . $requestID . '">
							<CustomerAdd>';
								if(isset($wc_order->billing_company)):
									$xml .='<CompanyName>'.$wc_order->billing_company.'</CompanyName>';
								endif;
								if(isset($wc_order->billing_company)):
									$xml .='<FirstName>'.$wc_order->billing_first_name.'</FirstName>';
								endif;
								if(isset($wc_order->billing_company)):
									$xml .='<LastName>'.$wc_order->billing_last_name.'</LastName>';
								endif;

								if(isset($wc_order->billing_state)):
									if($wc_order->billing_state == "TN"):
										$xml .= '<TaxCategory>Tennessee Sales Tax</TaxCategory>';
									else:
										$xml .= '<TaxCategory>Out-of-state</TaxCategory>';
									endif;
								endif;

								if(isset($wc_order->billing_address_1)):	
									$xml .='<BillAddress>';
										if($wc_order->billing_address_1):
											$xml .='<Street>'.$wc_order->billing_address_1.'</Street>';
										endif;
										if($wc_order->billing_address_2):
											$xml .='<Street2>'.$wc_order->billing_address_2.'</Street2>';
										endif;
										if(isset($wc_order->billing_city)):
											$xml .='<City>'.$wc_order->billing_city.'</City>';
										endif;
										if(isset($wc_order->billing_state)):
											$xml .='<State>'.$wc_order->billing_state.'</State>';
										endif;
										if(isset($wc_order->billing_postcode)):
											$xml .='<PostalCode>'.$wc_order->billing_postcode.'</PostalCode>';
										endif;
										if(isset($wc_order->billing_country)):
											$xml .='<Country>'.$wc_order->billing_country.'</Country>';
										endif;
									$xml .='</BillAddress>';
								endif;
								if(isset($wc_order->billing_phone)):
									$xml .='<Phone>'.$wc_order->billing_phone.'</Phone>';
								endif;
								if(isset($wc_order->billing_email)):
									$xml .='<EMail>'.$wc_order->billing_email.'</EMail>';
								endif;
							$xml .='</CustomerAdd>
						</CustomerAddRq>
					</QBPOSXMLMsgsRq>
				</QBPOSXML>';
return $xml;