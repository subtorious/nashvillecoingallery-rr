<?php

// Subclass of IGN_Metals found in plugins/woocommerce-metals/woocommerce-metals.php.
class HG_Metals extends IGN_Metals 
{	
	public $_bEmailSent_MinPrice_Gold= false;
	public $_bEmailSent_MinPrice_Silver= false;
	public $_bEmailSent_MinPrice_Platinum= false;
	public $_bEmailSent_MinPrice_Palladium= false;
	public $_fPriceChangePercThreshold= false;
	public $_fCC_PaypalMarkup= false;
	const AWAITING_RESPONSE= 'awaiting_response';
	const PROCEED_WITH_UPDATE= 'yes';
	const DONT_PROCEED_WITH_UPDATE= 'no';

	public $_aAddMarkup_ProductIDs= array();


	function update_products()
	{
		// HG_METALS_Update_Running is a locking mechanism to prevent multiple update_products processes from running at the same time. 
		// Return and thus dont update_products if HG_METALS_Update_Running is set.
		if(get_option('HG_METALS_Update_Running'))
	    {
	    	update_option('HG_METALS_Update_Running', 0, false);
	    	syslog(LOG_ERR, 'HG_METALS ALREADY RUNNING.');
	    	return;
	    }

	    // Lock update_products as running.
	    update_option('HG_METALS_Update_Running', 1, false);

	    syslog(LOG_DEBUG, "HG_METALS_Update_Running");

	    // Update markups for shipping classes before updating product prices.
	    $this->update_markups_for_shipping_classes();

	    // Update product prices via the parent classes method which triggers add_markup method of this subclass.
		parent::update_products();
		
		// Unlock update_products so next process will run. 
		update_option('HG_METALS_Update_Running', 0, false);
	}


	function update_markups_for_shipping_classes()
	{
		if(get_option("hg_shipping_class_markup_needs_update") == false)
		{
			return;
		}
		update_option("hg_shipping_class_markup_needs_update", false, false);

		global $wpdb;
		$nShippingClassMarkupCount= (int)get_option('hg_shipping_class_markup_count');
		for($i= 0; $i < $nShippingClassMarkupCount; $i++)
		{
			$termID= get_option("hg_shipping_class_markup_term_id_$i");
			$termTaxonomyID= get_option("hg_shipping_class_markup_term_taxonomy_id_$i");
			if(empty($termID) || empty($termTaxonomyID))
			{
				continue;
			}

			syslog(LOG_DEBUG, "update_markups_for_shipping_classes:: $termID");

			$markupValue= get_term_meta($termID, "hg_shipping_class_markup", true);
			if(empty($markupValue))
			{
				continue;
			}

			$aResults= $wpdb->get_results($wpdb->prepare("SELECT `object_id` 
														  FROM $wpdb->term_relationships 
														  WHERE `term_taxonomy_id` = %d", (int)$termTaxonomyID));
			if(count($aResults) > 0)
			{
				foreach($aResults as $result)
				{
					$productID= $result->object_id;
					if(empty($productID))
					{
						continue;
					}

					update_post_meta($productID, "_markup_rate_2", $markupValue);
				}
			}
		}
	}


	// Overriding the add_markup method to apply update price logic before woo-metals updates the regular price of a product. All of the code in this overriden method is the original except where marked // Horton Group.
	function add_markup( $product_id, $metal, $metal_purity = '', $weight = '' ) 
	{	
		// Horton Group
		// Prevent products from being processed multiple times in the same run. 
		if(isset($this->_aAddMarkup_ProductIDs[$product_id]))
		{
			return;
		}
		$this->_aAddMarkup_ProductIDs[$product_id]= true;
		// END Horton Group



		$_product = get_product( $product_id );

		// Prices are stored in grams, 
		// so how many GRAMS per unit of measure based on the setting detected? 
		if ( 'kg' == $this->weight_unit )
			$multiplier = 1000;
		else if ( 'g' == $this->weight_unit )
			$multiplier = 1;
		else if ( 'lbs' == $this->weight_unit )
			$multiplier = 453.488691744;
		else if ( 'oz' == $this->weight_unit ) // troy ounce = 31.1034768 grams
			$multiplier = 31.1034768;
		else if ( 'pw' == $this->weight_unit ) // penny weight = 1.55517 per gram
			$multiplier = 1.55517;

		if ( $_product->is_type( 'simple' ) || $_product->is_type( 'variation' ) ) {

			if ( empty( $weight ) )
				$weight = get_post_meta( $_product->id, '_weight', true );

			if ( empty( $weight ) ) {
				unset( $_product );
				return;
			}
			
			if ( empty( $metal_purity ) ) 
				$metal_purity = get_post_meta( $_product->id, '_metal_purity', true );
			
			if ( !empty( $metal_purity ) ) 
				$metal_purity = str_replace( array( 'g', 's', 'pl', 'pa' ), '', $metal_purity );
			
			if ( !empty( $metal ) && !empty( $metal_purity ) )
				$metal = $metal * $metal_purity;
				
			// Convert weight to grams
			$weight = $weight * $multiplier;

			$markup = get_post_meta( $_product->id, '_markup_rate', true );
			$markup2 = get_post_meta( $_product->id, '_markup_rate_2', true );
			
			$markup = apply_filters( 'ign_metal_product_markup', $markup );
			$markup2 = apply_filters( 'ign_metal_product_markup_2', $markup2 );

			// Percentage
			if ( false !== strpos( $markup, '%' ) ) {
			
				$markup = str_replace( '%', '', $markup );

				// Adjusted price for the weight of the item - this is the metal price based on the weight
				$adjusted = $weight * $metal;
				
				$cost = ( $adjusted * $markup ) + $adjusted;
				
			// Flat amount
			} else { 

				$cost = floatval( $markup ) + ( $weight * $metal );
				
			}

			if ( !empty( $markup2 ) ) { 
				if ( false !== strpos( $markup2, '%' ) ) {
					$markup2 = str_replace( '%', '', $markup2 );
					$cost = ( $cost * $markup2 ) + $cost;
				} else { 
					$cost = $cost + floatval( $markup2 );
				}
			}

			$cost = $this->format_price( $cost );
			
			$cost = apply_filters( 'ign_metal_product_cost', $cost, $_product, $markup, $metal, $weight, $markup, $markup2 );
			
			$min_cost = get_post_meta( $product_id, '_metal_min_price', true );
		
			if ( isset( $min_cost ) && !empty( $min_cost ) && floatval( $min_cost ) > $cost )
				$cost = $this->format_price( $min_cost );


			// Horton Group
			$this->update_price($product_id, $cost);
			// END Horton Group


			if ( $_product->is_type( 'variation' ) )
				$_product->variable_product_sync( $_product->post->ID );
			
			unset( $_product );
						
		} else if ( $_product->is_type( 'variable' ) ) { 

			$children = $_product->get_children();

			if ( empty( $children ) ) {
				unset( $_product );
				return;
			}

			// parent product weight set? if not try to get it
			if ( empty( $weight ) )
				$parent_weight = get_post_meta( $_product->id, '_weight', true ); 
			else
				$parent_weight = $weight;
				
			foreach( $children as $child_id ) { 

				$child_weight = get_post_meta( $child_id, '_metal_weight', true );

				$child_weight_unit = get_post_meta( $child_id, '_metal_weight_unit', true );
				
				if ( !empty( $child_weight ) && !empty( $child_weight_unit ) ) { 
				
					if ( 'kgs' == $child_weight_unit )
						$multiplier = 1000;
					else if ( 'g' == $child_weight_unit )
						$multiplier = 1;
					else if ( 'lbs' == $child_weight_unit )
						$multiplier = 453.488691744;
					else if ( 'oz' == $child_weight_unit )
						$multiplier = 31.1034768;
						
					$weight = $child_weight;
				
				} else { 

					if ( 'kgs' == $this->weight_unit )
						$multiplier = 1000;
					else if ( 'g' == $this->weight_unit )
						$multiplier = 1;
					else if ( 'lbs' == $this->weight_unit )
						$multiplier = 453.488691744;
					else if ( 'oz' == $this->weight_unit )
						$multiplier = 31.1034768;
						
					$weight = get_post_meta( $child_id, '_weight', true );
					
					if ( empty( $weight ) )
						$weight = $parent_weight;
				
				}

				if ( empty( $child_weight ) )
					$child_weight = $parent_weight;

				if ( !empty( $child_weight ) ) 
					$weight = $child_weight;

				$metal_purity = get_post_meta( $child_id, '_metal_purity', true );
				
				if ( !empty( $metal_purity ) ) 
					$metal_purity = str_replace( array( 'g', 's', 'pl', 'pa' ), '', $metal_purity );

				if ( !empty( $metal ) && !empty( $metal_purity ) )
					$metal_calced = $metal * $metal_purity;
				else 
					$metal_calced = $metal;

				$weight = $weight * $multiplier;

				$markup = get_post_meta( $child_id, '_markup_rate', true );
				$markup2 = get_post_meta( $_product->id, '_markup_rate_2', true );
				
				$markup = apply_filters( 'ign_metal_product_markup', $markup );
				$markup2 = apply_filters( 'ign_metal_product_markup', $markup2 );

				if ( false !== strpos( $markup, '%' ) ) {
				
					$markup = floatval( str_replace( '%', '', $markup ) );

					// Adjusted price for the weight of the item - this is the metal price based on the weight
					$adjusted = $weight * $metal_calced;

					$cost = ( $adjusted * $markup ) + $adjusted;

				} else { 
					
					$cost = floatval( $markup ) + ( $weight * $metal_calced );
				}

				if ( !empty( $markup2 ) ) { 
					if ( false !== strpos( $markup2, '%' ) ) {
						$markup2 = str_replace( '%', '', $markup2 );
						$cost = ( $cost * $markup2 ) + $cost;
					} else { 
						$cost = $cost + floatval( $markup2 );
					}
				}
				
				$cost = $this->format_price( $cost );
					
				$cost = apply_filters( 'ign_metal_product_cost', $cost, $_product, $markup, $metal_calced, $weight, $markup, $markup2 );

				$min_cost = get_post_meta( $child_id, '_metal_min_price', true );
				
				if ( isset( $min_cost ) && !empty( $min_cost ) && floatval( $min_cost ) > $cost )
					$cost = $this->format_price( $min_cost );
					
				// Horton Group
				$this->update_price($product_id, $cost);
				// END Horton Group
			}
		
		
			unset( $_product );
			
		}
	}


	function update_price($productID, $fProposedPrice)
	{
		if(empty($fProposedPrice))
		{
			return false;
		}
		
		$fProposedPrice= (float)$fProposedPrice;

		// Apply Global CC/PayPal Markup %
		$fProposedPrice= $fProposedPrice * $this->fCC_PaypalMarkup();
		$fProposedPrice= $this->hg_number_format($fProposedPrice);

		if($this->should_update_proposed_price($productID, $fProposedPrice))
		{
			$strLastRegularPrice= get_post_meta($productID, '_regular_price', true);
			$fLastRegularPrice= $this->hg_number_format($strLastRegularPrice);

			if($fProposedPrice != $fLastRegularPrice)
			{
				update_post_meta($productID, 'hg_last_regular_price', $strLastRegularPrice);
				update_post_meta($productID, '_price', $fProposedPrice);
				update_post_meta($productID, '_regular_price', $fProposedPrice);
				update_post_meta($productID, 'hg_general_price_changed', 'price_changed');
			}
		}
	}

	


	function should_update_proposed_price($productID, $fProposedPrice)
	{
		$product_min_price= $this->product_min_price($productID);
		if($product_min_price == 0)
		{
			// If product_min_price is 0, then do update for non-precious metal.
			return true;
		}
		else if($product_min_price < 0)
		{
			return false;
		}

		// Get Current "last" Regular Price for comparison.
		$strRegularPrice= get_post_meta($productID, '_regular_price', true);
		
		// Check if Regular Price is set.
		if(empty($strRegularPrice))
		{
			// Making sure Regular Price makes sense starting out.
			if($fProposedPrice > $product_min_price)
			{
				// Return true so that the Regular Price gets updated to the Proposed Price.
				return true;
			}
			else
			{
				// ALERT.
				syslog(LOG_ERR, "HG_Metals:: should_update_proposed_price:: if(empty(strRegularPrice)) >> if(!fProposedPrice > product_min_price)");
				$product_sku= get_post_meta($productID, '_sku', true);
				$this->email_alert( get_option('hg_email_regular_price_from'), 
									get_option('hg_email_regular_price_to'), 
									get_option('hg_sms_regular_price_to'),
									get_option('hg_sms_regular_price_carrier'), 
									"SKU:".$product_sku." - ".get_option('hg_email_regular_price_subject'), 
									get_option('hg_email_regular_price_body'));

				// Zero out inventory so this product can't be sold.
				$this->zero_out_inventory($productID, "REGULAR_PRICE_EMPTY");

				// Do not update Regular Price with Proposed Price.
				return false;
			}
		}
		else
		{
			// cast last regular price string to a float for use in math below.
			$fRegularPrice= floatval($strRegularPrice);
			unset($strRegularPrice);

			// Calculate Price Change.
			$fPriceChange= $fRegularPrice - $fProposedPrice;
			$fPriceChangeRatio= $fPriceChange / $fRegularPrice;

			// Updated the number_format function with additional parameters to remove the thousand's place separator.
			$fPriceChangePerc= $this->hg_number_format($fPriceChangeRatio * -100.0);

			// Check for price change greater than stored percentage.
			if(abs($fPriceChangePerc) > $this->priceChangePercThreshold())
			{
				// Get Price Change Proceed with Update Flag
				$strPriceChangeProceed= get_post_meta($productID, 'hg_price_change_proceed', true);
				if(empty($strPriceChangeProceed))
				{
					$strPriceChangeProceed= self::AWAITING_RESPONSE;
					update_post_meta($productID, 'hg_price_change_proceed', self::AWAITING_RESPONSE);
				}

				if($strPriceChangeProceed == self::AWAITING_RESPONSE)
				{
					// Store proposed price and the percent diff. 
					update_post_meta($productID, 'hg_price_change_proposed', $this->hg_number_format($fProposedPrice));
					update_post_meta($productID, 'hg_price_change_perc', $this->hg_number_format($fPriceChangePerc));
				}

				unset($strPriceChangeProceed);
				
				// Do not update price.
				return false;
			}
			else
			{
				$strPriceChangeProceed= get_post_meta($productID, 'hg_price_change_proceed', true);
				if(!empty($strPriceChangeProceed))
				{
					// Alert a NEVERMIND.
					$product_sku= get_post_meta($productID, '_sku', true);
					$this->email_alert( get_option('hg_email_price_change_from'), 
										get_option('hg_email_price_change_to'), 
										get_option('hg_sms_price_change_to'),
										get_option('hg_sms_price_change_carrier'), 
										"SKU:".$product_sku." - ".get_option('hg_email_price_change_subject'), 
										"NEVERMIND. Price of product with SKU in the subject went back below Price Change Percent Threshold.");

					// Remove all price change flags and data for this product.
					delete_post_meta($productID, 'hg_price_change_proceed');
					delete_post_meta($productID, 'hg_price_change_proposed');
				}
				unset($strPriceChangeProceed);
			}
		}

		// Update Price.
		return true;
	}




	function product_min_price($productID)
	{
		$product_min_price= 0;

		// If the product is a precious metal, get the product's min_price field which is calculated in hg_cron_products.php or overridden manually on the product's edit page.
		if(	   has_term('gold-coins-and-bullion-items', 'product_cat', $productID)
			|| has_term('silver-coins-and-bullion-items', 'product_cat', $productID)
			|| has_term('platinum-coins-and-bullion-items', 'product_cat', $productID)
			|| has_term('palladium-coins-and-bullion-items', 'product_cat', $productID))
		{
			$product_min_price= get_post_meta($productID, "_metal_min_price", true);
			if(empty($product_min_price))
			{
				// Product min price isn't set, so don't update. It should get set by next round by the cron job. 
				$product_min_price= -1;
			}
		}

		return $product_min_price;
	}




	function zero_out_inventory($productID, $strReason)
	{
		$zeroedOut= get_post_meta($productID, 'hg_inventory_zeroed_out', true);
		if(empty($zeroedOut))
		{
			// Mark product as being zeroed out. 
			update_post_meta($productID, 'hg_inventory_zeroed_out', 'zeroed_out');

			// Store current inventory data for reseting. 
			update_post_meta($productID, 'hg_inventory_stock', get_post_meta($productID, '_stock', true));
			update_post_meta($productID, 'hg_inventory_stock_status', get_post_meta($productID, '_stock_status', true));
			update_post_meta($productID, 'hg_inventory_backorders', get_post_meta($productID, '_backorders', true));
			update_post_meta($productID, 'hg_inventory_reason', $strReason);

			// Set stock to 0.
			update_post_meta($productID, '_stock', '0');

			// Set to out-of-stock.
			update_post_meta($productID, '_stock_status', 'outofstock');

			// Turn off backorders.
			update_post_meta($productID, '_backorders', 'no');
		}
	}



	public function priceChangePercThreshold()
	{
		if($this->_fPriceChangePercThreshold === false)
		{
			$strPriceChangePerc= get_option('hg_price_change_percentage');
			if(empty($strPriceChangePerc))
			{
				$strPriceChangePerc= 10;
			}

			// Remove % sign if it exists. 
			$this->_fPriceChangePercThreshold= floatval(str_replace("%", "", $strPriceChangePerc));
		}

		return $this->_fPriceChangePercThreshold;
	}



	function fCC_PaypalMarkup()
	{
		if($this->_fCC_PaypalMarkup === false)
		{
			$strCC_PaypalMarkupPerc= get_option('hg_cc_paypal_markup_percentage');
			if(empty($strCC_PaypalMarkupPerc))
			{
				$strCC_PaypalMarkupPerc= 0;
			}

			$fCC_PaypalMarkupPerc= (float)$strCC_PaypalMarkupPerc;
			$this->_fCC_PaypalMarkup= $this->hg_number_format(1.0 + ($fCC_PaypalMarkupPerc * 0.01));
		}

		return $this->_fCC_PaypalMarkup;
	}




	function hg_number_format($number)
	{
		// return the number formated to have 2 decimal places, a point for the decimal point symbol and nothing for the thousands place symbol.
		return number_format((float)$number, 2, '.', '');
	}




    public function email_alert($from, $to, $cell_to, $cell_carrier, $subject, $body)
    {
    	$cell= "";
    	if($cell_carrier == 'alltel')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_alltel');
    	}
    	else if($cell_carrier == 'att')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_att');
    	}
    	else if($cell_carrier == 'boost')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_boost');
    	}
    	else if($cell_carrier == 'sprint')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_sprint');
    	}
    	else if($cell_carrier == 'tmobile')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_tmobile');
    	}
    	else if($cell_carrier == 'uscellular')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_uscelular');
    	}
    	else if($cell_carrier == 'verizon')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_verizon');
    	}
    	else if($cell_carrier == 'virgin')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_virgin');
    	}

    	$bMailSent= wp_mail(array($to, $cell), $subject, $body, array('From:'.$from));
		if($bMailSent == false)
		{
			syslog(LOG_ERR, "ERROR SENDING WP_MAIL!!");
		}
    } 
}
?>