(function($)
{
	$(document).ready(function()
	{
		var data = 
		{
			'action': 'hg_integration_page_ajax'
		};

		$.post("admin-ajax.php", data, function(a_extra_settings) 
		{
			a_extra_settings= JSON.parse(a_extra_settings);

			// Add Price Change Settings 
			$('table.form-table tr').first().before(a_extra_settings.price_change_settings_html);

			// Add Inventory Zeroed Out Settings
			$('tr.price_change_settings').after(a_extra_settings.inventory_zeroed_out_html);

			// Add Min Price fields under each precious metal category.
			$('select[name="ignitewoo_metals_gold_category"]').after('<div>Min Price: <input type="text" name="hg_min_price_gold" value="'+a_extra_settings.min_price_gold+'"></div>');
			$('select[name="ignitewoo_metals_silver_category"]').after('<div>Min Price: <input type="text" name="hg_min_price_silver" value="'+a_extra_settings.min_price_silver+'"></div>');
			$('select[name="ignitewoo_metals_platinum_category"]').after('<div>Min Price: <input type="text" name="hg_min_price_platinum" value="'+a_extra_settings.min_price_platinum+'"></div>');
			$('select[name="ignitewoo_metals_palladium_category"]').after('<div>Min Price: <input type="text" name="hg_min_price_palladium" value="'+a_extra_settings.min_price_palladium +'"></div>');

			// Add Product Markup Settings HTML
			$('select[name="ignitewoo_metals_palladium_category"]').parents('tr').after(a_extra_settings.product_markup_settings);

			// Add Notification Settings
			$('.shipping_class_markup').after(a_extra_settings.notification_form_html);
		});		
    });

})(jQuery);