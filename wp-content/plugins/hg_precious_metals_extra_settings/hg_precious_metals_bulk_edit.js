(function($)
{
	$(document).ready(function()
	{
		console.log("precious_metals_bulk_edit_js");

		$( '#bulk_edit' ).click(function() 
		{
			// define the bulk edit row
			bulk_row = $( '#bulk-edit' );

			// get the selected post ids that are being edited
			var post_ids = new Array();
			bulk_row.find( '#bulk-titles' ).children().each( function() 
			{
				post_ids.push($(this).attr('id').replace(/^(ttle)/i, ''));
			});

			// get the release date
			shipping_class= bulk_row.find("select[name='_shipping_class']").val();

			//save the data
			$.ajax(
			{
				url: ajaxurl,
				type: 'POST',
				async: false,
				cache: false,
				data: 
				{
					action: 'hg_save_bulk_edit', 
					post_ids: post_ids,
					shipping_class: shipping_class
				}
			});

		});
    });

})(jQuery);