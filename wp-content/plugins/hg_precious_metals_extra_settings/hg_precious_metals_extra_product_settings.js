(function($)
{
	$(document).ready(function()
	{
		console.log("precious_metals_extra_product_settings_js");

		// Add Regular Price Note.
		$('p._regular_price_field').after(a_product.regular_price_note);

		// Add Last Regular Price below the Regular Price field on product pages.
		$('p.regular_price_note').after(a_product.last_regular_price_field);

		// Add Last Regular Price Note.
		$('p._last_regular_price_field').after(a_product.last_regular_price_note);

		// Add Markup Text 1 and 2
		$('[id^="_markup_rate["]').parent().append('<span>'+a_product.markup_text_1+'</span>');
		$('[id^="_markup_rate_2"]').parent().append('<span>'+a_product.markup_text_2+'</span>');

		// Make Markup2 readonly
		$('[id^="_markup_rate_2"]').prop('readonly', true);

		// Add Metal Weight Note.
		$('select[name^="_metal_weight_unit"]').parent().parent().after(a_product.metal_weight_note);

		// Add Metal Purity Note.
		$('select[name^="_metal_purity"]').parent().parent().after(a_product.metal_purity_note);

		// Add Min Price Note.
		$('[name^="_metal_min_price"]').parent().parent().after(a_product.min_price_note);

		// Add Ebay Start Price Note.
		$('p.wpl_ebay_start_price_field').after(a_product.ebay_start_price_note);


		// Make Custom Feilds panel fields Read Only
		$('#postcustom #the-list textarea[name^="meta"]').prop('readonly', true);
    });

})(jQuery);