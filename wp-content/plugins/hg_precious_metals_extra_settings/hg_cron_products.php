<?php
/*
NAME: Cron Sync Inventory and Price for Ebay

DESCRIPTION: For every published product, it syncs the _qbpos_data to the _stock in the postmeta table so that the wordpress and ebay stores have the same inventory as qbpos inventory, and  it also syncs the _regular_price to the _ebay_start_price (applying the ebay price modifier found in the ebay_profiles table 'start_price')in the postmeta table so that the ebay listings is the same as the wordpress price. Then it runs the wplister_revise_inventory_status action to attempt to update their corresponding ebay product prices and inventory. 

AUTHOR: Horton Group, Dev: Mark Freeman
VERSION: v1.16
*/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
$parentDir= $parse_uri[0];

if(file_exists($parentDir."wp-load.php"))
{
    require_once $parentDir."wp-load.php";

    // Make sure this cron job isnt already running so that the jobs don't overlap.
    if(get_option('HG_Cron_Ebay_Sync_Running'))
    {
    	update_option('HG_Cron_Ebay_Sync_Running', 0, false);
    	syslog(LOG_ERR, 'HG_Cron_Ebay_Sync_Running');
    	exit();
    }
    
    $pUpdate_Inventory_And_Price_For_Ebay= new Update_Inventory_And_Price_For_Ebay();
}
else
{
	syslog(LOG_ERR, 'CRON ERROR wp-load not found');
}


class Update_Inventory_And_Price_For_Ebay
{
	public $_ebay_price_modifier= false;
	public $_mail_output= '';
	public $_nPriceChangeAlerts= 0;
	public $_nPriceChangeAny= 0;
	public $_strPriceChangeLog= '';
	const AWAITING_RESPONSE= 'awaiting_response';
	const PROCEED_WITH_UPDATE= 'yes';
	const DONT_PROCEED_WITH_UPDATE= 'no';

	function __construct() 
	{
		update_option('HG_Cron_Ebay_Sync_Running', 1, false);
		date_default_timezone_set("America/Chicago");
		$this->run_update();
		update_option('HG_Cron_Ebay_Sync_Running', 0, false);
	}

	public function run_update()
	{
		global $wpdb;

		// Get all product IDs.
		$aProducts= $wpdb->get_results("SELECT `ID` FROM $wpdb->posts WHERE `post_type` = 'product' AND `post_status` = 'publish'");
		
		syslog(LOG_DEBUG, "update_wp_inventory_and_price_for_ebay():: NUMBER OF PRODUCTS= ".count($aProducts));

		if(count($aProducts) <= 0)
		{
			syslog(LOG_ERR, "update_wp_inventory_and_price_for_ebay():: NUMBER OF PRODUCTS= ".count($aProducts));
			return;
		}

		$nNumberUpdated= 0;

		ob_start();
		for($i= 0; $i < count($aProducts); ++$i)
		{	
			$bEbayPriceNeedsSync= false;
			$bEbayInventoryNeedsSync= false;

			$productID= $aProducts[$i]->ID;
			if(empty($productID))
			{
				// If productID is empty, skip this productID and keep going.
				continue;
			}

			$this->update_product_min_price($productID);
			$bEbayPriceNeedsSync= $this->update_price($productID);
			$bEbayInventoryNeedsSync= $this->update_inventory($productID);
			
			if($bEbayPriceNeedsSync || $bEbayInventoryNeedsSync)
			{
				do_action('wplister_revise_inventory_status', $productID);
				++$nNumberUpdated;
			}
		}

		syslog(LOG_DEBUG, "update_wp_inventory_and_price_for_ebay():: Number of products updated= ".$nNumberUpdated." out of ".count($aProducts));
		
		$output= ob_get_clean();
		if(!empty($output))
		{
			syslog(LOG_DEBUG, "update_wp_inventory_and_price_for_ebay():: OUTPUT= $output");
			unset($output);
		}

		// Send Price Change Alert via email/sms.
		if(strlen($this->_mail_output) > 0)
		{
			$this->_mail_output= "# Alerts= $this->_nPriceChangeAlerts \n$this->_mail_output";
			$this->email_alert( get_option('hg_email_price_change_from'), 
								get_option('hg_email_price_change_to'), 
								get_option('hg_sms_price_change_to'),
								get_option('hg_sms_price_change_carrier'), 
								get_option('hg_email_price_change_subject'), 
								$this->_mail_output);
		}

		// Price Change Log
		if(strlen($this->_strPriceChangeLog) > 0)
		{
			$this->save_price_change_log();
		}
	}


	public function update_product_min_price($productID)
	{
		// If the product's Minimum Price field is empty and it is a precious metal, set the Minimum Price field to the Integration page's min price for that metal * the metal weight of this product.
		$product_min_price= get_post_meta($productID, "_metal_min_price", true);
		if(empty($product_min_price))
		{
			if(has_term('gold-coins-and-bullion-items', 'product_cat', $productID))
			{
				$min_price= get_option('hg_min_price_gold');
				if(empty($min_price))
				{
					if(!$this->_bEmailSent_MinPrice_Gold)
					{
						$this->_bEmailSent_MinPrice_Gold= true;
						$this->email_alert( get_option('hg_email_min_price_from'), 
											get_option('hg_email_min_price_to'), 
											get_option('hg_sms_min_price_to'),
											get_option('hg_sms_min_price_carrier'), 
											"(GOLD MIN PRICE EMPTY): ".get_option('hg_email_min_price_subject'), 
											get_option('hg_email_min_price_body'));
					}

					// Set min price to default value of 1150 because it wasn't set on the settings page.
					$min_price= 1150;
				}
				$metal_weight= floatval(get_post_meta($productID, '_metal_weight', true));
				if(empty($metal_weight))
				{
					$this->metal_weight_empty($productID);
					return;
				}
				$product_min_price= $this->hg_number_format($min_price * $metal_weight);
			}
			else if(has_term('silver-coins-and-bullion-items', 'product_cat', $productID))
			{
				$min_price= get_option('hg_min_price_silver');
				if(empty($min_price))
				{
					if(!$this->_bEmailSent_MinPrice_Silver)
					{
						$this->_bEmailSent_MinPrice_Silver= true;
						$this->email_alert( get_option('hg_email_min_price_from'), 
											get_option('hg_email_min_price_to'), 
											get_option('hg_sms_min_price_to'),
											get_option('hg_sms_min_price_carrier'),
											"(SILVER MIN PRICE EMPTY): ".get_option('hg_email_min_price_subject'), 
											get_option('hg_email_min_price_body'));
					}

					// Set min price to default value of 15 because it wasn't set on the settings page.
					$min_price= 15;
				}
				$metal_weight= floatval(get_post_meta($productID, '_metal_weight', true));
				if(empty($metal_weight))
				{
					$this->metal_weight_empty($productID);	
					return;
				}
				$product_min_price= $this->hg_number_format($min_price * $metal_weight);
			}
			else if(has_term('platinum-coins-and-bullion-items', 'product_cat', $productID))
			{
				$min_price= get_option('hg_min_price_platinum');
				if(empty($min_price))
				{
					if(!$this->_bEmailSent_MinPrice_Platinum)
					{
						$this->_bEmailSent_MinPrice_Platinum= true;
						$this->email_alert( get_option('hg_email_min_price_from'), 
											get_option('hg_email_min_price_to'),
											get_option('hg_sms_min_price_to'),
											get_option('hg_sms_min_price_carrier'), 
											"(PLATINUM MIN PRICE EMPTY): ".get_option('hg_email_min_price_subject'), 
											get_option('hg_email_min_price_body'));
					}

					// Set min price to default value of 925 because it wasn't set on the settings page.
					$min_price= 925;
				}
				$metal_weight= floatval(get_post_meta($productID, '_metal_weight', true));
				if(empty($metal_weight))
				{
					$this->metal_weight_empty($productID);
					return;
				}
				$product_min_price= $this->hg_number_format($min_price * $metal_weight);
			}
			else if(has_term('palladium-coins-and-bullion-items', 'product_cat', $productID))
			{
				$min_price= get_option('hg_min_price_palladium');
				if(empty($min_price))
				{
					if(!$this->_bEmailSent_MinPrice_Palladium)
					{
						$this->_bEmailSent_MinPrice_Palladium= true;
						$this->email_alert( get_option('hg_email_min_price_from'), 
											get_option('hg_email_min_price_to'), 
											get_option('hg_sms_min_price_to'),
											get_option('hg_sms_min_price_carrier'), 
											"(PALLADIUM MIN PRICE EMPTY): ".get_option('hg_email_min_price_subject'), 
											get_option('hg_email_min_price_body'));
					}

					// Set min price to default value of 700 because it wasn't set on the settings page.
					$min_price= 700;
				}
				$metal_weight= floatval(get_post_meta($productID, '_metal_weight', true));
				if(empty($metal_weight))
				{
					$this->metal_weight_empty($productID);
					return;
				}
				$product_min_price= $this->hg_number_format($min_price * $metal_weight);
			}
			else
			{
				// Not a precious metal, so do nothing.
				return;
			}

			update_post_meta($productID, "_metal_min_price", $product_min_price);
		}
	}


	public function metal_weight_empty($productID)
	{
		$product_sku= get_post_meta($productID, '_sku', true);
		$this->email_alert( get_option('hg_email_metal_weight_from'), 
							get_option('hg_email_metal_weight_to'), 
							get_option('hg_sms_metal_weight_to'),
							get_option('hg_sms_metal_weight_carrier'), 
							"(Product SKU: $product_sku): ".get_option('hg_email_metal_weight_subject'), 
							get_option('hg_email_metal_weight_body'));

		$this->zero_out_inventory($productID, "METAL_WEIGHT_EMPTY");
	}



	public function update_price($productID)
	{
		// Get Price Change Proceed with Update Flag and do logic if set. 
		$strPriceChangeProceed= get_post_meta($productID, 'hg_price_change_proceed', true);
		if(!empty($strPriceChangeProceed))
		{
			if($strPriceChangeProceed == self::AWAITING_RESPONSE)
			{
				// ALERT.
				$this->_nPriceChangeAlerts++;
				$product_sku=      get_post_meta($productID, '_sku', true);
				$strCurrentPrice=  get_post_meta($productID, '_regular_price', true);
				$strProposedPrice= get_post_meta($productID, 'hg_price_change_proposed', true);
				$strPercChange=    get_post_meta($productID, 'hg_price_change_perc', true);
				$this->_mail_output.= "SKU= $product_sku - Proposed=$strProposedPrice. Current=$strCurrentPrice. Change=$strPercChange%\n";
				return false;
			}
			else if($strPriceChangeProceed == self::PROCEED_WITH_UPDATE)
			{
				$strPriceChangeProposed= get_post_meta($productID, 'hg_price_change_proposed', true);
				if(!empty($strPriceChangeProposed))
				{
					$strLastRegularPrice= get_post_meta($productID, '_regular_price', true);
					$strPercChange= get_post_meta($productID, 'hg_price_change_perc', true);
					
					// Price Change log.
					$this->_nPriceChangeAny++;
					$product_sku= get_post_meta($productID, '_sku', true);
					$this->_strPriceChangeLog.= "SKU= $product_sku -- New Price= $strPriceChangeProposed -- Last Price= $strLastRegularPrice -- Percent Change= $strPercChange%. \n";

					update_post_meta($productID, 'hg_last_regular_price', $strLastRegularPrice);
					update_post_meta($productID, '_regular_price', $strPriceChangeProposed);
					update_post_meta($productID, '_price', $strPriceChangeProposed);
				}

				unset($strPriceChangeProposed);
				delete_post_meta($productID, 'hg_price_change_proceed');
				delete_post_meta($productID, 'hg_price_change_proposed');
				delete_post_meta($productID, 'hg_price_change_perc');
			}
			else
			{
				return false;
			}
		}


		// Check for General Price Change.
		$strGeneralPriceChanged= get_post_meta($productID, 'hg_general_price_changed', true);
		if(!empty($strGeneralPriceChanged))
		{
			// Price Change log.
			$this->_nPriceChangeAny++;
			$product_sku= get_post_meta($productID, '_sku', true);
			$strRegularPrice= get_post_meta($productID, '_regular_price', true);
			$strLastRegularPrice= get_post_meta($productID, 'hg_last_regular_price', true);
			$this->_strPriceChangeLog.= "SKU= $product_sku -- New Price= $strRegularPrice -- Last Price= $strLastRegularPrice. \n";

			delete_post_meta($productID, 'hg_general_price_changed');
		}

		
		// Get Regular Price.
		$strRegularPrice= get_post_meta($productID, '_regular_price', true);
		if(empty($strRegularPrice))
		{
			// ALERT.
			$product_sku= get_post_meta($productID, '_sku', true);
			$this->email_alert( get_option('hg_email_regular_price_from'), 
								get_option('hg_email_regular_price_to'), 
								get_option('hg_sms_regular_price_to'),
								get_option('hg_sms_regular_price_carrier'), 
								"SKU:".$product_sku." - ".get_option('hg_email_regular_price_subject'), 
								get_option('hg_email_regular_price_body'));

			$this->zero_out_inventory($productID, "REGULAR_PRICE_EMPTY");

			return false;
		}

		// Cast regular price string to a float for use in math below.
		$fRegularPrice= floatval($strRegularPrice);
		unset($strRegularPrice);

		// Get Ebay Start Price from postmeta table.
		$strEbayStartPrice= get_post_meta($productID, '_ebay_start_price', true);
		$fEbayStartPrice= floatval($strEbayStartPrice);

		// Get the ebay price mod.
		$fEbayPriceModifier= $this->ebay_price_modifier();

		// Apply ebay price mod to regular price, and round to 2 decimal places.
		$fEbayPrice= number_format($fRegularPrice + ($fRegularPrice * $fEbayPriceModifier), 2, '.', '');

		// Compare prices, if they don't match then,
		if($fEbayPrice != $fEbayStartPrice || empty($strEbayStartPrice))
		{
			//Update _ebay_start_price with fEbayPrice.
			update_post_meta($productID, '_ebay_start_price', $fEbayPrice);
			return true;
		}
		
		return false;
	}


	function zero_out_inventory($productID, $strReason)
	{
		$zeroedOut= get_post_meta($productID, 'hg_inventory_zeroed_out', true);
		if(empty($zeroedOut))
		{
			// Mark product as being zeroed out. 
			update_post_meta($productID, 'hg_inventory_zeroed_out', 'zeroed_out');

			// Store current inventory data for reseting. 
			update_post_meta($productID, 'hg_inventory_stock', get_post_meta($productID, '_stock', true));
			update_post_meta($productID, 'hg_inventory_stock_status', get_post_meta($productID, '_stock_status', true));
			update_post_meta($productID, 'hg_inventory_backorders', get_post_meta($productID, '_backorders', true));
			update_post_meta($productID, 'hg_inventory_reason', $strReason);

			// Set stock to 0.
			update_post_meta($productID, '_stock', '0');

			// Set to out-of-stock.
			update_post_meta($productID, '_stock_status', 'outofstock');

			// Turn off backorders.
			update_post_meta($productID, '_backorders', 'no');
		}
	}


	public function ebay_price_modifier()
	{
		if($this->_ebay_price_modifier === false)
		{
			global $wpdb;
			$tablename= $wpdb->prefix."ebay_profiles";
			$ebay_profile_details= $wpdb->get_results("SELECT `details` FROM $tablename WHERE `profile_id` = 1");
			if(count($ebay_profile_details) <= 0)
			{
				syslog(LOG_ERR, "if(count(ebay_profile_details) <= 0)");
				$this->_ebay_price_modifier= 0;
				return $this->_ebay_price_modifier;
			}

			$ebay_profile_details= json_decode($ebay_profile_details[0]->details, true);
			$temp_ebay_price_mod= $ebay_profile_details['start_price'];
			if(!isset($temp_ebay_price_mod))
			{
				syslog(LOG_ERR, "if(!isset(temp_ebay_price_mod))");
				$this->_ebay_price_modifier= 0;
				return $this->_ebay_price_modifier;
			}

			// Remove % sign and devide by 100. 
			$this->_ebay_price_modifier= floatval(str_replace("%", "", $temp_ebay_price_mod)) * 0.01;
		}

		return $this->_ebay_price_modifier;
	}


	public function update_inventory($productID)
	{
		if(empty($productID))
		{
			return false;
		}

		$bInventoryNeedsSync= false;

		$zeroedOut= get_post_meta($productID, 'hg_inventory_zeroed_out', true);
		if(!empty($zeroedOut))
		{
			return;
		}

		$qbpos_data= get_post_meta($productID, '_qbpos_data', true);
		if(!isset($qbpos_data['QuantityOnHand']))
		{
			return false;
		}

		$qbpos_data= floatval($qbpos_data['QuantityOnHand']);
		$wp_stock= floatval(get_post_meta($productID, '_stock', true));


		// Check quantity from ebay_auctions table against wp_stock because we're having trouble with ebay being out of sync with website and QBPOS.
		global $wpdb;
		$strEbayAuctionsTable= $wpdb->prefix."ebay_auctions";
		$aResults= $wpdb->get_results("SELECT `quantity`, `quantity_sold` FROM $strEbayAuctionsTable WHERE `post_id` = $productID");
		if(!empty($aResults))
		{
			$fEbayQuantity= floatval($aResults[0]->quantity);
			$fEbayQuantitySold= floatval($aResults[0]->quantity_sold);

			// Subtract the quantity sold to get the correct quantity value.
			$fEbayQuantity= $fEbayQuantity - $fEbayQuantitySold;
			
			if($fEbayQuantity != $wp_stock)
			{
				syslog(LOG_DEBUG, "ID= $productID - fEbayQuantity != wp_stock :: $fEbayQuantity != $wp_stock");
				$bInventoryNeedsSync= true;
			}
		}
		
		// Compare if QBPOS inventory matches Woo stock
		if($qbpos_data != $wp_stock)
		{
			// Update Woo Stock with QBPOS QuantityOnHand.
			update_post_meta($productID, '_stock', $qbpos_data);
			if($qbpos_data > 0)
			{
				update_post_meta($productID, '_stock_status', 'instock');
			}
			else
			{
				update_post_meta($productID, '_stock_status', 'outofstock');
			}
			$bInventoryNeedsSync= true;
		}

		return $bInventoryNeedsSync;
	}



	public function save_price_change_log()
    {
        $file = get_stylesheet_directory().'/price_change_log.txt';
        $current = file_get_contents($file);
        if(strlen($current) > 100000)
        {
        	$current = substr($current, 0, 100000);
        }
        file_put_contents($file, date("Y-m-d h:i:sa")."\nNumber Of Price Changes This Minute= $this->_nPriceChangeAny\n".$this->_strPriceChangeLog . "\n\n". $current . "\n");
    }

    public function email_alert($from, $to, $cell_to, $cell_carrier, $subject, $body)
    {
    	$cell= "";
    	if($cell_carrier == 'alltel')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_alltel');
    	}
    	else if($cell_carrier == 'att')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_att');
    	}
    	else if($cell_carrier == 'boost')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_boost');
    	}
    	else if($cell_carrier == 'sprint')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_sprint');
    	}
    	else if($cell_carrier == 'tmobile')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_tmobile');
    	}
    	else if($cell_carrier == 'uscellular')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_uscelular');
    	}
    	else if($cell_carrier == 'verizon')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_verizon');
    	}
    	else if($cell_carrier == 'virgin')
    	{
    		$cell= $cell_to."@".get_option('hg_carrier_gateway_virgin');
    	}

    	$bMailSent= wp_mail(array($to, $cell), $subject, $body, array('From:'.$from));
		if($bMailSent == false)
		{
			syslog(LOG_ERR, "ERROR SENDING WP_MAIL!!");
		}
    }   



    public function hg_number_format($number)
	{
		// return the number formated to have 2 decimal places, a point for the decimal point symbol and nothing for the thousands place symbol.
		return number_format((float)$number, 2, '.', '');
	}
}
?>