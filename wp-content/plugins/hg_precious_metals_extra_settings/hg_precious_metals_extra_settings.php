<?php
/*
Plugin Name: HG Precious Metals Extra Settings
Plugin URI: http://hortongroup.com
Description: Adds some extra settings to the Woocommerce > Settings > Integration > Precious Metals page and to each product page.
Version: 1.11
Author: Horton Group - Dev: Mark Freeman
Author URI: http://hortongroup.com

Copyright: © 2016 Horton Group.
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/





/*   			Woocommerce Precious Metals UPDATE INSTRUCTIONS:				
	*********** How to modify Woocommerce Precious Metals for HG Precious Metals Extra Settings plugin ************
	
	Go to /wp-content/plugins/woocommerce-metals/woocommerce-metals.php and find the 2 lines of code shown below where the class IGN_Metals is instantiated. For version 2.6.26, this is on line 1346: 

		global $ign_metals; 
		$ign_metals = new IGN_Metals();

	Comment out those 2 lines like so:

		// global $ign_metals; 
		// $ign_metals = new IGN_Metals();

	Save and put on the live server. 

	HG Precious Metals Extra Settings plugin has a subclass of IGN_Metals called HG_Metals which lives here: /wp-content/plugins/hg_precious_metals_extra_settings/hg_metals.php. It is instantiated in the plugins_loaded action via the function hg_plugins_loaded(). HG_Metals overrides the update_products and add_markup methods to add a large amount of custom logic. 

	If the Woocommerce Precious Metals plugin has been updated to a version greater than the version stored in the global variable directly below,  $g_strWooMetalsCurrentVersion, then after you have completed the instructions above, change the version in that variable to the exact current version to stop the alerts and keep the version monitoring code below working.
*/
global $g_strWooMetalsCurrentVersion;
$g_strWooMetalsCurrentVersion= "2.6.26";







// woocommerce-metals__horton_group_extension
add_action('plugins_loaded', 'hg_plugins_loaded');
function hg_plugins_loaded() 
{
	// Load the HG_Metals subclass if IGN_Metals exists. This works because I've commented out the creation ($ign_metals= new IGN_Metals();) of the IGN_Metals class in plugins/woocommerce-metals/woocommerce-metals.php and thus am replacing it with my own subclass HG_Metals to do price update logic among other things. 
	if(class_exists('IGN_Metals')) 
	{
		require_once plugin_dir_path(__FILE__)."hg_metals.php";
		
		if(class_exists('HG_Metals'))
		{
			global $ign_metals; 
			if(!is_a($ign_metals, 'HG_Metals'))
			{
				$ign_metals= new HG_Metals();
			}
		}
	}
	else 
	{
		add_action('admin_notices', 'hg_ign_metals_not_loaded');
	}



	// Check on Woocommerce Precious Metals plugin's version to see if it has been updated.
	$wooMetalsPluginData= get_plugin_data(plugin_dir_path(__FILE__)."../woocommerce-metals/woocommerce-metals.php", false, false);
	if(empty($wooMetalsPluginData))
	{
		add_action('admin_notices', 'hg_cant_access_woo_metals_version');
	}
	else
	{
		global $g_strWooMetalsCurrentVersion;
		if($wooMetalsPluginData['Version'] !== $g_strWooMetalsCurrentVersion)
		{
			add_action('admin_notices', 'hg_alert_woo_metals_updated');
		}
	}
}

function hg_ign_metals_not_loaded() 
{
    printf(
      '<div class="notice notice-error"><p>%s</p></div>',
      __('Sorry cannot subclass IGN_Metals because it is not loaded. Contact Horton Group and investigate WooCommerce Precious Metals - Horton Group Subclass.')
    );
}

function hg_cant_access_woo_metals_version() 
{
	$strOutput= "Unable to access Woocommerce Precious Metals' version. This is needed to make sure that plugin has not been updated and thus to ensure that the Horton Group subclass of it is still functioning. Please notify HORTON GROUP immediately and inform them to follow the instructions at the top of wp-content/plugins/hg_precious_metals_extra_settings/hg_precious_metals_extra_settings.php titled 'How to modify Woocommerce Precious Metals for HG Precious Metals Extra Settings plugin'!";
    printf(
      "<div class='notice notice-error'><p>%s</p></div>",
      __($strOutput)
    );
    wp_mail(array("info@nashvillecoingallery.com"), "ATTENTION nashvillecoingallery.com ALERT:", $strOutput, array('From: attn@nashvillecoingallery.com'));
}

function hg_alert_woo_metals_updated() 
{
	$wooMetalsPluginData= get_plugin_data(plugin_dir_path(__FILE__)."../woocommerce-metals/woocommerce-metals.php", false, false);
	$strOutput="ATTENTION: Woocommerce Precious Metals has been UPDATED to version ".$wooMetalsPluginData["Version"]."! Please notify HORTON GROUP immediately and inform them to follow the instructions at the top of wp-content/plugins/hg_precious_metals_extra_settings/hg_precious_metals_extra_settings.php titled 'How to modify Woocommerce Precious Metals for HG Precious Metals Extra Settings plugin'!";
    printf(
      "<div class='notice notice-error'><p>%s</p></div>",
      __($strOutput)
    );
    wp_mail(array("info@nashvillecoingallery.com"), "ATTENTION nashvillecoingallery.com ALERT:", $strOutput, array('From: attn@nashvillecoingallery.com'));
}
// END woocommerce-metals__horton_group_extension




// Admin Code
add_action( 'admin_enqueue_scripts', 'enqueue_precious_metals_extra_settings_scripts' );
function enqueue_precious_metals_extra_settings_scripts()
{
	global $post_type;
	$currentScreen= get_current_screen();
	$screenBase= $currentScreen->base;

	/**********  PRODUCT Edit PAGES  **********/
	if($post_type == 'product' && $screenBase == 'post')
	{
		// Loads script on product pages to insert exta settings.
		wp_register_script('precious_metals_extra_product_settings_js', plugin_dir_url( __FILE__ ).'/hg_precious_metals_extra_product_settings.js', array('jquery'));
		wp_localize_script('precious_metals_extra_product_settings_js', 'a_product', hg_product_page_object());
		wp_enqueue_script('precious_metals_extra_product_settings_js');
	}
	else if($post_type == 'product' && $screenBase == 'edit') 
	{
		// Products table page, for search and bulk edit.
		wp_enqueue_script('precious_metals_bulk_edit_js', plugin_dir_url( __FILE__ ).'/hg_precious_metals_bulk_edit.js', array('jquery'));
	}
	else if(isset($_GET['page']) && isset($_GET['tab']))
	{
		/********** INTEGRATION PAGE **********/
		if($_GET['page'] == 'wc-settings' && $_GET['tab'] == 'integration' && $_GET['section'] == 'ignitewoo_metals')
		{	
			// Register script for Woocommerce > Settings > Integration page to insert extra settings.
			wp_enqueue_script('precious_metals_extra_settings_js', plugin_dir_url( __FILE__ ).'/hg_precious_metals_extra_settings.js', array('jquery'), '1.3');
			wp_enqueue_style('precious_metals_extra_settings_css', plugin_dir_url( __FILE__ ).'/hg_precious_metals_extra_settings.css', array(), '1.3');
		}
	}
}



add_action( 'wp_ajax_hg_integration_page_ajax', 'hg_integration_page_ajax' );
function hg_integration_page_ajax() 
{
	// Create array to be passed to js script.
	$a_extra_settings= array();

	// Price Change Settings/Notification HTML.
	hgUpdateObject_price_change_settings($a_extra_settings);

	// Inventory Zeroed Out Notification HTML.
	hgUpdateObject_inventory_zeroed_out_settings($a_extra_settings);

	// Min Prices for categoryes.
	$a_extra_settings['min_price_gold']= get_option('hg_min_price_gold');
	$a_extra_settings['min_price_silver']= get_option('hg_min_price_silver');
	$a_extra_settings['min_price_platinum']= get_option('hg_min_price_platinum');
	$a_extra_settings['min_price_palladium']= get_option('hg_min_price_palladium');

	//Product Markup Settings HTML
	hgUpdateObject_product_markup_settings($a_extra_settings);

	// Notification Settings HTML
	hgUpdateObject_notification_settings($a_extra_settings);

	echo json_encode($a_extra_settings);
	wp_die(); // this is required to terminate immediately and return a proper response
}




function hg_product_page_object()
{
	$a_product= array();	

	// Regular Price Note HTML
	ob_start();?>
	<p class="form-field regular_price_note">
		<label></label>
		<span>Regular Price Note: If this product has been added to one of these 4 Precious Metal Categories (gold-coins-and-bullion-items, silver-coins-and-bullion-items, platinum-coins-and-bullion-items, or palladium-coins-and-bullion-items), the Regular Price is set by HG Precious Metals Extra Settings plugin cronjob which uses the parameters below to calculate it. In this case, DO NOT CHANGE MANUALLY!<br>However, if this product does NOT belong to one of those 4 categories, the Regular Price needs to be set manually and needs to INCLUDE any and all markups when set. The Markup Amounts below will be ignored.</span>
	</p>
<?php 	
	$a_product['regular_price_note']= ob_get_clean();
	// END Regular Price Note HTML



	// Last Regular Price Field HTML
	ob_start();?>
	<p class="form-field _last_regular_price_field ">
		<label for="_last_regular_price">Last Regular price ($)</label>
		<input type="text" class="short wc_input_price" style="" name="hg_last_regular_price" id="hg_last_regular_price" value="<?php echo number_format(get_post_meta($_GET['post'], 'hg_last_regular_price', true), 2, '.', '') ?>" readonly>
	</p>
<?php 	
	$a_product['last_regular_price_field']= ob_get_clean();
	// END Last Regular Price Field HTML

	

	// Last Regular Price Note HTML
	ob_start();?>
	<p class="form-field last_regular_price_note"><label></label><span>Last Regular Price Note: It is set by the HG Precious Metals Extra Settings plugin cronjob. It is only used to view what the last price was.</span></p>
<?php 	
	$a_product['last_regular_price_note']= ob_get_clean();
	// END Last Regular Price Note HTML



	// Get Markup Text notes saved from Integration page.
	$a_product['markup_text_1']= stripslashes(get_option('hg_markup_text_1'));
	$a_product['markup_text_2']= stripslashes(get_option('hg_markup_text_2'));



	// Metal Weight Note HTML
	ob_start();?>
	<p class="form-field metal_weight_note"><label></label><span>Metal Weight Note: Use the ACTUAL precious metal weight, NOT the gross weight of the coin.  For a $10 Liberty gold piece, use .4838, etc.</span></p>
<?php 	
	$a_product['metal_weight_note']= ob_get_clean();
	// END Metal Weight Note HTML



	// Metal Purity Note HTML
	ob_start();?>
	<p class="form-field metal_purity_note"><label></label><span>Metal Purity Note: Use the HIGHEST purity listed in the chart, regardless of the item's actual purity, since we're specifying the ACTUAL precious metal weight above.</span></p>
<?php 	
	$a_product['metal_purity_note']= ob_get_clean();
	// END Metal Purity Note HTML


	
	// Min Price Note HTML
	ob_start();?>
	<p class="form-field min_price_note"><label></label><span>Minimum Price Note: If this field is empty and this product is in one of these 4 Precious Metal Categories (gold-coins-and-bullion-items, silver-coins-and-bullion-items, platinum-coins-and-bullion-items, or palladium-coins-and-bullion-items), the HG Precious Metals Extra Settings plugin cronjob sets it to the Integration page's min price for that metal * the metal weight of this product. If this product is not in one of those categories, this field is completely ignored. If you would like to override the calculated Min Price, pick a minimum price between 10-20% below current value that makes sense, and replace the value in this field. We'll need to revisit these periodically as metals prices fluctuate if overridden.</span></p>
<?php 	
	$a_product['min_price_note']= ob_get_clean();
	// END Min Price Note HTML


	// Ebay Start Price Note HTML
	ob_start();?>
	<p class="form-field ebay_start_price_note"><label>Price / Start Price Note:</label><span>It is set by the HG Precious Metals Extra Settings plugin cronjob: Start_Price = Regular_Price + (Regular_Price * Ebay_Price_Modifier). The Ebay_Price_Modifier is set here: <a href="https://www.nashvillecoingallery.com/wp-admin/admin.php?page=wplister-profiles" target="_blank">Ebay Profiles page</a>.</span></p>
<?php 	
	$a_product['ebay_start_price_note']= ob_get_clean();
	// END Ebay Start Price Note HTML

	return $a_product;
}



function hgUpdateObject_price_change_settings(&$a_extra_settings)
{
	// Price Change Settings/Notification HTML
	ob_start();?>
	<tr valign="top" class="price_change_settings">
		<th class="titledesc" scope="row">
			<label>Price Movement Percentage Notification Trigger:</label>
		</th>
		<td class="forminp">
			<fieldset>
				<div>
					<input class='hg_price_change_percentage' type="text" name="hg_price_change_percentage" value="<?php echo get_option('hg_price_change_percentage');?>">%
				</div>
			</fieldset>
		</td>
	</tr>
<?php		
	global $wpdb;
	$results= $wpdb->get_results("SELECT `post_id` FROM $wpdb->postmeta 
								  WHERE `meta_key` LIKE 'hg_price_change_proceed' AND `meta_value` LIKE 'awaiting_response'");
	if(!empty($results))
	{?>
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label>Price Movement ALERTS:</label>
			</th>
			<td class="forminp">
				<fieldset>
					<div class="price_change_alert">
						<div>**************************************************************</div>
						<div>**** ALERT!! IMMEDIATE ACTION REQUIRED!! ****</div>
						<div>**************************************************************</div>
						<br>
<?php	$nNumAlerts= 0;
		for($i=0; $i < count($results); ++$i)
		{
			$productID= $results[$i]->post_id;
			if(empty($productID))
			{
				continue;
			}

			
			$strCurrentPrice= get_post_meta($productID, '_regular_price', true);
			if(empty($strCurrentPrice))
			{
				$strCurrentPrice= "NOT SET";
			}
			else
			{
				$strCurrentPrice= number_format((float)$strCurrentPrice, 2, '.', '');
			}

			$strProposedPrice= get_post_meta($productID, 'hg_price_change_proposed', true);
			if(empty($strProposedPrice))
			{
				$strProposedPrice= "NOT SET";
			}
			else
			{
				$strProposedPrice= number_format((float)$strProposedPrice, 2, '.', '');
			}

			$strPercChange= get_post_meta($productID, 'hg_price_change_perc', true);
			if(empty($strPercChange))
			{
				$strPercChange= "NOT SET";
			}

			$strChangeDirection= (floatval($strPercChange) > 0) ? "INCREASE" : "DROP";

			$productSKU= get_post_meta($productID, '_sku', true);
			if(empty($productSKU))
			{
				$productSKU= "NOT SET";
			}?>
						<div>SIGNIFICANT PRICE <?php echo $strChangeDirection; ?> HAS BEEN DETECTED:</div>
						<div><span>PRODUCT TITLE:</span><?php echo get_the_title($productID); ?></div>
						<div><span>PRODUCT SKU #:</span><?php echo $productSKU; ?></div>
						<div><span>CURRENT PRICE:</span>$<?php echo $strCurrentPrice; ?></div>
						<div><span>PROPOSED PRICE:</span>$<?php echo $strProposedPrice; ?></div>
						<div><span>PCT PRICE DIFFERENCE:</span><?php echo $strPercChange; ?>%</div>
						<br>
						<div>PROCEED WITH PRICE UPDATE?</div>
						<input type="hidden" name="hg_proceed_with_update_id_<?php echo (string)$nNumAlerts; ?>" value="<?php echo $productID; ?>">
						<div><input type="radio" name="hg_proceed_with_update_<?php echo (string)$nNumAlerts; ?>" value="yes">YES</div>
						<div><input type="radio" name="hg_proceed_with_update_<?php echo (string)$nNumAlerts; ?>" value="no">NO</div>
						<div><input type="radio" name="hg_proceed_with_update_<?php echo (string)$nNumAlerts; ?>" value="awaiting_response" checked="checked">AWAITING RESPONSE</div><br><br><br>
<?php		$nNumAlerts++;
		}?>
						<input type="hidden" name="hg_proceed_with_update_count" value="<?php echo $nNumAlerts; ?>">
						<br>
						<input name="save" class="button-primary woocommerce-save-button" type="submit" value="Update">
						<br><br>
						<div>**************************************************************</div>
						<div>**** ALERT!! IMMEDIATE ACTION REQUIRED!! ****</div>
						<div>**************************************************************</div>
					</div>
				</fieldset>
			</td>
		</tr>
<?php		
	}
	$a_extra_settings['price_change_settings_html']= ob_get_clean();
}



function hgUpdateObject_inventory_zeroed_out_settings(&$a_extra_settings)
{
	global $wpdb;
	$results= $wpdb->get_results("SELECT `post_id` FROM $wpdb->postmeta WHERE `meta_key` LIKE 'hg_inventory_zeroed_out'");
	if(!empty($results))
	{
		ob_start();?>
		<tr valign="top">
			<th class="titledesc" scope="row">
				<label>Inventory Zeroed Out ALERTS:</label>
			</th>
			<td class="forminp">
				<fieldset>
					<div class="price_change_alert">
						<div>**************************************************************</div>
						<div>**** ALERT!! IMMEDIATE ACTION REQUIRED!! ****</div>
						<div>**************************************************************</div>
						<br>
<?php	$nNumAlerts= 0;
		for($i=0; $i < count($results); ++$i)
		{
			$productID= $results[$i]->post_id;
			if(empty($productID))
			{
				continue;
			}

			$previousStock= get_post_meta($productID, 'hg_inventory_stock', true); 
			if(empty($previousStock))
			{
				$previousStock= "NOT SET";
			}

			$previousStockStatus= get_post_meta($productID, 'hg_inventory_stock_status', true); 
			if(empty($previousStockStatus))
			{
				$previousStockStatus= "NOT SET";
			}

			$previousBackorders= get_post_meta($productID, 'hg_inventory_backorders', true); 
			if(empty($previousBackorders))
			{
				$previousBackorders= "NOT SET";
			}

			$strReason= get_post_meta($productID, 'hg_inventory_reason', true); 
			if(empty($strReason))
			{
				$strReason= "NOT SET";
			}

			$productSKU= get_post_meta($productID, '_sku', true);
			if(empty($productSKU))
			{
				$productSKU= "NOT SET";
			}?>
						<div>INVENTORY ZEROED OUT:</div>
						<div><span>PRODUCT TITLE:</span><?php echo get_the_title($productID); ?></div>
						<div><span>PRODUCT SKU #:</span><?php echo $productSKU; ?></div>
						<div><span>REASON FOR ZEROING:</span><?php echo $strReason; ?></div>
						<div><span>PREVIOUS STOCK:</span><?php echo $previousStock; ?></div>
						<div><span>PREVIOUS STOCK STATUS:</span><?php echo $previousStockStatus; ?></div>
						<div><span>PREVIOUS BACKORDER:</span><?php echo $previousBackorders; ?></div>
						<br>
						<div>RESTORE INVENTORY DATA?</div>
						<input type="hidden" name="hg_restore_inventory_id_<?php echo (string)$nNumAlerts; ?>" value="<?php echo $productID; ?>">
						<div><input type="radio" name="hg_restore_inventory_<?php echo (string)$nNumAlerts; ?>" value="yes">YES</div>
						<div><input type="radio" name="hg_restore_inventory_<?php echo (string)$nNumAlerts; ?>" value="no" checked="checked">NOT YET</div>
						<div>REMEMBER to fix the issue causing this inventory to be zeroed out before restoring!</div>
						<br><br><br>
<?php		$nNumAlerts++;
		}?>
						<input type="hidden" name="hg_restore_inventory_count" value="<?php echo $nNumAlerts; ?>">
						<br>
						<input name="save" class="button-primary woocommerce-save-button" type="submit" value="Update">
						<br><br>
						<div>**************************************************************</div>
						<div>**** ALERT!! IMMEDIATE ACTION REQUIRED!! ****</div>
						<div>**************************************************************</div>
					</div>
				</fieldset>
			</td>
		</tr>
<?php		
	}
	$a_extra_settings['inventory_zeroed_out_html']= ob_get_clean();
}



function hgUpdateObject_product_markup_settings(&$a_extra_settings)
{
	//Product Markup Settings HTML
	ob_start();?>
	<tr valign="top">
		<th class="titledesc" scope="row">
			<label>Global CC/PayPal Markup %:</label>
		</th>
		<td class="forminp">
			<fieldset>
				<div>
					<input type="text" class="hg_cc_paypal_markup_percentage" name="hg_cc_paypal_markup_percentage" value="<?php echo get_option('hg_cc_paypal_markup_percentage');?>">%
					<div>
						This percentage should reflect, as closely as possible, our actual credit card / PayPal fees.  FOR EXAMPLE:  If PayPal charges us 3%, back into our actual cost by dividing 1 / .97 to get 3.09 or 3.1%, etc.
					</div>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top" class="product_edit_screen_settings">
		<th class="titledesc" scope="row">
			<label>Product Edit Screen:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div>Markup Text 1: </div>
					<div class="email_label"></div>
					<textarea name="hg_markup_text_1" rows="10" cols="62"><?php echo stripslashes(get_option('hg_markup_text_1'));?></textarea>
				</div>
				<div>
					<div>Markup Text 2: </div>
					<div class="email_label"></div>
					<textarea name="hg_markup_text_2" rows="10" cols="62"><?php echo stripslashes(get_option('hg_markup_text_2'));?></textarea>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top" class="shipping_class_markup">
		<th class="titledesc" scope="row">
			<label>Shipping Class Markup:</label>
		</th>
		<td class="forminp email">
			<fieldset>
<?php		// Shipping Class Markups
			global $wpdb;

			// Get Shipping Class term and term_taxonomy IDs so we can lookup the names and apply markup to all products with these shipping_classes.
			$aResults= $wpdb->get_results("SELECT `term_taxonomy_id`,`term_id` FROM $wpdb->term_taxonomy WHERE `taxonomy` LIKE 'product_shipping_class'");
			if(empty($aResults))
			{?>
				<div>Couldn't find any Product Shipping Classes.</div>
<?php		}
			else
			{
				$aShippingClassNames= array();
				for($i= 0; $i < count($aResults); $i++)
				{
					// Get Shipping Class Name for term_id.
					$aShippingClassName= $wpdb->get_results($wpdb->prepare("SELECT `name` FROM $wpdb->terms WHERE `term_id` = %d", $aResults[$i]->term_id));
					if(empty($aShippingClassName))
					{
						continue;
					}

					$strShippingClassName= $aShippingClassName[0]->name;
					if(empty($strShippingClassName))
					{
						continue;
					}?>
					<div class="hg_shipping_class">
						<div>
							<div class="email_label">Shipping Class:</div>
							<div class="hg_shipping_class_name"><?php echo $strShippingClassName; ?></div>
						</div>
						<div>
							<div class="email_label">Total Estimated Cost:</div>
							<input type="text" name="hg_shipping_class_markup_<?php echo $i;?>" value="<?php echo get_term_meta($aResults[$i]->term_id, 'hg_shipping_class_markup', true); ?>">
							<input type="hidden" name="hg_shipping_class_markup_term_taxonomy_id_<?php echo $i;?>" value="<?php echo $aResults[$i]->term_taxonomy_id; ?>">
							<input type="hidden" name="hg_shipping_class_markup_term_id_<?php echo $i;?>" value="<?php echo $aResults[$i]->term_id; ?>">
						</div>
						<div>
							This value should reflect the total estimated cost for packing materials, shipping and insurance.  Changes to this field will automatically update the "Markup 2" field on the Edit screen for all products in this shipping class.
						</div>
					</div>
<?php			}?>
					<input type="hidden" name="hg_shipping_class_markup_count" value="<?php echo count($aResults); ?>">
<?php		}?>		
			</fieldset>
		</td>
	</tr>
<?php		
	$a_extra_settings['product_markup_settings']= ob_get_clean();
}


function hgUpdateObject_notification_settings(&$a_extra_settings)
{
	// Notification Settings HTML
	ob_start();?>
	<tr valign="top">
		<th class="titledesc" scope="row">
			<label>Cell Carrier Gateway Domains:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div class="email_label">AllTel:</div>
					<input type="text" name="hg_carrier_gateway_alltel" value="<?php echo get_option('hg_carrier_gateway_alltel');?>"><br>
					<div class="email_label">At&amp;t:</div>
					<input type="text" name="hg_carrier_gateway_att" value="<?php echo get_option('hg_carrier_gateway_att');?>"><br>
					<div class="email_label">Boost Mobile:</div>
					<input type="text" name="hg_carrier_gateway_boost" value="<?php echo get_option('hg_carrier_gateway_boost');?>"><br>
					<div class="email_label">Cricket:</div>
					<input type="text" name="hg_carrier_gateway_cricket" value="<?php echo get_option('hg_carrier_gateway_cricket');?>"><br>
					<div class="email_label">Sprint:</div>
					<input type="text" name="hg_carrier_gateway_sprint" value="<?php echo get_option('hg_carrier_gateway_sprint');?>"><br>
					<div class="email_label">T-Mobile:</div>
					<input type="text" name="hg_carrier_gateway_tmobile" value="<?php echo get_option('hg_carrier_gateway_tmobile');?>"><br>
					<div class="email_label">US Cellular:</div>
					<input type="text" name="hg_carrier_gateway_uscelular" value="<?php echo get_option('hg_carrier_gateway_uscelular');?>"><br>
					<div class="email_label">Verizon:</div>
					<input type="text" name="hg_carrier_gateway_verizon" value="<?php echo get_option('hg_carrier_gateway_verizon');?>"><br>
					<div class="email_label">Virgin Mobile:</div>
					<input type="text" name="hg_carrier_gateway_virgin" value="<?php echo get_option('hg_carrier_gateway_virgin');?>">
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top" class="notification_settings">
		<th class="titledesc" scope="row">
			<label>Notification for empty Min Price:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div class="email_label">Message From: </div>
					<input type="text" name="hg_email_min_price_from" value="<?php echo get_option('hg_email_min_price_from');?>">
				</div>
				<div>
					<div class="email_label">Email To: </div>
					<input type="text" name="hg_email_min_price_to" value="<?php echo get_option('hg_email_min_price_to');?>">
				</div>
				<div>
					<div class="email_label">Cell Number To:</div>
					<input type="text" name="hg_sms_min_price_to" value="<?php echo get_option('hg_sms_min_price_to');?>">
				</div>
				<div>
					<div class="email_label"></div>
					<span>NUMBERS ONLY!!  NO periods, hyphens or parentheses.</span>
				</div>
				<div>
					<div class="email_label">Cell Carrier:</div>
					<select name='hg_sms_min_price_carrier'>
						<option value="none">Select Carrier</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'alltel') ? 'selected="selected"' : ""; ?> value="alltel">AllTel</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'att') ? 'selected="selected"' : ""; ?> value="att">At&amp;t</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'boost') ? 'selected="selected"' : ""; ?> value="boost">Boost Mobile</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'cricket') ? 'selected="selected"' : ""; ?> value="cricket">Cricket</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'sprint') ? 'selected="selected"' : ""; ?> value="sprint">Sprint</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'tmobile') ? 'selected="selected"' : ""; ?> value="tmobile">T-Mobile</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'uscellular') ? 'selected="selected"' : ""; ?> value="uscellular">US Cellular</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'verizon') ? 'selected="selected"' : ""; ?> value="verizon">Verizon</option>
						<option <?php echo (get_option('hg_sms_min_price_carrier') == 'virgin') ? 'selected="selected"' : ""; ?> value="virgin">Virgin Mobile</option>
					</select> 
				</div>
				<div>
					<div class="email_label">Message Subject: (<i>Metal</i> Min Price Empty) </div>
					<input type="text" name="hg_email_min_price_subject" value="<?php echo get_option('hg_email_min_price_subject');?>">
				</div>
				<div>
					<div>Message Body: </div>
					<div class="email_label"></div>
					<textarea name="hg_email_min_price_body" rows="10" cols="62"><?php echo get_option('hg_email_min_price_body');?></textarea>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top">
		<th class="titledesc" scope="row">
			<label>Notification for empty Metal Weight:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div class="email_label">Message From: </div>
					<input type="text" name="hg_email_metal_weight_from" value="<?php echo get_option('hg_email_metal_weight_from');?>">
				</div>
				<div>
					<div class="email_label">Email To: </div>
					<input type="text" name="hg_email_metal_weight_to" value="<?php echo get_option('hg_email_metal_weight_to');?>">
				</div>
				<div>
					<div class="email_label">Cell Number To:</div>
					<input type="text" name="hg_sms_metal_weight_to" value="<?php echo get_option('hg_sms_metal_weight_to');?>">
				</div>
				<div>
					<div class="email_label"></div>
					<span>NUMBERS ONLY!!  NO periods, hyphens or parentheses.</span>
				</div>
				<div>
					<div class="email_label">Cell Carrier:</div>
					<select name='hg_sms_metal_weight_carrier'>
						<option value="none">Select Carrier</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'alltel') ? 'selected="selected"' : ""; ?> value="alltel">AllTel</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'att') ? 'selected="selected"' : ""; ?> value="att">At&amp;t</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'boost') ? 'selected="selected"' : ""; ?> value="boost">Boost Mobile</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'cricket') ? 'selected="selected"' : ""; ?> value="cricket">Cricket</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'sprint') ? 'selected="selected"' : ""; ?> value="sprint">Sprint</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'tmobile') ? 'selected="selected"' : ""; ?> value="tmobile">T-Mobile</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'uscellular') ? 'selected="selected"' : ""; ?> value="uscellular">US Cellular</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'verizon') ? 'selected="selected"' : ""; ?> value="verizon">Verizon</option>
						<option <?php echo (get_option('hg_sms_metal_weight_carrier') == 'virgin') ? 'selected="selected"' : ""; ?> value="virgin">Virgin Mobile</option>
					</select> 
				</div>
				<div>
					<div class="email_label">Message Subject: (ProductID) - </div>
					<input type="text" name="hg_email_metal_weight_subject" value="<?php echo get_option('hg_email_metal_weight_subject');?>">
				</div>
				<div>
					<div>Message Body: </div>
					<div class="email_label"></div>
					<textarea name="hg_email_metal_weight_body" rows="10" cols="62"><?php echo get_option('hg_email_metal_weight_body');?></textarea>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top">
		<th class="titledesc" scope="row">
			<label>Notification for empty Regular Price:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div class="email_label">Message From: </div>
					<input type="text" name="hg_email_regular_price_from" value="<?php echo get_option('hg_email_regular_price_from');?>">
				</div>
				<div>
					<div class="email_label">Email To: </div>
					<input type="text" name="hg_email_regular_price_to" value="<?php echo get_option('hg_email_regular_price_to');?>">
				</div>
				<div>
					<div class="email_label">Cell Number To:</div>
					<input type="text" name="hg_sms_regular_price_to" value="<?php echo get_option('hg_sms_regular_price_to');?>">
				</div>
				<div>
					<div class="email_label"></div>
					<span>NUMBERS ONLY!!  NO periods, hyphens or parentheses.</span>
				</div>
				<div>
					<div class="email_label">Cell Carrier:</div>
					<select name='hg_sms_regular_price_carrier'>
						<option value="none">Select Carrier</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'alltel') ? 'selected="selected"' : ""; ?> value="alltel">AllTel</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'att') ? 'selected="selected"' : ""; ?> value="att">At&amp;t</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'boost') ? 'selected="selected"' : ""; ?> value="boost">Boost Mobile</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'cricket') ? 'selected="selected"' : ""; ?> value="cricket">Cricket</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'sprint') ? 'selected="selected"' : ""; ?> value="sprint">Sprint</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'tmobile') ? 'selected="selected"' : ""; ?> value="tmobile">T-Mobile</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'uscellular') ? 'selected="selected"' : ""; ?> value="uscellular">US Cellular</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'verizon') ? 'selected="selected"' : ""; ?> value="verizon">Verizon</option>
						<option <?php echo (get_option('hg_sms_regular_price_carrier') == 'virgin') ? 'selected="selected"' : ""; ?> value="virgin">Virgin Mobile</option>
					</select> 
				</div>
				<div>
					<div class="email_label">Message Subject: (ProductID) - </div>
					<input type="text" name="hg_email_regular_price_subject" value="<?php echo get_option('hg_email_regular_price_subject');?>">
				</div>
				<div>
					<div>Message Body: </div>
					<div class="email_label"></div>
					<textarea name="hg_email_regular_price_body" rows="10" cols="62"><?php echo get_option('hg_email_regular_price_body');?></textarea>
				</div>
			</fieldset>
		</td>
	</tr>
	<tr valign="top">
		<th class="titledesc" scope="row">
			<label>Notification for Price Movement:</label>
		</th>
		<td class="forminp email">
			<fieldset>
				<div>
					<div class="email_label">Message From: </div>
					<input type="text" name="hg_email_price_change_from" value="<?php echo get_option('hg_email_price_change_from');?>">
				</div>
				<div>
					<div class="email_label">Email To: </div>
					<input type="text" name="hg_email_price_change_to" value="<?php echo get_option('hg_email_price_change_to');?>">
				</div>
				<div>
					<div class="email_label">Cell Number To:</div>
					<input type="text" name="hg_sms_price_change_to" value="<?php echo get_option('hg_sms_price_change_to');?>">
				</div>
				<div>
					<div class="email_label"></div>
					<span>NUMBERS ONLY!!  NO periods, hyphens or parentheses.</span>
				</div>
				<div>
					<div class="email_label">Cell Carrier:</div>
					<select name='hg_sms_price_change_carrier'>
						<option value="none">Select Carrier</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'alltel') ? 'selected="selected"' : ""; ?> value="alltel">AllTel</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'att') ? 'selected="selected"' : ""; ?> value="att">At&amp;t</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'boost') ? 'selected="selected"' : ""; ?> value="boost">Boost Mobile</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'cricket') ? 'selected="selected"' : ""; ?> value="cricket">Cricket</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'sprint') ? 'selected="selected"' : ""; ?> value="sprint">Sprint</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'tmobile') ? 'selected="selected"' : ""; ?> value="tmobile">T-Mobile</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'uscellular') ? 'selected="selected"' : ""; ?> value="uscellular">US Cellular</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'verizon') ? 'selected="selected"' : ""; ?> value="verizon">Verizon</option>
						<option <?php echo (get_option('hg_sms_price_change_carrier') == 'virgin') ? 'selected="selected"' : ""; ?> value="virgin">Virgin Mobile</option>
					</select> 
				</div>
				<div>
					<div class="email_label">Message Subject:</div>
					<input type="text" name="hg_email_price_change_subject" value="<?php echo get_option('hg_email_price_change_subject');?>">
				</div>
				<div>
					<div>Message Body: </div>
					<div class="email_label"></div>
					<span>Price Movement Notifications have their bodies generated by the cronjob.</span>
				</div>
			</fieldset>
		</td>
	</tr>
<?php		
	$a_extra_settings['notification_form_html']= ob_get_clean();
}



add_action( 'woocommerce_settings_save_integration', 'hg_woo_settings_save');
function hg_woo_settings_save()
{
	// Save Price Change Percentage.
	update_option('hg_price_change_percentage', $_POST['hg_price_change_percentage'], false);


	// Save Price Change Alert settings
	if(isset($_POST['hg_proceed_with_update_count']))
	{
		for($i= 0; $i < $_POST['hg_proceed_with_update_count']; $i++)
		{
			$productID= (int)$_POST["hg_proceed_with_update_id_$i"];
			$strResponse= $_POST["hg_proceed_with_update_$i"];
			update_post_meta($productID, 'hg_price_change_proceed', $strResponse);
		}
	}


	// Restore Inventory
	if(isset($_POST['hg_restore_inventory_count']))
	{
		for($i= 0; $i < $_POST['hg_restore_inventory_count']; $i++)
		{
			$productID= (int)$_POST["hg_restore_inventory_id_$i"];
			$strResponse= $_POST["hg_restore_inventory_$i"];
			if(empty($productID) || empty($strResponse))
			{
				continue;
			}

			if($strResponse == "yes")
			{
				update_post_meta($productID, '_stock', get_post_meta($productID, 'hg_inventory_stock', true));
				update_post_meta($productID, '_stock_status', get_post_meta($productID, 'hg_inventory_stock_status', true));
				update_post_meta($productID, '_backorders', get_post_meta($productID, 'hg_inventory_backorders', true));

				delete_post_meta($productID, 'hg_inventory_reason');
				delete_post_meta($productID, 'hg_inventory_zeroed_out');
			}
		}
	}


	// Save Min Price fields.
	update_option('hg_min_price_gold', $_POST['hg_min_price_gold'], false);
	update_option('hg_min_price_silver', $_POST['hg_min_price_silver'], false);
	update_option('hg_min_price_platinum', $_POST['hg_min_price_platinum'], false);
	update_option('hg_min_price_palladium', $_POST['hg_min_price_palladium'], false);


	// Save Product Markup Settings
	update_option("hg_cc_paypal_markup_percentage", $_POST["hg_cc_paypal_markup_percentage"], false);
	update_option('hg_markup_text_1', $_POST['hg_markup_text_1'], false);
	update_option('hg_markup_text_2', $_POST['hg_markup_text_2'], false);

	// Save Shipping Class Markups
	if(isset($_POST['hg_shipping_class_markup_count']))
	{
		// Set this flag to false so that the metals feed doesn't process markups until below is done.
		update_option('hg_shipping_class_markup_needs_update', false, false);

		$nShippingClassMarkupNeedsUpdate_Count= 0;
		$nShippingClassMarkupCount= (int)$_POST["hg_shipping_class_markup_count"];
		for($i= 0; $i < $nShippingClassMarkupCount; $i++)
		{
			$termTaxonomyID= $_POST["hg_shipping_class_markup_term_taxonomy_id_$i"];
			$termID= $_POST["hg_shipping_class_markup_term_id_$i"];
			if(!empty($termID) && !empty($termTaxonomyID))
			{
				$markupValue= (float)$_POST["hg_shipping_class_markup_$i"];
				if(empty($markupValue))
				{
					$markupValue= 0;
				}

				$storedMarkupValue= (float)get_term_meta($termID, "hg_shipping_class_markup", true);

				if(empty($storedMarkupValue) || ($markupValue != $storedMarkupValue))
				{
					update_term_meta($termID, "hg_shipping_class_markup", $markupValue);
					update_option("hg_shipping_class_markup_term_taxonomy_id_$nShippingClassMarkupNeedsUpdate_Count", $termTaxonomyID, false);
					update_option("hg_shipping_class_markup_term_id_$nShippingClassMarkupNeedsUpdate_Count", $termID, false);
					$nShippingClassMarkupNeedsUpdate_Count++;
				}
			}
		}

		if($nShippingClassMarkupNeedsUpdate_Count > 0)
		{
			update_option('hg_shipping_class_markup_needs_update', true, false);
			update_option('hg_shipping_class_markup_count', $nShippingClassMarkupNeedsUpdate_Count, false);
		}
	}



	// Save Notification Settings.
	update_option('hg_carrier_gateway_alltel', $_POST['hg_carrier_gateway_alltel'], false);
	update_option('hg_carrier_gateway_att', $_POST['hg_carrier_gateway_att'], false);
	update_option('hg_carrier_gateway_boost', $_POST['hg_carrier_gateway_boost'], false);
	update_option('hg_carrier_gateway_cricket', $_POST['hg_carrier_gateway_cricket'], false);
	update_option('hg_carrier_gateway_sprint', $_POST['hg_carrier_gateway_sprint'], false);
	update_option('hg_carrier_gateway_tmobile', $_POST['hg_carrier_gateway_tmobile'], false);
	update_option('hg_carrier_gateway_uscelular', $_POST['hg_carrier_gateway_uscelular'], false);
	update_option('hg_carrier_gateway_verizon', $_POST['hg_carrier_gateway_verizon'], false);
	update_option('hg_carrier_gateway_virgin', $_POST['hg_carrier_gateway_virgin'], false);

	update_option('hg_email_min_price_from', $_POST['hg_email_min_price_from'], false);
	update_option('hg_email_min_price_to', $_POST['hg_email_min_price_to'], false);
	update_option('hg_sms_min_price_to', $_POST['hg_sms_min_price_to'], false);
	update_option('hg_sms_min_price_carrier', $_POST['hg_sms_min_price_carrier'], false);
	update_option('hg_email_min_price_subject', $_POST['hg_email_min_price_subject'], false);
	update_option('hg_email_min_price_body', $_POST['hg_email_min_price_body'], false);

	update_option('hg_email_metal_weight_from', $_POST['hg_email_metal_weight_from'], false);
	update_option('hg_email_metal_weight_to', $_POST['hg_email_metal_weight_to'], false);
	update_option('hg_sms_metal_weight_to', $_POST['hg_sms_metal_weight_to'], false);
	update_option('hg_sms_metal_weight_carrier', $_POST['hg_sms_metal_weight_carrier'], false);
	update_option('hg_email_metal_weight_subject', $_POST['hg_email_metal_weight_subject'], false);
	update_option('hg_email_metal_weight_body', $_POST['hg_email_metal_weight_body'], false);

	update_option('hg_email_regular_price_from', $_POST['hg_email_regular_price_from'], false);
	update_option('hg_email_regular_price_to', $_POST['hg_email_regular_price_to'], false);
	update_option('hg_sms_regular_price_to', $_POST['hg_sms_regular_price_to'], false);
	update_option('hg_sms_regular_price_carrier', $_POST['hg_sms_regular_price_carrier'], false);
	update_option('hg_email_regular_price_subject', $_POST['hg_email_regular_price_subject'], false);
	update_option('hg_email_regular_price_body', $_POST['hg_email_regular_price_body'], false);

	update_option('hg_email_price_change_from', $_POST['hg_email_price_change_from'], false);
	update_option('hg_email_price_change_to', $_POST['hg_email_price_change_to'], false);
	update_option('hg_sms_price_change_to', $_POST['hg_sms_price_change_to'], false);
	update_option('hg_sms_price_change_carrier', $_POST['hg_sms_price_change_carrier'], false);
	update_option('hg_email_price_change_subject', $_POST['hg_email_price_change_subject'], false);
}


add_action( 'save_post', 'hg_product_meta_box_save', 11, 1 );
function hg_product_meta_box_save($post_id)
{	
	if($_POST['post_type'] == "product")
	{
		// Update the markup2 value with the shipping class markup from the Integration page. 
		if(isset($_POST["product_shipping_class"]))
		{
			$productShippingClass_termID= $_POST["product_shipping_class"];
			if(empty($productShippingClass_termID))
			{
				return;
			}

			$markupValue= get_term_meta($productShippingClass_termID, "hg_shipping_class_markup", true);
			if(empty($markupValue))
			{
				return;
			}

			update_post_meta($post_id, "_markup_rate_2", $markupValue);
		}
	}
}


add_action( 'wp_ajax_hg_save_bulk_edit', 'hg_save_bulk_edit' );
function hg_save_bulk_edit() 
{
	$post_ids = ( isset( $_POST[ 'post_ids' ] ) && !empty( $_POST[ 'post_ids' ] ) ) ? $_POST[ 'post_ids' ] : array();
	$shipping_class_slug = ( isset( $_POST[ 'shipping_class' ] ) && !empty( $_POST[ 'shipping_class' ] ) ) ? $_POST[ 'shipping_class' ] : NULL;
	
	if(!empty( $post_ids ) && is_array( $post_ids ) && !empty( $shipping_class_slug )) 
	{
		// get term_id from shipping class slug.
		global $wpdb;
		$aResults= $wpdb->get_results($wpdb->prepare("SELECT `term_id` FROM $wpdb->terms WHERE `slug` LIKE %s", $shipping_class_slug));
		if(empty($aResults))
		{
			return;
		}
		$productShippingClass_termID= $aResults[0]->term_id;

		// Get the markup value with the term_id.
		$markupValue= get_term_meta($productShippingClass_termID, "hg_shipping_class_markup", true);
		if(empty($markupValue))
		{
			return;
		}

		// Update the markup2 value with the shipping class markup from the Integration page for each product in the bulk edit update. 
		foreach($post_ids as $post_id) 
		{
			update_post_meta($post_id, "_markup_rate_2", $markupValue);
		}
	}
}
?>