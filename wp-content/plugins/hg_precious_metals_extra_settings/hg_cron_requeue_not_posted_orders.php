<?php
/*
NAME: Cron Requeue Not Posted Orders

DESCRIPTION: Looks at the last 5 orders and requeues the orders that are determined to be Not Posted.

AUTHOR: Horton Group, Dev: Mark Freeman
VERSION: v1.0
*/

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
$parentDir= $parse_uri[0];

if(file_exists($parentDir."wp-load.php"))
{
    require $parentDir."wp-load.php";
    $pUpdate_Inventory_And_Price_For_Ebay= new Requeue_Not_Posted_Orders();
}
else
{
	syslog(LOG_ERR, 'CRON ERROR wp-load not found');
}


class Requeue_Not_Posted_Orders
{
	function __construct() 
	{
		$this->requeue_not_posted_orders();
	}

	// Looks at the last 5 orders and uses code gotten from plugins/woocommerce-quickbooks-pos-2013/admin/interface.php > sod_qbpos_requeue_order() to requeue the orders that are determined to be Not Posted (uses code from sod_qbconnector_meta_box() to determine the Not Posted).
	public function requeue_not_posted_orders()
	{
		global $wpdb;
		$aLastOrders= $wpdb->get_results("SELECT `ID` FROM $wpdb->posts WHERE `post_type` = 'shop_order' AND `post_status` = 'wc-completed' ORDER BY `post_date` DESC LIMIT 5");
		if(count($aLastOrders) <= 0)
		{
			syslog(LOG_ERRG, "requeue_not_posted_orders:: if(count(aLastOrders) <= 0)");
			return;
		}

		for($i= 0; $i < count($aLastOrders); ++$i)
		{
			$order_id= $aLastOrders[$i]->ID;
			$qbpos_data= get_post_meta($order_id, '_qbpos_data', true);
			$auto_post_fail= get_post_meta($order_id,'_qbpos_auto_add_failed', true);
			$auto_post_fail= ($auto_post_fail) ? $auto_post_fail : false;
			$requeued_status= get_post_meta($order_id,'_qbpos_order_requeued', true);
			$requeued_status= ($requeued_status) ? $requeued_status : false;

			if($qbpos_data && $requeued_status != "yes")
			{
				// Do Nothing
				// syslog(LOG_DEBUG, "requeue_not_posted_orders:: ORDER POST ID= ". $order_id. " POSTED");
			}
			elseif($auto_post_fail == 'yes' && $requeued_status != "yes")
			{
				// Do Nothing
				// syslog(LOG_DEBUG, "requeue_not_posted_orders:: ORDER POST ID= ". $order_id. " FAILED");
			}
			elseif($requeued_status == 'yes')
			{
				// Do Nothing
				// syslog(LOG_DEBUG, "requeue_not_posted_orders:: ORDER POST ID= ". $order_id. " REQUEUED");
			}
			else
			{
				syslog(LOG_DEBUG, "requeue_not_posted_orders:: ORDER POST ID= ". $order_id. " NOT POSTED");

				$cust_list_id = get_post_meta($order_id,'_customerPOSListID', true);
				$quickbooks = new SOD_QuickbooksPOS_Data;
				$quickbooks->ID = $order_id;
				$Queue = new QuickBooks_WebConnector_Queue($quickbooks->dsn);
				update_post_meta($order_id, '_qbpos_order_requeued','yes');
				if($quickbooks->settings->post_orders =='on')
				{
					if($cust_list_id)
					{
						$Queue->enqueue('QBPOS_ADD_RECEIPT',$order_id,8,  NULL, $quickbooks->user);
					}
					else
					{
						$Queue->enqueue('QBPOS_CUST_QUERY',$order_id,6,  NULL, $quickbooks->user);
					}
				} 

				unset($quickbooks);
				unset($Queue);
			}
		}
	}
}
?>