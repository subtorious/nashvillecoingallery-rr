function fedex_label_add_package(){
	packages = Math.floor(jQuery('#shipment_packages').val()) + 1;
	var fedex_label_package = '<div id="shipment_package_' + packages + '" style="margin-top:15px"><h4>Package.</h4><p><button type="button" class="button-secondary" onClick="fedex_label_remove_package(' + packages + ');">Remove package</button></p><p><label for="shipment_preset">Save as preset</label><input type="checkbox" name="shipment_preset[]" value="1" style="margin-left:0.5em" /></p><ul><li style="float:left;margin-right:5px"><label for="shipment_weight">Weight<span> *</span>:</label><br /><input type="text" name="shipment_weight[]" size="5" /> lbs.</li><li style="float:left;margin-right:5px"><label for="shipment_height">Height<span> *</span>:</label><br /><input type="text" name="shipment_height[]" size="5" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_length">Length<span> *</span>:</label><br /><input type="text" name="shipment_length[]" size="5" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_width">Width<span> *</span>:</label><br /><input type="text" name="shipment_width[]" size="5" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_value">Declared Value<span> *</span>:</label><br /><input type="text" name="shipment_value[]" size="5" /> USD dollars.</li><br style="clear:both" /></ul></div>';
	jQuery('#shipment_packages_container').append(fedex_label_package);
	jQuery('#shipment_packages').val(packages);
	return false;
}



function fedex_label_remove_package(package){
	jQuery('#shipment_package_' + package).remove();
}



jQuery(document).ready(function(){
	jQuery('#shipment_packages_preset').change(function(){
		var id = jQuery(this).val();
		packages = Math.floor(jQuery('#shipment_packages').val()) + 1;
		var fedex_label_package = '<div id="shipment_package_' + packages + '" style="margin-top:15px"><h4>Package.</h4><p><button type="button" class="button-secondary" onClick="fedex_label_remove_package(' + packages + ');">Remove package</button></p><p><label for="shipment_preset">Save as preset</label><input type="checkbox" name="shipment_preset[]" value="1" style="margin-left:0.5em" /></p><ul><li style="float:left;margin-right:5px"><label for="shipment_weight">Weight<span> *</span>:</label><br /><input type="text" name="shipment_weight[]" size="5" value="' + shipment_presets[id]['weight'] + '" /> lbs.</li><li style="float:left;margin-right:5px"><label for="shipment_height">Height<span> *</span>:</label><br /><input type="text" name="shipment_height[]" size="5" value="' + shipment_presets[id]['height'] + '" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_length">Length<span> *</span>:</label><br /><input type="text" name="shipment_length[]" size="5" value="' + shipment_presets[id]['length'] + '" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_width">Width<span> *</span>:</label><br /><input type="text" name="shipment_width[]" size="5" value="' + shipment_presets[id]['width'] + '" /> in.</li><li style="float:left;margin-right:5px"><label for="shipment_value">Declared Value<span> *</span>:</label><br /><input type="text" name="shipment_value[]" size="5" /> USD dollars.</li><br style="clear:both" /></ul></div>';
		jQuery('#shipment_packages_container').append(fedex_label_package);
		jQuery('#shipment_packages').val(packages);
		return false;
	});
});