<?php
require_once(dirname(__FILE__) . '/includes/fedex_shipper.php');
function fedex_labels_admin() {
	$my_page = add_submenu_page( 'woocommerce','Fedex Labels', 'Fedex Label', 'manage_woocommerce', 'fedex-create-shipment', 'fedex_create_shipment' );
	add_action( 'load-' . $my_page, 'fedex_label_load_admin_js' );
	
	$my_page = add_submenu_page( 'woocommerce','Fedex Labels Config', 'Fedex Label Config', 'manage_woocommerce', 'fedex-labels-config', 'fedex_label_config' );
	add_action( 'load-' . $my_page, 'fedex_label_load_admin_js' );
	wp_register_style( 'myPluginStylesheet', plugins_url('/css/style.css', __FILE__) );
	wp_enqueue_style( 'myPluginStylesheet' );
}
add_action( 'admin_menu', 'fedex_labels_admin' );


// This function is only called when our plugin's page loads!
function fedex_label_load_admin_js(){
	// Unfortunately we can't just enqueue our scripts here - it's too early. So register against the proper action hook to do it
	add_action( 'admin_enqueue_scripts', 'fedex_label_enqueue_admin_js' );
}

function fedex_label_enqueue_admin_js(){	
	wp_deregister_script('jquery');
    wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js", false, null);
    wp_enqueue_script('jquery');
	// Isn't it nice to use dependencies and the already registered core js files?
	wp_enqueue_script( 'fedex-label-admin-script', plugins_url('/js/admin.js',__FILE__), array( 'jquery' ) );
}

function fedex_create_shipment() {
	if ( !current_user_can( 'manage_woocommerce' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	require_once(dirname(__FILE__) . '/templates/admin-index.php');
}

function fedex_label_config(){
	if ( !current_user_can( 'manage_woocommerce' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	require_once(dirname(__FILE__) . '/templates/admin-config.php');
}


function woocommerce_pvit_fdx_label_create_box_content($column) {
global $post;
    $order = new WC_Order( $post->ID );
    
    switch ($column) {
      case "order_actions" :

  			?><p>
  				<a class="button" href="<?php echo wp_nonce_url(admin_url('admin.php?page=fedex-create-shipment&order_id='.$post->ID), 'print-fdx-label'); ?>"><?php _e('Fedex Label', 'woocommerce-pvit-fdx'); ?></a>
  				
  			</p><?php

  		  break;
    }
	}
	
	add_action('manage_shop_order_posts_custom_column', 'woocommerce_pvit_fdx_label_create_box_content', 4);	
?>