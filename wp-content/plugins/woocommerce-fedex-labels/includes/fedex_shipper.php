<?php
session_start();
require_once (dirname ( __FILE__ ) . '/configure.php');
require_once (dirname ( __FILE__ ) . '/library/fedex-common.php5');

class pvit_woo_fedex_shipper {
	private $_order;
	private $_client;
	private $_request = array ();
	private $_include_path = '';
	private $_save_path = '';
	private $_save_url = '';
	private $_wsdl = '';

	public function __construct() {
		$this->_include_path = plugin_dir_path ( __FILE__ );
		$this->_save_path = plugin_dir_path ( __FILE__ ) . '/../generated_labels/';
		$this->_save_url = plugin_dir_url(dirname(__FILE__)) . 'generated_labels/';
		$fdx_mode =  get_option('pvit_fedex_shipper_test');		
		if($fdx_mode == 1){
			$this->_wsdl = $this->_include_path . '/wsdl/ShipService_v13_test.wsdl';
		}else{
			$this->_wsdl = $this->_include_path . '/wsdl/ShipService_v13.wsdl';
		}

		ini_set("soap.wsdl_cache_enabled", "0");

		$this->_client = new SoapClient ($this->_wsdl, array('trace' => 1));
		$this->initRequest();
	}

	private function initRequest() {
		$this->_request['WebAuthenticationDetail'] = array (
				'UserCredential' => array (
						'Key' => getProperty ( 'key' ),
						'Password' => getProperty ( 'password' ) 
				) 
		);

		$this->_request['ClientDetail'] = array(
				'AccountNumber' => getProperty('shipaccount'),
				'MeterNumber' => getProperty('meter')
		);

		

		//$this->_request['TransactionDetail'] = array('CustomerTransactionId' => '*** Express Domestic Shipping Request v10 using PHP ***');
		$this->_request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic Shipping Request v13 using PHP ***');

		$this->_request['Version'] = array(
			'ServiceId' => 'ship', 
			'Major' => '13', 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
	}

	public function getLabel($shipping_info) {
	$totalboxes = count($shipping_info['items']);


    $_SESSION['shipping_info'] = $shipping_info;

	if ($totalboxes > 1) {  //if we have more than 1 box
		//$output = "<script>console.log( 'totalboxes  : " . $totalboxes . "' );</script>"; 	echo $output;
		//$output = "<script>console.log( 'Debug Objects: " . implode( ',', $this->_request['TransactionDetail']) . "' );</script>"; echo $output;
		//$output = "<script>console.log( 'TransactionDetail  : " . $this->_request['TransactionDetail'] . "' );</script>"; 	echo $output;


$this->_request['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic MPS Shipping Request v13 - Master using PHP ***');

$this->_request['RequestedShipment'] = array(
	'ShipTimestamp' => date('c'),
	'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
	'ServiceType' => $shipping_info['service_type'], // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
	'PackagingType' => $shipping_info['packaging_type'], // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
	'Shipper' =>  $this->addShipper(),
	'Recipient' => $this->addRecipient(),
	'ShippingChargesPayment' =>  $this->addShippingChargesPayment(),
	'LabelSpecification' =>  $this->addLabelSpecification(), 
	'RateRequestTypes' => array('ACCOUNT'), // valid values ACCOUNT and LIST
	'RateRequestTypes' => array('LIST'), // valid values ACCOUNT and LIST
	'PackageCount' => count($shipping_info['items']),
	//'PackageDetail' => 'INDIVIDUAL_PACKAGES',  //PACKAGE_GROUPS  INDIVIDUAL_PACKAGES                                    
	'RequestedPackageLineItems' => array(
		'0' =>  $packageLineItem = array(
			'SequenceNumber'=>1,
			'GroupPackageCount'=>1,
			'InsuredValue' => array(
				'Amount' => $shipping_info['customs_value']['amount'], 
				'Currency' => $shipping_info['customs_value']['currency']
			),
			'Weight' => array(
				'Value' => $shipping_info['items'][0]["weight"],
				'Units' => $shipping_info['weight_unit']
			),
			'Dimensions' => array(
				'Length' => $shipping_info['items'][0]["length"],
				'Width' => $shipping_info['items'][0]["width"],
				'Height' => $shipping_info['items'][0]["height"],
				'Units' => $shipping_info['dimension_unit']
			),
			'CustomerReferences' => array(
				'0' => array(
					'CustomerReferenceType' => 'INVOICE_NUMBER', 
					'Value' => $shipping_info['order_id']
				)
			),
			'SpecialServicesRequested' => $this->addSpecialServices()
		)
	)

); //ENDS THIS REQUESTSHIPMENT
 

$output = "<script>console.log( 'PackageCount  : " . count($shipping_info['items']) . "' );</script>"; 	echo $output;


 	} else {  //JUST 1 BOX STARTS

		$this->_request['RequestedShipment'] = array(
				'ShipTimestamp' => date('c'),
				'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
				'ServiceType' => $shipping_info['service_type'], // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
				'PackagingType' => $shipping_info['packaging_type'], // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
				'TotalWeight' => array('Value' => $shipping_info['total_weight'], 'Units' => $shipping_info['weight_unit']), // valid values LB and KG
				'Shipper' =>  $this->addShipper(),
				'Recipient' => $this->addRecipient(),
				'ShippingChargesPayment' =>$this->addShippingChargesPayment(),
				//'SpecialServicesRequested' => addSpecialServices(),
				'LabelSpecification' => $this->addLabelSpecification(),
				'RateRequestTypes' => array('ACCOUNT'), // valid values ACCOUNT and LIST
				//'PackageCount' => $shipping_info['total_packages']);
 				'PackageDetail' => 'INDIVIDUAL_PACKAGES',  //PACKAGE_GROUPS  INDIVIDUAL_PACKAGES                                    
				'PackageCount' => count($shipping_info['items']));	

				for($i = 0; $i < count($shipping_info['items']); $i++){
					$this->_request['RequestedShipment']['RequestedPackageLineItems'][$i] = array(
							'SequenceNumber' => (int)($i + 1),
							'GroupPackageCount' => 1,
							'Weight' => array('Value' => $shipping_info['items'][$i]['weight'],
									'Units' => $shipping_info['weight_unit']),
									'Dimensions' => array('Length' => $shipping_info['items'][$i]['length'],
									'Width' => $shipping_info['items'][$i]['width'],
									'Height' => $shipping_info['items'][$i]['height'],
									'Units' => $shipping_info['dimension_unit']));

				} //end for
			
} //JUST 1 BOX ENDS
		


	

		try	{
			if(setEndpoint('changeEndpoint')) {
				$newLocation = $this->_client->__setLocation(setEndpoint('endpoint'));
			}

			$response = $this->_client->processShipment($this->_request);  // FedEx web service invocation

 			if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
				//printSuccess($client, $response);
				$trackingNumber = $response->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;
				//COD Return label
				//$CODtrackingNumber = $response->CompletedShipmentDetail->CodReturnDetail->TrackingId->TrackingNumber;
				//COD label
				//$fp = fopen(PATH_TO_STOCK_LABELS . $trackingNumber . '.pdf', 'wb');
				//fwrite($fp, $response->CompletedShipmentDetail->CodReturnDetail->Label->Parts->Image); //Create COD Return PNG or PDF file
				//fclose($fp);
				//echo '<a href="./'.PATH_TO_STOCK_LABELS . $trackingNumber . '.pdf'.'">'.$trackingNumber . '.pdf'.'</a> was generated.'.Newline;
				// Create PNG or PDF label
				// Set LabelSpecification.ImageType to 'PDF' or 'PNG for generating a PDF or a PNG label

				update_post_meta($shipping_info['order_id'], '_tracking_number', $trackingNumber);
				update_post_meta($shipping_info['order_id'], '_custom_tracking_provider', 'Fedex');
				update_post_meta($shipping_info['order_id'], '_custom_tracking_link', 'http://www.fedex.com/Tracking?action=track&tracknumbers=' . $trackingNumber);




				$imagetype = get_option('pvit_fedex_image_type');
				$fp = fopen($this->_save_path . $trackingNumber . '.'. $imagetype, 'wb');
				fwrite($fp, $response->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image); //Create PNG or PDF file
				fclose($fp);
	
				$masterlabel = $this->_save_url . $trackingNumber . '.'. $imagetype;
				update_post_meta ($shipping_info['order_id'], 'woo_fedex_master_label', $masterlabel );

				//return $this->_save_url . $trackingNumber . '.'. $imagetype;


$totalboxes = count($_SESSION['shipping_info']['items']);
if ($totalboxes > 1) {
	$boxesnomaster = array_slice($_SESSION['shipping_info']['items'],1); 
	$boxid = -1;
	foreach($boxesnomaster as $box){
		$boxid++; 
 		
  
	  		$this->_childRequest[$boxid]['WebAuthenticationDetail'] = array(
		    		'UserCredential' =>array(
		    		'Key' => getProperty('key'), 
		    		'Password' => getProperty('password')
		    	)
		    );
		    $this->_childRequest[$boxid]['ClientDetail'] = array(
		    	'AccountNumber' => getProperty('shipaccount'), 
		    	'MeterNumber' => getProperty('meter')
		    );
		    $this->_childRequest[$boxid]['TransactionDetail'] = array('CustomerTransactionId' => '*** Ground Domestic MPS Shipping Request v13 - Child using PHP ***');
		    $this->_childRequest[$boxid]['Version'] = array(
		    	'ServiceId' => 'ship', 
		    	'Major' => '13', 
		    	'Intermediate' => '0', 
		    	'Minor' => '0'
		    );
		   	$this->_childRequest[$boxid]['RequestedShipment'] = array(
		    	'ShipTimestamp' => date('c'),
				'DropoffType' => 'REGULAR_PICKUP', // valid values REGULAR_PICKUP, REQUEST_COURIER, DROP_BOX, BUSINESS_SERVICE_CENTER and STATION
				'ServiceType' => $shipping_info['service_type'], // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
				'PackagingType' => $shipping_info['packaging_type'], // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
				'Shipper' =>  $this->addShipper(),
				'Recipient' => $this->addRecipient(),
				'ShippingChargesPayment' =>  $this->addShippingChargesPayment(),
				'LabelSpecification' =>  $this->addLabelSpecification(), 
				'MasterTrackingId' => $response->CompletedShipmentDetail->MasterTrackingId,
				'RateRequestTypes' => array('ACCOUNT', 'LIST'), // valid values ACCOUNT and LIST
				'PackageCount' => count($shipping_info['items']),
				'RequestedPackageLineItems' => array(
					'0' => $packageLineItem = array(
							'SequenceNumber'=> $boxid + 2,
							'GroupPackageCount'=>1,
							'InsuredValue' => array(
								'Amount' => $shipping_info['customs_value']['amount'], 
								'Currency' => $shipping_info['customs_value']['currency']
							),
							'Weight' => array(
								'Value' => $boxesnomaster[$boxid]["weight"],
								'Units' => $shipping_info['weight_unit']
							),
							'Dimensions' => array(
								'Length' => $boxesnomaster[$boxid]["length"],
								'Width' => $boxesnomaster[$boxid]["width"],
								'Height' => $boxesnomaster[$boxid]["height"],
								'Units' => $shipping_info['dimension_unit']
							),
							'CustomerReferences' => array(
								'0' => array(
									'CustomerReferenceType' => 'INVOICE_NUMBER', 
									'Value' => $shipping_info['order_id']
								)
							/*	'1' => array(
									'CustomerReferenceType' => 'CUSTOMER_REFERENCE',  // valid values CUSTOMER_REFERENCE, INVOICE_NUMBER, P_O_NUMBER and SHIPMENT_INTEGRITY
									'Value' => 'GR4567892'
									),
								'2' => array(
									'CustomerReferenceType' => 'P_O_NUMBER', 
									'Value' => 'PO4567892'
								)*/
							),
							'SpecialServicesRequested' => $this->addSpecialServices()
					)
				)
		    );                                                                                                                                                                                                                                                                
		    $childResponse[$boxid] = $this->_client->processShipment($this->_childRequest[$boxid]); // FedEx web service invocation  


 	} //FOR EACH CLOSE
 

 	
	$totalboxes = count($_SESSION['shipping_info']['items']);
	if ($totalboxes > 1) { 
		$boxesnomaster = array_slice($_SESSION['shipping_info']['items'],1); 
		$boxid = -1;
		$wanderlustfinal = array();
			foreach($boxesnomaster as $box){
			$boxid++; 	 
	 

				if ($childResponse[$boxid]->HighestSeverity != 'FAILURE' && $childResponse[$boxid]->HighestSeverity != 'ERROR'){ //CHECK FOR ERROR IF NOT, CREATES LABEL
			        //printSuccess($client, $childResponse);
						$childtrackingNumber[$boxid] = $childResponse[$boxid]->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;

						$imagetype = get_option('pvit_fedex_image_type');
						$fp = fopen($this->_save_path . $childtrackingNumber[$boxid] . 'child.'. $imagetype, 'wb');
						fwrite($fp, $childResponse[$boxid]->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image); //Create PNG or PDF file
						fclose($fp);

						$labelok[$boxid] = $this->_save_url . $childtrackingNumber[$boxid] . 'child.'. $imagetype;

						update_post_meta ($shipping_info['order_id'], 'woo_fedex_label_child_' . $boxid, $labelok[$boxid] );

						

						$wanderlustfinal[] = array(
							  $boxid  => $labelok[$boxid], 
							);

 				
						//return $this->_save_url . $childtrackingNumber . 'child.'. $imagetype;
			       		//return $wanderlustfinal;


		 
			        	//echo '<a href="' . $labelok . '" target="_blank">Master Shipping Label</a> was generated';

 

?>


<?php
	    		} //CHECK FOR ERROR CLOSE
	     			else { //ERROR FOUND OPEN
	        			echo 'Processing child master' . Newline;
	    				printError($client, $childResponse[$boxid]);
	    		} //ERROR FOUND CLOSE

			} //FOR EACH CLOSE ?>
						<h1 style="clear: both;display:none;"><img src="<?php echo plugins_url('img/FedExExpressLogo_9.png',dirname(__FILE__));?>" style="height: 25px"> Label </h1>
						<div style="width: 100%; margin-right: 5%;">
						<h3 style="clear: both;">Shipping to</h3>
						<strong><?php echo $_SESSION['shipping_info']['recipient']['contact_name'];?></strong><br/>
								<?php echo $_SESSION['shipping_info']['recipient']['address1'],$_SESSION['shipping_info']['recipient']['address2'];?><br/>
					 			<?php echo $_SESSION['shipping_info']['recipient']['city']; ?>, <?php echo $_SESSION['shipping_info']['recipient']['state']; ?>, <?php echo $_SESSION['shipping_info']['recipient']['postcode'];?><br />
								Contact phone: <?php echo $_SESSION['shipping_info']['recipient']['phone'];?><br/> <br/> 
								<strong>Using:</strong><?=$_POST ['shipment_method']?><br/> 
								<strong>Tracking Code:</strong><?php echo $trackingNumber;  ?> <br/> 
						<p>
<?php if (empty($labelok[$boxid])){ 
			echo '<a href="' . $masterlabel . '" target="_blank">Master Shipping Label</a> was generated';
		} else { 
			echo '<a href="' . $masterlabel . '" target="_blank">Master Shipping Label</a> was generated</br>';				
 			$boxid = -1;
 			foreach($wanderlustfinal as $labelslink){
			$boxid++; 
			echo '<a href="' . $labelslink[$boxid] . '" target="_blank">Child Shipping Label</a> was generated</br>';
			}
		}
?>		


<?php	} //MORE THAN ONE BOX CLOSE
} //MORE THAN ONE BOX CLOSE
	else { //IF ONLY ONE BOX DO THIS
		return $this->_save_url . $trackingNumber . '.'. $imagetype; }

			} else {
				printError($this->_client, $response);
				?>

<div class="error fade">
	<p>
		<?php echo $response->Notifications->Message?> <a href="<?php echo  $_SERVER['SCRIPT_NAME']."?".$_SERVER['QUERY_STRING'];?>"> Retry </a>
	</p>
</div>

<?php }
			//writeToLog($client);    // Write to log file
		} catch (SoapFault $exception) {
			echo $exception;			
			printFault($exception, $this->_client);
			if(is_admin()){
				printRequestResponse($this->_client);
			}
		}
		return false;
	}

public function addShipper(){
	$shipper = array(
		'Contact' => array(
			'PersonName' => $_SESSION['shipping_info']['sender']['contact_name'],
			'CompanyName' => $_SESSION['shipping_info']['sender']['company'],
			'PhoneNumber' => $_SESSION['shipping_info']['sender']['phone']
		),
		'Address' => array(
			'StreetLines' => array($_SESSION['shipping_info']['sender']['address1'],$_SESSION['shipping_info']['sender']['address2']),
			'City' => $_SESSION['shipping_info']['sender']['city'],
			'StateOrProvinceCode' =>  $_SESSION['shipping_info']['sender']['state'],
			'PostalCode' => $_SESSION['shipping_info']['sender']['postcode'],
			'CountryCode' => $_SESSION['shipping_info']['sender']['country_code']
		)
	);
 	return $shipper;
}	


  
function addRecipient(){
	$recipient = array(
		'Contact' => array(
			'PersonName' => $_SESSION['shipping_info']['recipient']['contact_name'],
			'CompanyName' => $_SESSION['shipping_info']['recipient']['company'],
			'PhoneNumber' =>$_SESSION['shipping_info']['recipient']['phone']
		),
		'Address' => array(
			'StreetLines' => array($_SESSION['shipping_info']['recipient']['address1'],$_SESSION['shipping_info']['recipient']['address2']),
			'City' => $_SESSION['shipping_info']['recipient']['city'],
			'StateOrProvinceCode' => $_SESSION['shipping_info']['recipient']['state'],
			'PostalCode' => $_SESSION['shipping_info']['recipient']['postcode'],
			'CountryCode' => $_SESSION['shipping_info']['recipient']['country_code'],
			'Residential' => $_SESSION['shipping_info']['is_residential']
		)
	);
	return $recipient;	                                    
}


 	function addShippingChargesPayment(){
	$shippingChargesPayment = array(
		'PaymentType' => 'SENDER',
        'Payor' => array(
			'ResponsibleParty' => array(
				'AccountNumber' => getProperty('billaccount'),
				'Contact' => null,
				'Address' => array(
					'CountryCode' => $_SESSION['shipping_info']['sender']['country_code']
				)
			)
		)
	);
	return $shippingChargesPayment;
	}




	function addLabelSpecification(){
		$labelSpecification = array(
				'LabelFormatType' => 'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
				'ImageType' => get_option('pvit_fedex_image_type'),  // valid values DPL, EPL2, PDF, ZPLII and PNG
				'LabelStockType' => get_option('pvit_fedex_label_type'));
		return $labelSpecification;
	}

	function addSpecialServices(){
	$specialServices = array(
		'SpecialServiceTypes' => array('COD'),
		'CodDetail' => array(
			'CodCollectionAmount' => array(
				'Currency' => $_SESSION['shipping_info']['customs_value']['currency'], 
				'Amount' => $_SESSION['shipping_info']['customs_value']['amount']
			),
			'CollectionType' => 'ANY' // ANY, GUARANTEED_FUNDS
		)
	);
	return $specialServices; 
	}
}
?>