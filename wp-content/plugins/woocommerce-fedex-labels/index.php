<?php
/*
 Plugin Name: Woocommerce Print Fedex Label MPS
Plugin URI: http://www.wanderlust-webdesign.com
Description: This allows you to create labels for shipment directly from WooCommerce.
Version: 2.4
Author: wanderlust-webdesign.com
Author URI: http://www.wanderlust-webdesign.com
License: GPL2
*/

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {	
	require_once(dirname(__FILE__) . '/admin.php');
	require_once(dirname(__FILE__) . '/includes/functions.php');

}