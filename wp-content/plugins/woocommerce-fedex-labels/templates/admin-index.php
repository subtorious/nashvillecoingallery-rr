<?php
global $wpdb;
$prefixbox = $wpdb->prefix;
$site = get_site_url(); 

$shipping_info ['weight_unit'] = 'LB';
$shipping_info ['dimension_unit'] = 'IN';
$shipping_info ['packaging_type'] = 'YOUR_PACKAGING';
$shipping_info ['customs_value']['currency'] = 'USD';
$fedex_licensekey = get_option ('pvit_fedex_licensekey');
$autoweight = get_option ('pvit_fedex_auto_weight');		
$shipping_info ['sender'] = array (
		'country_code' => 'US',
		'company' => get_option ( 'pvit_fedex_sender_company' ),
		'contact_name' => get_option ( 'pvit_fedex_sender_name' ),
		'address1' => get_option ( 'pvit_fedex_sender_address1' ),
		'address2' => get_option ( 'pvit_fedex_sender_address2' ),
		'city' => get_option ( 'pvit_fedex_shipper_city', 'Austin' ),
		'state' => get_option ( 'pvit_fedex_sender_state', 'TX' ),
		'postcode' => get_option ( 'pvit_fedex_shipper_zipcode' ),
		'phone' => get_option ( 'pvit_fedex_shipper_phone' ) 
);

if ($_GET ['order_id']) {
	$order_id = $_GET ['order_id'];
	$order = get_post ( $order_id );
	if (! $order || $order->post_type != 'shop_order') { 
		
		?>
<div class="error fade">
	<p>
		Sorry the order you're trying to make a label for doesn't exist,
		please go to the <a href="/wp-admin/edit.php?post_type=shop_order">order
			lists</a> and select one.
	</p>
</div>
<?php
	} else {
		$order = new WC_Order ( $order->ID );
		$shipping_info ['recipient'] = array (
				'country_code' => $order->shipping_country,
				'company' => $order->shipping_company,
				'contact_name' => $order->shipping_first_name . ' ' . $order->shipping_last_name,
				'address1' => $order->shipping_address_1,
				'address2' => $order->shipping_address_2,
				'city' => $order->shipping_city,
				'state' => $order->shipping_state,
				'postcode' => $order->shipping_postcode,
				'phone' => $order->billing_phone,
				'ShippingM' => $order->shipping_method_title
		);
	}
}

?>
<div class="wrap">
	    <?php screen_icon(); ?>
	    <h1>Generate</h1>
<h1><img src="<?php echo plugins_url('img/FedExExpressLogo_9.png',dirname(__FILE__));?>" style="height: 25px"> Labels </h1>
<small style="margin-top: -15px;float: left;margin-bottom: 20px;"><a href="http://wanderlust-webdesign.com" target="_blank">Wanderlust Webdesign Development</a></small>
	    
	    <?php
	    $presets = get_option ( 'wc_fedex_label_presets', array () );
	    if (! is_array ( $presets ))
	    	$presets = unserialize ( $presets );
					
if (isset ( $_POST ['shipment_process'] )) {

 	$shipping_info['customs_value']['amount'] = $_POST ['shipment_value'][0];
 	$shipping_info['package_description'] = $_POST ['package_description'];
  	$shipping_info['package_numberofpieces'] = $_POST ['package_numberofpieces'];
  	$shipping_info['package_quantity'] = $_POST ['package_quantity'];
  	$shipping_info['package_itm_weight'] = $_POST ['package_itm_weight'];
  	$shipping_info['package_itm_price'] = $_POST ['package_itm_price'];

 
 if($_POST['return_label'] == 1){add_option('return_label',1);update_option('return_label', 1);}else{add_option('return_label',0);update_option('return_label', 0);}

  					 	$shipping_info['return_label'] = $_POST['return_label'];
						$shipping_info['order_id'] = $order_id;
						$shipping_info['service_type'] = $_POST['shipment_method'];
						$shipping_info['is_residential'] = $_POST['is_residential'];
						$shipping_info['recipient'] = array (
								'country_code' => $_POST ['shipment_to_country'],
								'company' => $_POST ['shipment_to_company'],
								'contact_name' => $_POST ['shipment_to_name'],
								'address1' => $_POST ['shipment_to_address1'],
								'address2' => $_POST ['shipment_to_address2'],
								'city' => $_POST ['shipment_to_city'],
								'state' => $_POST ['shipment_to_state'],
								'postcode' => $_POST ['shipment_to_zipcode'],
								'phone' => $_POST ['shipment_to_phone'],
								'ShippingM' => $_POST ['shipping_method_title']
						);
						$admin_email = get_option('admin_email'); 
						$shipping_info_items = array ();
						$total_weight = 0;
						
						
						foreach ( ( array ) $_POST['shipment_weight'] as $key => $weight ) {
							$shipping_info_items [] = array (
									'weight' => $weight,
									'length' => $_POST ['shipment_length'] [$key],
									'width' => $_POST ['shipment_width'] [$key],
									'height' => $_POST ['shipment_height'] [$key] 
							);
						
							if ($_POST ['shipment_preset'][$key] == 1) {
								
								$presets [$weight . 'we' . $_POST ['shipment_length'] [$key] . 'l' . $_POST ['shipment_width'] [$key] . 'wi' . $_POST ['shipment_height'] [$key] . 'h'] = array (
										'weight' => $weight,
										'length' => $_POST ['shipment_length'] [$key],
										'width' => $_POST ['shipment_width'] [$key],
										'height' => $_POST ['shipment_height'] [$key] 
								);
								
							}	
							$total_weight = $total_weight + $weight;
						}

 
  						update_option('wc_fedex_label_presets', serialize($presets)); 
						
						$shipping_info['items'] = $shipping_info_items;
						$shipping_info['total_weight'] = $total_weight;

 						
						$shipment = new pvit_woo_fedex_shipper();
						$label_url = $shipment->getLabel($shipping_info);
						if ($label_url != false) {
							//add_post_meta ( $order_id, 'woo_fedex_label', $label_url, true );
							//update_post_meta ( $order_id, 'woo_fedex_label', $label_url );
							//var_dump($label_url);
							?>
<h1 style="clear: both;display:none;"><img src="<?php echo plugins_url('img/FedExExpressLogo_9.png',dirname(__FILE__));?>" style="height: 25px"> Label </h1>
	<div style="width: 100%; margin-right: 5%;">
		<h3 style="clear: both;">Shipping to</h3>
		<strong><?php echo $order->shipping_first_name . ' ' . $order->shipping_last_name;?></strong><br />
			<?=$order->shipping_address_1?><br />
			<?php if(!empty($order->shipping_address_2)) echo $order->shipping_address_2 . '<br />'?>
			<?=$order->shipping_city?>, <?=$order->shipping_state?>, <?=$order->shipping_postcode?><br />
			Contact phone: <?=$order->billing_phone?><br /> <br /> <strong>Using:
		</strong><?=$_POST ['shipment_method']?>
			<p>
			<a href="<?=$label_url?>" target="_blank">Shipping Label</a>

			<?php $pvit_fedex_email_label =  get_option('pvit_fedex_email_label');		
					if($pvit_fedex_email_label == 1){  ?>
						<?php if($label_url !== '') { // EMAIL LABEL   ?>
						<?php require_once(dirname(__FILE__) . '/../email_label.php');
						$sendto = get_option('pvit_fedex_email_label_to');
						$sendfrom = get_option('pvit_fedex_email_label_from');
						$mailer = new AttachMailer($sendfrom, $sendto, "FedEx Label", " ");
						$mailer->attachFile($label_url);
						$mailer->send() ? "envoye": "probleme envoi";
						?>
						<?php } else {  ?>
						<?php }  ?> 
			<?php } else {  ?>
			<?php } ?>

			</p>
	</div>
	<hr style="margin: 2em 0em" />
<?php
						}
					}
					echo '<script>var shipment_presets =' . json_encode($presets) . '</script>';
					?>	    

<form action="" method="post" id="wps_poll_question">
		<div style="clear: both">
		<div id="toinfo" style="float: left; margin-right: 40px">
				<h3>To Information.</h3>
				<ul>
					<li><label for="shipment_to_country">Country/Location<span> *</span>:
					</label> <br /> <select name="shipment_to_country">
	<option value="AF"<?php selected('AF', $shipping_info['recipient']['country_code']);?>>Afghanistan</option>
							<option value="AX"<?php selected('AX', $shipping_info['recipient']['country_code']);?>>Åland Islands</option>
							<option value="AL"<?php selected('AL', $shipping_info['recipient']['country_code']);?>>Albania</option>
							<option value="DZ"<?php selected('DZ', $shipping_info['recipient']['country_code']);?>>Algeria</option>
							<option value="AS"<?php selected('AS', $shipping_info['recipient']['country_code']);?>>American	Samoa</option>
							<option value="AD"<?php selected('AD', $shipping_info['recipient']['country_code']);?>>Andorra</option>
							<option value="AO"<?php selected('AO', $shipping_info['recipient']['country_code']);?>>Angola</option>
							<option value="AI"<?php selected('AI', $shipping_info['recipient']['country_code']);?>>Anguilla</option>
							<option value="AQ"<?php selected('AQ', $shipping_info['recipient']['country_code']);?>>Antarctica</option>
							<option value="AG"<?php selected('AG', $shipping_info['recipient']['country_code']);?>>Antigua and Barbuda</option>
							<option value="AR"<?php selected('AR', $shipping_info['recipient']['country_code']);?>>Argentina</option>
							<option value="AM"<?php selected('AM', $shipping_info['recipient']['country_code']);?>>Armenia</option>
							<option value="AW"<?php selected('AW', $shipping_info['recipient']['country_code']);?>>Aruba</option>
							<option value="AU"<?php selected('AU', $shipping_info['recipient']['country_code']);?>>Australia</option>
							<option value="AT"<?php selected('AT', $shipping_info['recipient']['country_code']);?>>Austria</option>
							<option value="AZ"<?php selected('AZ', $shipping_info['recipient']['country_code']);?>>Azerbaijan</option>
							<option value="BS"<?php selected('BS', $shipping_info['recipient']['country_code']);?>>Bahamas</option>
							<option value="BH"<?php selected('BH', $shipping_info['recipient']['country_code']);?>>Bahrain</option>
							<option value="BD"<?php selected('BD', $shipping_info['recipient']['country_code']);?>>Bangladesh</option>
							<option value="BB"<?php selected('BB', $shipping_info['recipient']['country_code']);?>>Barbados</option>
							<option value="BY"<?php selected('BY', $shipping_info['recipient']['country_code']);?>>Belarus</option>
							<option value="BE"<?php selected('BE', $shipping_info['recipient']['country_code']);?>>Belgium</option>
							<option value="BZ"<?php selected('BZ', $shipping_info['recipient']['country_code']);?>>Belize</option>
							<option value="BJ"<?php selected('BJ', $shipping_info['recipient']['country_code']);?>>Benin</option>
							<option value="BM"<?php selected('BM', $shipping_info['recipient']['country_code']);?>>Bermuda</option>
							<option value="BT"<?php selected('BT', $shipping_info['recipient']['country_code']);?>>Bhutan</option>
							<option value="BO"<?php selected('BO', $shipping_info['recipient']['country_code']);?>>Bolivia, Plurinational State of</option>
							<option value="BQ"<?php selected('BQ', $shipping_info['recipient']['country_code']);?>>Bonaire, Sint Eustatius and Saba</option>
							<option value="BA"<?php selected('BA', $shipping_info['recipient']['country_code']);?>>Bosnia and Herzegovina</option>
							<option value="BW"<?php selected('BW', $shipping_info['recipient']['country_code']);?>>Botswana</option>
							<option value="BV"<?php selected('BV', $shipping_info['recipient']['country_code']);?>>Bouvet Island</option>
							<option value="BR"<?php selected('BR', $shipping_info['recipient']['country_code']);?>>Brazil</option>
							<option value="IO"<?php selected('IO', $shipping_info['recipient']['country_code']);?>>British Indian Ocean Territory</option>
							<option value="BN"<?php selected('BN', $shipping_info['recipient']['country_code']);?>>Brunei Darussalam</option>
							<option value="BG"<?php selected('BG', $shipping_info['recipient']['country_code']);?>>Bulgaria</option>
							<option value="BF"<?php selected('BF', $shipping_info['recipient']['country_code']);?>>Burkina Faso</option>
							<option value="BI"<?php selected('BI', $shipping_info['recipient']['country_code']);?>>Burundi</option>
							<option value="KH"<?php selected('KH', $shipping_info['recipient']['country_code']);?>>Cambodia</option>
							<option value="CM"<?php selected('CM', $shipping_info['recipient']['country_code']);?>>Cameroon</option>
							<option value="CA" <?php selected('CA', $shipping_info['recipient']['country_code']);?>>Canada</option>
							<option value="CV"<?php selected('CV', $shipping_info['recipient']['country_code']);?>>Cape Verde</option>
							<option value="KY"<?php selected('KY', $shipping_info['recipient']['country_code']);?>>Cayman Islands</option>
							<option value="CF"<?php selected('CF', $shipping_info['recipient']['country_code']);?>>Central African Republic</option>
							<option value="TD"<?php selected('TD', $shipping_info['recipient']['country_code']);?>>Chad</option>
							<option value="CL"<?php selected('CL', $shipping_info['recipient']['country_code']);?>>Chile</option>
							<option value="CN"<?php selected('CN', $shipping_info['recipient']['country_code']);?>>China</option>
							<option value="CX"<?php selected('CX', $shipping_info['recipient']['country_code']);?>>Christmas Island</option>
							<option value="CC"<?php selected('CC', $shipping_info['recipient']['country_code']);?>>Cocos (Keeling) Islands</option>
							<option value="CO"<?php selected('CO', $shipping_info['recipient']['country_code']);?>>Colombia</option>
							<option value="KM"<?php selected('KM', $shipping_info['recipient']['country_code']);?>>Comoros</option>
							<option value="CG"<?php selected('CG', $shipping_info['recipient']['country_code']);?>>Congo</option>
							<option value="CD"<?php selected('CD', $shipping_info['recipient']['country_code']);?>>Congo, the Democratic Republic of the</option>
							<option value="CK"<?php selected('CK', $shipping_info['recipient']['country_code']);?>>Cook Islands</option>
							<option value="CR"<?php selected('CR', $shipping_info['recipient']['country_code']);?>>Costa Rica</option>
							<option value="CI"<?php selected('CI', $shipping_info['recipient']['country_code']);?>>Côte d'Ivoire</option>
							<option value="HR"<?php selected('HR', $shipping_info['recipient']['country_code']);?>>Croatia</option>
							<option value="CU"<?php selected('CU', $shipping_info['recipient']['country_code']);?>>Cuba</option>
							<option value="CW"<?php selected('CW', $shipping_info['recipient']['country_code']);?>>Curaçao</option>
							<option value="CY"<?php selected('CY', $shipping_info['recipient']['country_code']);?>>Cyprus</option>
							<option value="CZ"<?php selected('CZ', $shipping_info['recipient']['country_code']);?>>Czech Republic</option>
							<option value="DK"<?php selected('DK', $shipping_info['recipient']['country_code']);?>>Denmark</option>
							<option value="DJ"<?php selected('DJ', $shipping_info['recipient']['country_code']);?>>Djibouti</option>
							<option value="DM"<?php selected('DM', $shipping_info['recipient']['country_code']);?>>Dominica</option>
							<option value="DO" <?php selected('DO', $shipping_info['recipient']['country_code']);?>>Dominican Republic</option>
							<option value="EC" <?php selected('EC', $shipping_info['recipient']['country_code']);?>>Ecuador</option>
							<option value="EG" <?php selected('EG', $shipping_info['recipient']['country_code']);?>>Egypt</option>
							<option value="SV" <?php selected('SV', $shipping_info['recipient']['country_code']);?>>El Salvador</option>
							<option value="GQ" <?php selected('GQ', $shipping_info['recipient']['country_code']);?>>Equatorial Guinea</option>
							<option value="ER" <?php selected('ER', $shipping_info['recipient']['country_code']);?>>Eritrea</option>
							<option value="EE" <?php selected('EE', $shipping_info['recipient']['country_code']);?>>Estonia</option>
							<option value="ET" <?php selected('ET', $shipping_info['recipient']['country_code']);?>>Ethiopia</option>
							<option value="FK" <?php selected('FK', $shipping_info['recipient']['country_code']);?>>Falkland Islands (Malvinas)</option>
							<option value="FO"<?php selected('FO', $shipping_info['recipient']['country_code']);?>>Faroe Islands</option>
							<option value="FJ"<?php selected('FJ', $shipping_info['recipient']['country_code']);?>>Fiji</option>
							<option value="FI"<?php selected('FI', $shipping_info['recipient']['country_code']);?>>Finland</option>
							<option value="FR"<?php selected('FR', $shipping_info['recipient']['country_code']);?>>France</option>
							<option value="GF"<?php selected('GF', $shipping_info['recipient']['country_code']);?>>French Guiana</option>
							<option value="PF"<?php selected('PF', $shipping_info['recipient']['country_code']);?>>French Polynesia</option>
							<option value="TF"<?php selected('TF', $shipping_info['recipient']['country_code']);?>>French Southern Territories</option>
							<option value="GA"<?php selected('GA', $shipping_info['recipient']['country_code']);?>>Gabon</option>
							<option value="GM"<?php selected('GM', $shipping_info['recipient']['country_code']);?>>Gambia</option>
							<option value="GE"<?php selected('GE', $shipping_info['recipient']['country_code']);?>>Georgia</option>
							<option value="DE"<?php selected('DE', $shipping_info['recipient']['country_code']);?>>Germany</option>
							<option value="GH"<?php selected('GH', $shipping_info['recipient']['country_code']);?>>Ghana</option>
							<option value="GI"<?php selected('GI', $shipping_info['recipient']['country_code']);?>>Gibraltar</option>
							<option value="GR"<?php selected('GR', $shipping_info['recipient']['country_code']);?>>Greece</option>
							<option value="GL"<?php selected('GL', $shipping_info['recipient']['country_code']);?>>Greenland</option>
							<option value="GD"<?php selected('GD', $shipping_info['recipient']['country_code']);?>>Grenada</option>
							<option value="GP" <?php selected('GP', $shipping_info['recipient']['country_code']);?>>Guadeloupe</option>
							<option value="GU"<?php selected('GU', $shipping_info['recipient']['country_code']);?>>Guam</option>
							<option value="GT"<?php selected('GT', $shipping_info['recipient']['country_code']);?>>Guatemala</option>
							<option value="GG"<?php selected('GG', $shipping_info['recipient']['country_code']);?>>Guernsey</option>
							<option value="GN"<?php selected('GN', $shipping_info['recipient']['country_code']);?>>Guinea</option>
							<option value="GW"<?php selected('GW', $shipping_info['recipient']['country_code']);?>>Guinea-Bissau</option>
							<option value="GY"<?php selected('GY', $shipping_info['recipient']['country_code']);?>>Guyana</option>
							<option value="HT"<?php selected('HT', $shipping_info['recipient']['country_code']);?>>Haiti</option>
							<option value="HM"<?php selected('HM', $shipping_info['recipient']['country_code']);?>>Heard Island and McDonald Islands</option>
							<option value="VA"<?php selected('VA', $shipping_info['recipient']['country_code']);?>>Holy See (Vatican City State)</option>
							<option value="HN"<?php selected('HN', $shipping_info['recipient']['country_code']);?>>Honduras</option>
							<option value="HK"<?php selected('HK', $shipping_info['recipient']['country_code']);?>>Hong Kong</option>
							<option value="HU"<?php selected('HU', $shipping_info['recipient']['country_code']);?>>Hungary</option>
							<option value="IS"<?php selected('IS', $shipping_info['recipient']['country_code']);?>>Iceland</option>
							<option value="IN"<?php selected('IN', $shipping_info['recipient']['country_code']);?>>India</option>
							<option value="ID"<?php selected('ID', $shipping_info['recipient']['country_code']);?>>Indonesia</option>
							<option value="IR"<?php selected('IR', $shipping_info['recipient']['country_code']);?>>Iran, Islamic Republic of</option>
							<option value="IQ"<?php selected('IQ', $shipping_info['recipient']['country_code']);?>>Iraq</option>
							<option value="IE"<?php selected('IE', $shipping_info['recipient']['country_code']);?>>Ireland</option>
							<option value="IM"<?php selected('IM', $shipping_info['recipient']['country_code']);?>>Isle of Man</option>
							<option value="IL"<?php selected('IL', $shipping_info['recipient']['country_code']);?>>Israel</option>
							<option value="IT"<?php selected('IT', $shipping_info['recipient']['country_code']);?>>Italy</option>
							<option value="JM"<?php selected('JM', $shipping_info['recipient']['country_code']);?>>Jamaica</option>
							<option value="JP"<?php selected('JP', $shipping_info['recipient']['country_code']);?>>Japan</option>
							<option value="JE"<?php selected('JE', $shipping_info['recipient']['country_code']);?>>Jersey</option>
							<option value="JO"<?php selected('JO', $shipping_info['recipient']['country_code']);?>>Jordan</option>
							<option value="KZ"<?php selected('KZ', $shipping_info['recipient']['country_code']);?>>Kazakhstan</option>
							<option value="KE"<?php selected('KE', $shipping_info['recipient']['country_code']);?>>Kenya</option>
							<option value="KI"<?php selected('KI', $shipping_info['recipient']['country_code']);?>>Kiribati</option>
							<option value="KP"<?php selected('KP', $shipping_info['recipient']['country_code']);?>>Korea, Democratic People's Republic of</option>
							<option value="KR"<?php selected('KR', $shipping_info['recipient']['country_code']);?>>Korea, Republic of</option>
							<option value="KW"<?php selected('KW', $shipping_info['recipient']['country_code']);?>>Kuwait</option>
							<option value="KG"<?php selected('KG', $shipping_info['recipient']['country_code']);?>>Kyrgyzstan</option>
							<option value="LA"<?php selected('LA', $shipping_info['recipient']['country_code']);?>>Lao People's Democratic Republic</option>
							<option value="LV"<?php selected('LV', $shipping_info['recipient']['country_code']);?>>Latvia</option>
							<option value="LB"<?php selected('LB', $shipping_info['recipient']['country_code']);?>>Lebanon</option>
							<option value="LS"<?php selected('LS', $shipping_info['recipient']['country_code']);?>>Lesotho</option>
							<option value="LR"<?php selected('LR', $shipping_info['recipient']['country_code']);?>>Liberia</option>
							<option value="LY"<?php selected('LY', $shipping_info['recipient']['country_code']);?>>Libya</option>
							<option value="LI"<?php selected('LI', $shipping_info['recipient']['country_code']);?>>Liechtenstein</option>
							<option value="LT"<?php selected('LT', $shipping_info['recipient']['country_code']);?>>Lithuania</option>
							<option value="LU"<?php selected('LU', $shipping_info['recipient']['country_code']);?>>Luxembourg</option>
							<option value="MO"<?php selected('MO', $shipping_info['recipient']['country_code']);?>>Macao</option>
							<option value="MK"<?php selected('MK', $shipping_info['recipient']['country_code']);?>>Macedonia, the former Yugoslav Republic of</option>
							<option value="MG"<?php selected('MG', $shipping_info['recipient']['country_code']);?>>Madagascar</option>
							<option value="MW"<?php selected('MW', $shipping_info['recipient']['country_code']);?>>Malawi</option>
							<option value="MY"<?php selected('MY', $shipping_info['recipient']['country_code']);?>>Malaysia</option>
							<option value="MV"<?php selected('MV', $shipping_info['recipient']['country_code']);?>>Maldives</option>
							<option value="ML"<?php selected('ML', $shipping_info['recipient']['country_code']);?>>Mali</option>
							<option value="MT"<?php selected('MT', $shipping_info['recipient']['country_code']);?>>Malta</option>
							<option value="MH"<?php selected('MH', $shipping_info['recipient']['country_code']);?>>Marshall Islands</option>
							<option value="MQ"<?php selected('MQ', $shipping_info['recipient']['country_code']);?>>Martinique</option>
							<option value="MR"<?php selected('MR', $shipping_info['recipient']['country_code']);?>>Mauritania</option>
							<option value="MU"<?php selected('MU', $shipping_info['recipient']['country_code']);?>>Mauritius</option>
							<option value="YT"<?php selected('YT', $shipping_info['recipient']['country_code']);?>>Mayotte</option>
							<option value="MX"<?php selected('MX', $shipping_info['recipient']['country_code']);?>>Mexico</option>
							<option value="FM"<?php selected('FM', $shipping_info['recipient']['country_code']);?>>Micronesia, Federated States of</option>
							<option value="MD"<?php selected('MD', $shipping_info['recipient']['country_code']);?>>Moldova, Republic of</option>
							<option value="MC"<?php selected('MC', $shipping_info['recipient']['country_code']);?>>Monaco</option>
							<option value="MN"<?php selected('MN', $shipping_info['recipient']['country_code']);?>>Mongolia</option>
							<option value="ME"<?php selected('ME', $shipping_info['recipient']['country_code']);?>>Montenegro</option>
							<option value="MS"<?php selected('MS', $shipping_info['recipient']['country_code']);?>>Montserrat</option>
							<option value="MA"<?php selected('MA', $shipping_info['recipient']['country_code']);?>>Morocco</option>
							<option value="MZ"<?php selected('MZ', $shipping_info['recipient']['country_code']);?>>Mozambique</option>
							<option value="MM"<?php selected('MM', $shipping_info['recipient']['country_code']);?>>Myanmar</option>
							<option value="NA"<?php selected('NA', $shipping_info['recipient']['country_code']);?>>Namibia</option>
							<option value="NR"<?php selected('NR', $shipping_info['recipient']['country_code']);?>>Nauru</option>
							<option value="NP"<?php selected('NP', $shipping_info['recipient']['country_code']);?>>Nepal</option>
							<option value="NL"<?php selected('NL', $shipping_info['recipient']['country_code']);?>>Netherlands</option>
							<option value="NC"<?php selected('NC', $shipping_info['recipient']['country_code']);?>>New Caledonia</option>
							<option value="NZ"<?php selected('NZ', $shipping_info['recipient']['country_code']);?>>New Zealand</option>
							<option value="NI"<?php selected('NI', $shipping_info['recipient']['country_code']);?>>Nicaragua</option>
							<option value="NE"<?php selected('NE', $shipping_info['recipient']['country_code']);?>>Niger</option>
							<option value="NG"<?php selected('NG', $shipping_info['recipient']['country_code']);?>>Nigeria</option>
							<option value="NU"<?php selected('NU', $shipping_info['recipient']['country_code']);?>>Niue</option>
							<option value="NF"<?php selected('NF', $shipping_info['recipient']['country_code']);?>>Norfolk Island</option>
							<option value="MP"<?php selected('MP', $shipping_info['recipient']['country_code']);?>>Northern Mariana Islands</option>
							<option value="NO"<?php selected('NO', $shipping_info['recipient']['country_code']);?>>Norway</option>
							<option value="OM"<?php selected('OM', $shipping_info['recipient']['country_code']);?>>Oman</option>
							<option value="PK"<?php selected('PK', $shipping_info['recipient']['country_code']);?>>Pakistan</option>
							<option value="PW"<?php selected('PW', $shipping_info['recipient']['country_code']);?>>Palau</option>
							<option value="PS"<?php selected('PS', $shipping_info['recipient']['country_code']);?>>Palestinian Territory, Occupied</option>
							<option value="PA"<?php selected('PA', $shipping_info['recipient']['country_code']);?>>Panama</option>
							<option value="PG"<?php selected('PG', $shipping_info['recipient']['country_code']);?>>Papua New Guinea</option>
							<option value="PY"<?php selected('PY', $shipping_info['recipient']['country_code']);?>>Paraguay</option>
							<option value="PE"<?php selected('PE', $shipping_info['recipient']['country_code']);?>>Peru</option>
							<option value="PH"<?php selected('PH', $shipping_info['recipient']['country_code']);?>>Philippines</option>
							<option value="PN"<?php selected('PN', $shipping_info['recipient']['country_code']);?>>Pitcairn</option>
							<option value="PL"<?php selected('PL', $shipping_info['recipient']['country_code']);?>>Poland</option>
							<option value="PT"<?php selected('PT', $shipping_info['recipient']['country_code']);?>>Portugal</option>
							<option value="PR"<?php selected('PR', $shipping_info['recipient']['country_code']);?>>Puerto Rico</option>
							<option value="QA"<?php selected('QA', $shipping_info['recipient']['country_code']);?>>Qatar</option>
							<option value="RE"<?php selected('RE', $shipping_info['recipient']['country_code']);?>>Réunion</option>
							<option value="RO"<?php selected('RO', $shipping_info['recipient']['country_code']);?>>Romania</option>
							<option value="RU"<?php selected('RU', $shipping_info['recipient']['country_code']);?>>Russian Federation</option>
							<option value="RW"<?php selected('RW', $shipping_info['recipient']['country_code']);?>>Rwanda</option>
							<option value="BL"<?php selected('BL', $shipping_info['recipient']['country_code']);?>>Saint Barthélemy</option>
							<option value="SH"<?php selected('SH', $shipping_info['recipient']['country_code']);?>>Saint Helena, Ascension and Tristan da Cunha</option>
							<option value="KN"<?php selected('KN', $shipping_info['recipient']['country_code']);?>>Saint Kitts and Nevis</option>
							<option value="LC"<?php selected('LC', $shipping_info['recipient']['country_code']);?>>Saint Lucia</option>
							<option value="MF"<?php selected('MF', $shipping_info['recipient']['country_code']);?>>Saint Martin (French part)</option>
							<option value="PM"<?php selected('PM', $shipping_info['recipient']['country_code']);?>>Saint Pierre and Miquelon</option>
							<option value="VC"<?php selected('VC', $shipping_info['recipient']['country_code']);?>>Saint Vincent and the Grenadines</option>
							<option value="WS"<?php selected('WS', $shipping_info['recipient']['country_code']);?>>Samoa</option>
							<option value="SM"<?php selected('SM', $shipping_info['recipient']['country_code']);?>>San Marino</option>
							<option value="ST"<?php selected('ST', $shipping_info['recipient']['country_code']);?>>Sao Tome and Principe</option>
							<option value="SA"<?php selected('SA', $shipping_info['recipient']['country_code']);?>>Saudi Arabia</option>
							<option value="SN"<?php selected('SN', $shipping_info['recipient']['country_code']);?>>Senegal</option>
							<option value="RS"<?php selected('RS', $shipping_info['recipient']['country_code']);?>>Serbia</option>
							<option value="SC"<?php selected('SC', $shipping_info['recipient']['country_code']);?>>Seychelles</option>
							<option value="SL"<?php selected('SL', $shipping_info['recipient']['country_code']);?>>Sierra Leone</option>
							<option value="SG"<?php selected('SG', $shipping_info['recipient']['country_code']);?>>Singapore</option>
							<option value="SX"<?php selected('SX', $shipping_info['recipient']['country_code']);?>>Sint Maarten (Dutch part)</option>
							<option value="SK"<?php selected('SK', $shipping_info['recipient']['country_code']);?>>Slovakia</option>
							<option value="SI"<?php selected('SI', $shipping_info['recipient']['country_code']);?>>Slovenia</option>
							<option value="SB"<?php selected('SB', $shipping_info['recipient']['country_code']);?>>Solomon Islands</option>
							<option value="SO"<?php selected('SO', $shipping_info['recipient']['country_code']);?>>Somalia</option>
							<option value="ZA"<?php selected('ZA', $shipping_info['recipient']['country_code']);?>>South Africa</option>
							<option value="KR"<?php selected('KR', $shipping_info['recipient']['country_code']);?>>South Corea</option>
							<option value="GS"<?php selected('GS', $shipping_info['recipient']['country_code']);?>>South Georgia and the South Sandwich Islands</option>
							<option value="SS"<?php selected('SS', $shipping_info['recipient']['country_code']);?>>South Sudan</option>
							<option value="ES"<?php selected('ES', $shipping_info['recipient']['country_code']);?>>Spain</option>
							<option value="LK"<?php selected('LK', $shipping_info['recipient']['country_code']);?>>Sri Lanka</option>
							<option value="SD"<?php selected('SD', $shipping_info['recipient']['country_code']);?>>Sudan</option>
							<option value="SR"<?php selected('SR', $shipping_info['recipient']['country_code']);?>>Suriname</option>
							<option value="SJ"<?php selected('SJ', $shipping_info['recipient']['country_code']);?>>Svalbard and Jan Mayen</option>
							<option value="SZ"<?php selected('SZ', $shipping_info['recipient']['country_code']);?>>Swaziland</option>
							<option value="SE"<?php selected('SE', $shipping_info['recipient']['country_code']);?>>Sweden</option>
							<option value="CH"<?php selected('CH', $shipping_info['recipient']['country_code']);?>>Switzerland</option>
							<option value="SY"<?php selected('SY', $shipping_info['recipient']['country_code']);?>>Syrian Arab Republic</option>
							<option value="TW"<?php selected('TW', $shipping_info['recipient']['country_code']);?>>Taiwan, Province of China</option>
							<option value="TJ"<?php selected('TJ', $shipping_info['recipient']['country_code']);?>>Tajikistan</option>
							<option value="TZ"<?php selected('TZ', $shipping_info['recipient']['country_code']);?>>Tanzania, United Republic of</option>
							<option value="TH"<?php selected('TH', $shipping_info['recipient']['country_code']);?>>Thailand</option>
							<option value="TL"<?php selected('TL', $shipping_info['recipient']['country_code']);?>>Timor-Leste</option>
							<option value="TG"<?php selected('TG', $shipping_info['recipient']['country_code']);?>>Togo</option>
							<option value="TK"<?php selected('TK', $shipping_info['recipient']['country_code']);?>>Tokelau</option>
							<option value="TO"<?php selected('TO', $shipping_info['recipient']['country_code']);?>>Tonga</option>
							<option value="TT"<?php selected('TT', $shipping_info['recipient']['country_code']);?>>Trinidad and Tobago</option>
							<option value="TN"<?php selected('TN', $shipping_info['recipient']['country_code']);?>>Tunisia</option>
							<option value="TR"<?php selected('TR', $shipping_info['recipient']['country_code']);?>>Turkey</option>
							<option value="TM"<?php selected('TM', $shipping_info['recipient']['country_code']);?>>Turkmenistan</option>
							<option value="TC"<?php selected('TC', $shipping_info['recipient']['country_code']);?>>Turks and Caicos Islands</option>
							<option value="TV"<?php selected('TV', $shipping_info['recipient']['country_code']);?>>Tuvalu</option>
							<option value="UG"<?php selected('UG', $shipping_info['recipient']['country_code']);?>>Uganda</option>
							<option value="UA"<?php selected('UA', $shipping_info['recipient']['country_code']);?>>Ukraine</option>
							<option value="AE"<?php selected('AE', $shipping_info['recipient']['country_code']);?>>United Arab Emirates</option>
							<option value="GB"<?php selected('GB', $shipping_info['recipient']['country_code']);?>>United Kingdom</option>
							<option value="US"<?php selected('US', $shipping_info['recipient']['country_code']);?>>United States</option>
							<option value="UM"<?php selected('UM', $shipping_info['recipient']['country_code']);?>>United States Minor Outlying Islands</option>
							<option value="UY"<?php selected('UY', $shipping_info['recipient']['country_code']);?>>Uruguay</option>
							<option value="UZ"<?php selected('UZ', $shipping_info['recipient']['country_code']);?>>Uzbekistan</option>
							<option value="VU"<?php selected('VU', $shipping_info['recipient']['country_code']);?>>Vanuatu</option>
							<option value="VE"<?php selected('VE', $shipping_info['recipient']['country_code']);?>>Venezuela, Bolivarian Republic of</option>
							<option value="VN"<?php selected('VN', $shipping_info['recipient']['country_code']);?>>Viet Nam</option>
							<option value="VG"<?php selected('VG', $shipping_info['recipient']['country_code']);?>>Virgin Islands, British</option>
							<option value="VI"<?php selected('VI', $shipping_info['recipient']['country_code']);?>>Virgin Islands, U.S.</option>
							<option value="WF"<?php selected('WF', $shipping_info['recipient']['country_code']);?>>Wallis and Futuna</option>
							<option value="EH"<?php selected('EH', $shipping_info['recipient']['country_code']);?>>Western Sahara</option>
							<option value="YE"<?php selected('YE', $shipping_info['recipient']['country_code']);?>>Yemen</option>
							<option value="ZM"<?php selected('ZM', $shipping_info['recipient']['country_code']);?>>Zambia</option>
							<option value="ZW"<?php selected('ZW', $shipping_info['recipient']['country_code']);?>>Zimbabwe</option>
					</select></li>
	<li><label for="shipment_to_country">Company: </label> <br /> <input type="text" size="45" name="shipment_to_company" value="<?php echo $shipping_info['recipient']['company']?>" /></li>
	<li><label for="shipment_to_name">Contact name<span> *</span>: </label> <br /> <input type="text" size="45" name="shipment_to_name" value="<?php echo $shipping_info['recipient']['contact_name']?>" />	</li>
	<li><label for="shipment_to_address1">Address 1: </label> <br /> <input type="text" size="45" name="shipment_to_address1" value="<?php echo $shipping_info['recipient']['address1']?>" /></li>
	<li><label for="shipment_to_address2">Address 2<span> *</span>: </label> <br /> <input type="text" size="45" name="shipment_to_address2" value="<?php echo $shipping_info['recipient']['address2']?>" /></li>
	<li><label for="shipment_to_zipcode">Zipcode<span> *</span>: </label> <br /> <input type="text" size="45" name="shipment_to_zipcode" value="<?php echo $shipping_info['recipient']['postcode']?>" /></li>
	<li><label for="shipment_to_city">City<span> *</span>: </label> <br /> <input type="text" size="45" name="shipment_to_city" value="<?php echo $shipping_info['recipient']['city']?>" /></li>
	<li><label for="shipment_to_state">State<span> *</span>: </label> <br /> 
						<select name="shipment_to_state" style="display: block;">
							<option value="<?php echo $shipping_info['recipient']['state'];?>"><?php echo $shipping_info['recipient']['state'];?></option>
							<option value="AL"<?php selected('AL', $shipping_info['recipient']['state']);?>>Alabama</option>
							<option value="AK"<?php selected('AK', $shipping_info['recipient']['state']);?>>Alaska</option>
							<option value="AZ"<?php selected('AZ', $shipping_info['recipient']['state']);?>>Arizona</option>
							<option value="AR"<?php selected('AR', $shipping_info['recipient']['state']);?>>Arkansas</option>
							<option value="CA"<?php selected('CA', $shipping_info['recipient']['state']);?>>California</option>
							<option value="CO"<?php selected('CO', $shipping_info['recipient']['state']);?>>Colorado</option>
							<option value="CT"<?php selected('CT', $shipping_info['recipient']['state']);?>>Connecticut</option>
							<option value="DE"<?php selected('DE', $shipping_info['recipient']['state']);?>>Delaware</option>
							<option value="DC"<?php selected('DC', $shipping_info['recipient']['state']);?>>District of Columbia</option>
							<option value="FL"<?php selected('FL', $shipping_info['recipient']['state']);?>>Florida</option>
							<option value="GA"<?php selected('GA', $shipping_info['recipient']['state']);?>>Georgia</option>
							<option value="HI"<?php selected('HI', $shipping_info['recipient']['state']);?>>Hawaii</option>
							<option value="ID"<?php selected('ID', $shipping_info['recipient']['state']);?>>Idaho</option>
							<option value="IL"<?php selected('IL', $shipping_info['recipient']['state']);?>>Illinois</option>
							<option value="IN"<?php selected('IN', $shipping_info['recipient']['state']);?>>Indiana</option>
							<option value="IA"<?php selected('IA', $shipping_info['recipient']['state']);?>>Iowa</option>
							<option value="KS"<?php selected('KS', $shipping_info['recipient']['state']);?>>Kansas</option>
							<option value="KY"<?php selected('KY', $shipping_info['recipient']['state']);?>>Kentucky</option>
							<option value="LA"<?php selected('LA', $shipping_info['recipient']['state']);?>>Louisiana</option>
							<option value="ME"<?php selected('ME', $shipping_info['recipient']['state']);?>>Maine</option>
							<option value="MD"<?php selected('MD', $shipping_info['recipient']['state']);?>>Maryland</option>
							<option value="MA"<?php selected('MA', $shipping_info['recipient']['state']);?>>Massachusetts</option>
							<option value="MI"<?php selected('MI', $shipping_info['recipient']['state']);?>>Michigan</option>
							<option value="MN"<?php selected('MN', $shipping_info['recipient']['state']);?>>Minnesota</option>
							<option value="MS"<?php selected('MS', $shipping_info['recipient']['state']);?>>Mississippi</option>
							<option value="MO"<?php selected('MO', $shipping_info['recipient']['state']);?>>Missouri</option>
							<option value="MT"<?php selected('MT', $shipping_info['recipient']['state']);?>>Montana</option>
							<option value="NE"<?php selected('NE', $shipping_info['recipient']['state']);?>>Nebraska</option>
							<option value="NV"<?php selected('NV', $shipping_info['recipient']['state']);?>>Nevada</option>
							<option value="NH"<?php selected('NH', $shipping_info['recipient']['state']);?>>New Hampshire</option>
							<option value="NJ"<?php selected('NJ', $shipping_info['recipient']['state']);?>>New Jersey</option>
							<option value="NM"<?php selected('NM', $shipping_info['recipient']['state']);?>>New Mexico</option>
							<option value="NY"<?php selected('NY', $shipping_info['recipient']['state']);?>>New York</option>
							<option value="NC"<?php selected('NC', $shipping_info['recipient']['state']);?>>North Carolina</option>
							<option value="ND"<?php selected('ND', $shipping_info['recipient']['state']);?>>North Dakota</option>
							<option value="OH"<?php selected('OH', $shipping_info['recipient']['state']);?>>Ohio</option>
							<option value="OK"<?php selected('OK', $shipping_info['recipient']['state']);?>>Oklahoma</option>
							<option value="OR"<?php selected('OR', $shipping_info['recipient']['state']);?>>Oregon</option>
							<option value="PA"<?php selected('PA', $shipping_info['recipient']['state']);?>>Pennsylvania</option>
							<option value="RI"<?php selected('RI', $shipping_info['recipient']['state']);?>>Rhode Island</option>
							<option value="SC"<?php selected('SC', $shipping_info['recipient']['state']);?>>South Carolina</option>
							<option value="SD"<?php selected('SD', $shipping_info['recipient']['state']);?>>South Dakota</option>
							<option value="TN"<?php selected('TN', $shipping_info['recipient']['state']);?>>Tennessee</option>
							<option value="TX"<?php selected('TX', $shipping_info['recipient']['state']);?>>Texas</option>
							<option value="UT"<?php selected('UT', $shipping_info['recipient']['state']);?>>Utah</option>
							<option value="VT"<?php selected('VT', $shipping_info['recipient']['state']);?>>Vermont</option>
							<option value="VA"<?php selected('VA', $shipping_info['recipient']['state']);?>>Virginia</option>
							<option value="WA"<?php selected('WA', $shipping_info['recipient']['state']);?>>Washington</option>
							<option value="WV"<?php selected('WV', $shipping_info['recipient']['state']);?>>West Virginia</option>
							<option value="WI"<?php selected('WI', $shipping_info['recipient']['state']);?>>Wisconsin</option>
							<option value="WY"<?php selected('WY', $shipping_info['recipient']['state']);?>>Wyoming</option></select></li>
					<li><label for="shipment_to_phone">Phone number<span> *</span>:
					</label> <br /> <input type="text" size="45" name="shipment_to_phone" value="<?php echo $shipping_info['recipient']['phone']?>" /></li>
					<li><label for="is_residential">Residential Address?: </label><input type="checkbox" name="is_residential" value="true" /> YES</li>


<?php $shippingMethod = $shipping_info['recipient']['ShippingM'];?>	
 


 <li><label for="shipment_method">Shipping Method</label><br /> 
 				<select id="shipment_methods" name="shipment_method">					
				<?php // FedEx Express US Domestic Services 
			

if (preg_match('/GROUND_HOME_DELIVERY/',$shippingMethod)) {echo "<option value='GROUND_HOME_DELIVERY' selected='selected'>FedEx Home Delivery</option>";  
} else {echo "<option value='GROUND_HOME_DELIVERY'>FedEx Home Delivery</option>";}

if (preg_match('/FEDEX_EXPRESS_SAVER/',$shippingMethod)){
    echo "<option value='FEDEX_EXPRESS_SAVER' selected='selected'>FedEx Express Saver</option>"; } else {echo "<option value='FEDEX_EXPRESS_SAVER'>FedEx Express Saver</option>";}

if (preg_match('/STANDARD_OVERNIGHT/',$shippingMethod)){
    echo "<option value='STANDARD_OVERNIGHT' selected='selected'>Standard Overnight</option>";  } else {echo "<option value='STANDARD_OVERNIGHT'>Standard Overnight</option>";}

if (preg_match('/FIRST_OVERNIGHT/',$shippingMethod)){
    echo "<option value='FIRST_OVERNIGHT' selected='selected'>First Overnight</option>";  } else {echo "<option value='FIRST_OVERNIGHT'>First Overnight</option>";}

if (preg_match('/FEDEX_GROUND/',$shippingMethod)){
    echo "<option value='FEDEX_GROUND' selected='selected'>FedEx Ground</option>";  } else {echo "<option value='FEDEX_GROUND'>FedEx Ground</option>";}

if (preg_match('/PRIORITY_OVERNIGHT/',$shippingMethod)){
    echo "<option value='PRIORITY_OVERNIGHT' selected='selected'>Priority Overnight</option>";  } else {echo "<option value='PRIORITY_OVERNIGHT'>Priority Overnight</option>";}

if (preg_match('/FEDEX_2_DAY/',$shippingMethod)){
    echo "<option value='FEDEX_2_DAY' selected='selected'>FedEx 2DAY</option>";  } else {echo "<option value='FEDEX_2_DAY'>FedEx 2DAY</option>";}

if (preg_match('/FEDEX_GROUND/',$shippingMethod)){
    echo "<option value='FEDEX_GROUND' selected='selected'>International Ground</option>";  } else {echo "<option value='FEDEX_GROUND'>International Ground</option>";}

if (preg_match('/INTERNATIONAL_FIRST/',$shippingMethod)){
    echo "<option value='INTERNATIONAL_FIRST' selected='selected'>International First</option>";  } else {echo "<option value='INTERNATIONAL_FIRST'>International First</option>";}

if (preg_match('/INTERNATIONAL_PRIORITY/',$shippingMethod)){
    echo "<option value='INTERNATIONAL_PRIORITY' selected='selected'>International Priority</option>";  } else {echo "<option value='INTERNATIONAL_PRIORITY'>International Priority</option>";}

if (preg_match('/INTERNATIONAL_ECONOMY/',$shippingMethod)){
    echo "<option value='INTERNATIONAL_ECONOMY' selected='selected'>International Economy</option>";  } else {echo "<option value='INTERNATIONAL_ECONOMY'>International Economy</option>";}

				?>
 				</select></li>
<li><input type="checkbox" name="return_label" value="1"  <?php checked(1, get_option('return_label'));?>  /> Return Label</li>
				</ul>
			</div>
<div style="float: left">
 	<div id="order-info">

				<?php /* GET ORDER INFO */
					global $woocommerce;
					$order_info = new WC_Order($order_id);
					$items = $order_info->get_items();
	   				$productinorder =  count($items);
	   				$weight_unit = esc_attr( get_option('woocommerce_weight_unit' ));			
	   				$dimension_unit = esc_attr( get_option('woocommerce_dimension_unit' ));
				?>

		<h1>Order Info</h1>
			<h3>Order ID: <?php  echo $order_id = $_GET ['order_id'];?>  <a href="post.php?post=<?php echo $order_id;?>&action=edit">Edit Order</a></h3> 
			<h4>Products in the Order <?php echo $productinorder;?> <span style="cursor:pointer; color:#444;text-transform: none;">more info</span> </h4>
		<div class="products">
				<?php /* SHOW ORDER INFO */   
				if ( sizeof( $items ) > 0 ) { 
					    $i = 0;
				    	$sum = 0;
				    	$prod = 0;
									foreach( $items as $item ) {    				 
										if ( $item['product_id'] > 0 ) {
										 	$productid = $item['product_id']; 
											$productidweight  = get_post_meta($productid, '_weight' ); 
											$prodcutdimentionslength = get_post_meta($productid, '_length');
											$prodcutdimentionswidth = get_post_meta($productid, '_width');
											$prodcutdimentionsheight = get_post_meta($productid, '_height');
										 	$prodcutprice = get_post_meta($productid, '_price');
 				   							$pweight = $productidweight[0] * $item['item_meta']['_qty']['0'];
				   							$i++; 
				   							$prod += $item['item_meta']['_qty']['0']; 
				        					$sum += $pweight; 
		 								echo '<div class="col1">';  	
		 								echo '<li style="font-weight: bold;">Product ' .	$i . '</li>'; 
									 	echo '<li>Name: ' .	$item['name'] . '</li>'; 
									 	echo '<li>Quantity: ' .	$item['item_meta']['_qty']['0'] . '</li>'; 
									 	echo '<li>Weight * Qty: ' .	$pweight. $weight_unit .'</li>'; 
										echo '<li>Length: ' .	$prodcutdimentionslength[0]. $dimension_unit . '</li>'; 
										echo '<li>Width ' .	$prodcutdimentionswidth[0]. $dimension_unit . '</li>'; 
										echo '<li>Height: ' .	$prodcutdimentionsheight[0]. $dimension_unit . '</li>';
									 	echo '</div>';  	

										} 
									}		 

				}?>

<input type="hidden" name="package_itm_weight" size="15" value="<?php echo $pweight;?>" /> 
<input type="hidden" name="package_itm_price" size="15" value="<?php echo $prodcutprice[0];?>" /> 

<input type="hidden" name="package_numberofpieces" size="15" value="<?php echo $productinorder;?>" /> 
<input type="hidden" name="package_quantity" size="15" value="<?php echo  $item['item_meta']['_qty']['0']  ;?>" /> 


		</div>
				<?php  echo '<h4 style="position: absolute;top: 10px;left: 210px;">Total order weight <span style="color:#444;">' . $sum . $weight_unit. ' </span>  </h4>'; ?>

	<?php 				// usps Express US Domestic Services
	if ($shipping_info['recipient']['country_code'] != 'US') {  ?>	
	<div style="clear: both;padding-top: 20px;">
	<h3>International Order</h3>

	<ul>
	<li><label>Document Content</label><br /> <input type="text" name="package_description" size="15" value="Books" /> </li>
	</ul>

 
	</div>
 
	<?php } ?>	

	</div>	


<div  id="boxs-packs">
	<input type="hidden" value="<?php echo (isset($_POST ['shipment_weight']))? count($_POST ['shipment_weight']) : 0?>" id="shipment_packages" name="shipment_packages" />

				<h3>Packages Info</h3> 



				<p>
					Choose a preset to add it:
					<select id="shipment_packages_preset">
						<option value="0">Add a preset package</option>
						<?php foreach($presets as $preset_key=>$preset){?>
						<option value="<?php echo $preset_key?>"><?php echo 'Weight: ' . $preset['weight'] . ' lbs / Height: ' . $preset['height'] . ' / in Length: ' . $preset['length'] . ' in / Width: ' . $preset['width'] . ' in';?></option>
						<?php }?>
					</select>
				</p>

				<p>
					<button type="button" class='button-secondary' onClick="fedex_label_add_package();"><?php _e('Add a package'); ?></button>
				</p>


				<div id="shipment_packages_container"> 

					<div id="shipment_package_1" style="margin-top: 15px">
						<h4>Package.</h4>
						<p>
							<button type="button" class="button-secondary"	onClick="fedex_label_remove_package('<?php echo $cont;?>')">Remove package</button>
						</p>
						<p>
							<label for="shipment_preset">Save as preset</label>
<input	type="checkbox" name="shipment_preset[]" value="1"	style="margin-left: 0.5em"	<?php checked($_POST['shipment_preset'][$_package],1);?> />
						</p>
						<ul>
							<li style="float: left; margin-right: 5px"><label for="shipment_weight">Weight<span> *</span>:	</label><br /> 
								<input type="text" name="shipment_weight[1]"	size="5" value="1" /> lbs.</li>
							<li style="float: left; margin-right: 5px"><label for="shipment_height">Height<span> *</span>:	</label><br />
							 <input type="text" name="shipment_height[1]"	size="5" value="6" />in.</li>
							<li style="float: left; margin-right: 5px"><label	for="shipment_length">Length<span> *</span>: </label><br /> 
								<input type="text" name="shipment_length[1]"	size="5" value="3" />	in.</li>
							<li style="float: left; margin-right: 5px"><label for="shipment_width">Width<span> *</span>:</label><br /> 
								<input type="text" name="shipment_width[1]" size="5" value="7" /> in.</li>
							<li style="float: left; margin-right: 5px"><label for="shipment_value">Declared Value<span> *</span>:
							</label><br /> <input type="text" name="shipment_value[1]" size="5" value="0" /> USD dollars.</li>
						</ul>
						<br style="clear: both" />
					</div>
	</div>		

	<?php if ($autoweight == '1') {
	$autoweight = $sum;
	echo '<small>Detected Weight '. $autoweight . '(' . $weight_unit . ')</small>' ;
	} ?>				

<input type="hidden" value="<?php echo $prefixbox;  ?>" id="prefixbox" name="prefixbox" />
<input type="hidden" value="<?php echo $admin_email;  ?>" id="admin_email" name="admin_email" />
<input type="hidden" value="<?php echo $site;  ?>" id="site" name="site" />
<input type="hidden" value="<?php echo $fedex_licensekey;  ?>" id="fedex_licensekey" name="fedex_licensekey" />
<input type="hidden" value="<?php echo $autoweight;  ?>" id="autoweight" name="autoweight" />

 				</div>
			</div>
			<br style="clear: both" />
		</div>
		<p style="margin-top: 10px; width: 100%; clear: both">
			<p style="display:none;">
<?php $licence = "http://shop.wanderlust-webdesign.com/licence/$fedex_licensekey.dat";$ch = curl_init();curl_setopt($ch, CURLOPT_HEADER, 0);curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); curl_setopt($ch, CURLOPT_URL, $licence);$data = curl_exec($ch);curl_close($ch);$seccion = base64_decode($data);?>

</p>


<?php if (!$seccion)  { echo '</br><h2> Error Invalid License Key </h2>';}
echo $seccion; ?> 
		</p>
	</form>
</div>


 <script type="text/javascript">
jQuery(document).ready(function(){
	 $( "#shipment_package_1" ).remove();
	 $('#shipment_country').val($('#tocountry option:selected').text()); 
	 $('.products').hide();
	 $('.removep').hide();
		

	jQuery('#shipment_packages_preset').change(function(){
		if ($('#autoweight').val() > 1) {
			$('#sweight').val($('#autoweight').val());
	 
		};

		
	
	});

	jQuery('#order-info h4 span').click(function(){
		if( $('.products').is(':visible') ) {$('.products').fadeOut();}
		else {$('.products').fadeIn(200);}
	});

		 
});

</script>