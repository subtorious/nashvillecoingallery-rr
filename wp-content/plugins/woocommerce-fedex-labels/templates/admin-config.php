<?php 
$validation = true;
$error = "";
if(isset($_POST['pvit_fedex_shipper_config_save'])){
	if(!empty($_POST['pvit_fedex_licensekey'])){add_option('pvit_fedex_licensekey',$_POST['pvit_fedex_licensekey']);update_option('pvit_fedex_licensekey', $_POST['pvit_fedex_licensekey']);}else{$error .= "License Key Error <br />";}
	if(!empty($_POST['pvit_fedex_account'])){add_option('pvit_fedex_account',$_POST['pvit_fedex_account']);update_option('pvit_fedex_account', $_POST['pvit_fedex_account']);}else{$error .= "We need your Fedex Account Number <br />";}
	if(!empty($_POST['pvit_fedex_meter'])){add_option('pvit_fedex_meter',$_POST['pvit_fedex_meter']);update_option('pvit_fedex_meter', $_POST['pvit_fedex_meter']);}else{$error .= "We need your Fedex Account Meter <br />";}
	if(!empty($_POST['pvit_fedex_password'])){add_option('pvit_fedex_password',$_POST['pvit_fedex_password']);update_option('pvit_fedex_password', $_POST['pvit_fedex_password']);}else{$error .= "We need your Fedex Password <br />";}
	if(!empty($_POST['pvit_fedex_key'])){add_option('pvit_fedex_key',$_POST['pvit_fedex_key']);update_option('pvit_fedex_key', $_POST['pvit_fedex_key']);}else{$error .= "We need your Fedex API Key <br />";}
	if(!empty($_POST['pvit_fedex_hubid'])){add_option('pvit_fedex_hubid',$_POST['pvit_fedex_hubid']);update_option('pvit_fedex_hubid', $_POST['pvit_fedex_hubid']);}else{$error .= "We need your Fedex HUB ID <br />";}
	if(!empty($_POST['pvit_fedex_sender_name'])){add_option('pvit_fedex_sender_name',$_POST['pvit_fedex_sender_name']);update_option('pvit_fedex_sender_name', $_POST['pvit_fedex_sender_name']);}else{$error .= "We need your Sender Name <br />";}
	if(!empty($_POST['pvit_fedex_sender_company'])){add_option('pvit_fedex_sender_company',$_POST['pvit_fedex_sender_company']);update_option('pvit_fedex_sender_company', $_POST['pvit_fedex_sender_company']);}
	if(!empty($_POST['pvit_fedex_sender_address1'])){add_option('pvit_fedex_sender_address1',$_POST['pvit_fedex_sender_address1']);update_option('pvit_fedex_sender_address1', $_POST['pvit_fedex_sender_address1']);}else{$error .= "We need your Sender Address 1<br />";}
	if(!empty($_POST['pvit_fedex_sender_address2'])){add_option('pvit_fedex_sender_address2',$_POST['pvit_fedex_sender_address2']);update_option('pvit_fedex_sender_address2', $_POST['pvit_fedex_sender_address2']);}
	if(!empty($_POST['pvit_fedex_sender_state'])){add_option('pvit_fedex_sender_state',$_POST['pvit_fedex_sender_state']);update_option('pvit_fedex_sender_state', $_POST['pvit_fedex_sender_state']);}else{$error .= "We need your Sender State<br />";}	
	if(!empty($_POST['pvit_fedex_shipper_city'])){add_option('pvit_fedex_shipper_city',$_POST['pvit_fedex_shipper_city']);update_option('pvit_fedex_shipper_city', $_POST['pvit_fedex_shipper_city']);}else{$error .= "We need your Sender City<br />";}
	if(!empty($_POST['pvit_fedex_shipper_phone'])){add_option('pvit_fedex_shipper_phone',$_POST['pvit_fedex_shipper_phone']);update_option('pvit_fedex_shipper_phone', $_POST['pvit_fedex_shipper_phone']);}else{$error .= "We need your Sender Phone<br />";}
	if(!empty($_POST['pvit_fedex_shipper_zipcode'])){add_option('pvit_fedex_shipper_zipcode',$_POST['pvit_fedex_shipper_zipcode']);update_option('pvit_fedex_shipper_zipcode', $_POST['pvit_fedex_shipper_zipcode']);}else{$error .= "We need your Sender Zipcode<br />";}
	if(!empty($_POST['pvit_fedex_label_type'])){add_option('pvit_fedex_label_type',$_POST['pvit_fedex_label_type']);update_option('pvit_fedex_label_type', $_POST['pvit_fedex_label_type']);}else{$error .= "We need Label Type <br />";}
	if(!empty($_POST['pvit_fedex_image_type'])){add_option('pvit_fedex_image_type',$_POST['pvit_fedex_image_type']);update_option('pvit_fedex_image_type', $_POST['pvit_fedex_image_type']);}else{$error .= "We need Label Image Type <br />";}

	if($_POST['pvit_fedex_email_label'] == 1){add_option('pvit_fedex_email_label',1);update_option('pvit_fedex_email_label', 1);}else{add_option('pvit_fedex_email_label',0);update_option('pvit_fedex_email_label', 0);}

	if(!empty($_POST['pvit_fedex_email_label_to'])){add_option('pvit_fedex_email_label_to',$_POST['pvit_fedex_email_label_to']);update_option('pvit_fedex_email_label_to', $_POST['pvit_fedex_email_label_to']);}else{ }
	if(!empty($_POST['pvit_fedex_email_label_from'])){add_option('pvit_fedex_email_label_from',$_POST['pvit_fedex_email_label_from']);update_option('pvit_fedex_email_label_from', $_POST['pvit_fedex_email_label_from']);}else{ }

	if($_POST['pvit_fedex_auto_weight'] == 1){add_option('pvit_fedex_auto_weight',1);update_option('pvit_fedex_auto_weight', 1);}else{add_option('pvit_fedex_auto_weight',0);update_option('pvit_fedex_auto_weight', 0);}



	if($_POST['pvit_fedex_shipper_test'] == 1){		
		add_option('pvit_fedex_shipper_test',1);update_option('pvit_fedex_shipper_test', 1);
	}else{
		add_option('pvit_fedex_shipper_test',0);update_option('pvit_fedex_shipper_test', 0);
	}
}
if(!empty($error)){
?>
<div class="error fade">
<p><?php echo $error;?></p>
</div>
<?php 
}
?>

<script type="text/javascript">
jQuery(document).ready(function(){
	 $('.fedexinfo').hide();
	 $('.senderinfo').hide();
	 $('.pluginoptions').hide();	
	 
	jQuery('#fedexinfo').click(function(){
		if( $('.fedexinfo').is(':visible') ) {$('.fedexinfo').fadeOut();}
		else {$('.fedexinfo').fadeIn(200);}
	});

	jQuery('#senderinfo').click(function(){
		if( $('.senderinfo').is(':visible') ) {$('.senderinfo').fadeOut();}
		else {$('.senderinfo').fadeIn(200);}
	});

	jQuery('#pluginoptions').click(function(){
		if( $('.pluginoptions').is(':visible') ) {$('.pluginoptions').fadeOut();}
		else {$('.pluginoptions').fadeIn(200);}
	});	


});

</script>


<div style="margin-top: 20px">
	<form action="" method="post">
		<img src="<?php echo plugins_url('img/FedExExpressLogo_9.png',dirname(__FILE__));?>" style="max-height: 100px" />
<div style="clear: both;min-width: 275px;">	
		<p>Welcome to the Fedex Labels plugin</p>
		<h2 id="fedexinfo" style="cursor:pointer;float: left;clear: both;width: 250px;">Fedex API Information <img src="<?php echo plugins_url('img/up-arrow-icon.png',dirname(__FILE__));?>" style="position: absolute;margin: -3px 5px;width: 24px;" /> </h2>
	<div class="fedexinfo" style="float: left;clear: both;">

		<table class="form-table">
		<tbody>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_licensekey">Plugin License Key</label> </th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_licensekey" type="password" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_licensekey')?>" />
		</fieldset></td>
			</tr>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_account">Fedex Account Number</label>
		</th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_account" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_account')?>" />
		</fieldset></td>
			</tr>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_meter">Fedex Meter Number</label>
		</th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_meter" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_meter')?>"  />
		</fieldset></td>
			</tr>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_password">Fedex Web Services Password</label>
		</th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_password" type="password" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_password')?>" />
		</fieldset></td>
			</tr>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_key">Fedex API Key</label> 
		</th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_key" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_key')?>" />
		</fieldset></td>
			</tr>

			<tr valign="top">
		<th scope="row" class="titledesc">
			<label for="pvit_fedex_hubid">Fedex HUBID</label>
		</th>
		<td class="forminp">
		<fieldset>
			<input name="pvit_fedex_hubid" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_hubid')?>" />
		</fieldset></td>
			</tr>

		</tbody></table>

			 
	</div>	
</div>			

<div style="clear: both;float: left;min-width: 275px;">	
		<h2 id="senderinfo" style="cursor:pointer;float: left;clear: both;width: 250px;">Sender Address <img src="<?php echo plugins_url('img/up-arrow-icon.png',dirname(__FILE__));?>" style="position: absolute;margin: -3px 5px;width: 24px;" /> </h2>

		<table  class="form-table senderinfo">
		<tbody>

			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_sender_name">Sender Name <span> *</span>:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 <input	name="pvit_fedex_sender_name" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_sender_name')?>" />
			</fieldset></td>
			</tr>
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_sender_company">Sender Company:</label> 
			</th>
			<td class="forminp">
			<fieldset>
				 <input name="pvit_fedex_sender_company" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_sender_company')?>" />
			</fieldset></td>
			</tr>
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_sender_address1">Sender Address 1<span> *</span>:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 <input	name="pvit_fedex_sender_address1" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_sender_address1')?>" />
			</fieldset></td>
			</tr>		

			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_sender_address2">Sender Address 2:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 <input name="pvit_fedex_sender_address2" type="text" size="45" class="input medium" value="<?php echo get_option('pvit_fedex_sender_address2')?>" />
			</fieldset></td>
			</tr>					

			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_sender_state">State<span> *</span>:</label> 
			</th>
			<td class="forminp">
			<fieldset>
				<select name="pvit_fedex_sender_state" style="display: block;width:385px;">
							<option value="AL" <?php selected('AL', get_option('pvit_fedex_sender_state'));?>>Alabama</option>
							<option value="AK" <?php selected('AK', get_option('pvit_fedex_sender_state'));?>>Alaska</option>
							<option value="AZ" <?php selected('AZ', get_option('pvit_fedex_sender_state'));?>>Arizona</option>
							<option value="AR" <?php selected('AR', get_option('pvit_fedex_sender_state'));?>>Arkansas</option>
							<option value="CA" <?php selected('CA', get_option('pvit_fedex_sender_state'));?>>California</option>
							<option value="CO" <?php selected('CO', get_option('pvit_fedex_sender_state'));?>>Colorado</option>
							<option value="CT" <?php selected('CT', get_option('pvit_fedex_sender_state'));?>>Connecticut</option>
							<option value="DE" <?php selected('DE', get_option('pvit_fedex_sender_state'));?>>Delaware</option>
							<option value="DC" <?php selected('DC', get_option('pvit_fedex_sender_state'));?>>District of Columbia</option>
							<option value="FL" <?php selected('FL', get_option('pvit_fedex_sender_state'));?>>Florida</option>
							<option value="GA" <?php selected('GA', get_option('pvit_fedex_sender_state'));?>>Georgia</option>
							<option value="HI" <?php selected('HI', get_option('pvit_fedex_sender_state'));?>>Hawaii</option>
							<option value="ID" <?php selected('ID', get_option('pvit_fedex_sender_state'));?>>Idaho</option>
							<option value="IL" <?php selected('IL', get_option('pvit_fedex_sender_state'));?>>Illinois</option>
							<option value="IN" <?php selected('IN', get_option('pvit_fedex_sender_state'));?>>Indiana</option>
							<option value="IA" <?php selected('IA', get_option('pvit_fedex_sender_state'));?>>Iowa</option>
							<option value="KS" <?php selected('KS', get_option('pvit_fedex_sender_state'));?>>Kansas</option>
							<option value="KY" <?php selected('KY', get_option('pvit_fedex_sender_state'));?>>Kentucky</option>
							<option value="LA" <?php selected('LA', get_option('pvit_fedex_sender_state'));?>>Louisiana</option>
							<option value="ME" <?php selected('ME', get_option('pvit_fedex_sender_state'));?>>Maine</option>
							<option value="MD" <?php selected('MD', get_option('pvit_fedex_sender_state'));?>>Maryland</option>
							<option value="MA" <?php selected('MA', get_option('pvit_fedex_sender_state'));?>>Massachusetts</option>
							<option value="MI" <?php selected('MI', get_option('pvit_fedex_sender_state'));?>>Michigan</option>
							<option value="MN" <?php selected('MN', get_option('pvit_fedex_sender_state'));?>>Minnesota</option>
							<option value="MS" <?php selected('MS', get_option('pvit_fedex_sender_state'));?>>Mississippi</option>
							<option value="MO" <?php selected('MO', get_option('pvit_fedex_sender_state'));?>>Missouri</option>
							<option value="MT" <?php selected('MT', get_option('pvit_fedex_sender_state'));?>>Montana</option>
							<option value="NE" <?php selected('NE', get_option('pvit_fedex_sender_state'));?>>Nebraska</option>
							<option value="NV" <?php selected('NV', get_option('pvit_fedex_sender_state'));?>>Nevada</option>
							<option value="NH" <?php selected('NH', get_option('pvit_fedex_sender_state'));?>>New Hampshire</option>
							<option value="NJ" <?php selected('NJ', get_option('pvit_fedex_sender_state'));?>>New Jersey</option>
							<option value="NM" <?php selected('NM', get_option('pvit_fedex_sender_state'));?>>New Mexico</option>
							<option value="NY" <?php selected('NY', get_option('pvit_fedex_sender_state'));?>>New York</option>
							<option value="NC" <?php selected('NC', get_option('pvit_fedex_sender_state'));?>>North Carolina</option>
							<option value="ND" <?php selected('ND', get_option('pvit_fedex_sender_state'));?>>North Dakota</option>
							<option value="OH" <?php selected('OH', get_option('pvit_fedex_sender_state'));?>>Ohio</option>
							<option value="OK" <?php selected('OK', get_option('pvit_fedex_sender_state'));?>>Oklahoma</option>
							<option value="OR" <?php selected('OR', get_option('pvit_fedex_sender_state'));?>>Oregon</option>
							<option value="PA" <?php selected('PA', get_option('pvit_fedex_sender_state'));?>>Pennsylvania</option>
							<option value="RI" <?php selected('RI', get_option('pvit_fedex_sender_state'));?>>Rhode Island</option>
							<option value="SC" <?php selected('SC', get_option('pvit_fedex_sender_state'));?>>South Carolina</option>
							<option value="SD" <?php selected('SD', get_option('pvit_fedex_sender_state'));?>>South Dakota</option>
							<option value="TN" <?php selected('TN', get_option('pvit_fedex_sender_state'));?>>Tennessee</option>
							<option value="TX" <?php selected('TX', get_option('pvit_fedex_sender_state'));?>>Texas</option>
							<option value="UT" <?php selected('UT', get_option('pvit_fedex_sender_state'));?>>Utah</option>
							<option value="VT" <?php selected('VT', get_option('pvit_fedex_sender_state'));?>>Vermont</option>
							<option value="VA" <?php selected('VA', get_option('pvit_fedex_sender_state'));?>>Virginia</option>
							<option value="WA" <?php selected('WA', get_option('pvit_fedex_sender_state'));?>>Washington</option>
							<option value="WV" <?php selected('WV', get_option('pvit_fedex_sender_state'));?>>West Virginia</option>
							<option value="WI" <?php selected('WI', get_option('pvit_fedex_sender_state'));?>>Wisconsin</option>
							<option value="WY" <?php selected('WY', get_option('pvit_fedex_sender_state'));?>>Wyoming</option></select>
			</fieldset></td>
			</tr>		
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_shipper_city">City<span> *</span>:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 <input type="text" size="45" name="pvit_fedex_shipper_city" value="<?php echo get_option('pvit_fedex_shipper_city')?>" />
			</fieldset></td>
			</tr>								
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_shipper_city">Country Code:<span> *</span>:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 US.
			</fieldset></td>
			</tr>			
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_shipper_phone">Phone number<span> *</span>:</label> 
			</th>
			<td class="forminp">
			<fieldset>
				 <input type="text" size="45" name="pvit_fedex_shipper_phone" value="<?php echo get_option('pvit_fedex_shipper_phone')?>" />
			</fieldset></td>
			</tr>		
			<tr valign="top">
			<th scope="row" class="titledesc">
				 <label for="pvit_fedex_shipper_zipcode">ZipCode<span> *</span>:</label>
			</th>
			<td class="forminp">
			<fieldset>
				 <input type="text" size="45" name="pvit_fedex_shipper_zipcode" value="<?php echo get_option('pvit_fedex_shipper_zipcode')?>" />
			</fieldset></td>
			</tr>											

		</tbody></table>	



</div>

<div style="clear: both;float: left;min-width: 275px;">	
		<h2 id="pluginoptions" style="cursor:pointer;float: left;clear: both;width: 250px;">Plugin Options <img src="<?php echo plugins_url('img/up-arrow-icon.png',dirname(__FILE__));?>" style="position: absolute;margin: -3px 5px;width: 24px;" /> </h2>

		<table  class="form-table pluginoptions">
		<tbody>

			<tr valign="top">
			<th scope="row" class="titledesc">
				  <label>Auto-Detect Weight from Order: </label>
			</th>
			<td class="forminp">
			<fieldset>
				  <input type="checkbox" name="pvit_fedex_auto_weight" value="1" <?php checked(1, get_option('pvit_fedex_auto_weight'));?> /> Enabled
			</fieldset></td>
			</tr>

			<tr valign="top">
			<th scope="row" class="titledesc">
				  <label for="pvit_fedex_label_type">Label Type:</label> 
			</th>
			<td class="forminp">
			<fieldset>
				  <select name="pvit_fedex_label_type" style="display: block;width:385px;">
							<option value="PAPER_4X6" <?php selected('PAPER_4X6', get_option('pvit_fedex_label_type'));?>>PAPER_4X6</option>
							<option value="PAPER_4X8" <?php selected('PAPER_4X8', get_option('pvit_fedex_label_type'));?>>PAPER_4X8</option>
							<option value="PAPER_4X9" <?php selected('PAPER_4X9', get_option('pvit_fedex_label_type'));?>>PAPER_4X9</option>
							<option value="PAPER_7X4.75" <?php selected('PAPER_7X4.75', get_option('pvit_fedex_label_type'));?>>PAPER_7X4.75</option>
							<option value="PAPER_8.5X11_BOTTOM_HALF_LABEL" <?php selected('PAPER_8.5X11_BOTTOM_HALF_LABEL', get_option('pvit_fedex_label_type'));?>>PAPER_8.5X11_BOTTOM_HALF_LABEL</option>
							<option value="PAPER_8.5X11_TOP_HALF_LABEL" <?php selected('PAPER_8.5X11_TOP_HALF_LABEL', get_option('pvit_fedex_label_type'));?>>PAPER_8.5X11_TOP_HALF_LABEL</option>
							<option value="PAPER_LETTER" <?php selected('PAPER_LETTER', get_option('pvit_fedex_label_type'));?>>PAPER_LETTER</option>
					</select>
			</fieldset></td>
			</tr>

			<tr valign="top">
			<th scope="row" class="titledesc">
				  <label for="pvit_fedex_image_type">Image Type:</label> 
			</th>
			<td class="forminp">
			<fieldset>
				  <select name="pvit_fedex_image_type" style="display: block;width:385px;">
							<option value="PNG" <?php selected('PNG', get_option('pvit_fedex_image_type'));?>>PNG</option>
							<option value="PDF" <?php selected('PDF', get_option('pvit_fedex_image_type'));?>>PDF</option>
					</select>
			</fieldset></td>
			</tr>

			<tr valign="top">
			<th scope="row" class="titledesc">
				  <label for="pvit_fedex_email_label">Email Label to printer: </label> 
			</th>
			<td class="forminp">
			<fieldset>
				  <input type="checkbox" name="pvit_fedex_email_label" value="1" <?php checked(1, get_option('pvit_fedex_email_label'));?> />Enabled
					<br /> <input type="text" size="45" name="pvit_fedex_email_label_to" value="<?php echo get_option('pvit_fedex_email_label_to')?>" placeholder="Email To" />
					<br /> <input type="text" size="45" name="pvit_fedex_email_label_from" value="<?php echo get_option('pvit_fedex_email_label_from')?>" placeholder="Email From" />
				
			</fieldset></td>
			</tr>			

			<tr valign="top">
			<th scope="row" class="titledesc">
				  <label for="pvit_fedex_shipper_zipcode">Activate Testing Mode: </label>
			</th>
			<td class="forminp">
			<fieldset>
				  <input type="checkbox" name="pvit_fedex_shipper_test" value="1" <?php checked(1, get_option('pvit_fedex_shipper_test'));?> /> Enabled
			</fieldset></td>
			</tr>

		</tbody></table>	

 
</div>		
		<p style="margin-top: 10px; width: 100%; clear: both">
			<input type="submit" name="pvit_fedex_shipper_config_save" value="<?php _e('Save Configuration');?>" class="button-primary" />
		</p>
	</form>
</div>