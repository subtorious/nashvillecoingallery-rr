<?php
/** 

Displays the price table

Copyright (c) 2012, 2013 - IgniteWoo.com - ALL RIGHTS RESERVED 

*/ 

global $product;

// Load variations and attributes, if any exist. Displayed in the pricing table below.
if ( method_exists( $product, 'get_available_variations' ) )
	$vars = $product->get_available_variations();

$attrs = array();

if ( !empty( $vars  ) )
foreach( $vars as $v ) { 

	if ( !empty( $v['attributes'] ) )
		foreach ( $v['attributes'] as $name => $options ) {
						
			if ( taxonomy_exists( $name ) ) {
		
				$orderby = $woocommerce->attribute_orderby( $name );

				switch ( $orderby ) {
					case 'name' :
						$args = array( 'orderby' => 'name', 'hide_empty' => false, 'menu_order' => false );
					break;
					case 'id' :
						$args = array( 'orderby' => 'id', 'order' => 'ASC', 'menu_order' => false );
					break;
					case 'menu_order' :
						$args = array( 'menu_order' => 'ASC' );
					break;
				}
								
				$terms = get_terms( $name, $args );
				
				foreach ( $terms as $term ) {

					if ( ! in_array( $term->slug, $options ) )
						continue;

					$attrs[ $v['variation_id'] ][] = $term->name;
				}
		
			} else { 
			
				$aname = str_replace( 'Attribute', '', $name );
			
				$aname = str_replace( 'attribute_', '', $aname );
				
				$aname = str_replace( 'attribute_pa_', '', $aname );
				
				$aname = str_replace( '_', '', $aname );
				
				$aname = ucwords( $aname );
				
				
				$attrs[ $v['variation_id'] ][] = $aname . ' ' . ucwords( $options ) ;
				
			}
		}
}


?>

<div style="clear:both;">
	
	<table class="discounts_table">
		<tr>
			<th colspan="3" class="discounts_table_label">
				<?php _e( 'Volume Discounts', 'ignitewoo_dynamic_discounts' ); ?>
			</th>
		</tr>
                <tr>
			<th>
				<?php _e( 'Quantity', 'ignitewoo_dynamic_discounts' ); ?>
			</th>
			<th>
				<?php _e( 'Discount', 'ignitewoo_dynamic_discounts' ); ?>
			</th>
			<th>
				<?php _e( 'For', 'ignitewoo_dynamic_discounts' ); ?>
			</th>
		</tr>
		
		<?php $i = 0;  ?>
		
		<?php foreach ( $discounts_to_display as $discount ) { ?>

			<?php if ( 0 == $i % 2 ) $class = 'alt'; else $class = ''; ?>
		
			<tr class="<?php echo $class ?>">
			
				<td style="width: 25%">
					<?php echo $discount['from'] . '-' . $discount['to']  ?>
				</td>
				
				<td style="width: 35%">
					<?php 
						if ( 'price_discount' == $discount['type'] )
							echo woocommerce_price( $discount['amount'] ) . ' off each';
						else if ( 'percentage_discount' == $discount['type'] )
							echo $discount['amount'] . '% off each';
						if ( 'fixed_price' == $discount['type'] )
							echo woocommerce_price( $discount['amount'] ) . ' each';
					?>
				</td>

				<td>
					<?php 
					if ( 'cart_item' == $discount['applies_to'] ) { 
					
						_e( 'Overall quantity', 'ignitewoo_dynamic_discounts' );
					
					} else if ( 'cat' == $discount['applies_to']['type'] ) {
					
						echo __( 'Categories', 'ignitewoo_dynamic_discounts' ) . ': <br/>';
						
						$names = array(); 
						
						foreach( $discount['applies_to']['args']['cats'] as $cid ) { 
							
							$term = get_term_by( 'id', $cid, 'product_cat' );
					
							if ( !empty( $term ) && !is_wp_error( $term ) )
								$names[] = $term->name;
								
						}
							
						echo implode( ', ', $names );
					
					
					} else if ( 'product' == $discount['applies_to']['type'] ) {
					
						if ( !empty( $discount['applies_to']['args']['variations']  ) ) { 
						
							echo __( 'Variations', 'ignitewoo_dynamic_discounts' ) . ':<br/>';
						
							foreach( $discount['applies_to']['args']['variations'] as $v ) {
							
								echo implode( ' - ',  $attrs[ $v ] ) . ' <br/> ';
								
							}
							
						} else { 
					
						_e( 'All variations', 'ignitewoo_dynamic_discounts' );
					
						}
						
					} else if ( 'product' == $discount['applies_to']['type'] ) {
					
						_e( 'Overall quantity', 'ignitewoo_dynamic_discounts' );
					
					}
					
					?>
				</td>
			</tr>

			<?php $i++ ?>
			
		<?php } ?>
	
	</table>
	
</div>