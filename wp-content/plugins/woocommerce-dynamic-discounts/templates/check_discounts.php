<?php
/** 

For pricing checks

Copyright (c) 2012, 2013 - IgniteWoo.com - ALL RIGHTS RESERVED 

*/ 

?>
	
	<div class="estimated_discounts" style="clear:both; display:none">
	
		<p><?php _e( 'Select a quantity before checking the price discount', 'ignitewoo_dynamic_discounts' ) ?></p>
		
		<button type="button" class="check_discounts button alt" style="margin-top: 5px;">
			<?php _e( 'Check Price Discount', 'ignitewoo_dynamic_discounts' ) ?>
		</button>
		
		
		<div class="estimated_cost_wrap" style="clear:both">
			<div class="estimated_cost" style="clear:both"></div>
			<div class="estimated_cost_note" style="clear:both;display:none">
				<?php _e( 'Estimates are based on your current cart contents (if any).', 'ignitewoo_dynamic_discounts' )?> 
				<?php _e( 'Discounts may change if your cart contents changes. View your cart to see any discounts.', 'ignitewoo_dynamic_discounts' )?>
			</div>
			
		</div>
		
	</div>
	

	