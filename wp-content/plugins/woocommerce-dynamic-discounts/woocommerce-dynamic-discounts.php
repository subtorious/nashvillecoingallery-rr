<?php
/*
Plugin Name: WooCommerce Dynamic Discounts
Plugin URI: http://ignitewoo.com
Description: Set pricing rules that offer volume discounts. Copyright (c) 2013 - IgniteWoo.com - All Rights Reserved. 
Author: IgniteWoo.com
Version: 2.2.8
Author URI: http://ignitewoo.com
*/


/** 

Copyright (c) 2012, 2013 - IgniteWoo.com - ALL RIGHTS RESERVED 

Each domain or subdomain where this plugin is used 
requires an individual license. 

The software is distrbuted WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. You use this software at your own risk.

*/

class IgniteWoo_Dynamic_Discounts { 

	public $plugin_url;
	
	public $plugin_path;
	
	public $compatible = false;
	
	public $discounts = null;

	function __construct() { 

		if ( !$this->pre_init() )
			return;

		$this->plugin_url = WP_PLUGIN_URL . '/' . str_replace( basename( __FILE__ ), '' , plugin_basename( __FILE__ ) );

		$this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
		
		add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );
		
		add_action( 'wp_head', array( &$this, 'maybe_add_hooks' ), 9999 );
		
		add_action( 'wp_head', array( &$this, 'wp_head' ), -1 );
		
		add_action( 'wp_footer', array( &$this, 'wp_footer' ), 999 );



		
	}

	 
	function load_plugin_textdomain() {

		$locale = apply_filters( 'plugin_locale', get_locale(), 'ignitewoo_dynamic_discounts' );

		load_textdomain( 'ignitewoo_dynamic_discounts', WP_LANG_DIR.'/woocommerce/ignitewoo_dynamic_discounts-'.$locale.'.mo' );

		$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		load_plugin_textdomain( 'ignitewoo_dynamic_discounts', false, $plugin_rel_path );

	}
	
	
	function pre_init() { 
		global $ignitewoo_dd_pricing_base_version;

		$res = true;

		if ( class_exists( 'woocommerce_dynamic_pricing' ) ) { 

			add_action( 'admin_notices', array( &$this, 'warning' ), -1 );

			$res = false;
		}

		$this->compatible = $res; 

		return $res; 
	}


	function warning() { 

		echo '<div class="error" style="font-weight: bold; font-style: italic; color: #cf0000; font-size: 1.3em" ><p>';
		_e( 'Warning: IgniteWoo Quantity Discounts plugin is not compatible with the WooCommerce Dynamic Pricing plugin. Deactivate one or the other.', 'ignitewoo_dynamic_discounts');
		echo '</div>';

	}


	function wp_head() { 
		global $woocommerce;
		
		if ( !is_product() ) 
			return;
			
		wp_register_script( 'jquery-blockui', $woocommerce->plugin_url() . '/assets/js/jquery-blockui/jquery.blockUI.min.js', array( 'jquery' ), '2.60', true );
		
		wp_enqueue_script( 'jquery-blockui' );
		
		$css = file_exists( get_stylesheet_directory() . '/woocommerce/product_discount_style.css' ) ? get_stylesheet_directory_uri() . '/woocommerce/product_discount_style.css' : $this->plugin_url . '/templates/product_discount_style.css';

		wp_enqueue_style( 'ignitewoo_dynamic_discounts', $css );
	
	}
	
	
	function wp_footer() { 
	
		$js = file_exists( get_stylesheet_directory() . '/woocommerce/product_discounts.js' ) ? get_stylesheet_directory_uri() . '/woocommerce/product_discounts.js' : $this->plugin_url . '/templates/product_discounts.js';

		?>
		<script src="<?php echo $js ?>"></script>

		<?php 
	}
	

	function maybe_add_hooks() { 

		if ( !is_product() ) 
			return;
				
		$show_table = get_option( 'woocommerce_dd_show_discount_table', false );
		
		$table_place = get_option( 'woocommerce_dd_discount_table_placement', false );
		
		$show_calc = get_option( 'woocommerce_dd_show_discount_calculator', false );
		
		if ( 'yes' == $show_calc ) 
			add_action( 'woocommerce_after_add_to_cart_button', array( &$this, 'after_add_to_cart_button' ), -99 );

		if ( 'yes' == $show_table && $table_place ) {
		
			if ( 'the_content_top' == $table_place ) 
				add_filter( 'the_content', array( &$this, 'table_top_content' ), -1, 1 );
			else 
				add_action( $table_place, array( &$this, 'get_dynamic_discounts' ), 5 );
			
		}
	
	}
	
        function after_add_to_cart_button() { 
		global $woocommerce, $ignitewoo_dd, $ignitewoo_dd_pricing;
			
		if ( !is_product() ) 
			return;

		if ( empty( $this->discounts ) )
			$this->discounts = $ignitewoo_dd_pricing->pricing_by_product->get_cart_item_discounts();

		if ( empty( $this->discounts ) )
			return;

        	$template_name = 'check_discounts.php'; 
	
		if ( ! defined( 'WC_TEMPLATE_PATH' ) )
			$template_path = $woocommerce->template_url;
		else
			$template_path = WC_TEMPLATE_PATH;
		
		$default_path = $ignitewoo_dd->plugin_path . '/templates/';

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		if ( !$template )
			$template = $default_path . $template_name;

		include( $template );

        }

        
	function get_dynamic_discounts() { 
		global $woocommerce, $ignitewoo_dd_pricing, $discounts_to_display;
		
		if ( !is_product() ) 
			return;

		if ( empty( $this->discounts ) )
			$discounts_to_display = $ignitewoo_dd_pricing->pricing_by_product->get_cart_item_discounts();
		else
			$discounts_to_display = $this->discounts;

		if ( empty( $discounts_to_display ) ) 
			return;

		if ( !defined( 'SORT_NATURAL' ) )
			define( 'SORT_NATURAL', 6 );
			
		ksort( $discounts_to_display, SORT_NATURAL );
			
		$template_name = 'product_discounts.php'; 
		
		if ( ! defined( 'WC_TEMPLATE_PATH' ) )
			$template_path = $woocommerce->template_url;
		else
			$template_path = WC_TEMPLATE_PATH;
		
		
		$default_path = $this->plugin_path . '/templates/';

		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);

		if ( !$template )
			$template = $default_path . $template_name;

		include( $template );
		
	}
	
	
	function table_top_content( $content = '' ) { 

		$content = $this->get_dynamic_discounts() . $content;
		
		return $content; 
	
	}

	
}


add_action( 'plugins_loaded', 'ignite_wdd_init', -99 ); 

function ignite_wdd_init() { 

	global $ignitewoo_dd;

	$ignitewoo_dd = new IgniteWoo_Dynamic_Discounts();


}


add_action( 'init', 'ignitewoo_dd_loader', 1 );

function ignitewoo_dd_loader() { 
	global $ignitewoo_dd;

	if ( !class_exists( 'Woocommerce' ) ) 
		return;

	if ( !$ignitewoo_dd->compatible )
		return;

	if ( is_admin() ) { 

		require_once( dirname( __FILE__ ) . '/woocommerce-dynamic-discounts-admin.php' );
	
		require_once( dirname( __FILE__ ) . '/woocommerce-dynamic-discounts-product-edit.php' );

		global $ignitewoo_dd_product_edit;

		$ignitewoo_dd_product_edit = new IgniteWoo_Dynamic_Discounts_Rules_Admin();
	}

	require_once( dirname( __FILE__ ) . '/woocommerce-dynamic-discounts-product-process.php' );

}

// Template tag
function ign_insert_discount_table() { 
	global $ignitewoo_dd;
 
	$this->get_dynamic_discounts();
 
}

add_action( 'wp_ajax_get_simulated_price' , 'get_simulated_price' );

add_action( 'wp_ajax_no_priv_get_simulated_price' , 'get_simulated_price' );

function get_simulated_price() { 
	global $woocommerce;
	
	$data = $_POST['item_data'];
	
	if ( empty( $data ) )
		die ( __( 'Error getting price', 'ignitewoo_dynamic_discounts' ) );

	$data = explode( '&', $data );
	
	if ( !is_array( $data ) )
		die ( __( 'Error getting price', 'ignitewoo_dynamic_discounts' ) );
		
	foreach( $data as $val ) {
	
		$vals = explode( '=', $val );
		
		$values[ $vals[0] ] = $vals[1]; 
	
	}
		
	if ( empty( $values['product_id'] ) && empty( $values['variation_id'] ) )
		die ( __( 'Error getting price', 'ignitewoo_dynamic_discounts' ) );
		
	if ( empty( $values['quantity'] ) || absint( $values['quantity'] ) <= 0 )
		die ( '<span style="color:#cf0000">' . __( 'Enter a quantity first', 'ignitewoo_dynamic_discounts' ) . '</span>' );
	
	$type = wp_get_post_terms( $values['product_id'], 'product_type' );
	
	if ( empty( $type ) || is_wp_error( $type ) )
		die ( __( 'Error detecting product type', 'ignitewoo_dynamic_discounts' ) );
	
	if ( 'variable' == $type[0]->slug && empty( $values['variation_id'] ) )
		die ( '<span style="color:#cf0000">' . __( 'SELECT A VARIATION', 'ignitewoo_dynamic_discounts' ) . '</span>' );
	
	$attrs = array();
	
	foreach( $values as $key => $val ) { 
	
		if ( 'attribute_' == substr( $key, 0, 10 ) ) { 
			$attrs[ substr( $key, 10 ) ] = $val; 
			
		}
	
	}
	
	$product_data = get_product( $values['variation_id'] ? $values['variation_id'] : $values['product_id'] );
	
	if ( !$product_data )
		die( __( 'Error locating product', 'ignitewoo_dynamic_discounts' ) );


	$woocommerce->cart->get_cart_from_session(); 

	$woocommerce->cart->add_to_cart( $values['product_id'], $values['quantity'], $values['variation_id'], $attrs );

	$woocommerce->cart->get_cart_from_session(); 
	
	$woocommerce->cart->calculate_totals();


	$price = $product_data->get_price();
	
	foreach( $woocommerce->cart->get_cart() as $cart_item_key => $cart_item ) { 

		if ( empty( $values['variation_id'] ) && $values['product_id'] == $cart_item['product_id'] ) {

			$price = get_option('woocommerce_tax_display_cart') == 'excl' ? $cart_item['data']->get_price_excluding_tax() : $cart_item['data']->get_price_including_tax(); 

			$price = woocommerce_price( $price );
		
			if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 )
				$price = apply_filters( 'woocommerce_cart_item_price', $price, $cart_item, $cart_item_key );
			else
				$price = apply_filters( 'woocommerce_cart_item_price_html', $price, $cart_item, $cart_item_key );
		
			$key = $cart_item_key; 
			
			$qty = $cart_item['quantity'] - $values['quantity']; 
			
			break;
			
		} else if ( !empty( $values['variation_id'] ) && $values['product_id'] == $cart_item['product_id'] && $values['variation_id'] == $cart_item['variation_id']  ) { 

			$price = get_option('woocommerce_tax_display_cart') == 'excl' ? $cart_item['data']->get_price_excluding_tax() : $cart_item['data']->get_price_including_tax(); 

			$price = woocommerce_price( $price );
			
			if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 )
				$price = apply_filters( 'woocommerce_cart_item_price', $price, $cart_item, $cart_item_key );
			else
				$price = apply_filters( 'woocommerce_cart_item_price_html', $price, $cart_item, $cart_item_key );

			$key = $cart_item_key;
			
			$qty = $cart_item['quantity'] - $values['quantity']; 
			
			break; 
		}
	}
	
	$woocommerce->cart->set_quantity( $key, $qty );
	
	echo $price;
	
	die;

}


// END OF CODE










































































































































































































































































































































if ( ! function_exists( 'ignitewoo_queue_update' ) )
	@require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );

ignitewoo_queue_update( plugin_basename( __FILE__ ), 'a9818b43343791b85355e7d87fc67b42', '5691' );
