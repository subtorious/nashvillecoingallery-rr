<?php 

class IgniteWoo_Dynamic_Discounts_Admin {


	function __construct() { 
		global $ignitewoo_dd;
		
		$this->plugin_url = $ignitewoo_dd->plugin_url;
		
		$this->plugin_path = $ignitewoo_dd->plugin_path;
		
		add_action( 'init', array( &$this, 'init' ), 99999 );
		
		add_filter ( 'woocommerce_settings_tabs_array', array( &$this, 'add_tab' ), -99, 1 );

		add_action( 'woocommerce_settings_tabs_qty_discounts', array( &$this, 'dynamic_settings' ) );

		add_action( 'woocommerce_update_options_qty_discounts',  array( &$this, 'save_settings' ) );

		add_action( 'wp_ajax_ignitewoo_dd_create_new_ruleset', array( &$this, 'create_new_ruleset' ) );

	}


	function init() { 

		wp_enqueue_script( 'jquery-ui-sortable' );

	}
	
	
	function add_tab( $tabs = '' ) { 

		$tabs['qty_discounts'] = __( 'Qty Discounts', 'ignitewoo_dynamic_discounts' );

		return $tabs;

	}


	function dynamic_settings() { 

		$rules = get_option( 'woocommerce_dd_discount_rules', false );

		$show_table = get_option( 'woocommerce_dd_show_discount_table', false );
		
		$table_place = get_option( 'woocommerce_dd_discount_table_placement', false );
		
		$show_calc = get_option( 'woocommerce_dd_show_discount_calculator', false );
		
		?>
		<style>
		    #woocommerce_extensions { display: none !important; }
		</style>
		
		<script>
			jQuery( document ).ready( function() { jQuery( "#woocommerce_extensions" ).css( "display", "none" ) } );
		</script>
		
		<h2><?php _e( 'Quantity Discount Settings', '' ) ?></h2>

		<p><?php _e( 'Configure the settings to control how your price discounts work in your store.', 'ignitewoo_dynamic_discounts' ) ?></p>

		<div>
		    <table class="form-table" style="width:99%;margin:0;padding:0">
		    <tr>
		    <td width="80%">

			<table>

			<tr>
				<th style="width: 120px; vertical-align:top">
					<h4 style="margin:0"><label for="logo_image">
						<?php _e( 'Enable discounts table', 'ignitewoo_dynamic_discounts' ) ?></label>
					</h4>
				</th>
				<td>
				
					<p class="description">
						<input type="checkbox" value="yes" name="dd_show_discount_table" <?php checked( $show_table, 'yes', true ) ?>>
						<?php _e( 'Show discount table on product pages.', 'ignitewoo_dynamic_discounts' ) ?><br/>
					</p>


					
				</td>
			</tr>
			
			<tr>
				<th style="width: 120px; vertical-align:top">
					<h4 style="margin:0"><label for="logo_image">
						<?php _e( 'Discount table placement', 'ignitewoo_dynamic_discounts' ) ?></label>
					</h4>
				</th>
				<td>
				
					<p class="description">
						<select name="dd_discount_table_placement">
						
							<option value="the_content_top" <?php selected( $table_place, 'the_content_top', true ) ?>><?php _e( 'Before Product Description', 'ignitewoo_dynamic_discounts' ) ?></option>
						
							<option value="woocommerce_before_add_to_cart_form" <?php selected( $table_place, 'woocommerce_before_add_to_cart_form', true ) ?>><?php _e( 'Before Add To Cart Form', 'ignitewoo_dynamic_discounts' ) ?></option>
							
							<option value="woocommerce_after_add_to_cart_form" <?php selected( $table_place, 'woocommerce_after_add_to_cart_form', true ) ?>><?php _e( 'After Add To Cart Form', 'ignitewoo_dynamic_discounts' ) ?></option>
						
							<option value="woocommerce_before_add_to_cart_button" <?php selected( $table_place, 'woocommerce_before_add_to_cart_button', true ) ?>><?php _e( 'Before Add To Cart Button', 'ignitewoo_dynamic_discounts' ) ?></option>
							
							<option value="woocommerce_after_add_to_cart_button" <?php selected( $table_place, 'woocommerce_after_add_to_cart_button', true ) ?>><?php _e( 'After Add To Cart Button', 'ignitewoo_dynamic_discounts' ) ?></option>
							
							<option value="woocommerce_after_single_product_summary" <?php selected( $table_place, 'woocommerce_after_single_product_summary', true ) ?>><?php _e( 'Before Information Tabs', 'ignitewoo_dynamic_discounts' ) ?></option>
							
							
							
						</select> 
						
						<?php _e( "Where to show the discounts table. If the placement does not come out right it's probably because your theme is built differently than normal. Experiment, or insert the table manually. See the documentation.", 'ignitewoo_dynamic_discounts' ) ?><br/>
					</p>


					
				</td>
			</tr>
			
			<tr>
				<th style="width: 120px; vertical-align:top">
					<h4 style="margin:0"><label for="logo_image">
						<?php _e( 'Enable discount calculator', 'ignitewoo_dynamic_discounts' ) ?></label>
					</h4>
				</th>
				<td>
				
					<p class="description">
						<input type="checkbox" value="yes" name="dd_show_discount_calculator" <?php checked( $show_calc, 'yes', true ) ?>>
						<?php _e( 'Show discount calculator button on product pages.', 'ignitewoo_dynamic_discounts' ) ?><br/>
					</p>


					
				</td>
			</tr>

			<tr>
				<th style="width: 120px; vertical-align:top">
					<h4 style="margin:0"><label for="logo_image">
						<?php _e( 'Sitewide Discounts', 'ignitewoo_dynamic_discounts' ) ?></label>
					</h4>
				</th>
				<td>
					<p class="description">
						<?php _e( 'You can enable sitewide discounts for products.', 'ignitewoo_dynamic_discounts' ) ?><br/>
						<?php _e( 'These rules will be overridden by the per-product discount rules if set for  individual products.', 'ignitewoo_dynamic_discounts' ) ?><br/>
						<?php _e( 'You can rearrange rulesets by dragging and dropping them into a new order.', 'ignitewoo_dynamic_discounts' ) ?><br/>
					</p>


					<?php $this->sitewide_discount_rule_box() ?>
				</td>
			</tr>

			
			</table>

		    </td>

		    <td style="width:20%; vertical-align:top" valign="top">
			    <div style="width:250px; border: 3px solid #000000; background-color: #f7f7f7; padding: 12px 0; font-weight:bold; font-style:italic; margin-top: 15px; text-align:center; border-radius:7px;-webkit-border-radius:7px">
				    <a title=" More Extensions + Custom WooCommerce Site Development " href="http://ignitewoo.com" target="_blank" style="color:#0000aa; text-decoration:none">
					    <img style="height:50px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABWCAYAAAB1s6tmAAAgAElEQVR4nO2de3xcV3Xvv2ce0uj9smVJli2/bYwSJ9iQmITQxPmEkKTNbdq0F27b3JIQ8rmUe00L96alcG9KA4HehA95XMItNB8gBZoCBZqSB05IQiC5TkyC67cdSdbbsiWNpJFmNI9z7h97ztF57DNz5iFZCef3+cgz3rPXWvvMOXvNWmuvvbbCeUZra2tIUZR6oDYQCDRWV1dvCoVC2wOBwEZFUdYqitKpKEozUA1EgMD5HbEPH285qEACiGmaNqZp2rCmaf2qqp5MpVJHZmdnTwBzQAyInTlzJn2+BqqcL8HAOmBdKBTaEg6HdwaDwYsCgUC3oijV53FMPnz4sEHTtAlVVQ9lMpmD8/PzB1RVPQWcAkaXeixLrbDqgYuAdwHvBbYDa4HQEo/Dhw8fxSEJ9AC/Al4C9gOHEBbYomOpFFY1cANwI0Jhrcu2+fDh482LaaAPobT+BfjJYgtcTIUVQFhO1wKfRFhT9fjWlA8fbzUkEcrrBeAB4JdAGhEbKysWQ2EFgGbgcuCjwBVAxSLI8eHDx/JDDHgS+ArwKkKRlQ2LobCuAP4Y+B2gdRH4+/DhY/mjH/ge8PfAsXIxDZaLEcKquhm4C7gOqCkjbx8+fLy50ADsBjYBs8AgwnUsCeVSWJuAvwH+S/a9Dx8+fABsAC4DmoDDCJexaJSqsEKIFIUvIVYBG0rk58OHj7cWFIRe2AVsQcS1JotlVorCqkCkKTyQHUw53UsfPny8tRAC3gZciVBa54BMoUyKVTK1wB8C9wJdRfLw4cPHbx5WIbyyYUQOV0HbfIpZJawF/jMit2ptEfQ+fPh4E+DDbbCzVv7ZrAr/cg5enIbL6+F3V8BUGv6m3xNrFTgC3A38ELGP0RMKtbAiwAeBvwQ6Ob97EX348LGI2BCBSAA+1AbNYXh5BmYy8Cer4D0NMK/Cvkn4i064owPaK+D/jHhirQArgLcjtvn04DHJtBCFVQH8LkIrrsFXVj58vKVxfA5+MS0U1qFZ+HiPsKj+ZJX4PACkNLimCWqDEFc9KywQ+qMFuAR4BRjwQlRIqZYrgM8iLCsfPny8xZFBKCQ00LLvU9n3/fMwlISPrRav/fNFiQgg0qC+ikh/8ETgBVuAL+DnWPnw4QOYTMP+GXhbtXidLK1CVjdCv+SNiXvZiLwCYVm9o6QhLSEiAagMQFqD2YIXTn34WFxEIhEikQiapqFpGtPTZd1uJ0VdXR2BgLBPUqkU8XgcTdOK5jevwnNReGAIno/C1Y0lD/E6xHaeu8ix/9CLwvogcE3JwzEhEoArGiAkiYLNqsJfHk8VxnNzBN7bCO+qg45KISOtwUQKjsyJL/fVGCQK2D9eG4DuWmh2+ZYUBQ7MwGieDQdtFbCzDmTPx3gKDs4K/9+MSAAuyiH7tRiMJJ00u+qgvswZcRpwbA5OJ6yR0UgA3lkHdWWSpyhwdFYEds/a7v+ePXuorKyU0g0ODnLs2DGSSeeNaG1tZdeuXVK6VCrFsWPHGBiQh0+6u7vp6uqSTuyZmRkOHjzI1NRUnqsSaGlp4YorruCaa66hq6uLyspKQ2GNjo6yf/9+fvrTn/LGG2+QShX48EsQDAZZs2YNe/bs4bLLLqOzs9OisAYHB3n22Wd57rnnGBnxHngy42gcvjAIc+WpyVAN/D7wC+AHbp1yBc4DCK13L8IlLBu6KuHoOyEcQswG02hOxmHvCXjaQy5spSJM0g+1wR+tgvqwuCD7RWlARoUDMfjfA2JlY8qD5fX2anhwK1zeYBunjgB8sRc+1Zebz80r4dsXIE2T+/kU3HYMemwLu2sr4R+2w3tlsgNwy2H49piT5kfd0F3nMt5ioAhen+qF+wetCn9NJTx9IWyqKZO8IHzqFByJweMT1o9ee+01uru7pWSPP/44H//4x+nr63N8dvvtt/PQQw9J6ZLJJHfffTdf+MIXyGSsNycUCvHd736XG2+8UUr70ksvcfvtt3PsmPu+3kAgwOrVq7n55pu57bbb2LhxI6FQCEVxTjtVVTl37hw//vGPeeCBBzhy5IhjTF6gKAqbN2/mIx/5CDfffDMdHR0oiuKQqWkaqqoyNDTEN7/5TR599FFOnTqFqjq1T0gRP7b6aPRpax5dEPGolKF28svAxxAFAh2DyWVhdQJ/SpmVFWQDeCpUmYajaYACiuZtfbMuCL/bAh9fAxfUQkATtBZ+WQQU8WVeUg9f2wr/MApfHYZT8dyytOw/QQ1LR0VZ4P/uemgI5laAqgZBVfAw06KIcbvNdUWn0xbkCoaCp5uskLZAYxJlvS6vbdn7IbMONSCtZsdYigy9TRVyZJd28OBBduzYYZl4uuWzatUqGhrku8K6uroIBoUJaKeNRCKsWrWK6upqZmZmLHTNzc20trZKaVVVJRqNMjmZ+1d1586dfPKTn+T666+nurrakKsoivGqtwWDQVpbW/nwhz/Me97zHu68806eeeYZYjHvW+8ikQiXXnop99xzD5dccolDhlmu/tfV1cWdd97J7t27+exnP8sLL7zg4Ju23RCZUipj5OVSRLWXE0hcQ7egewRReO+3yjcOOTTEQ6oo3vMkqgPwR63wv9bDjhqhUJQsMwu/7B8mZdgYhNvb4TPrYEu1B5kmZSHjt74aLvRQl0LBRuvxenU6gxZ3BWcfs6aZLM4ytOUc4yLLOHLkiEPh6JNuxYoV1NfXO2iCwSBtbW0OC8NM297eLlV2jY2NrFy5UkqbSqUYGBhwKDkzuru7ue+++/i93/s9h7ICHIrE3LZ161buu+8+PvjBDxq0+RAOh7nhhhv48pe/zCWXXJJXhrktHA5z9dVXc9999xm05xl/ALxb9oGbwloB3IIoGbOoMCZyAbhxBfxlF6yvdE56Oz/Zr3pNAP7DCti7GpryRfFsisXOryEM76j1ttwqG0u+vrL/56VX3PsV1Vbg/SlFrht6enossR2zEqmvr6e5udmI0eiora2lo6PDKdc0eVeuXClVdi0tLdTUOH+JFEUhmUwyNDREPB6XjnXdunXcfffdXH755ZZxmpVFvrZ169axd+9errnmGsPKy4Xdu3fziU98gre//e2eZdjbdu7cyT333MOOHTvyyltktAH/TfaB2zz7A8R+nyWD1xBIZwXc3QVrTDVMDVoXRaVJ2qoV+ONVYkuBl4G58asFLm2Aeg/LF7KxOP8jFW/QelnY0bQFF9vMp9g2TC5mTrklyMgnYnR0lGg0agSqzdAVUyhkvQn19fV0di6kDcpoW1paqKurc8hra2ujomLhITPTplIpRkdHpcH4SCTCHXfcwZ49ewCrYtXhpS0QCLBlyxY+8pGPsHnzZkd/M9auXcsdd9zBO97xDqkLW4jc3bt382d/9mc0Npa+7FcirkJ4eRbIFFY98BcsQe11y+TNEcvRUaHAnWthXY2ENkuvAjENxjNixVEjGzcy9QPhzlUF4dNroSNPAWcHraktqMC2GthW5e1azbQyRStrMss1rsUFZhfSrAQsbXZ+mks/C1Nv1+aFX642NwwODjI4OGi4aGYFUlVVxerVqy0KBkQcqq6uzugno21sbKSpqckxcdesWWOkHthp4/E4PT09jjEqisJVV13FTTfdRHV1tUFrftU0jUQiwcTEBBMTE8zPz7v2CwaD7Nmzx+AnQ0VFBe973/u4/vrrCYfDlmszv87OzjI+Pk40GiWdTrv2q6io4Nprr+X973+/w2JdYlQg9itbdjPKlNIfAU47erGgZR9UD/7Bpiq4ukkoG2PSmWhnVXh1RuSF9M/Dxgj8VpOIMVUHFiaERpZGha5qeF8TPHLGRajuXtlptYW2jgp4e41IoHML4isutG6wuI/KAm2uryqpwvE4qIrcEuuogFVhoWQBNGVBycQzcCwBGTtddrxjSXfrTh+PmV9KE/egkITCQBDGUjAtieDG43HDqrHHlRRFoa2tjXA4bL3ejg4qKioccRszbUNDA6tXryYUCllczs7OTlfaubk5BgcHHWOsqakxUgjc4kfHjh1j3759HD58GE3T2LFjBzfccIOFxkwbDoe57rrr+Na3vsXcnPMkraamJq6//nrDrbXLTafTHDp0iMcff5y+vj7q6+vZvXs3V111FS0tLdL4VltbG1deeSX79u3j7Nmz8pu1NLgQkanwmN5gV1grgA8s5YjITi4vcax31sGKCiyzWadNaPCjcfhiv8i7Smki7eGfz8Gfd8JNK4TSypJZlMe1zfDNMclklQ93gTaLxqBQig2h3BNURpurr5Q2BybS8LenoTro7KwBt7bBLe0gMwaHkvCxkyIh0C5bAwaTkPTqtwMzqliJ/VnUe6xKUYSSi0sUViKRoL/fvRRAZ2enI09r9erVrrlbOmpqali1apVFYVVVVRnKToazZ88yPDzsaG9tbeXCCy+kqmrhGzavzPX29rJ3715efPFFZmdnDfkHDhzgr//6r+nq6nIoYk3T6O7uZsOGDdJ8sfb2dnbs2GFRPDqtqqo8++yzfP7zn+cXv/gFqVSKQCDAP/3TP3HLLbfwmc98xrAizbShUIju7m46OjrOt8JqBK4Hngai4FRYV7AIaQw5oS24B/ke7O5qqDHHH020p+LwudNCWenzal6D12Pw0BBsr4aLaxcmvjkmtKUaVoThTI4EUDONQZt9H1Zge42wXnIpLBltPjjk5iBManA4x3GW709mlbJJ0etWU1wVyajFJgHa+aWB3oRI1i0HEokEPT09ZDIZS5xGn2xr1qxxuE1mhWVe1jfT6ukE4XDYCKI3NTXR3NwsTQkAsQCQSDgrojQ3N7N69WqLDB2pVIovfelLPPXUUxaa2dlZHnvsMTo7O/mrv/orw0o009bV1XHRRRfx/PPPO2Ru3LiR1tZWY5xm2r6+Ph588EF+/vOfGzldqqoyPDzMQw89xCWXXGLkmdlp29vbaW1ttVz3eUAIscPmQsQRYpYYVgB4P0uwMqhDQbgRQF7zoVKBlRULGlZRTLQBeHrCqqx0aIiE0f83DbrBr9heG4K541iKaXyKS9vbqmF9JLcSktF6hUGreLdYZDwUk7Ky8wsUyViPE9r5FTtOGZLJJH19fczPz1viSfoE6+josKz2hUIh2tvbjbiO2e2x03Z1dRGJRAzaFStWGApLRnvs2DFpgmV9fT0tLS0WGTr6+/t58sknpdc2MzPDc889Z1iQdlpFUQylZEdLS4vUotM0jV//+tccPHhQmoA6PT3Nv/7rvzpWXs2xvRUrVpzvOBaI/cvvIqurzKO5KPu3pAed6pM338NdExTpCMZk16y0A0l3HZDRxHaPjCnKaw48BxFWkhRmRSWh1ds6KkRialWe+6vZFJ9ZhqxJJrdcv3fLnZ8ZqqoyMjJibCMxT2hN06itrWXdunVGm55HZU4JsAeZ9ffr16+3KCxZqoOZ9tSpU3nHa5c1MjIijUHpmJycZGxszHWcXmGmnZmZkVqCOmZmZkinF1wCM61sRfE8IQK8B1gHVoXVzRJXENVdsnyrUA4oNlry50EpjjdYZlbeR8M+Rvu4Fbi03lt6g8MflFy74vqf0i0X+7NY6rPpiLct0rMejUY5c+ZMVoY1eB4MBtm2bZvx/+bmZlpaWggEAo7JZ6dtb2+ntnZhMaqhoYGamhpHP4B0Oi3dAmSHOZvcC/StMsXQyuTa37v1LWXMS4gLyeomfZ5XAxcggu5LBiX7jwaF/SybVhYL/g2SrC7mhNlltdOa2xB7DzvzpEhYaIsZcxmx3PnZMTU1xejoqESumGTr16832pqbm2loaHAEomW0NTU1Rr5WMBhk5cqVjniYTjs5OZlzs3Apk91NWXjlWarsZYxOYBtQoSustmzDkjusiib+CrWw0Ey0BQks0mWRKUhzmwZtlfAuZ9K0AQdtAbLNtKW6XCY9uyz5uSEWizE2NmbJIzK7TWvXLjgIjY2N1NTUSPvZ2yKRCBs2iPpxlZWVtLe3G3lUdtqzZ88aVl4umGkLVQbF0JoXCAp1Je205zHI7oYQsBNRQAUQeVfb3PsvHjR90hdqcSgmWg8kxm23uYF5PVLN5H7aaW1tQQX2eEgQNmg9QGZ9luwS2mKGJfOjvPzcMDc3x8DAAPF4XOq+dHR0EIlEUBSxv7C2tlbaz94WCATYtEnUpqyoqKC1tdXgY6cdGRlhfj5/ec1yuHWluoRLKXcJsAuoDSGsqg7OQ+ljfTLqq31edZZmeqN5/G5Hk2IVUeaxjSZhLsd2c7P3p2Bawre3AVc1wsqws56TG631giTX6EJbCsxK0JyKsFz4uSGRSDAwMMDs7Kwl5iTkKnR2dtLW1sbw8DBtbW2GhWXvJ2vTLayamhpjX6Ks36FDh3KOURYwL0QBeBmvV9pCsAytKju2kC0PV4GIwEdydl8EGKGcQieiIqwEgzZPdxX47ll4JioJOCOSJXvdFlOy4zKPz3hv4qW3NYbhtxpEwqqUnYRfrqC7woJSLtsPn1KEG56f5eKZViacO3eOmZkZVq0SJyHYs9ZbWlo4e/YsbW1tljhUvtiQnihaX1/PypUrXfudOHEi5/hkgexClIEsV8yLwrPnihUCWY7aMkQ1sCWEUFQbz9co9F/kXAmRDmT76laHl1s0lBR/RY0Rk9WQHWNKhcF5WF9l66eIons/OOdeI8jg5+F6DeulnMrlTaqsAIaHh5mYENX97BMrEAiwZs0a+vv7aWlpIRQKuU54e1tnZyeNjY3U1dXR0tIi7ZdKpaR7CN1QiMIplVaWIV+IrGJplxjbAgiFdd5OwjHHkLzeViVrmhWTEVEsFF1uFhngpRmI2bSSosHuBnFGm53ewS+HPLN+lFmF5cRirBZqOAu/lQPDw8OMj4+7yFXYtGkTtbW1NDc3G22yfna0trbS1NRksbDs/SYmJjxvVTGnFhSKctAWg1LkLhE26y7h0m12tkHRrJPTO6GV1o4NEZHI6VaZ04zZDPxyWuzFc4PdKKlQRPb8ughcZipjrGnQUQU7amFwQsLIhZ8dSo5+JesBG8Ny6xUNqFFE6Z4NVe7XqSgwPC+sUXtNezdMTU3R399PKpVy7PXTNI3t27fzzDPP0NbWZrTZJ6CsLRQK0dXVRX19vVFuxt6vp6eHc+dcfH0bSkm+fDPSLhHW6QprybbjmKHHchT9PwUQaoqJVoIrG+Er20DLVy1AgYEE/MejMCHb92ay5Ay5iG0scyr8ZBze3ZBVnlklWh8QdeCfmrCWk9Uw9fN4qRa5Jle4LFhEfrUBUW/sj3P1V+CVaXhq0rvCSqfTnDp1ikQi4VBYiqKwbt06mpqaWLFihdFmDC+Hq6MoClu3bkVRFIOvffLmKtpnl3G+cqLewrlYAG0BxCphjuyhxYOhqPQ0Bf2DPMrLrEDc+gYRxfPDgYW/ioD1/+Y21310pui3Lkp/DSmwLwqTKdNqpQIRRZRubjcVCjBoJfzywUFbIowtNOXip7+a+Mm26TjaNG8WsEWWpnH69GkSiYR0Ra6pqYmNGzdaqoXqn2cyGaampshkMlLaLVu2sHr1akuZGnO/4eFh6ck8ZsgUZKlbbArpXy7aZYpGXWF5KxxdZmiIh1zLxqSKpnX5HGzxsayCk7XlEm+2AM2xp5AiAu+vzVotRQVYVw1bqpx8FBs/rzDLLfWR0udU2fjZXnXL2UtbMRgaGjI2QcOCktA0jaamJnbu3GlYSWY3J5lMcuTIEWKxmJR206ZNdHV1GRt+zbSpVIqRkZG8CkuHrM6UF5piac3XUailVArtEqNWV1h5NpQsDowJrNkUT77vTLHR5uiqYbMoFFtbvkHKaLMfBRSxqfqV6YVaWvp1tIVFSRvz2YuOseQXa6RPmGlLhb4qq1mElMAPJz+ZDEdbkeJHR0eZmJiQZmY3Nzezc+dOh5WkaeLwiBdffJGRkREp7aZNm+ju7pZmxc/OztLb25tXYZVi6ZhX6MyvXujN/Qp1S2W0yxSR8147Qma55IPdHczlzSlYf9llbfmF2WhNH89kRJE6/TBV/fPGILyzAVaEco8lj1jnmMvwPC0FP69txWBkZISBgQFpZnZNTQ0XXHCBo+SKoijMz8/zyiuvMDo6KqVdv34927ZtcyzzK4rC9PQ0Z86cyXtWYCkpAvZk02KVR6lxrOWstAKIvMoiM5TKBPv36+X7svshNgRMM11mzXi+J5J+9qbeBPy7rRaXgtgMvdpW8FIq14MMg7YcFrv+vWgL78vB08LP/N0HrH/mtnCgcPF6xrsO8wQLBAJUVlZK6zhFo1FLiRo7bTAYJBgMSifsmTNnPJ3yXMpkX+bu2HJAIoRQWHOcB7fQHHy13CsP983sasi6n02JtAM1I3ivrRBHxgeytJ6fDVs/2faYkXn41TS811QPSwPWV8LGqoV8JFe5kjaHDjfRluWxXmR+8xocnoWRXKeuK9A3J2rRFwrzicuyeI+sbWxsjOnpad54442Cac+cOUM0Gs07LjttOZI4i0keLRSy61+GiOkKaxpRP3nJYc50L/R7Mmglnz0XhVNHFz7buxr+0ypRuVSPCRXimRjjU5w0s6ooxXw2A2tMFkN9SJxZeEBPlzDL9Xi9hlGl05bRylpMfjMq/N8R+Gnuw5FJqM7kWy84evSo8d5rrtXY2BhTU1McOXKkYNrR0VFPFlapcBtPsbSlyF2GiIYQ7uAES1y8DxYmbjETRldWbrQT6YVEUAU4l1o40aao22KzRsyPkIaopT4QhzXhhX5BBXbWwcA8TkuwUOVcJJ0MFk9wkfipwHgKetwLXpaEEydOGPXd7akE9tiUPuEnJiaIxWIcP37ctZ+sLZVKcfbs2ZzVO2UoVAmY+5e6Ulgo7HKXaRxrNIBQWM4jQJYQpYRRvNDa+2geAvYLna393O7jibhYLZw3uX9o8M7ahaPsC5Gri7HIzf4V+yhp9jc6v3I9mzZ+i/l7PTEx4djXJ5tkels8HmdoaIhEIsHY2BiTk5Oeaaempujt7fVUVsZOW6hLZ1+x86o87LSFwE3uMkRfAEgAzkPWlgAaWJ7qQuLgdtpCZWp4n6jm+S1zCUHEqZ6fginV1E+D+qDIetetQbNchwIxwaJgzXJLeI4U22up/Ox8y8XPC+bm5oxYlKyUi71tdnbWUFjRaNQ4V9ALrV44MN8KoRu/QpArplYMbSlylyFO6grrjXw9FwMK2ZhHEb/wRrXRYmj1P4/3RrG9dyM7EIP+OacMPYHUVa6Eof2ySrFCHbC5t2VmuyTIZDKGwvKywXlubo7x8XFUVWVubs5yQk0+2unpac97CPPxWipaKH7FchkrrWO6S9iHUFxLDsPTKeS7NVsrFKizzL6WF/cqe+80D4KG58XJ00ah6ayMoExuHl4Wq8VGW1bFVcRix5Lxy4Oenh7LcVu5lM/s7KzhBmqaRm9vr7SfvU3TNKanp10rRLjJK1c8qVha+/vFlrsEmANO6HlYw5wnt1BRsn9LSGvcjgJoZcF2OzLAExOgqXIZZZFbjiC5+SLKFHQvJz+vOHnypOPoLLdYVDQaNY7RAjh16pTjbEEZbSaTYXh42ELrFcXEgmRxpEL3BJa6d3GZBt1PABO6LTAMHMvReUngeRKXEscxaQBPt8TUz4vcZ6LQn8BhwcnkGuzyWVtmrVVC0H2B4QKvsvBjEfh5wMmTJy2pBm7KStM0xsfHLUrn+PHjllU/N9pMJuM5adRO68bXK00piqcYLPOg+6tATFdYowiFVeRB5cVBd3ccQWgXmFfMHLR5iI3AedalKqZSgUGbo8+cCk9MCiVjr15gl+t5kcFMW0b3bVnz84DZ2VmGh4dzrvCBsJL0/Yc6BgcHmZmZyUs7Pz/P6dOnpSc9y2DnV0jA3H6GYrFpDW5j8Uq7DJVVGjiASWHNAf8OFBdZLBJK9h89ITPX15TWFjLGDTdQWbA+qnPsitQQm5DNAW8j4K/zyTFIu6x8t/PZSZhLm2QoErlmmXkYWuSWwXzRr9vgVyJPxfxaBn5ekUgk6O3tzTvJk8kkg4ODllW+6elpYz9iLlr7NqB8cJv4DQ0NhELup+xWVVVRX19vKBlZHlk+mGkVRdT1Mp98bUcoFLJUpsg1/vOMQYRBlTRP80NA/1KOQg+2O4LukvsTy8BUJls/SXPSvqvefSv3irCoQBo2u0ImUYkMTOUp9GfOg8qHf5/NHmphD9ib5Xqc1EbQ3dS/LI+RmV85GJabnwfE43FLLpZb4Hx+ft5IY9AxPz9vrBTmotWVohek02mjmoOd3/r169m6dauULhQKsXXrVrq6uhyJqzovtyoR5vwpO+3mzZuNyqsy7Nq1y6hqYadNpVLMz88vl1jWQbK6yTzFX8/+5avRWTboFo/Dk5A88CrQl4CEyToxaDW4vBFubxOWlpm8NggfXAmXNWYv1iwsSx9Nw1CenEDdyvKiLc4kRU6WZhqrTK4XyGhLhlLeID648FPEj0Qhf4EChqRbTvbJbJ9kiUTCopz0NtmR83baWCzmuY6726nQmqZRU1PD5z//ebq6uiyWVigUYteuXezdu9dxdJlOm0wmXU/rOXnyJJOTk1JXdMeOHdx22210dnYalpSiKEQiEa677jo+9KEPSY8zA7GN6cyZM55d4UVEAvg5IpMBs42qAk8AvwO0LtlwspPay1P6WkzsUavT1axOCzQH4X90iSqfT0yIrTiNIbi+BW5pg5U2a9wgDcDLMxBzuy+OCHl+I2smAy9MwgdWQlPISWsooQJRrt86fR+jwa9ExhpOftUKvL9JbDj3DAV+GYWJFPR6SCpPp9MMDg4yPj5usSTsq1zxeNxxWnMymWRkZIR0Om1xm+y0vb29jqx4N4yOjnL8+HF2795tURA6z4svvphvfetbfO1rX+P48eMAbNu2jb1793LRRRcBVoWp0w4NDfH6669LZfb09NDb20tLS4uDNhQKceutt9La2sojjzzCyMgINTU17N69m49+9KNG7XrZquCpU6cYGhrydN2LjJHVqdIAAAmkSURBVFPAfrLxdbtT/QJi+XBJFJZ9wlhWwyT4VQyOxqCjCTTV2lcB2sPw553w+ythLCUONF1XueAKmpWFLiquwo9ypdiY3Dozba45ngGOxeFIHN5dJzpb5GoLkzwfNCQxr1Jhj8WVibGZX40Cf9oGfypT1ri0BeHOU3A45k1hqarK2NgY4+PjtLe3C36SWMz4+Lgl4A4Lym5qaso41ktGOzAwQCwmK/bvRDQaZf/+/fz2b/82ra2t0lW3yy+/nIsvvthwUdeuXWvU7pKlMmiaxvPPP+9QuDomJyf52c9+xsUXX2woXjNtOBzmpptu4rLLLuPcuXNUVVUZJ2S7pVDEYjH279/vKnMJkQZ+hXAJAatLCCLo/p2lHJFie6BFo7zvWAq+MYqxBcRBq4nTbDZGYHc9bIosVPzUTP2MlUUFfjYJL0/nHqOMNh+GknAolj2bULHJtfHNB4vccgTdixlEgfwsAXhVvCqm925tqlbYkOxJnbJYlPksQx3pdNqRriCjLWSFMJVK8cwzz/Daa6+RyWQMy8VswSiKQk1NDdu2bWPr1q1UV1e79tM0jcHBQR5//HGmp+UP6ezsLE888YSxodtMa+a3atUqtm/fzoYNG6isrHTtB6J0z5NPPunIcTsPiAL/ln0FnAoL4FGWcjO06QH38kP/g3Pw9AQWM8egNQXVFZWF7TumPua+I/Nw74BIRcgFB60HTKREyZnpjJO2EF6yvmVxDc3KvhwMZfyKaCsUU1NTnDlzxnXjr6ZpjIyMSE+7mZ6etmS/y2i9Btx19Pb28vWvf12aGZ8vhcLelkwm+eEPf8jzzz9POi0PLauqyiuvvMJ3vvMdpqenC5Zhb5uamuLRRx/l4MGDjs/PAw4CPzE3yBTWNHAvSxl8L+CBnVVh7xvwiylI61aWjd5iedmD1QqoCpyeh0/1Cj6FwGv+lgrsn4E3Eibjo4TAuZm2bK6hzq+cDGX8vLYVgWg0Sl9fH6lUynWlz1ywz047OjrqmiiZSqU4fPhwQeNRVZXvf//73HXXXUxPT1usGHPaRL62eDzO9773Pe699968+xhjsRgPP/ww//iP/2i4r8XIjcfjPPjggzz88MOkUrkqLy4JksDfARZ/3C176TFEoGtRoVs6BWQMAHByDu7sEVnlc9oCrR7rsfPTXSpNES7aiTh8rg/++exCOZhckPHzglNxODwj8sccYzHzkTC09C9QrlcsBb9S2rwgFosxODjoqFVltiDcjpfXSybbrQ39/7FYTLqSmA+qqvLwww/zxS9+UVoVQjZGc1ssFuOxxx7j05/+NKdPn/Yk89y5c9xzzz088sgjnrL/7RgZGeGrX/0qn/vc5zyfDLTIeBZ40t7olsl2DvgGsI1FOGRVIRtbCmAJSOuHo+Y7GSODiDv99zfg1na4cQWs1Wuna1ZFaLiKiijFuy8KXx+Gp6PCWss3ToWsJRCw8tO0/OOcyZ4o/TuroMmcbmGOq7lYGrpcJWAK+Os0uc5RzAG9zr1urZkXD4JFWG76fdRM99E+FQpqCxSW1qBjZGSEqakpY9ULsMRl3CysmZkZRkZGjHiTnXZwcNBzwN0OVVW5//77GR4e5pZbbmHXrl1UVy+cpierX5XJZDh58iTf//73+cpXvsLwcGGRmYGBAe666y5GR0f5wAc+wNve9jZHjXq73EQiweuvv843vvENHnvssbwHxS4RRoEvyz5wU1gJhHZ7H3BTuUcTV8VJM5GQNX6iKDCcFJUq8yEDHJyFvz0NT4zDH66C3XXQXgE1pgk9r8K5NByag6fG4d8moCfuzd+NZUQcKqNZD/zUJ1x/npUsDXhpGp48B6sqnLEiJXsNshhaQoVfz4CqOukCCox6ryVnoCch0i3CdhcakeOWLtDMSaji+oZS5YmDhQLCVT9bYDDixIkT7Nu3j7Vr1zomZyaTcbVSUqkUr7/+Ok8//bRl1QzEYRYvv/xyUdehY2Zmhm9/+9scOHCAK6+8kmuvvZYLL7yQ5uZmKioqjPFNTk7S29vLc889x5NPPsmrr77KzMxMUTInJia4//77efHFF7n66qu56qqr2LRpE83NzYTDYVRVJZlMEo1GOXr0KD/5yU949tlnOXLkSMEVVRcRjwG/lH2Q68csAFyHiGdtKedoAojs8+wPs2UwaWA6DckCJkAAaAiJNIYNkexhE9kri2XgZFyUfommvbmAOkKIuuwVitxlmcnkD9iHFJEPFkJuVSQ1kWVvLw0XQNDJZCuIa0kUqCRqAiKRVoa0JkpKF8IygMgzC7t8P4VCUWAmDSmtsPsfDoepr68nHA67JkG6ZWxXVVVRV1cnzS7Xi/2VA5WVlTQ0NNDe3s7mzZupq6sjEAiQTCbp6emhr6+PqakpZmdny5JdrigK1dXV1NfXs2bNGjZt2kRVVRV6uZy+vj4GBweJRqPLKaMd4GXgY4h0Bsfs8mJ9/1fgLs7TIRU+fPj4jUE/8HHgB24dvByk+m3g6XKNyIcPHz4kmAO+B+zL1cl9K7eV0UHgMqC99HH58OHDhwM/Av4GyFkp0YvCAhgHeoB3swirhj58+PiNxiHgVrIbnHPBq8ICUZNmFLgUqC9qWD58+PCxABVxAM6tmPYL5kIhCiuD2Dk9BewA6ihz0rUPHz5+Y6AiivJ9Angej9WOC1FYILIOjiPiWhcADQXS+/Dhw4cKHAH+FrFX0HNqfaEKiyzzI4jg2KWAs+qYDx8+fLjj18D/BB6nwOMFi1FYsKC0eoCLgJYi+fjw4eM3CweB24AXgYL3axSrsCC7jxixSXoDsLpEfj58+HjrIgH8GBFgP4xzc4cnlKpgVMTq4S8R8ax2fBfRhw8fC1CBAeDvgc9k3xeNcllEEwgTrx9YD7gf1eHDh4/fJDyFCK5/A6EnSkI5Xbg4Iph2FGHudQI1ZeTvw4ePNw/6ga8Dn0MYM2UpsrUYeVQBRDb85cBHgSuAQs5O8eHDx5sXMURpqq8gjpfPc2JCYVjMxM8AoqrKtcAnge2IDHn34299+PDxZkQSoZheAB5AxLTTeEwGLQRLlaleDdwA3IhIg1iXbfPhw8ebF9OI/X/7gX/BdmDEYmCpt9bUIxTWu4D3IqyutfhWlw8fbxYkEfmXvwJeQiirQ4jdL4uO87kXcF32bwuwE6HIuvEtLx8+lhsmEErpIHAAsaf4FKIYwpJiOWxeDiEsr9pAINAYiUQ2hUKh7YFAYKOiKGsVRelEBPGrgYiiKF6KDvrw4cMjNE1TEYmdMWBM07RhTdP6VVU9mUqljsTj8RMICyqW/VuyIwDt+P/9JGiKxnQFFwAAAABJRU5ErkJggg%3D%3D">
				    </a>
				    <br>
				    Get more custom plugins <br/> for WooCommerce <br/> and/or custom development
				    <br><br>
				    <a title=" More Extensions + Custom WooCommerce Site Development " href="http://ignitewoo.com" target="_blank" style="color:#0000cc; text-decoration:none">Contact us at<br>IgniteWoo.com</a>
			    </div>
		    </td>

		    </tr>
		    </table>
		</div>


		<?php
	}


	function save_settings() {
//var_dump( $_POST ); die;

		if ( isset( $_POST['dd_pricing_rules'] ) )
			update_option( '_woocommerce_sitewide_rules', $_POST['dd_pricing_rules'] );
		else
			update_option( '_woocommerce_sitewide_rules', '' );
			
		if ( !empty( $_POST['dd_show_discount_table'] ) )
			update_option( 'woocommerce_dd_show_discount_table', 'yes' );
		else 
			update_option( 'woocommerce_dd_show_discount_table', 'no' );
			
		if ( !empty( $_POST['dd_show_discount_calculator'] ) )
			update_option( 'woocommerce_dd_show_discount_calculator', 'yes' );
		else 
			update_option( 'woocommerce_dd_show_discount_calculator', 'no' );
			
		if ( !empty( $_POST['dd_discount_table_placement'] ) )
			update_option( 'woocommerce_dd_discount_table_placement', $_POST['dd_discount_table_placement'] );
		else 
			update_option( 'woocommerce_dd_discount_table_placement', 'the_content_top' );	
			
	}


	function sitewide_discount_rule_box() {

		?>
		<script>
		jQuery( document ).ready( function() { 
			jQuery( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).bind( 'DOMSubtreeModified', function() { 
				setTimeout( function() { 
					jQuery( '.chosen' ).chosen();	
					}, 500 );
			})
		});
		</script>
		<div id="woocommerce-pricing-category">

			<?php //settings_fields('_woocommerce_sitewide_rules'); ?>

			<?php 
			
			$pricing_rule_sets = get_option('_woocommerce_sitewide_rules', array()); 
			
			$set_index = 0;
			
			if ( !empty( $pricing_rule_sets ) )
			foreach( $pricing_rule_sets as $key => $vals ) { 
			
				$set_index = absint( str_replace( 'set_', '', $key ) );
				
				break;
			
			}
			
			
			
			?>

			<div id="woocommerce-dd-sitewide-pricing-rules-wrap" class="inside" data-setindex="<?php echo $set_index; ?>">

				<?php $this->meta_box_javascript(); ?>

				<?php $this->meta_box_css(); ?>  

				<?php if ($pricing_rule_sets && is_array($pricing_rule_sets) && sizeof($pricing_rule_sets) > 0) : ?>
					<?php $this->create_rulesets($pricing_rule_sets); ?>
				<?php endif; ?>        

			</div>

			<button id="woocommerce-dd-pricing-add-ruleset" type="button" class="button button-secondary"> <?php _e( 'Add New Ruleset', 'ignitewoo_dynamic_discounts' )?></button>

		</div>
		<?php
        }

	function create_rulesets($pricing_rule_sets, $index_offset = null ) {

		if ( empty( $index_offset ) )
			$index_offset = 0;
			
		$i = $index_offset;
	
		foreach ( $pricing_rule_sets as $name => $pricing_rule_set ) {

			$name = 'set_' . $index_offset; 

			$index_offset++;
			
			$dd_pricing_rules = isset($pricing_rule_set['rules']) ? $pricing_rule_set['rules'] : null;
			$pricing_conditions = isset($pricing_rule_set['conditions']) ? $pricing_rule_set['conditions'] : null;
			$collector = isset($pricing_rule_set['collector']) ? $pricing_rule_set['collector'] : null;

			$invalid = isset($pricing_rule_set['invalid']);
			$validation_class = $invalid ? 'invalid' : '';

			?>
			<div id="woocommerce-dd-sitewide-pricing-ruleset-<?php echo $name; ?>" class="woocommerce_sitewide_dd_pricing_ruleset <?php echo $validation_class; ?>">

				<h4 class="first">Sitewide Wholesale Ruleset<a href="#" data-name="<?php echo $name; ?>" class="delete_dd_pricing_ruleset" ><img  src="<?php echo $this->plugin_url; ?>/assets/images/delete.png" title="delete this set" alt="delete this set" style="cursor:pointer; margin:0 3px;float:right;" /></a></h4>    


				<div id="woocommerce-pricing-collector-<?php echo $name; ?>" class="section" style="" >
					<?php
					if (is_array($collector) && count($collector) > 0) {
						$this->create_collector($collector, $name);
					} else {
						$product_cats = array();
						$this->create_collector(array('type' => 'cat', 'args' => array('cats' => $product_cats)), $name);
					}
					?>
				</div>


				<div id="woocommerce-pricing-conditions-<?php echo $name; ?>" class="section">
				<?php
				if ( empty( $index_offset ) )
					$condition_index = 0;
				else 
					$condition_index = $index_offset;
					
				if (is_array($pricing_conditions) && sizeof($pricing_conditions) > 0):
					?>
					<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" />
					<?php

					foreach ($pricing_conditions as $condition) :
					$condition_index++;
					
					$this->create_condition($condition, $name, $condition_index);
					endforeach;
				else :
					?>
					<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions_type]" value="all" />
					<?php
					$this->create_condition(array('type' => 'apply_to', 'args' => array('applies_to' => 'everyone', 'roles' => array())), $name, 1 );
				endif;
				?>
				</div>

				<div class="section">
				<table id="woocommerce-dd-sitewide-pricing-rules-table-<?php echo $name; ?>" data-lastindex="<?php echo (is_array($dd_pricing_rules) && sizeof($dd_pricing_rules) > 0) ? count($dd_pricing_rules) : '1'; ?>">
					<thead>
					<th>
					<?php _e('Minimum Quantity', 'ignitewoo_dynamic_discounts'); ?>
					</th>
					<th>
					<?php _e('Max Quantity', 'ignitewoo_dynamic_discounts'); ?>
					</th>
					<th>
					<?php _e('Type', 'ignitewoo_dynamic_discounts'); ?>
					</th>
					<th>
					<?php _e('Amount', 'ignitewoo_dynamic_discounts'); ?>
					</th>
					<th>&nbsp;</th>
					</thead>
					<tbody>
					<?php
					$index = 0;
					if ( is_array( $dd_pricing_rules ) && sizeof( $dd_pricing_rules ) > 0) {
						foreach ( $dd_pricing_rules as $rule ) {
							$index++;
							$this->get_row( $rule, $name, $index );
						}
					} else {
						$this->get_row( array( 'to' => '', 'from' => '', 'amount' => '', 'type' => '' ), $name, 1 );
					}
					?>
					</tbody>
					<tfoot>
					</tfoot>
				</table>
				</div>
			</div><?php
		    }
        }

	function create_new_ruleset() {

		$set_index = $_POST['set_index'];

		$pricing_rule_sets = array();

		$pricing_rule_sets['set_' . $set_index] = array();

		$pricing_rule_sets['set_' . $set_index]['title'] = 'Rule Set ' . $set_index;

		$pricing_rule_sets['set_' . $set_index]['rules'] = array();

		$this->create_rulesets( $pricing_rule_sets, $set_index );

		die;
	}


	function create_condition($condition, $name, $condition_index) {
		global $wp_roles;

		switch ( $condition['type'] ) {

		    case 'apply_to':
			$this->create_condition_apply_to( $condition, $name, $condition_index );
			break;

		    default:
			break;
		}
	}


	function create_condition_apply_to( $condition, $name, $condition_index ) {
		global $wpdb;
		
		if ( !isset( $wp_roles ) )
		    $wp_roles = new WP_Roles();

		$all_roles = $wp_roles->roles;
		
		$sql = '
			SELECT DISTINCT ID, user_login, user_email, m1.meta_value AS fname, m2.meta_value AS lname
			FROM ' . $wpdb->users . '
			LEFT JOIN ' . $wpdb->usermeta . ' m1 ON ID = m1.user_id
			LEFT JOIN ' . $wpdb->usermeta . ' m2 ON ID = m2.user_id
			WHERE 
			m1.meta_key = "billing_first_name"
			AND
			m2.meta_key = "billing_last_name"
			ORDER BY m2.meta_value ASC 
		';

		$all_users = $wpdb->get_results( $sql );

		$div_style = ( $condition['args']['applies_to'] != 'roles' ) ? 'display:none;' : '';
		
		?>

		<div>
			<label for="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>"><?php _e( 'Applies To:', 'ignitewoo_dynamic_discounts'); ?></label>

			<input type="hidden" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][type]" value="apply_to" />
			
			<?php /*
			<input type="hidden" value="roles" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][applies_to]" >
			
			<input type="hidden" id="<?php echo $name; ?>_role_<?php echo $role_id; ?>" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][roles][]" value="dd_buyer" />
			*/ ?>
			
			<select class="pricing_rule_apply_to" id="pricing_rule_apply_to_<?php echo $name . '_' . $condition_index; ?>" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][applies_to]">
				<option <?php selected('everyone', $condition['args']['applies_to']); ?> value="everyone">Everyone</option>
				<option <?php selected('roles', $condition['args']['applies_to']); ?> value="roles"><?php _e( 'Specific Roles', 'ignitewoo_dynamic_discounts');?></option>
				<option <?php selected( 'users', $condition['args']['applies_to'] ); ?> value="users"><?php _e( 'Specific Users', 'ignitewoo_dynamic_discounts'); ?></option>
			</select>

			<div class="roles" style="<?php echo $div_style; ?> margin-top: 10px">
				<?php $chunks = array_chunk($all_roles, ceil(count($all_roles) / 3), true); ?>

				<p class="description"><?php _e( 'Click in the box and start typing a role name to select roles. Add as many as you need.', 'ignitewoo_dynamic_discounts' )?></p>
				
				<select class="chosen dd_plus_rule_roles" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][roles][]" >
				
				<?php foreach ($chunks as $chunk) : ?>

					<?php foreach ($chunk as $role_id => $role) : ?>
					
					<?php $role_checked = (isset($condition['args']['roles']) && is_array($condition['args']['roles']) && in_array($role_id, $condition['args']['roles'])) ? 'selected="selected"' : ''; ?>
					
					<option <?php echo $role_checked ?> value="<?php echo $role_id; ?>">
							<?php echo $role['name']; ?>
					</option>

					<?php endforeach; ?>
				
				<?php endforeach; ?>
				</select>
			</div>
			
			<div style="clear:both;"></div>

			<?php $div_style = ( $condition['args']['applies_to'] != 'users' ) ? 'display:none;' : ''; ?>
			
			<div class="users" style="clear: both; z-index: 999999; <?php echo $div_style; ?> ">

				<p class="description"><?php _e( 'Click in the box and start typing a name to select users. Add as many as you need.', 'ignitewoo_dynamic_discounts' )?></p>

				<select class="chosen dd_plus_rule_users" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][conditions][<?php echo $condition_index; ?>][args][users][]" >

					<?php foreach( $all_users as $key => $data ) { ?>

						<?php $user_selected = (isset( $condition['args']['users'] ) && is_array( $condition['args']['users'] ) && in_array( $data->ID, $condition['args']['users'] )) ? 'selected="selected"' : ''; ?>

						<option <?php echo $user_selected ?> value="<?php echo $data->ID ?>">
							<?php echo $data->lname . ', ' . $data->fname . ' &mdash; ' . $data->user_email; ?>
						</option>

					<?php } ?>

				</select>

			</div>
		
			<div class="clear"></div>

		
		</div>
		<?php
	}


        function create_collector($collector, $name) {
		$terms = (array) get_terms('product_cat', array('get' => 'all'));
		?>
		<label for="pricing_rule_when_<?php echo $name; ?>"><?php _e('Quantities based on:', 'ignitewoo_dynamic_discounts'); ?></label>
		
		<select title="Choose how to calculate the quantity.  This tallied amount is used in determining the min and max quantities used below in the Quantity Pricing section." class="dd_pricing_rule_when" id="pricing_rule_when_<?php echo $name; ?>" name="dd_pricing_rules[<?php echo $name; ?>][collector][type]">
		
			<option title="Calculate quantity based on cart item quantity" <?php selected('cat_product', $collector['type']); ?> value="cart_item"><?php _e('Cart Line Item Quantity', 'ignitewoo_dynamic_discounts'); ?></option>
			
			<option title="Calculate quantity based on total sum of the categories in the cart" <?php selected('cat', $collector['type']); ?> value="cat"><?php _e('Sum of of Category', 'ignitewoo_dynamic_discounts'); ?></option>
			
		</select>
		
		<div class="cats">   
			<label style="margin-top:10px;"><?php _e( 'Categories', 'ignitewoo_dynamic_discounts' )?>:</label>

			<?php 
				$size = ceil( count( $terms ) / 3 );
				
				if ( $size )
					$chunks = array_chunk( $terms, $size ); 
				else
					$chunks = array();

			?>
				
			<select class="chosen dd_plus_rule_cats" multiple="multiple" name="dd_pricing_rules[<?php echo $name; ?>][collector][args][cats][]" style="width:200px !important">
						
			<?php foreach ($chunks as $chunk) : ?>

				<?php foreach ($chunk as $term) : ?>
				
					<?php $term_checked = (isset($collector['args']['cats']) && is_array($collector['args']['cats']) && in_array($term->term_id, $collector['args']['cats'])) ? 'selected="selected"' : ''; ?> 
					
					<option value="<?php echo $term->term_id; ?>" <?php echo $term_checked; ?>><?php echo $term->name; ?> </option>
					
					
				<?php endforeach; ?>

				
			<?php endforeach; ?>
			
			</select>
			
			<div style="clear:both;"></div>
		</div>
		<?php
        }


        function get_row($rule, $name, $index ) {
		?>
		<tr id="pricing_rule_row_<?php echo $name . '_' . $index; ?>">
		    <td>
				<input class="int_pricing_rule" id="pricing_rule_from_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][from]" value="<?php echo $rule['from']; ?>" />
		    </td>
		    <td>
			    <input class="int_pricing_rule" id="pricing_rule_to_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index ?>][to]" value="<?php echo $rule['to']; ?>" />
		    </td>
		    <td>
			    <select id="pricing_rule_type_value_<?php echo $name . '_' . $index; ?>" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][type]">
				    <option <?php selected('price_discount', $rule['type']); ?> value="price_discount">Price Discount</option>
				    <option <?php selected('percentage_discount', $rule['type']); ?> value="percentage_discount">Percentage Discount</option>
				    <option <?php selected('fixed_price', $rule['type']); ?> value="fixed_price">Fixed Price</option>
			    </select>
		    </td>
		    <td>
			<input class="float_rule_pricing" id="pricing_rule_amount_input_<?php echo $name . '_' . $index; ?>" type="text" name="dd_pricing_rules[<?php echo $name; ?>][rules][<?php echo $index; ?>][amount]" value="<?php echo $rule['amount']; ?>" /> 
		    </td>
		    <td><a class="add_pricing_rule" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><img 
				src="<?php echo $this->plugin_url . '/assets/images/add.png'; ?>" 
				title="add another rule" alt="add another rule" 
				style="cursor:pointer; margin:0 3px;" /></a><a <?php echo ($index > 1) ? '' : 'style="display:none;"'; ?> class="delete_pricing_rule" data-index="<?php echo $index; ?>" data-name="<?php echo $name; ?>"><img 
				src="<?php echo $this->plugin_url . '/assets/images/remove.png'; ?>" 
				title="add another rule" alt="add another rule" 
				style="cursor:pointer; margin:0 3px;" /></a>
		    </td>
		</tr>
		<?php
        }


	function meta_box_javascript() {
		?>
		<script type="text/javascript">
																																
		    jQuery(document).ready(function($) {
			var set_index = 0;
			var rule_indexes = new Array();

			$('.woocommerce_sitewide_dd_pricing_ruleset').each(function(){
				var length = $('table tbody tr', $(this)).length;
				if (length==1) {
					$('.delete_pricing_rule', $(this)).hide(); 
				}
			});

			$("#woocommerce-dd-pricing-add-ruleset").click(function(event) {
				event.preventDefault();

				//var set_index = $("#woocommerce-dd-sitewide-pricing-rules-wrap").data('setindex') + 1;

				var set_index = $( '.woocommerce_sitewide_dd_pricing_ruleset'). length;
				
				//set_index++;;
				
				$("#woocommerce-dd-sitewide-pricing-rules-wrap").data('setindex', set_index );
																																
				var data = {
					set_index:set_index,
					post:<?php echo isset($_GET['post']) ? $_GET['post'] : 0; ?>,
					action:'ignitewoo_dd_create_new_ruleset'
				}
																																
				$.post(ajaxurl, data, function(response) { 
					$('#woocommerce-dd-sitewide-pricing-rules-wrap').append(response);

				});                                                                                                                                            
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.pricing_rule_apply_to', 'change', function(event) {  
				var value = $(this).val();

				if (value != 'roles' && value != 'users' ) {
				    $( '.users', $(this).parent()).hide();
				    $( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.roles', $(this).parent()).hide();
				    $( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				} else if ( value == 'roles' ) {
				    $( '.users', $(this).parent()).hide();
				    $( '.users input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.roles', $(this).parent()).fadeIn();
				} else if ( value == 'users' ) { 
				    $( '.roles', $(this).parent()).hide();
				    $( '.roles input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );
				    $( '.users', $(this).parent()).fadeIn();                               
				}                                                             
			});

			$( '#woocommerce-dd-sitewide-pricing-rules-wrap' ).delegate( '.dd_pricing_rule_when', 'change', function(event) {  

			    var value = $(this).val();

			    if (value != 'cat' ) {
				$( '.cats', $(this).closest( 'div' )).fadeOut();
				$( '.cats input[type=checkbox]', $(this).closest( 'div' ) ).removeAttr( 'checked' );

			    } else {                                                            
				$( '.cats', $(this).closest( 'div' )).fadeIn();
			    }                                                              
			});
			

			$('.dd_pricing_rule_when').change();
			
			//Remove Pricing Set
			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.delete_dd_pricing_ruleset', 'click', function(event) {  
				event.preventDefault();
				DeleteRuleSet( $(this).data('name') );
			});

			//Add Button
			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.add_pricing_rule', 'click', function(event) {  
				event.preventDefault();
				InsertRule( $(this).data('index'), $(this).data('name') );
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.delete_pricing_rule', 'click', function(event) {  
				event.preventDefault();
				DeleteRule($(this).data('index'), $(this).data('name'));
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.int_pricing_rule', 'keydown', function(event) {  
				// Allow only backspace, delete and tab
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 ) {
				    // let it happen, don't do anything
				}
				else {
				    if (event.shiftKey && event.keyCode == 56){
					if ( $(this).val().length > 0) {
					    event.preventDefault();
					} else {
					    return true;    
					}
				    }else if (event.shiftKey){
					event.preventDefault();
				    } else if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
					event.preventDefault(); 
				    } else {
					if ( $(this).val() == "*") {
						event.preventDefault();
					}
				    }
				}
			});

			$('#woocommerce-dd-sitewide-pricing-rules-wrap').delegate('.float_rule_pricing', 'keydown', function(event) {  
				// Allow only backspace, delete and tab
				if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 190 ) {
				    // let it happen, don't do anything
				}
				else {
				    if (event.shiftKey && event.keyCode == 56){
					if ( $(this).val().length > 0) {
					    event.preventDefault();
					} else {
					    return true;    
					}
				    }else if (event.shiftKey){
					event.preventDefault();
				    } else if ( (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 ) ) {
					event.preventDefault(); 
				    } else {
					if ( $(this).val() == "*") {
						event.preventDefault();
					}
				    }
				}
			});

			$("#woocommerce-dd-sitewide-pricing-rules-wrap").sortable(
			{ 
			    handle: 'h4.first',
			    containment: 'parent',
			    axis:'y'
			});

			function InsertRule(previousRowIndex, name) {

				var $index = $("#woocommerce-dd-sitewide-pricing-rules-table-" + name).data('lastindex') + 1;

				$("#woocommerce-dd-sitewide-pricing-rules-table-" + name).data('lastindex', $index );

				var html = '';
				html += '<tr id="pricing_rule_row_' + name + '_' + $index + '">';
				html += '<td>';
				html += '<input class="int_pricing_rule" id="pricing_rule_from_input_'  + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][from]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<input class="int_pricing_rule" id="pricing_rule_to_input_' + name + '_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][to]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<select id="pricing_rule_type_value_' + name + '_' + $index + '" name="dd_pricing_rules[' + name + '][rules][' + $index + '][type]">';
				html += '<option value="price_discount">Price Discount</option>';
				html += '<option value="percentage_discount">Percentage Discount</option>';
				html += '<option value="fixed_price">Fixed Price</option>';
				html += '</select>';
				html += '</td>';
				html += '<td>';
				html += '<input class="float_pricing_rule" id="pricing_rule_amount_input_' + $index + '" type="text" name="dd_pricing_rules[' + name + '][rules][' + $index + '][amount]" value="" /> ';
				html += '</td>';
				html += '<td>';
				html += '<a data-index="' + $index + '" data-name="' + name + '" class="add_pricing_rule"><img  src="<?php echo $this->plugin_url . '/assets/images/add.png'; ?>" title="add another rule" alt="add another rule" style="cursor:pointer; margin:0 3px;" /></a>';         
				html += '<a data-index="' + $index + '" data-name="' + name + '" class="delete_pricing_rule"><img data-index="' + $index + '" src="<?php echo $this->plugin_url . '/assets/images/remove.png'; ?>" title="remove rule" alt="remove rule" style="cursor:pointer; margin:0 3px;" /></a>';         
				html += '</td>';
				html += '</tr>';
																																
				$('#pricing_rule_row_' + name + '_' + previousRowIndex).after(html);
				$('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).show();

			} 

			function DeleteRule(index, name) {
				if (confirm("Are you sure you would like to remove this price adjustment?")) {
					$('#pricing_rule_row_' + name + '_' + index).remove();
																																
					var $index = $('tbody tr', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).length;
					if ($index > 1) {
					    $('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).show();
					} else {
					    $('.delete_pricing_rule', "#woocommerce-dd-sitewide-pricing-rules-table-" + name).hide();
					}
				}
			}

			function DeleteRuleSet(name) {
				if (confirm('Are you sure you would like to remove this dynamic price set?')){
					$('#woocommerce-dd-sitewide-pricing-ruleset-' + name ).slideUp().remove();  
				}
			}
																																
		    });
																																
		</script>
		<?php
        }


        function meta_box_css() {
		?>
		<style>
		    #woocommerce-pricing-category div.section {
			margin-bottom: 10px;
		    }

		    #woocommerce-pricing-category label {
			display:block;
			font-weight: bold;
			margin-bottom:5px;
		    }

		    #woocommerce-pricing-category .list-column {
			float:left;
			margin-right:25px;
			margin-top:0px;
			margin-bottom: 0px;
		    }

		    #woocommerce-pricing-category .list-column label {
			margin-bottom:0px;
		    }

		    #woocommerce-dd-sitewide-pricing-rules-wrap {
			margin:10px;
		    }

		    #woocommerce-dd-sitewide-pricing-rules-wrap h4 {
			border-bottom: 1px solid #E5E5E5;
			padding-bottom: 6px;
			font-size: 1em;
			margin: 1em 0 1em;
		    }

		    #woocommerce-dd-sitewide-pricing-rules-wrap h4.first {
			margin-top:0px;
			cursor:move;
		    }

		    .woocommerce_sitewide_dd_pricing_ruleset {

			border-color:#dfdfdf;
			border-width:1px;
			border-style:solid;
			-moz-border-radius:3px;
			-khtml-border-radius:3px;
			-webkit-border-radius:3px;
			border-radius:3px;
			padding: 10px;
			border-style:solid;
			border-spacing:0;
			background-color:#F9F9F9;
			margin-bottom: 25px;
		    }

		    .woocommerce_sitewide_dd_pricing_ruleset.invalid {
			border-color:#EACBCC;
			background-color:#FFDFDF;
		    }

		    .woocommerce_sitewide_dd_pricing_ruleset th {
			background-color: #efefef;
		    }

		    .woocommerce_sitewide_dd_pricing_ruleset .selectit {
			font-weight: normal !important;
		    }
		    .woocommerce_sitewide_dd_pricing_ruleset .chzn-choices, .woocommerce_sitewide_dd_pricing_ruleset .chzn-drop, .chzn-choices .search-field input {
			min-width: 300px !important;
		    }
		    
		</style>
		<?php	
        }

}

global $ignitewoo_dynamic_discounts_admin;

$ignitewoo_dynamic_discounts = new IgniteWoo_Dynamic_Discounts_Admin();

