<?php
/**
 * Product Class with girth and lettermail enabled
 *
 * @class       EW_Product
 * @version     1.1
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 */

class EW_Product {

    private $id;

    private $length    = 0.0;

    private $height    = 0.0;

    private $width     = 0.0;

    private $weight    = 0.0;

    private $price     = 0.0;

    private $dimension_unit;

    private $weight_unit;

    private $is_letter = false;

    private $name = '';

    private $location ='';

    private $city = '';

    private $state = '';

    public function __construct( $length, $width, $height,  $weight, $price, $dimension_unit, $weight_unit, $name='' ) {

        $this->length         = $length;

        $this->height         = $height;

        $this->width          = $width;

        $this->weight         = $weight;

        $this->price          = $price;

        $this->dimension_unit = $dimension_unit;

        $this->weight_unit    = $weight_unit;

        $this->name           = $name;
    }

    public function get_city(){
        return $this->city;
    }

    public function set_city( $var ){
        $this->city = $var;
        return $this;
    }

    public function get_state(){
        return $this->state;
    }

    public function set_state( $var ){
        $this->state = $var;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param String  $newname
     */
    public function set_name( $name ) {
        $this->name = $name;

        return $this;
    }



    /**
     * Get id
     *
     * @return string
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param String  $newid
     */
    public function set_id( $id ) {
        $this->id = $id;

        return $this;
    }



    /**
     * Get Item length
     *
     * @return float
     */
    public function get_length() {
        return $this->length;
    }

    /**
     * Set Item length
     *
     * @param Float   $newlength
     */
    public function set_length( $length ) {
        $this->length = $length;

        return $this;
    }



    /**
     * Get item height
     *
     * @return Float
     */
    public function get_height() {
        return $this->height;
    }

    /**
     * Set item height
     *
     * @param Float   $newheight
     */
    public function set_height( $height ) {
        $this->height = $height;

        return $this;
    }


    /**
     * Get item width
     *
     * @return Float
     */
    public function get_width() {
        return $this->width;
    }

    /**
     * Set item width
     *
     * @param Float   $newwidth
     */
    public function set_width( $width ) {
        $this->width = $width;

        return $this;
    }


    /**
     * Get item weight
     *
     * @return Float
     */
    public function get_weight() {
        return $this->weight;
    }

    /**
     * Set item weight
     *
     * @param Float   $newweight
     */
    public function set_weight( $weight ) {
        $this->weight = $weight;

        return $this;
    }



    /**
     * Get item dimension unit
     *
     * @return String
     */
    public function get_dimension_unit() {
        return $this->dimension_unit;
    }

    /**
     * Set item dimension unit
     *
     * @param String  $newdimension_unit
     */
    public function set_dimension_unit( $dimension_unit ) {
        $this->dimension_unit = $dimension_unit;

        return $this;
    }



    /**
     * Get item weight unit
     *
     * @return String
     */
    public function get_weight_unit() {
        return $this->weight_unit;
    }

    /**
     * Set item weight unit
     *
     * @param String  $newweight_unit
     */
    public function set_weight_unit( $weight_unit ) {
        $this->weight_unit = $weight_unit;

        return $this;
    }


    /**
     * Get item price
     *
     * @return Float
     */
    public function get_price() {
        return $this->price;
    }

    /**
     * Set item price
     *
     * @param Float   $newprice
     */
    public function set_price( $price ) {
        $this->price = $price;

        return $this;
    }


    public function get_volume() {
        return $this->width * $this->length * $this->height;
    }


    public function is_letter_mail() {

        return $this->is_letter;
    }

    public function set_letter_mail( $enable ) {
        $this->is_letter = $enable;
    }

    public function get_location(){
        return $this->location;
    }

    public function set_location( $var ){
        $this->location = $var;

        return $this;
    }

    public function to_array(){

        $data = array(
            'id' =>  $this->get_id(),
            'name' => $this->get_name(),
            'length' => $this->get_length(),
            'height' =>  $this->get_height(),
            'width' => $this->get_width(),
            'weight' =>  $this->get_weight(),
            'price' => $this->get_price(),
            'dimension_unit' =>  $this->get_dimension_unit(),
            'weight_unit' => $this->get_weight_unit(),
            'is_letter' =>  $this->is_letter_mail(),

        );

        return $data;

    }

}

?>
