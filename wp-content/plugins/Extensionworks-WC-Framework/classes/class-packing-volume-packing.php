<?php

class EW_Volume_Packing extends EW_Box_Packing{

	protected function can_fit( $container, $item ) {


		//check max weight
		if ( $item->get_weight() > $container->get_weight( 'available' ) ) {
			return false;
		}

		//check volume
		if ( $item->get_volume() > $container->get_volume( 'available' ) ) {
			return false;
		}

		return true;
	}


}

?>
