<?php

class EW_Packing_Algorithm {

	/**
	 *
	 *
	 * @var array
	 */
	var $items = array();

	/**
	 *
	 *
	 * @var array
	 */
	var $containers = array();

	/**
	 *
	 *
	 * @var array
	 */
	var $packed_containers = array();

	/**
	 *
	 *
	 * @var array
	 */
	var $packed_items = array();

	/**
	 *
	 *
	 * @var array
	 */
	var $unpackable_items = array();

	var $debug = true;



	/**
	 * strategy for packing letter mail
	 * together is packing letter mail with parcel
	 * seperate is packing letter mail separately
	 *
	 * @var string
	 */
	var $letter_mail_setting = 'together';


	var $packages = array();
	var $robot = '';

	public function __construct( $robot ) {
		$this->robot = $robot;
	}

	public function pick_containers( $containers = array() ) {

		if ( !$containers )
			$containers = $this->robot->get_containers();

		$this->containers['letter'] = array();

		$this->containers['box'] = array();

		foreach ( $containers as $key => $container ) {

			//remove weight based boxes
			if ( $container->is_weight_based() ) {
				$this->add_debug_message( "{$container->get_label()} is weight based, removed" , 'Box packing');
				continue;
			}

			
			if ( $container->is_letter_container() ){
				$this->containers['letter'][] = $container;
				$this->add_debug_message( $container , "Packing add container {$container->get_label()} to letter section");
			}
			else{
				$this->containers['box'][] = $container;
				$this->add_debug_message( $container , "Packing add container {$container->get_label()} to box section");
			}
					
		}
	}

	public function pick_items( $items = array() ) {

		if ( !$items )
			$items = $this->robot->get_items();

		$this->items['letter'] = array();

		$this->items['box'] = array();

		foreach ( $items as $key => $item ) {

			if ( $item->is_letter_mail() ){
				$this->items['letter'][] = $item;
				$this->add_debug_message( $item , "Packing add item {$item->get_id()} to letter section");
			}else{
				$this->items['box'][] = $item;
				$this->add_debug_message( $item , "Packing add item {$item->get_id()} to box section");
			}
		}
	}

	public function packing() {

		//init packages
		$this->packages = array();

		switch ( $this->letter_mail_setting ) {
			case 'separate':
				$this->packing_letter_separate();
				break;
			default:
				$this->packing_letter_together();
				break;
		}

	}


	protected function packing_letter_together() {

	}

	protected function packing_letter_separate() {

	}

	public function set_letter_mail_setting( $letter_mail ) {

		$this->letter_mail_setting = $letter_mail;
		
	}

	public function get_packages() {
		return $this->packages;
	}

	public function get_containers() {
		return $this->containers;
	}

	public function get_unpackable_items() {
		return $this->unpackable_items;
	}

	public function set_debug( $on ){
		$this->debug = $on;
	}

	public function add_debug_message( $msg, $title ) {
        if ( !$this->debug  )
            return;

        ew_add_response_message( $msg, $title );
    }


}

?>
