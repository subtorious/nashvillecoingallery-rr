<?php
include_once 'class-packing-box-packing.php';
class EW_Weight_Based_Packing extends EW_Box_Packing{


	public function pick_containers( $containers = array() ) {

		if ( !$containers )
			$containers = $this->robot->get_containers();

		$this->containers['letter'] = array();

		$this->containers['box'] = array();

		foreach ( $containers as $key => $container ) {

			//keep weight based box
			if ( !$container->is_weight_based() ){
				$this->add_debug_message( "{$container->get_label()} isn't weight based, removed" , 'Weight Based Packing');
				continue;
			}
				

			//to-do:
			//weight based box must has max weight
			if ( $container->get_weight( 'max' ) == 0 ){
				$this->add_debug_message( "Max weight of {$container->get_label()} is 0, removed" , 'Weight Based Packing');
				continue;
				// throw new Exception( "box ".$container->get_label().' max weight is 0', 1 );
			}
		
			if ( $container->is_letter_container() ){
				$this->containers['letter'][] = $container;
				$this->add_debug_message( $container , "Weight Based Packing add container {$container->get_label()} to letter section");
			}
			else{
				$this->containers['box'][] = $container;
				$this->add_debug_message( $container , "Weight Based Packing add container {$container->get_label()} to box section");
			}
		}
	}



	protected function packing_items( $container, $items ) {

		$unpackable_items = array();

		$packed_items = array();

		while ( count( $items ) >0 ) {
			$item = array_shift( $items );

			//check container dimensioins
			if ( !$this->can_fit( $container, $item ) )
				$unpackable_items[] = $item;
			else
				$container->put( $item );
		}

		$percent = ( sizeof( $container->get_items() ) / (  sizeof( $unpackable_items ) + sizeof( $container->get_items() ) ) ) * 100;

		$remaining_weight = $container->get_weight( 'available' );

		return array( 'container' => $container,
			'percent' => $percent,
			'unpacked' => $unpackable_items,
			'remaining_weight' => $remaining_weight
		);
	}

	protected function can_fit( $container, $item ) {

		//check max weight
		if ( $item->get_weight() > $container->get_weight( 'max' ) )
			return false;

		//check available weight
		if ( $item->get_weight() > $container->get_weight( 'available' ) )
			return false;

		return true;
	}

}

?>
