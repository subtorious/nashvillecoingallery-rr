<?php
/**
 * EW_Shipper
 *
 * The ew_shipper class is skeleton class to be inherented by actual shipping extension.
 * It handle some basics such as settings, availabilities etc
 *
 * @class       EW_Shipper
 * @version     1.0
 * @package     ExtensionWorks-Shipper/Classes
 * @author      Extension Works
 * @distributor www.extensionworks.com
 */

class EW_Shipper extends WC_Shipping_Method {

    /**
     *
     *
     * @var string
     */
    var $id = '';

    /**
     *
     *
     * @var string
     */
    var $carrier = '';


    /**
     *
     *
     * @var string
     */
    var $dimension_unit = 'in';

    /**
     *
     *
     * @var string
     */
    var $weight_unit = 'lbs';

    /**
     *
     *
     * @var string
     */
    var $method_description = '';

    /**
     *
     *
     * @var string
     */
    var $endpoint = '';

    /**
     *
     *
     * @var string
     */
    var $dev_endpoint = '';

    /**
     *
     *
     * @var array
     */
    var $carrier_boxes = array();

    /**
     *
     *
     * @var array
     */
    var $carrier_envelopes = array();

    /**
     *
     *
     * @var array
     */
    var $allowed_origin_countries = array();

    /**
     *
     *
     * @var array
     */
    var $allowed_currencies = array();

    /**
     *
     *
     * @var array
     */
    var $package_shipping_methods = array();

    /**
     *
     *
     * @var array
     */
    var $letter_shipping_methods = array();

    /**
     *
     *
     * @var boolean
     */
    var $letter_mail_available = false;

    /**
     *
     *
     * @var array
     */
    var $settings_order = array();

    var $unpackable_products = array();

    var $robat;

    var $log = '';

    var $enable_rename_method = true;

    var $packages = '';

    var $letter_setting = 'together';

    public function __construct() {



        $this->init_form_fields();

        $this->add_form_fields();

        $this->sort_form_fiels();

        $this->load_setting();

        add_action( 'admin_notices', array( &$this, 'notification' ) );

        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( &$this, 'process_admin_options' ), 1 );

        add_action( 'woocommerce_checkout_update_order_meta', array( &$this, 'save_package'), 10, 2);

    }

    protected function load_setting() {
        global $woocommerce;

        // Load the settings.
        $this->init_settings();

        foreach ( $this->settings as $key => $value ) {
            $this->$key = array_key_exists( $key, $this->form_fields )? $value: array();
        }

        $this->boxes  = isset( $this->settings['boxes'] ) && $this->boxes ? $this->settings['boxes'] : array();

        if ( !$this->boxes && $this->carrier_boxes )
            $this->boxes = array_merge( $this->boxes, $this->carrier_boxes );

        $this->origin_country = $woocommerce->countries->get_base_country();

        $this->currency = get_woocommerce_currency();
    }

    /**
     * Notification upon condition checks
     */
    public function notification( $issues=array() ) {

        $setting_url = 'admin.php?page=woocommerce_settings&tab=shipping&section=' . $this->id;

        $woocommerce_url = 'admin.php?page=woocommerce_settings&tab=general';
        // $setting_url = admin_url( 'admin.php?page=woocommerce_settings&tab=shipping&section=EW_WC_Shipping_Fedex_WSDL' );

        if ( check_woo_version('2.1') ){
            $setting_url = 'admin.php?page=wc-settings&tab=shipping&section=' . strtolower( $this->id );
            $woocommerce_url = 'admin.php?page=wc-settings&tab=general';
        }


        if ( !$this->origin && $this->enabled == 'yes' ) {
            $issues[] = 'no origin postcode entered';
        }

        if ( !empty( $this->allowed_origin_countries ) ) {
            if ( !in_array( $this->origin_country, $this->allowed_origin_countries ) ) {
                $issues[] = 'base country is not correct';
            }
        }

        if ( !in_array( $this->currency, $this->allowed_currencies ) ) {
            $issues[] = 'currency is not accepted';
        }

        if ( !empty( $issues ) ) {
            echo '<div class="error"><p>' . sprintf( __( $this->carrier . ' is enabled, but %s.
                Please update ' . $this->carrier .' settings <a href="%s">here</a> and WooCommerce settings <a href="%s">here</a>.', 'extensionworks' ),
                implode( ", ", $issues ), admin_url( $setting_url ), admin_url( $woocommerce_url ) ) . '</p></div>';
        }

        //add addtional notification
        do_action( 'extensionworks_notification' );
    }

    /**
     * Initialise Gateway Settings Form Fields
     */
    public function init_form_fields() {
        global $woocommerce;

        $this->form_fields = array(

            'general' => array(
                'title' => 'General Setting',
                'type'  => 'title'
            ),
            'enabled' => array(
                'title'   => __( 'Enable/Disable', 'extensionworks' ),
                'type'    => 'checkbox',
                'label'   => __( 'Enable', 'extensionworks' ),
                'default' => 'yes'
            ),
            'title' => array(
                'title'       => __( 'Method Title', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'This controls the title which the user sees during checkout.', 'extensionworks' ),
                'default'     => __( $this->method_title, 'extensionworks' )
            ),
            'origin' => array(
                'title'       => __( 'Origin Postcode', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'Enter your origin post code.', 'extensionworks' ),
                'default'     => __( '', 'extensionworks' )
            ),
            'availability' => array(
                'title'   => __( 'Method availability', 'extensionworks' ),
                'type'    => 'select',
                'default' => 'all',
                'class'   => 'availability',
                'options' => array(
                    'all'      => __( 'All allowed countries', 'extensionworks' ),
                    'specific' => __( 'Specific Countries', 'extensionworks' )
                )
            ),
            'countries' => array(
                'title'   => __( 'Specific Target Countries', 'extensionworks' ),
                'type'    => 'multiselect',
                'class'   => 'chosen_select',
                'css'     => 'width: 25em;',
                'default' => '',
                'options' => $woocommerce->countries->countries
            ),
            'debug' => array(
                'title'       => __( 'Debug mode', 'extensionworks' ),
                'type'        => 'checkbox',
                'label'       => __( 'Enable Debug Mode', 'extensionworks' ),
                'description' => __( 'This will output some debug information on your cart page, remember to turn this off when you done testing.', 'extensionworks' ),
                'default'     => 'no'
            ),
            'order_limit' => array(
                'title' => 'Order Limit',
                'type'  => 'text',
                'default' => '0',
                'description' => 'When the order\'s value over the limit, it will be shipped as free. "0" has disabled this feature.'
            ),

            'free_text' => array(
                'title' => 'Free delivery text',
                'type'  => 'text',
                'default' => 'free delivery',
                'description' => 'The text for showing free delivery.'
            ),

            'packing_setting' => array(
                'title' => 'Packing Setting',
                'type'  => 'title'
            ),
            'fee' => array(
                'title'       => __( 'Handling fee or Discount fee', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'You can add or subtract an amount from your Shipping rate. You can provide a extra cost/discount cost for you shipping rate.
                                Fee excluding tax. Enter an amount, e.g. + or -2.50, or a percentage, e.g. + or -5%.', 'extensionworks' ),
                'default'     => '0'
            ),
            'fee_to_cart' => array(
                'title'       => __( '', 'extensionworks' ),
                'label'       => __( 'Apply handling fee to the value of cart.', 'extensionworks' ),
                'type'        => 'checkbox',
                'description' => __( 'Instead of applying handling fee to shipping rate, apply it to the value of cart.', 'extensionworks' ),
                'default'     => 'no'
            ),
            // 'letter_setting' => array(
            //     'title'   => 'Letter mail setting',
            //     'type'    => 'select',
            //     'class'   => 'chosen_select',
            //     'css'     => 'width: 30em;',
            //     'default' => 'together',
            //     'options' => array(
            //         'together' => __( 'Packing letter mail togather with parcel', 'extensionworks' ),
            //         'separate' => __( 'Packing letter mail separately', 'extensionworks' ),
            //     ),

            // ),
            'unpackable_setting' => array(
                'title'   => 'Unpackable products setting',
                'type'    => 'select',
                'class'   => 'chosen_select',
                'css'     => 'width: 30em;',
                'default' => 'direct',
                'options' => array(
                    'direct'    => __( 'Send unpackable products directly', 'extensionworks' ),
                    'remove'    => __( 'Remove unpackable products', 'extensionworks' ),
                    'terminate' => __( 'Terminate calculate process', 'extensionworks' ),
                ),
            ),
            'packing_method'  => array(
                'title'   => __( 'Choose Packing Method', 'extensionworks' ),
                'type'    => 'select',
                'default' => 'box_packing',
                'class'   => 'chosen_select',
                'css'     => 'width: 50em;',
                'description' => __( 'Pack into boxes with weights, dimensions and volume.', 'extensionworks' ),
                'options' => array(
                    'volume_packing' => __( 'Volume packing', 'extensionworks' ),
                    'per_item'       => __( 'Pack items individually', 'extensionworks' ),
                    'box_packing'    => __( 'Recommended: Box packing', 'extensionworks' ),
                    'weight_based'   => __( 'Weight based', 'extensionworks' ),
                ),
            ),
            'boxes'  => array(
                'type' => 'box_packing'
            ),
            'shipping_method' => array(
                'title'   => __( 'Shipping Method', 'extensionworks' ),
                'type'    => 'checkbox',
                'label'   => 'Enable customise shipping methods',
                'description' => __( 'Please use this format when adding a price adjustment.<br />Enter an amount, e.g. + or -2.50, or a percentage, e.g. + or -5%.', 'extensionworks' ),
                'default' => 'no',
            ),
            'custom_services'  => array(
                'type'    => 'services'
            ),
            'rate_options' => array(
                'title' => 'Rate Option',
                'type'  => 'title'
            ),
            'rate_type' => array(
                'title'   => 'Select shipping rate to display',
                'type'    => 'select',
                'default' => '0',
                'css'     => 'width: 15em;',
                'class'   => 'chosen_select',
                'options' => array(
                    '0' => 'All',
                    '1' => 'Cheapest rate',
                    '2' => 'Highest rate'
                )
            ),
            'rate_order' => array(
                'title'   => 'Select shipping order to display',
                'type'    => 'select',
                'default' => '0',
                'css'     => 'width: 15em;',
                'class'   => 'chosen_select',
                'options' => array(
                    '0' => 'None',
                    '1' => 'From low to high',
                    '2' => 'From high to low'
                )
            ),
            'fallback_name' => array(
                'title'       => __( 'Fallback name', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'Add your own FallBack name here, this will be displayed to your website customers.', 'extensionworks' ),
                'default'     => 'Fallback'
            ),
            'fallback' => array(
                'title'       => __( 'Fallback', 'extensionworks' ),
                'type'        => 'text',
                'description' => __( 'If shipping method returns no matching rates, offer this amount for shipping so that the user can still checkout. Leave blank to disable.', 'extensionworks' ),
                'default'     => ''
            ),
            'api_setting' => array(
                'title' => 'Plugin Setting',
                'type' => 'title',
            )
        );
    }

    /**
     * Add additional form fields
     */
    public function add_form_fields() {
        return array();
    }


    /**
     * Sort admin fields for displaying in order
     */
    public function sort_form_fiels() {

        $fields = array();

        // Merge fields order
        if ( empty( $this->settings_order ) ) {
            $this->settings_order = array_keys( $this->form_fields );
        } else {
            $this->settings_order = array_merge( $this->settings_order, array_keys( $this->form_fields ) );
        }

        // Sorting
        foreach ( $this->settings_order as $order ) {

            if ( isset( $this->form_fields[$order] ) ) {
                $fields[$order] = $this->form_fields[$order];
            }
        }

        $this->form_fields = $fields;

    }

    /**
     * Shipping method available conditions
     */
    public function is_available( $package ) {

        if( !$this->is_activated() )
            return false;

        if ( !in_array( $this->currency, $this->allowed_currencies ) )
            return false;

        if ( !empty( $this->allowed_origin_countries ) )
            if ( !in_array( $this->origin_country, $this->allowed_origin_countries ) )
                return false;

        return parent::is_available( $package );
    }



    function is_activated() {

        $response = get_option( 'ew-updater-activated', array() );

        if ( isset( $response[ $this->plugin ] ) )
            return true;

        return false;
    }

    /**
     * Check if this shippment is international shipping
     */
    public function is_intel_shipping() {

        $country = $this->get_shipping_country();

        return !in_array( $country, $this->allowed_origin_countries );
    }


    /**
     * prepare products, find shippable products
     *
     * @return [type] [description]
     */
    public function prepare_products( $packages ) {

        global $woocommerce;
        if ( empty( $packages ) )
            return;

        $items = array();

        foreach ( $packages['contents'] as $key => $value ) {

            $product = $value['data'];

            $option_data = get_post_custom( $product->id );

            $letter_mail = isset( $option_data['_letter_mail'] ) && $option_data['_letter_mail'][0] == 'yes' ? true : false;

            if ( ! $product->needs_shipping() )
                continue;

            if ( $product->length && $product->height && $product->width && $product->weight ) {

                for ( $i=0; $i < $value['quantity']; $i++ ) {
                    $item  = new EW_Product( $product->length,
                        $product->height,
                        $product->width,
                        $product->get_weight(),
                        $product->get_price(),
                        get_option( 'woocommerce_dimension_unit' ),
                        get_option( 'woocommerce_weight_unit' ),
                        $product->get_title()
                        );

                    $item->set_id( $product->id );
                    $item->set_letter_mail( $letter_mail );
                    $item->set_location(  isset( $option_data['_postcode'] ) && $option_data['_postcode'][0] != '' ? $option_data['_postcode'][0]: apply_filters('extensionworks_product_postcode',$this->origin ));
                    $item->set_city( isset( $option_data['_city'] ) && $option_data['_city'][0] != '' ? $option_data['_city'][0]: apply_filters('extensionworks_product_city',$woocommerce->countries->get_base_city()) );
                    $item->set_state( isset( $option_data['_state'] ) && $option_data['_state'][0] != '' ? $option_data['_state'][0]: apply_filters('extensionworks_product_state',$woocommerce->countries->get_base_state()) );

                    $items []= $item;
                }

            } else {

                for ( $i=0; $i < $value['quantity'] ; $i++ ) {
                    $item  = new EW_Product( 1, 1, 1,
                        $product->get_weight(),
                        $product->get_price(),
                        get_option( 'woocommerce_dimension_unit' ),
                        get_option( 'woocommerce_weight_unit' ) );

                    $item->set_id( $product->id );
                    $item->set_location(  isset( $option_data['_postcode'] ) && $option_data['_postcode'][0] != '' ? $option_data['_postcode'][0]: apply_filters('extensionworks_product_postcode',$this->origin ));
                    $item->set_city( isset( $option_data['_city'] ) && $option_data['_city'][0] != '' ? $option_data['_city'][0]: apply_filters('extensionworks_product_city',$woocommerce->countries->get_base_city()) );
                    $item->set_state( isset( $option_data['_state'] ) && $option_data['_state'][0] != '' ? $option_data['_state'][0]: apply_filters('extensionworks_product_state',$woocommerce->countries->get_base_state()) );

                    $items []= $item;
                }

                // $this->add_debug_message( sprintf( __( 'Product # is missing dimensions. Using 1x1x1.', 'extensionworks' ), $product->product_id ) , 'FW' );
            }
        }

        $num_item = count( $items );
        $this->add_debug_message( "$num_item products prepare to pack", 'FW shippable products' );

        $this->letter_mail_available = $this->letter_mail_available( $items );

        return $items;

    }


    public function prepare_boxes( $boxes, $type ) {

        $containers = array();

        foreach ( $boxes as $key => $box ) {

            if ( ! $box['is_enable'] )
                continue;

            $container = new EW_Container( $box );

            $container->set_dimension_unit( $this->dimension_unit );

            $container->set_weight_unit( $this->weight_unit );

            $containers []= $container;
        }


        return $containers;
    }

    public function calculate_shipping( $packages = array() ) {

        if( $this->get_shipping_postcode() == '')
            return;

        global $woocommerce;

        $shippable_products =  $this->prepare_products( $packages );

        //order limit
        $total = 0;
        foreach ($shippable_products as $key => $product ) {
            $total += $product->get_price();
        }
        if( $this->order_limit > 0  && $total >= $this->order_limit ){

            $rate = array(
                    'id'    => $this->id,
                    'cost'  => 0,
                    'label' => $this->free_text,
                );
            $this->add_to_rate( $rate );

            return;
        }

        $available_boxes = $this->prepare_boxes( $this->boxes, $this->packing_method );

        $this->add_debug_message( $available_boxes, 'FW available boxes' );

        try {
            $robot = $this->set_packing_robot( $this->dimension_unit, $this->weight_unit );

            $robot->set_containers( $available_boxes );


            $locations = $this->pick_items_by_location($shippable_products);

            $packages = array();
            foreach ($locations as $key => $products ) {

                $packages[ $key ] = $this->package_items($robot, $products );
            }

            $this->add_debug_message( $packages, 'FW packages' );

            $locations = $packages;
            
           //set package location inform
            foreach ($locations as $l => $packages ) {

                foreach ($packages as $key => $package) {
                     $items = $package['container']->get_items();

                    if( $items[0]->get_location() != '' ){
                        $package['location'] = $items[0]->get_location();
                        $package['city'] = $items[0]->get_city();
                        $package['state'] = $items[0]->get_state();
                        $locations[$l][$key] = $package;
                    }
                }
               
            }

            set_transient( '_package_', $locations );


            //calculate rates
            foreach ($locations as $key => $packages) {

                $rate = $this->get_package_rates( $packages, $this->prepare_methods() );
                if( empty( $rate ) ){
                    if ( $this->fallback ) {
                        $rate = array(
                            array( 
                                'id'    => 'fallback',
                                'cost'  => $this->fallback,
                            )
                        );
                    }else{
                        return ;
                    }

                }

                $rates[$key] = $rate;
            }

            $this->add_debug_message( $rates , 'Location rates');

            $rates = $this->merge_rates( $rates);

            $this->add_debug_message( $rates , 'Result for sum rates');

            $this->add_rates( $rates );
        } catch (Exception $e) {
            $this->add_debug_message( $e->getMessage() , 'Packing Robot Error');
        }
    }

    /**
     * merge different location rates
     * */
    public function merge_rates( $rates ){

        $ids = array();
        foreach ($rates as $location => $rate) {
            foreach ($rate as $value) {
                $ids[$location][] = $value['id'];
            }
        }

        $first = array_pop( $ids );

        foreach ($ids as $location => $list ) {
            $first = array_intersect($first, $list );
        }

        $results = array();
        foreach ($first as $key => $id) {
            $result = array();
            foreach ($rates as $location => $r) {
                foreach ($r as $v) {
                    if( $v['id'] == $id ){
                        $result = $this->sum_rate( $result, $v );
                    }
                }
            }

            $results[] = $result;
        }
        return $results;
    }

    public function sum_rate( $a, $b ){

        if( count( $a ) ==0 && count($b) == 0 ){
            return array();
        }

        if( count($a) == 0 ){
            return $b;
        }

        if( count($b) == 0 ){
            return $a;
        }

        $keys = array_keys( $a );

        $result = array();
        foreach ($keys as $key ) {
            if( $key == 'id'){
                $result['id'] = $a[ $key ];
            }else{
                $result[ $key] = $a[ $key] + $b[$key];
            }
        }
        return $result;
    }

    public function package_items( $robot, $products ){

        $robot->set_items( $products );

        $packages = $robot->packing();

        $packages = $this->handle_unpacked_items( $packages );

        return $packages;
    }

    public function pick_items_by_location( $products ){

        $locations = array();

        foreach ($products as $item ) {

            $locations[ $item->get_location() ][] = $item;
        }

        return $locations;
    }


    public function save_package( $order_id, $posted ){


        $packages = get_transient( '_package_' );


        if( $packages ){
            foreach ($packages as $key => $package ) {
                foreach ($package as $k => $v) {
                    $packages[$key][$k]['container'] = $v['container']->to_array();
                }

            }

            update_post_meta( $order_id, '_order_package', $packages );
        }


    }

    public function add_rates( $rates ) {

        if ( is_array( $rates ) && count( $rates ) >0 ) {

            //apply fee to rate
            $rates = $this->fee_to_cart( $rates );

            $rates = $this->apply_adjustment_fee( $rates );

            //set rates order
            $rates = $this->arrange_rates( $rates, $this->rate_type, $this->rate_order );

            //set rate label and remove zero rate
            foreach ( $rates as $key => $rate ) {

                if( $rate['id'] == 'fallback'){
                    $rate['label'] = $this->fallback_name;
                }else if ( $rate['cost'] > 0  && isset($this->custom_services[ $rate['id'] ]) &&$this->custom_services[ $rate['id'] ]['enabled']  ) {
                    $rate['label'] = $this->custom_services[ $rate['id'] ]['name'] == '' ?  $this->services[ $rate['id'] ]['name'] : $this->custom_services[ $rate['id'] ]['name'];
                   
                }

                $this->add_to_rate( $rate );
            }
        }else if ( $this->fallback ) {
                $rate = array(
                    'id'    => 'fallback',
                    'cost'  => $this->fallback,
                    'label' => $this->fallback_name,
                );
                $this->add_to_rate( $rate );
            }
    }

    public function fee_to_cart( $rates ) {

        if ( !$this->fee )
            return $rates;

        if ( $this->fee_to_cart == 'yes' ) {
            foreach ( $rates as $id => $rate ) {
                $rates[$id]['cost'] += $this->adjust_fee( $this->get_products_total_price(), $this->fee );
            }
        }else {
            foreach ( $rates as $id => $rate ) {
                $rates[$id]['cost'] = $this->get_fee( $rates[$id]['cost'], $this->fee );
            }
        }

        return $rates;
    }


    public function apply_adjustment_fee( $rates ) {

        foreach ( $rates as $key => $rate ) {

            if ( isset($this->custom_services[ $rate['id'] ] ) && $this->custom_services[ $rate['id'] ]['adjustment'] != '' )
                $rates[$key]['cost'] = $this->get_fee( $rates[$key]['cost'], $this->custom_services[ $rate['id'] ]['adjustment'] );
            elseif ( isset($this->custom_services[ $rate['id'] ] ) && $this->custom_services[ $rate['id'] ]['adjustment_percent'] != '' )
                $rates[$key]['cost'] = $this->get_fee( $rates[$key]['cost'], $this->custom_services[ $rate['id'] ]['adjustment_percent'] );
        }

        return $rates;
    }

    public function handle_unpacked_items( $packages ) {

        global $woocommerce;

        switch ( $this->unpackable_setting ) {

            case 'direct':
                $packages = $this->handle_unpackable_products( $packages );
                break;
            case 'remove':
                break;
            case 'terminate':
                $woocommerce->add_error( 'There are some unpackable products.' );
                break;
        }

        return $packages['packages'];
    }

    public function set_packing_robot(  $dimension_unit, $weight_unit ) {


        $robot = new EW_Packing_Robat(  $dimension_unit, $weight_unit );

        switch ( $this->packing_method ) {

            case 'box_packing':
                include_once 'class-packing-box-packing.php';
                $robot->set_packing_method( new EW_Box_Packing( $robot ) );
                $this->add_debug_message( 'Box packing algorithem is set', 'FW setting packing algorithem' );
                break;

            case 'volume_packing':
                include_once 'class-packing-box-packing.php';
                include_once 'class-packing-volume-packing.php';
                $robot->set_packing_method( new EW_Volume_Packing( $robot ) );
                $this->add_debug_message( 'Volume packing algorithem is set', 'FW setting packing algorithem' );
                break;
            case 'weight_based':
                include_once 'class-packing-box-packing.php';
                include_once 'class-packing-weight-based.php';
                $robot->set_packing_method( new EW_Weight_Based_Packing( $robot ) );
                $this->add_debug_message( 'Weight based packing algorithem is set', 'FW setting packing algorithem' );
                break;
            case 'per_item':
                include_once 'class-packing-box-packing.php';
                include_once 'class-packing-per-item.php';
                $robot->set_packing_method( new EW_Per_Item_Packing( $robot ) );
                $this->add_debug_message( 'Per item packing algorithem is set', 'FW setting packing algorithem' );
                break;
        }

        $robot->set_letter_mail_setting(  $this->letter_setting );

        if ( $this->debug == 'yes' )
            $robot->set_debug_on();

        $this->robot = $robot;

        return $robot;
    }

    public function get_robot() {
        return $this->robot;
    }

    public function prepare_methods() {

        $methods = array();
        foreach ( $this->custom_services as $key => $setting ) {
            if ( $setting['enabled'] )
                $methods[] = $key;
        }

        return $methods;
    }

    /**
     * create package directly for unpackable products
     *
     * @return [type] [description]
     */
    public function handle_unpackable_products( $packages ) {

        if ( $this->letter_setting == 'separate' ) {
            if ( isset( $packages['unpacked'] ) && isset( $packages['unpacked']['box'] ) ) {

                foreach ( $packages['unpacked']['box'] as  $item ) {
                    $container = new EW_Container();

                    $container->set_inner_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                    ->set_outer_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                    ->put( $item );

                    $packages['packages']['box'][] = array( 'container' => $container, 'precent'=>100, 'remaining_volume'=>0 );
                }

            }

            if ( isset( $packages['unpacked'] ) && isset( $packages['unpacked']['letter'] ) ) {
                foreach ( $packages['unpacked']['letter'] as $item ) {
                    $container = new EW_Container();

                    $container->set_inner_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                    ->set_outer_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                    ->put( $item );

                    $packages['packages']['letter'][] = array( 'container' => $container, 'precent'=>100, 'remaining_volume'=>0 );
                }
            }

        }elseif ( isset( $packages['unpacked'] ) ) {

            foreach ( $packages['unpacked'] as $item ) {
                $container = new EW_Container();

                $container->set_inner_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                ->set_outer_dimension( $item->get_length(), $item->get_width(), $item->get_height() )
                ->put( $item );

                $packages['packages'][] = array( 'container' => $container, 'precent'=>100, 'remaining_volume'=>0 );

            }
        }

        return $packages;
    }

    /**
     * arrange rates to display
     *
     * @param string  $type  0 is return all rates, 1 is return cheapest rate, 2 is highest rates.
     * @param string  $order 0 is none, 1 is low to high, 2 is high to low
     * @return array        rates to display
     */
    public function arrange_rates( $rates, $type='0', $order='0' ) {

        $new_rates = array();

        switch ( $type ) {
            case '1': //return cheapest rate
                $new_rates = $this->get_cheapest_rate( $rates );
                break;
            case '2': //return highest rate
                $new_rates = $this->get_highest_rate( $rates );
                break;
            default: //return all rates
                switch ( $order ) {
                    case '1':   //from low to high
                        $new_rates = $this->sort_rates( $rates, '0' );
                        break;
                    case '2': //from high to low
                        $new_rates = $this->sort_rates( $rates, '1' );
                        break;
                    default:
                        $new_rates = $rates;
                        break;
                }
                break;
        }

        return $new_rates;
    }

    public function get_package_rates( $packages, $methods ) {

        $rates = array();

        return $rates;
    }

    function get_cheapest_rate( $rates ) {


        $c_rates = $this->sort_rates( $rates, '0' );

        $rate = array_shift( $c_rates );

        return array( $rate['id'] => $rate );
    }

    function get_highest_rate( $rates ) {

        $c_rates = $this->sort_rates( $rates, '1' );

        $rate = array_shift( $c_rates );

        return array( $rate['id'] => $rate );
    }

    function sort_rates( $rates, $order = '0' ) {

        usort( $rates, array( 'EW_Shipper', 'compare_rate' ) );

        if ( '0' == $order ) {
            return $rates;
        }else
            return array_reverse( $rates );
    }

    static public function compare_rate( $a, $b ) {
        return ( $a['cost'] > $b['cost'] ) ? 1 : -1;
    }


    public function get_products_total_price() {
        global $woocommerce;
        return $woocommerce->cart->cart_contents_total;
    }

    public function add_to_rate( $rate ) {

        $this->add_rate( $rate );
    }

    public function get_available_methods() {

        $methods = array();

        if ( is_array( $this->selected_package_methods ) )
            $methods = array_merge( $methods, $this->selected_package_methods );

        if ( isset( $this->selected_letter_methods ) && is_array( $this->selected_letter_methods ) )
            $methods = array_merge( $methods, $this->selected_letter_methods );

        return $methods;
    }

    public function get_all_shipping_methods() {
        return $this->package_shipping_methods + $this->letter_shipping_methods;
    }



    public function get_containers() {

        if ( $this->letter_mail_available )
            return $this->selected_envelopes;
        else
            return $this->selected_boxes;
    }

    /**
     * Check if letter mail is available for this cart
     */
    public function letter_mail_available( $products ) {
        foreach ( $products as $product ) {
            if ( !$product->is_letter_mail() )
                return false;
        }
        return true;
    }


    /**
     * Validate Settings Field Data.
     *
     * Validate the data on the "Settings" form.
     *
     * @since 1.0.0
     * @uses method_exists()
     * @param bool    $form_fields (default: false)
     * @return void
     */
    public function validate_settings_fields( $form_fields = false, &$other_sanitized_fields = NULL ) {

        if ( ! $form_fields )
            $form_fields = $this->form_fields;

        $sanitized_fields = array();

        foreach ( $form_fields as $k => $v ) {
            if ( ! isset( $v['type'] ) || ( $v['type'] == '' ) ) { $v['type'] = 'text'; } // Default to "text" field type.

            if ( ! isset( $v['rules'] ) || ( $v['rules'] == '' )  ) { $v['rules'] = 'none'; }

            $rules = explode( ' ', $v['rules'] );

            if ( method_exists( $this, 'validate_' . $v['type'] . '_field' ) ) {
                $field = $this->{'validate_' . $v['type'] . '_field'}( $k );

                if ( $field == '' &&  !in_array( 'none', $rules ) )
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' can not be empty.';

                if ( in_array( 'int', $rules ) && $field && !is_numeric( $field ) )
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be an integer number.';

                if ( in_array( 'float', $rules ) && $field && !is_numeric( $field ) )
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a float number.';

                if ( in_array( 'numeric', $rules ) && $field && !is_numeric( $field ) )
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a number.';

                if ( in_array( 'string', $rules ) && $field && !is_string( $field ) )
                    $this->errors['field_error_' . $k] = 'Value of ' . $v['title'] . ' must be a string.';

                $sanitized_fields[$k] = $field;
            } else {
                $sanitized_fields[$k] = $this->settings[$k];
            }
        }

        if ( is_array( $other_sanitized_fields ) ) {
            $other_sanitized_fields = $sanitized_fields;
        } else {
            $this->sanitized_fields = $sanitized_fields;
        }
    }


    /************** Help functions *****************/

    /**
     * Use WooCommerce logger if debug is enabled.
     */
    public function add_log( $message='test', $debug_on = true ) {

        global $woocommerce;

        if ( $this->debug == 'yes' || !$debug_on ) {
            if ( !$this->log ) $this->log = $woocommerce->logger();

            $this->log->add( $file=$this->id, $message );
        }
    }

    public function adjust_fee( $value, $adj ) {

        if ( strstr( $adj, '%' ) )
            $fee = number_format( ( ( $value / 100 ) * str_replace( '%', '', $adj ) ), 2 );
        elseif ( !$adj )
            return 0;
        else
            $fee = number_format( $adj , 2 );

        return $fee;
    }

    public function get_fee( $value, $adj ) {

        $symbol = '';

        if ( strpos( $adj, '-' ) === 0 ) {
            $symbol = '-';
            $adj = str_replace( '-', '', $adj );
        }else if ( strstr( $adj, '+' ) ) {
                $adj = str_replace( '+', '', $adj );
            }

        $fee = $this->adjust_fee( $value, $adj );

        switch ( $symbol ) {
        case '-':
            $fee = $value - $fee;
            break;
        default:
            $fee = $value + $fee;
            break;
        }

        if ( $fee > 0 )
            return $fee;
        else
            return 0;

    }

    public function add_debug_message( $msg, $title ) {

        if ( $this->debug != 'yes' )
            return;

        ew_add_response_message( $msg, $title );
    }

    public function get_shipping_postcode() {

        global $woocommerce;

        return $woocommerce->customer->get_shipping_postcode();
    }

    public function get_shipping_country() {

        global $woocommerce;

        return $woocommerce->customer->get_shipping_country();
    }

    /**
     * Show response returns when debug is on
     */
    // public function show_response( $response ) {

    //     global $woocommerce;

    //     if ( $this->debug == 'yes' ) {
    //         $woocommerce->clear_messages();
    //         wc_add_notice( '<p>'. $this->carrier .' Response:</p><ul><li>' . implode( '</li><li>', $response ) . '</li></ul>' );
    //     }
    // }

    /**
     * install carrier class if one doesn't exist
     * This function is triggered by woocommerce_register_post_type hook
     */
    public function install_category() {

        if ( !term_exists( $this->carrier, 'product_shipping_class' ) ) {
            wp_insert_term(
                $this->carrier, // the term
                'product_shipping_class', // the taxonomy
                array(
                    'description'=> $this->description,
                    'slug' => trim( $this->carrier )
                )
            );
            return true;
        }
        return false;
    }

    /**
     * Generate Select HTML.
     *
     * @access public
     * @param mixed   $key
     * @param mixed   $data
     * @since 1.0.0
     * @return string
     */
    public function generate_select_html( $key, $data ) {

        $html = '';

        if ( isset( $data['title'] ) && $data['title'] != '' ) $title = $data['title']; else $title = '';

        $data['options'] = ( isset( $data['options'] ) ) ? (array) $data['options'] : array();
        $data['class']   = ( isset( $data['class'] ) ) ? $data['class'] : '';
        $data['css']     = ( isset( $data['css'] ) ) ? $data['css'] : '';

        $html .= '<tr valign="top">' . "\n";
        $html .= '<th scope="row" class="titledesc">';
        $html .= '<label for="' . $this->plugin_id . $this->id . '_' . $key . '">' . $title . '</label>';
        $html .= '</th>' . "\n";
        $html .= '<td class="forminp">' . "\n";
        $html .= '<fieldset><legend class="screen-reader-text"><span>' . $title . '</span></legend>' . "\n";
        $html .= '<select name="' . $this->plugin_id . $this->id . '_' . $key . '" id="' . $this->plugin_id . $this->id . '_' . $key . '" style="'.$data['css'].'" class="select '.$data['class'].'">';

        foreach ( $data['options'] as $option_key => $option_value ) :
            $html .= '<option value="'.$option_key.'" '.selected( $option_key, esc_attr( isset( $this->settings[$key] ) ? $this->settings[$key] : '' ), false ).'>'.$option_value.'</option>';
        endforeach;

        $html .= '</select>';
        if ( isset( $data['description'] ) && $data['description'] != '' ) { $html .= ' <p class="description">' . $data['description'] . '</p>' . "\n"; }
        $html .= '</fieldset>';
        $html .= '</td>' . "\n";
        $html .= '</tr>' . "\n";

        return $html;
    }

    function generate_services_html() {
        ob_start();
        ?>
        <tr valign="top" id="service_options">
            <th scope="row" class="titledesc"><?php _e( 'Services', 'extensionworks' ); ?></th>
            <td class="forminp">
                <style type="text/css">
                    .shipping_services th.sort {
                        width: 1%;
                    }
                    .shipping_services td.sort {
                        cursor: move;
                        width: 1%;
                        padding: 0;
                        cursor: move;
                        background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAHUlEQVQYV2O8f//+fwY8gJGgAny6QXKETRgEVgAAXxAVsa5Xr3QAAAAASUVORK5CYII=) no-repeat center;                   }
                    .shipping_services input{
                        width: 90%;
                    }
                </style>
                <table class="shipping_services widefat">
                    <thead>
                        <th class="sort">&nbsp;</th>
                        <th ><?php _e( 'Service Code', 'extensionworks' ); ?></th>
                        <th ><?php _e( 'Name', 'extensionworks' ); ?></th>
                        <th style='width: 3%' ><?php _e( 'Enabled', 'extensionworks' ); ?></th>
                        <th style='width: 10%' ><?php echo sprintf( __( 'Price Adjustment (%s)', 'extensionworks' ), get_woocommerce_currency_symbol() ); ?></th>
                        <th style='width: 10%' ><?php _e( 'Price Adjustment (%)', 'extensionworks' ); ?></th>
                    </thead>
                    <tbody>
                    <?php

                    $sort = 0;

                    $this->ordered_services = array();

                    foreach ( $this->services as $code => $values ) {

                        if ( isset( $this->custom_services[ $code ]['order'] ) ) {
                            $sort = $this->custom_services[ $code ]['order'];
                        }

                        while ( isset( $this->ordered_services[ $sort ] ) )
                            $sort++;

                        $this->ordered_services[ $sort ] = array( $code, $values );

                        $sort++;
                    }

                    ksort( $this->ordered_services );

                    foreach ( $this->ordered_services as $value ) {
                        $code   = $value[0];
                        $values = $value[1];
                        if ( ! isset( $this->custom_services[ $code ] ) )
                            $this->custom_services[ $code ] = array();

                    ?>
                        <tr>
                            <td class="sort">
                                <input type="hidden" class="order" name="shipping_service[<?php echo $code; ?>][order]" value="<?php echo isset( $this->custom_services[ $code ]['order'] ) ? $this->custom_services[ $code ]['order'] : ''; ?>" />
                            </td>
                            <td>
                                <input type="text" name="shipping_service[<?php echo $code; ?>][code]" placeholder="<?php echo $code; ?>" value="<?php echo $code ?>" size="10" readonly=false />
                            </td>
                            <td>
                                <input type="text" name="shipping_service[<?php echo $code; ?>][name]" placeholder="<?php echo $values['name']; ?> (<?php echo $this->title; ?>)" value="<?php echo isset( $this->custom_services[ $code ]['name'] ) ? $this->custom_services[ $code ]['name'] : ''; ?>" size="35" />
                            </td>
                            <td>
                                <input type="checkbox" name="shipping_service[<?php echo $code; ?>][enabled]" <?php checked( ( ! isset( $this->custom_services[ $code ]['enabled'] ) || ! empty( $this->custom_services[ $code ]['enabled'] ) ), true ); ?> />
                            </td>
                            <td>
                                <?php echo get_woocommerce_currency_symbol(); ?><input type="text" name="shipping_service[<?php echo $code; ?>][adjustment]" placeholder="N/A" value="<?php echo isset( $this->custom_services[ $code ]['adjustment'] ) ? $this->custom_services[ $code ]['adjustment'] : ''; ?>" size="4" />
                            </td>
                            <td>
                                <input type="text" name="shipping_service[<?php echo $code; ?>][adjustment_percent]" placeholder="N/A" value="<?php echo isset( $this->custom_services[ $code ]['adjustment_percent'] ) ? $this->custom_services[ $code ]['adjustment_percent'] : ''; ?>" size="4" />%
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                </table>
                <script type="text/javascript">

                    jQuery(window).load(function(){


                        if ( jQuery('#woocommerce_<?php echo $this->id; ?>_shipping_method').is(':checked') ) {
                            jQuery('#service_options').show();
                        }else{
                            jQuery('#service_options').hide();
                        }

                        jQuery('#woocommerce_<?php echo $this->id; ?>_shipping_method').click(function(){
                            if ( jQuery('#woocommerce_<?php echo $this->id; ?>_shipping_method').is(':checked') ) {
                                jQuery('#service_options').show();
                            }else{
                                jQuery('#service_options').hide();
                            }
                        });



                        // Ordering
                        jQuery('.shipping_services tbody').sortable({
                            items:'tr',
                            cursor:'move',
                            axis:'y',
                            handle: '.sort',
                            scrollSensitivity:40,
                            forcePlaceholderSize: true,
                            helper: 'clone',
                            opacity: 0.65,
                            placeholder: 'wc-metabox-sortable-placeholder',
                            start:function(event,ui){
                                ui.item.css('baclbsround-color','#f6f6f6');
                            },
                            stop:function(event,ui){
                                ui.item.removeAttr('style');
                                shipping_services_row_indexes();
                            }
                        });

                        function shipping_services_row_indexes() {
                            jQuery('.shipping_services tbody tr').each(function(index, el){
                                jQuery('input.order', el).val( parseInt( jQuery(el).index('.shipping_services tr') ) );
                            });
                        };

                    });

                </script>
            </td>
        </tr>
        <?php
        return ob_get_clean();
    }

    public function validate_services_field( $key ) {

        $services         = array();

        $posted_services  = $_POST['shipping_service'];

        foreach ( $posted_services as $code => $settings ) {

            $services[ $code ] = array(
                'name'               => ew_clean( $settings['name'] ),
                'order'              => ew_clean( $settings['order'] )
            );

            $services[ $code ]['enabled']            = isset( $settings['enabled'] ) ? true : false;
            $services[ $code ]['adjustment']         = ew_clean( $settings['adjustment'] );
            $services[ $code ]['adjustment_percent'] = ew_clean( $settings['adjustment_percent'] );

        }
        return $services;
    }


    public function generate_box_packing_html() {
        ob_start();
        ?>
        <tr valign="top" id="packing_options">
            <th scope="row" class="titledesc"><?php _e( 'Box Settings', 'extensionworks' ); ?></th>
            <td class="forminp">
                <style type="text/css">
                    .box_form td, .shipping_services td {
                        vertical-align: middle;
                        padding: 4px 7px;
                    }
                    .box_form td input {
                        margin-right: 4px;
                    }
                    .box_form .check-column {
                        vertical-align: middle;
                        text-align: left;
                        padding: 0 7px;
                    }
                    .description > img {
                        width: 15px;
                    }
                </style>
                <p class="description">Box must be selected for packing service(with a ticked box <img src="<?php echo plugins_url( 'images/ticket.jpeg', __FILE__ ) ?>"></img> as an example)</p>
                <table class="box_form widefat">
                    <thead>
                        <tr>
                            <th class="check-column"><input type="checkbox" /><span>Select Box</span></th>
                            <th><?php _e( 'Outer Length', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Outer Width', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Outer Height', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Inner Length', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Inner Width', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Inner Height', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Box Weight', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Max Weight', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Label', 'extensionworks' ); ?></th>
                            <th class="weight_based" ><?php _e( 'Weight Based', 'extensionworks' ); ?></th>
                            <th><?php _e( 'Letter', 'extensionworks' ); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="3">
                                <a href="#" class="button plus insert"><?php _e( 'Add Box', 'extensionworks' ); ?></a>
                                <a href="#" class="button minus remove"><?php _e( 'Remove selected box(es)', 'extensionworks' ); ?></a>
                            </th>
                            <th colspan="7">
                                <small class="description"><?php _e( 'Items will be packed into these boxes based on item dimensions and volume. Outer dimensions will be passed to service api, whereas inner dimensions will be used for packing. Items not fitting into boxes will be packed individually.', 'extensionworks' ); ?></small>
                            </th>
                        </tr>
                    </tfoot>
                    <tbody id="boxes">
                    <?php
                    if ( $this->boxes ) {
                        foreach ( $this->boxes as $key => $box ) {
                    ?>
                        <tr>
                            <td class="check-column"><input type="checkbox" name="boxes_is_enable[<?php echo $key; ?>]" <?php checked( isset( $box['is_enable'] ) && $box['is_enable'] == true, true ); ?> /></td>
                            <td><input type="text" size="5" name="boxes_outer_length[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_length'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_outer_width[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_width'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_outer_height[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['outer_height'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_inner_length[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_length'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_inner_width[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_width'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_inner_height[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['inner_height'] ); ?>" /><?php echo $this->dimension_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_box_weight[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['box_weight'] ); ?>" /><?php echo $this->weight_unit; ?></td>
                            <td><input type="text" size="5" name="boxes_max_weight[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['max_weight'] ); ?>" /><?php echo $this->weight_unit; ?></td>
                            <td><input type="text" size="25" name="boxes_label[<?php echo $key; ?>]" value="<?php echo esc_attr( $box['label'] ); ?>" /><input type='hidden' name="boxes_id[<?php echo $key; ?>]" value="<?php echo isset( $box['id'] )?$box['id']:'';  ?>" /></td>
                            <td class="check-column weight_based"><input type="checkbox" name="boxes_is_weight_based[<?php echo $key; ?>]" <?php checked( isset( $box['is_weight_based'] ) && $box['is_weight_based'] == true, true ); ?> /></td>
                            <td><input type="checkbox" name="boxes_is_letter[<?php echo $key; ?>]" <?php checked( isset( $box['is_letter'] ) && $box['is_letter'] == true, true ); ?> /></td>

                        </tr>
                    <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <script type="text/javascript">

                    jQuery(window).load(function(){

                        jQuery('.box_form .insert').click( function() {
                            var $tbody = jQuery('.box_form').find('tbody');
                            var size = $tbody.find('tr').size();
                            var code = '<tr class="new">\
                                    <td class="check-column"><input type="checkbox" /></td>\
                                    <td><input type="text" size="5" name="boxes_outer_length[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_outer_width[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_outer_height[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_inner_length[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_inner_width[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_inner_height[' + size + ']" /><?php echo $this->dimension_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_box_weight[' + size + ']" /><?php echo $this->weight_unit; ?></td>\
                                    <td><input type="text" size="5" name="boxes_max_weight[' + size + ']" /><?php echo $this->weight_unit; ?></td>\
                                    <td><input type="text" size="25" name="boxes_label[' + size + ']" /><input type="hidden" name="boxes_id[' + size + ']" value="" /></td>\
                                    <td class="weight_based"><input type="checkbox" name="boxes_is_weight_based[' + size + ']" /></td>\
                                    <td><input type="checkbox" name="boxes_is_letter[' + size + ']" /></td>\
                                </tr>';

                            $tbody.append( code );

                            jQuery('#woocommerce_<?php echo $this->id; ?>_packing_method').change();

                            return false;
                        });

                        jQuery('.box_form .remove').click(function() {
                            var $tbody = jQuery('.box_form').find('tbody');

                            $tbody.find('.check-column input:checked').each(function() {
                                jQuery(this).closest('tr').hide().find('input').val('');
                            });

                            return false;
                        });

                        jQuery('#woocommerce_<?php echo $this->id; ?>_packing_method').change(function(){
                            var msg = '';
                            switch( jQuery(this).val() ){
                                case 'volume_packing':
                                    jQuery('table .box_form').find('.weight_based').hide();
                                    msg = 'Pack into boxes with weights and volume. ';
                                    break;
                                case 'box_packing':
                                    msg = 'Pack into boxes with weights, dimensions and volume. ';
                                    jQuery('table .box_form').find('.weight_based').hide();
                                    break;
                                case 'per_item':
                                    msg = 'Pack items individually into boxes. ';
                                    jQuery('table .box_form').find('.weight_based').hide();
                                    break;
                                case 'weight_based':
                                    msg = 'Regular sized items are grouped and quoted for weights only. Large items are quoted individually. '
                                    jQuery('table .box_form').find('.weight_based').show();
                                    break;
                            }

                            msg += 'For more information, please click <a href="https://extensionworks.zendesk.com/hc/en-us/articles/200873938-Latest-WooCommerce-extension-update-and-the-new-Settings-available-">this</a>.'

                            jQuery(this).next().next().html( msg );

                        }).change();

                        //checking max weight value for weight based method
                        jQuery('.box_form .weight_based input').click(function(){
                           var val = jQuery(this).closest('tr').find('input[name^="boxes_max_weight"]').val();

                            if( val == 0 || val.length == 0 ){
                                alert("Weight based packing requires a maximum weight to be applied.");
                                return false;
                            }

                        });

                        jQuery('#woocommerce_<?php echo $this->id; ?>_rate_type').change(function(){

                            if( jQuery(this).val() != 0 ){
                                jQuery(this).closest('tr').next().hide();
                            }else{
                                jQuery(this).closest('tr').next().show();
                            }
                        });

                        jQuery('#mainform').submit(function(){

                            var $origin = jQuery('#woocommerce_<?php echo $this->id; ?>_origin');

                            if( $origin.length != 0 && $origin.val().length == 0 ){
                                alert('Origin Postcode cannot be empty');
                                return false;
                            }

                            if( !checking_selected_box() ){
                                return false;
                            }


                            if ( !checking_weight_based_setting() ){
                                return false;
                            }

                            if( !checking_selected_service() ){

                                return false;
                            }


                            return true;
                        });

                    });

                    function checking_weight_based_setting(){
                        var method = jQuery('#woocommerce_<?php echo $this->id; ?>_packing_method').val();

                        if( method == 'weight_based' && jQuery('.box_form .weight_based input:checked').length == 0 ){
                            var result = confirm('Warning: You have selected the weight based packing method. Please select the option "weight based" as this is required.'+
                             '\nPress Ok to continue or press Cancel to stay on page.');

                            return result;
                        }
                        return true;
                    }

                    function checking_selected_service(){
                        var $services =  jQuery('.shipping_services input[type="checkbox"]:checked');


                        if( $services.length == 0 ){

                            var result = confirm('Warning: To use your shipping plugin, you need to select at least one service.\nPress Ok to continue or press Cancel to stay on page.');

                            if( !result )
                                return false;
                        }

                        return true;
                    }


                    function checking_selected_box(){

                        var $boxes =  jQuery('#boxes tr[style!="display: none;"]');

                        if( $boxes.length == 0 ){
                            var result = confirm('Warning: You haven\'t added any box.\nPress Ok to continue or press cancel to stay in page.');

                            if( !result )
                                return false;
                        }

                        var $select_boxes =  $boxes.find('input[name^="boxes_is_enable"]:checked');

                        if( $select_boxes.length == 0 ){

                            var result = confirm('Warning: To use your boxes for packing, you need to be selected.\nPress Ok to continue or press Cancel to stay on page.');

                            if( !result )
                                return false;
                        }

                        return true;
                    }

                </script>
            </td>
        </tr>
        <?php
        return ob_get_clean();
    }

    public function validate_box_packing_field( $key ) {

        $boxes = array();

        if ( isset( $_POST['boxes_outer_length'] ) ) {
            $boxes_is_enable       = isset( $_POST['boxes_is_enable'] ) ? $_POST['boxes_is_enable'] : array();
            $boxes_outer_length    = $_POST['boxes_outer_length'];
            $boxes_outer_width     = $_POST['boxes_outer_width'];
            $boxes_outer_height    = $_POST['boxes_outer_height'];
            $boxes_inner_length    = $_POST['boxes_inner_length'];
            $boxes_inner_width     = $_POST['boxes_inner_width'];
            $boxes_inner_height    = $_POST['boxes_inner_height'];
            $boxes_box_weight      = $_POST['boxes_box_weight'];
            $boxes_max_weight      = $_POST['boxes_max_weight'];
            $boxes_label           = $_POST['boxes_label'];
            $boxes_id           = $_POST['boxes_id'];
            $boxes_is_weight_based = isset( $_POST['boxes_is_weight_based'] ) ? $_POST['boxes_is_weight_based'] : array();
            $boxes_is_letter       = isset( $_POST['boxes_is_letter'] ) ? $_POST['boxes_is_letter'] : array();

            for ( $i = 0; $i < sizeof( $boxes_outer_length ); $i ++ ) {

                if ( $boxes_outer_length[ $i ] && $boxes_outer_width[ $i ] && $boxes_outer_height[ $i ] && $boxes_inner_length[ $i ] && $boxes_inner_width[ $i ] && $boxes_inner_height[ $i ] ) {
                    $boxes[] = array(
                        'label'           => $boxes_label[$i] == '' ? $boxes_outer_length[ $i ] .'*' . $boxes_outer_width[ $i ] .'*' .$boxes_outer_height[ $i ] : $boxes_label[$i],
                        'id'              => $boxes_id[$i],
                        'outer_length'    => floatval( $boxes_outer_length[ $i ] ),
                        'outer_width'     => floatval( $boxes_outer_width[ $i ] ),
                        'outer_height'    => floatval( $boxes_outer_height[ $i ] ),
                        'inner_length'    => floatval( $boxes_inner_length[ $i ] ),
                        'inner_width'     => floatval( $boxes_inner_width[ $i ] ),
                        'inner_height'    => floatval( $boxes_inner_height[ $i ] ),
                        'box_weight'      => floatval( $boxes_box_weight[ $i ] ),
                        'max_weight'      => floatval( $boxes_max_weight[ $i ] ),
                        'is_weight_based' => isset( $boxes_is_weight_based[ $i ] ) && $boxes_max_weight[ $i ]!=0 ? true : false,
                        'is_letter'       => isset( $boxes_is_letter[ $i ] ) ? true : false,
                        'is_enable'       => isset( $boxes_is_enable[$i] ) ? true: false
                    );

                }

            }
        }

        return $boxes;
    }

}

?>
