<?php
/*
  Plugin Name: Extension Works Framework
  Plugin URI: http://www.extensionworks.com
  Description: Extensionworks framework for woo commerce.
  Version: 2.0.8
  Author: Extension Works
  Author URI: http://www.extensionworks.com

  Copyright: © Extension Works.

 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'plugins_loaded', 'extensionworks_framework_ini', 0 );

function extensionworks_framework_ini(){

    if ( ! class_exists('ExtensionWork_Framework') ) { // Exit if framework alreay in use.
        /**
         * Required functions
         */
        include_once 'extensionworks-includes/extensionworks-functions.php';

        if ( !(is_woo_active()) )
            return;
        if( is_shipper_active() || is_updater_active() )
            return;


        /**
         * Plugin updates
         */
        class ExtensionWork_Framework {

            /**
             * @var string
             */
            var $version = '2.0.8';

            var $shipper;
            var $payer;
            var $updater;

            /**
             * HipperShipper Constructor.
             *
             * @access public
             * @return void
             */
            function __construct() {



                // Define version constant
                define( 'HIPPERSHIPPER_VERSION', $this->version );

                // Include required files
                $this->init_payer();
                $this->init_shipper();
                $this->init_updater();


                add_action( 'admin_menu', array($this, 'label_page') );
            }


            public function label_page(){
                $my_page = add_submenu_page( 'woocommerce','Extensionworks Reboot', 'Extensionworks Reboot', 'manage_woocommerce', 'ew-reboot-page', array( $this,'label_page_content') );

            }

            public function label_page_content(){
                if ( !current_user_can( 'manage_woocommerce' ) )  {
                    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
                }

                require_once(dirname(__FILE__) . '/screens/reboot-page.php');
            }


            function usps_package_box() {
                add_meta_box( 'usps_package_box', __( 'Package detail', 'extensionworks' ), array( $this, 'usps_package_box_content' ), 'shop_order', 'side', 'default' );
            }

            function usps_package_box_content( $post ) {
                wp_enqueue_script( 'bootstrapjs' );
                wp_enqueue_style( 'bootstrapcss' );

                $data = get_post_meta( $post->ID, '_order_package', true );


                if( !empty( $data ) && isset($data[0]['container'] )){

                    $newData['base'] = $data;

                    $data = $newData;
                }

                $showed_meta = array(
                    'outer_width'  => 'Outer Width',
                    'outer_length' => 'Outer Length',
                    'outer_height' => 'Outer Height',
                    'net_weight'   => 'Net Weight',
                    'max_weight'   => 'Max Weight',
                    'box_weight'   => 'Box Weight',
                    'gross_weight' => 'Gross Weight',
                    'girth'        => 'Girth',
                    'label'        => 'Box Label',

                );

                ?>

                <div class="panel-group" id="packages-info-widget">

                    <?php if( $data != false ):  ?>
                    <?php foreach ( $data as $l => $packages ): ?>
                    <?php foreach ( $packages as $k => $package ): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#packages-info-widget" href="#package-panel-<?php echo $l .'-'.$k; ?>">
                                Package #<?php echo $k +1 . ' for location ' . $l ; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="package-panel-<?php echo $l .'-'.$k; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul class="">

                                    <?php foreach ($package['container'] as $meta_key => $meta_value ): ?>
                                    <?php if ( array_key_exists( $meta_key, $showed_meta ) ): ?>
                                    <li class="">
                                        <div class="input-group input-group-sm">
                                            <span class="input-group-addon"><?php echo $showed_meta[ $meta_key] ?></span>
                                            <input class="form-control" type="text" id="_package_<?php echo $key. '_' . $meta_key; ?>" name="_package_<?php echo $key. '_' . $meta_key; ?>" placeholder="" value="<?php
                                                if ( isset( $package['container'][ $meta_key ] ) )
                                                echo esc_attr( $package['container'][ $meta_key ] );
                                                ?>" class="first" />
                                        </div>
                                    </li>

                                    <?php endif; ?>
                                    <?php endforeach ?>

                                    <li>
                                        <ul class='list-group'>
                                            <?php if(isset($package['container']['items'])): ?>
                                            <?php foreach ($package['container']['items'] as $num => $item): ?>
                                            <li class='list-group-item' style="font-weight: bold; background-color:#428BCA;color:#FFFFFF;">Name: <?php echo isset( $item['name'])? $item['name']:'';  ?></li>
                                            <li class='list-group-item'>

                                                <?php echo 'Length: ' . $item['length'] . ' ' . $item['dimension_unit'] ?>
                                            </li>
                                            <li class='list-group-item'>

                                                <?php echo 'Width: ' . $item['width'] . ' ' . $item['dimension_unit'] ?>
                                            </li>
                                            <li class='list-group-item'>

                                                <?php echo 'Height: ' . $item['height'] . ' ' . $item['dimension_unit'] ?>
                                            </li>
                                            <li class='list-group-item'>

                                                <?php echo 'Weight: ' . $item['weight'] . ' ' . $item['weight_unit'] ?>
                                            </li>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                    <?php endforeach ?>
                    <?php else: ?>
                    <h3>No package founded</h3>
                    <?php endif; ?>

                </div>


                <?php
            }

            function init_payer(){
                 include( 'classes/class-payer.php' );
            }

            function init_updater(){
                include( 'classes/class-extensionworks-updater.php');
                $GLOBALS['ew_updater'] = new EW_Updater( __FILE__ );
            }

            function init_shipper(){


                include( 'classes/class-packing-robat.php' );
                include( 'classes/short-codes.php' );
                include( 'classes/class-packing-algorithem.php' );
                include( 'classes/class-product.php' );     //contains shipping class skeleton
                include( 'classes/class-shipper.php' );     //contains shipping class skeleton
                include( 'classes/class-container.php' );   //contains container class
                include( 'classes/class-xmlparser.php' );   //contains xmlparser class
                include( 'classes/product-meta.php' );              //contains extra product meta processors

                add_action( 'admin_enqueue_scripts', array( $this, 'ew_scripts' ));
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_girth', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_lettermail', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_postcode', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_city', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_state', 10 );
                add_action( 'woocommerce_product_options_dimensions', 'woocommerce_product_ship', 10 );

                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_girth_metabox', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_lettermail_metabox', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_postcode', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_city', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_state', 1 );
                add_action( 'woocommerce_process_product_meta', 'woocommerce_process_product_ship', 1 );
                add_action('add_meta_boxes', array( $this, 'usps_package_box') );

                add_filter( 'woocommerce_cart_shipping_packages', 'ew_cart_shipping_packages');
            }

            
            function ew_scripts() {
                wp_enqueue_script( 'jquery-ui-sortable' );
                wp_enqueue_script( 'jquery-ui-accordion' );

                wp_register_style( 'bootstrapcss', plugins_url('/templates/css/bootstrap.min.css', __FILE__) );
                wp_register_script( 'bootstrapjs', plugins_url('/templates/js/bootstrap.js',__FILE__), array( 'jquery' ) );
            }

            public function get_shipping_class(){

                $methods = array();

                $methods = apply_filters('extensionworks_shipping_methods', $methods);

                foreach ($methods as $key => $class) {
                    $methods[$key] = new $class;
                }

                return $methods;
            }
        }

        $GLOBALS['hippershipper'] = new ExtensionWork_Framework();

        $GLOBALS['hipperxmlparser'] = new HipperXMLParser();
    }
}





function wc_framework_plugin_links( $links ) {

    $plugin_links = array(
        '<a href="http://help.extensionworks.com/hc/en-us/categories/200037608-General-Questions">' . __( 'FAQs', 'extensionworks' ) . '</a>',
        '<a href="http://help.extensionworks.com/hc/en-us/requests/new">' . __( 'Support', 'extensionworks' ) . '</a>',
        '<a href="http://help.extensionworks.com/hc/en-us">' . __( 'Docs', 'extensionworks' ) . '</a>',
    );

    return array_merge( $plugin_links, $links );
}

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_framework_plugin_links' );

?>
