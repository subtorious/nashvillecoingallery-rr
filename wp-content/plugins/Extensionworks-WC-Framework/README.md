WooCommerce Framework
===================

Requires WooCommerce at least: 1.5.4

== Description ==

The WooCommerce shipper will help create a better shipping solution for WooCommerce. Help other shipping extension to delivery more accurate shipping quotes, build clean and comprehensive UI, and provide the ability to simulate real-life shipping situations.

WooCommerce Shipper is built by Extension Works who also contributes to other shipping extensions for WooCommerce that provide real-time shipping quotes for your shop.

We are the original developers of the WooCommerce Shipping Extensions for FedEx, UPS, USPS, Australia Post and Canada Post.

= STRENGTH & FLEXIBILITY =

Build upon core Wordpress and WooCommerce functionality for stability, utilise native WooCommerce functions and classes and easily accessible and modifiable.

= SMART PACKING =

We are attempting something that other shipping extensions (including other e-commerce platforms) never do. By allowing the customer to utilise carrier sell boxes, as well as define your own boxes, we are simulating the real world packing solution in a fast, simple way.


= ACKNOWLEDGEMENT =

The HipperXMLParser is based on: http://snipplr.com/view/3491
