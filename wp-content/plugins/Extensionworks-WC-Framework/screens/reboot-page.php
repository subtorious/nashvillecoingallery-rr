<?php

 wp_enqueue_style( 'bootstrapcss' );

class EW_Setting_Store{

	var $slug = 'ew_plugin_backup_settings';

	var $setting_table = array();

	var $msg = array();

	var $plugins = array();

	public function __construct(){
		global $ew_updater;

		$data = array();
		$reference = $ew_updater->get_products();

		$this->init_setting_table();

		$plugin = get_plugins();


		if( is_array( $reference ) && count( $reference )>0 ){
			foreach ($reference as $k => $v) {

				if ( array_key_exists( $k, $plugin ) && array_key_exists( $k, $this->setting_table ) ) {
					$data[$k] = array( 'product_name' => $plugin[$k]['Name'], 'product_version' => $plugin[$k]['Version'], 'file_id' => $v['file_id'], 'product_id' => $v['product_id'], 'product_file_path' => $k );
				}


			}
		}

		$this->plugins = $data;
	}


	public function init_setting_table(){

		$this->setting_table = array(

			'WC-Shipping-FastwayAU/shipping-fastwayAU.php'         => array(  array( 'version' => '1.5.1', 'id' => 'WC_Shipping_FastWayAU') ),
			'WC-Shipping-AustraliaPost/shipping-austpost.php'      => array(  array( 'version' => '1.6.6', 'id' => 'WC_Shipping_AU_Post') ),
			'WC-Shipping-CanadaPost/shipping-canadapost.php'       => array(  array( 'version' => '1.5.5', 'id' => 'WC_Shipping_Canada_Post') ),
			'WC-Shipping-Fastway-IE/shipping-fastwayIE.php'        => array(  array( 'version' => '1.2.4', 'id' => 'WC_Shipping_FastWayIE') ),
			'WC-Shipping-FastwayNZ/shipping-fastwayNZ.php'         => array(  array( 'version' => '1.3.4', 'id' => 'WC_Shipping_FastWayNZ') ),
			'WC-Shipping-FastwayZA/shipping-fastwayZA.php'         => array(  array( 'version' => '1.2.4', 'id' => 'WC_Shipping_FastWayZA') ),
			'WC-Shipping-FedEx/shipping-fedex.php'                 => array(  array( 'version' => '2.7.5', 'id' => 'WC_Shipping_Fedex_WSDL' ),  array( 'version' =>'2.7.6', 'id' => 'EW_WC_Shipping_Fedex_WSDL' ) ),
			'WC-Shipping-Interparcel/shipping-interparcel.php'     => array(  array( 'version' => '1.3.5', 'id' => 'WC_Shipping_Interparcel') ),
			'WC-Shipping-New-Zealand-Post/shipping-nzlpost.php'    => array(  array( 'version' => '1.0.5', 'id' => 'WC_Shipping_NZL_Post') ),
			'WC-Shipping-Royal-Mail/shipping-royalmail.php'        => array(  array( 'version' => '1.2.5', 'id' => 'royalmail') ),
			'WC-Shipping-TollIPEC/shipping-tollipec.php'           => array(  array( 'version' => '1.2.0', 'id' => 'tollipec') ),
			'WC-Shipping-UPS/shipping-ups.php'                     => array(  array( 'version' => '1.5.5', 'id' => 'WC_Shipping_UPS') ),
			'WC-Shipping-USPS/shipping-usps.php'                   => array(  array( 'version' => '2.7.7', 'id' => 'us_ps') ),
			'WC-Payment-ANZeGate/gateway-anz-egate.php'            => array(  array( 'version' => '1.3.4', 'id' => 'anz_egate') ),
			'WC-Payment-CommonwealthBank/gateway-commonwealth.php' => array(  array( 'version' => '1.4.4', 'id' => 'cba') ),
			'WC-Payment-eWay/gateway-eway.php'                     => array(  array( 'version' => '1.5.3', 'id' => 'eway') ),
			'WC-Payment-eWayAU/gateway-eway.php'                   => array(  array( 'version' => '1.5.2', 'id' => 'eway') ),
			'WC-Payment-eWayNZ/gateway-ewaynz.php'                 => array(  array( 'version' => '1.1.2', 'id' => 'ewayNZ') ),
			'WC-Payment-eWayUK/gateway-ewayuk.php'                 => array(  array( 'version' => '1.2.0', 'id' => 'ewayUK') ),
			'WC-Payment-Realex/gateway_realex.php'                 => array(  array( 'version' => '1.1.3', 'id' => 'realex') ),
			'WC-Payment-Suncorp/gateway-suncorp.php'               => array(  array( 'version' => '1.3.4', 'id' => 'suncorp') ),
			'WC-Payments-Pin-Payments/gateways-pin.php'            => array(  array( 'version' => '1.1.3', 'id' => 'pin_payment') ),

		);


	}

	public function clean_all_option(){

		foreach ($this->setting_table as $key => $value) {

			foreach ($value as $key => $v) {
				$id = 'woocommerce_' . $v['id'] .'_settings';

				delete_option( $id );
			}
		}

		$this->msg[] = array(
			'type' => 'success',
			'message'  => 'Clean all data successful.'
		);

		return true;
	}

	public function clean_option( $key  ){

		if( array_key_exists( $key, $this->setting_table ) ){

			$id = $this->get_id( $key );

			$this->msg[] = array(
				'type' => 'success',
				'message'  => 'Clean ' . $this->plugins[ $key ]['product_name'] .' option successful.'
			);

			return delete_option( $id );
		}
	}

	public function get_id( $key ){


		$id = '';

		if( array_key_exists( $key, $this->setting_table ) ){


			$data = $this->plugins[ $key ];

			$version_list = $this->setting_table[$key ];


			foreach ($version_list as $key => $v) {

				$fv = $v;

				if( isset( $version_list[ $key +1 ] ) )
					$sv =  $version_list[ $key +1 ];
				else
					$sv = '';

				if( $data['product_version'] >= $fv['version'])
					$id = 'woocommerce_' . $fv['id'] .'_settings';



				if( $sv !='' && $data['product_version'] >= $sv['version'])
					$id = 'woocommerce_' . $sv['id'] .'_settings';

			}

			return $id;
		}


	}


	public function backup_option( $key ){


		if( array_key_exists( $key, $this->setting_table ) ){


			$id = $this->get_id( $key );


			$option = get_option( $id );

			if( $option === false ){

				$this->msg[] = array(
					'type' => 'warning',
					'message'  => 'Cannot find ' .$this->plugins[ $key ]['product_name'] .' option.'
				);

				return false;
			}else{


				$this->msg[] = array(
					'type' => 'success',
					'message'  => 'Backup ' . $this->plugins[ $key ]['product_name'] .' option successful.'
				);

				return $this->add_backup( $key, $option );
			}


		}

	}


	public function add_backup( $key, $setting ){


		$settings = get_option( $this->slug );


		$data = array(
			'setting' => $setting,
			'date'    => date("Y-m-d H:i:s"),
		);

		//first backup
		if( $settings === false ){
			$settings = array(
				$key => $data,
			);
		}else{

			$settings[ $key ] = $data;

		}

		return update_option( $this->slug, $settings );

	}

	public function get_backup( $key ){
		$settings = get_option( $this->slug );

		if( isset( $settings[ $key ] ) )
			return $settings[ $key ];
		else
			return false;
	}


	public function restore_option( $key ){

		$backup = $this->get_backup( $key );

		if( $backup !== false ){

			$option = $backup['setting'];

			$id = $this->get_id( $key );

			$this->msg[] = array(
				'type' => 'success',
				'message'  => 'Restore ' . $this->plugins[ $key ]['product_name'] .' option successful.'
			);

			return update_option( $id, $option );

		}else{

			$this->msg[] = array(
				'type' => 'error',
				'message'  => 'Restore ' . $this->plugins[ $key ]['product_name'] .' option failure. Cannot find backup for ' . $this->plugins[ $key ]['product_name'] .'.',
			);
			return false;
		}
	}


	public function generate_plugin_table( ){




		foreach ( $this->msg as $msg ) {

			if( $msg['type'] == 'error'){
				echo '<div class="alert alert-danger">' . $msg['message'] . '</div>';
			}

			if( $msg['type'] == 'success'){
				echo '<div class="alert alert-success">' . $msg['message'] . '</div>';
			}

			if( $msg['type'] == 'warning'){
				echo '<div class="alert alert-warning">' . $msg['message'] . '</div>';
			}
		}


		?>


		<form action="" method="post">

			<table class='table'>

				<thead>
					<th>
						ExtensionWorks Plugins
					</th>
					<th>
						Actions
					</th>
				</thead>
				<tbody>

					<?php foreach ( $this->plugins as $k => $v): ?>
					<tr>
						<td><?php echo $v['product_name'] ?></td>

						<td>
							<table>
								<tr>
									<td><button type="submit" value="<?php echo $k; ?>" class="btn btn-primary btn-sm" name="clean">Clean Settings</button></td>
									<td><button type="submit" value="<?php echo $k; ?>" class="btn btn-success btn-sm" name="backup">Backup Settings</button></td>
									<td><button type="submit" value="<?php echo $k; ?>" class="btn btn-info btn-sm" name="restore">Restore Settings</button></td>
									<td>

										<?php

											$backup = $this->get_backup( $k );

											if( $backup )
												echo '<span class="label label-default">' . $backup['date'] . '</span>';

										?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<?php endforeach ?>

					<tr>
						<td></td>
						<td>
							<button type="submit" value="<?php echo $k; ?>" class="btn btn-primary btn-sm" name="clean_all">Clean All</button>
						</td>
					</tr>
				</tbody>

			</table>

		</form>

		<?php

	}

}



$store = new EW_Setting_Store();

if( isset( $_POST['clean']) ){

	$store->clean_option( $_POST['clean'] );
}


if( isset( $_POST['backup']) ){

	$store->backup_option( $_POST['backup'] );

}

if( isset( $_POST['restore']) ){
	$store->restore_option( $_POST['restore'] );
}

if( isset( $_POST['clean_all']) ){
	$store->clean_all_option();
}



$store->generate_plugin_table();


?>

