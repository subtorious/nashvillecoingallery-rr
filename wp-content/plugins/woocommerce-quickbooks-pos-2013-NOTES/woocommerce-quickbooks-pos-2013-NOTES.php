<?php
/*
Plugin Name: QuickBooks Point of Sale 2013 (v 11)Connector for WooCommerce NOTES
Description: NOTES about custom code that lives inside QuickBooks Point of Sale 2013 (v 11)Connector for WooCommerce. This plugin serves only as a convenient place to remind us that their is custom code inside QuickBooks Point of Sale 2013 (v 11)Connector for WooCommerce, and to store that code in the event that that plugin is updated and thus the custom code is erased. Look in wp-content/plugins/woocommerce-quickbooks-pos-2013-NOTES/woocommerce-quickbooks-pos-2013-NOTES.php for instructions and to find the code. It does not matter if this plugin is active or not. Best to leave it inactive.
Version: 1.3.10
Author: HORTON GROUP




************* NOTES ************* 

Custom code was added to wp-content/plugins/woocommerce-quickbooks-pos-2013/templates/add-salesreceipt.php in order to fix an out-of-balence sync error that occured with out of state billing addresses because of a from with shipping tax being added when it shouldn't be for out of TN shipping. A copy of add-salesreceipt.php is here wp-content/plugins/woocommerce-quickbooks-pos-2013-NOTES/add-salesreceipt.php.



*/
?>