(function($)
{
	$(document).ready(function()
	{		
		//console.log('nc_product_sort 2');
		
		fix_product_list_items();
		$(window).resize(function()
		{
		//	console.log('resized window');
			fix_product_list_items();
		})
	});	

	function fix_product_list_items()
	{
		setTimeout(function()
		{
			$('article.product').each(function()
			{
				$(this).css(
				{
					'position':'relative', 
					'float':'left',
					'padding-bottom':0,
					'min-height':'360px', 
					'display':'block',
					'left':'auto',
					'top':'auto'
				});
			});
		}, 1000);
	}
})(jQuery);
