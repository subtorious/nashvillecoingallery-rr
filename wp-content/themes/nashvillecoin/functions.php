<?php
add_filter('woocommerce_get_catalog_ordering_args', 'am_woocommerce_catalog_orderby');
function am_woocommerce_catalog_orderby( $args ) 
{
	if(!isset($_GET['orderby']) || $_GET['orderby'] == 'sku')
	{
        	$args['meta_key'] = '_sku';
		$args['orderby'] = 'meta_value';
		$args['order'] = 'asc';
	}
	return $args;
}

add_filter( 'woocommerce_default_catalog_orderby_options', 'custom_woocommerce_catalog_orderby' );
add_filter( 'woocommerce_catalog_orderby', 'custom_woocommerce_catalog_orderby' );
function custom_woocommerce_catalog_orderby( $sortby ) {
	$sortby['sku'] = 'Sort by SKU';
	return $sortby;
}

add_action('woocommerce_after_shop_loop', 'nc_woocommerce_after_shop_loop');
function nc_woocommerce_after_shop_loop()
{
	wp_enqueue_script('nc_product_sort', get_stylesheet_directory_uri().'/nc_product_sort.js', array('jquery'), '1.0', true);
}




// **************  RECAPTCHA CODE ********************

// hg_default_register_form, and hg_registration_errors are used for the https://www.nashvillecoingallery.com/hglogin?action=register page.
add_action( 'register_form', 'hg_default_register_form' );
function hg_default_register_form() 
{?>
    <style>
        #login
        {
          width:400px !important;
        }
    </style>
     <script type="text/javascript">
        var googleRecaptchaLoad= function() 
        {
            (function($)
            {
                $(document).ready(function()
                {
                    if($('#g-recaptcha_register').length)
                    {
                        grecaptcha.render('g-recaptcha_register', 
                        {
                            'sitekey' : '6LeziiIUAAAAAP6BCXB-8qFtcO5wBu0OLWgUPoXx'
                        });
                    }
                });

            })(jQuery);
        };
    </script>
    <script src='https://www.google.com/recaptcha/api.js?onload=googleRecaptchaLoad&render=explicit'></script>
    <div id="g-recaptcha_register"></div>
<?php
}


add_filter( 'registration_errors', 'hg_registration_errors', 10, 3 );
function hg_registration_errors( $errors, $sanitized_user_login, $user_email ) 
{
    // Verify the Google Recaptcha 
    if(!empty($_POST['g-recaptcha-response'])) 
    {
        if(!hg_verify_recaptcha($_POST['g-recaptcha-response']))
        {
            $errors->add('recaptcha_error','<strong>ERROR</strong>: Recaptcha test failed.');
        }
    }
    else
    {
        $errors->add('recaptcha_error', '<strong>ERROR</strong>: Recaptcha test failed.');
    }

    return $errors;
}




// hg_register_form is used in the site's header register form.
add_action( 'wp_ajax_nopriv_hg_register_form', 'hg_register_form');
add_action( 'wp_ajax_hg_register_form', 'hg_register_form');
function hg_register_form() 
{
    // Verify the Google Recaptcha 
    if(!empty($_POST) && isset($_POST['g-recaptcha-response'])) 
    {
        if(!hg_verify_recaptcha($_POST['g-recaptcha-response']))
        {
            echo -1;
            wp_die();
        }
    }
    else
    {
        echo -1;
        wp_die();
    }

    $newUser= register_new_user( $_POST['user_login'], $_POST['user_email'] );
    if(is_wp_error($newUser)) 
    {
    	echo -1;
    	wp_die();
    }

    echo 'success';
    wp_die();
}


function hg_verify_recaptcha($recaptcha_response)
{
    $params = array();
    $params['secret']= '6LeziiIUAAAAAJPLQUjXbI6SH-JVd9lG4yxWdO7h';
    if(!empty($recaptcha_response)) 
    {
        $params['response'] = urlencode($recaptcha_response);
    }
    else
    {
        echo -1;
        wp_die();
    }

    $params['remoteip'] = $_SERVER['REMOTE_ADDR'];

    $params_string = http_build_query($params);
    $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $params_string;

    // Get cURL resource
    $curl = curl_init();

    // Set some options
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $requestURL,
    ));

    // Send the request
    $response = curl_exec($curl);

    // Close request to clear up some resources
    curl_close($curl);

    $response = @json_decode($response, true);

    if($response["success"] == true)  
    {
        return true;
    }

    return false;
}
// ************** END - RECAPTCHA CODE ********************
?>
