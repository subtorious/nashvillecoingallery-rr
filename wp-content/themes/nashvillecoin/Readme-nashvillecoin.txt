1. content-product.php line 136 set max width 200px

2. functions.php and nc_product_sort.js tweaks for sorting products
   
   2016/10/10 -- Updated functions.php and nc_product_sort.js to fix broken sorting.
   nc_product_sort.js: Changed the jquery selection to get article.product instead of li.product because the elements were no longer li's. Changed the                       css to be dispaly:block, float:left, padding-bottom:0 so that the element arranged correctly to show the sort.

   functions.php -- Commented out woocommerce_get_catalog_ordering_args filter because it was blocking the other sorting methods from working.   

3. Added /nashvillecoin/views/header/toolbar/login.php because Jupiter only had a disabled Edit Profile (not woocomm customer) and Login Logout. Added file and added Acct Orders etc
